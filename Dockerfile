# https://hub.docker.com/_/wordpress/
FROM public.ecr.aws/docker/library/wordpress:6.1.1-php7.4-apache
LABEL maintainer "Miguel Ángel Tomé Villas <ma.tome@qualoom.es>"

# Install available patches and packages
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y libmemcached-dev zlib1g-dev less unzip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Composer Install
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Apache mods
RUN a2enmod headers \
    && a2enmod rewrite

# Apache config
COPY docker/apache2 ${APACHE_CONFDIR}

# CORS Headers
RUN sed -ri -e 's/^([ \t]*)(<\/VirtualHost>)/\1\tHeader set Access-Control-Allow-Origin "*"\n\1\2/g' /etc/apache2/sites-available/*.conf

# Elasticache cluster client extension for PHP7.4
COPY docker/php/AmazonElastiCacheClusterClient-PHP74-64bit-libmemcached-1.0.18.tar.gz /tmp/AmazonElastiCacheClusterClient.tgz
RUN EXTENSION_DIR=$(php -i | grep '^extension_dir' | cut -d' ' -f3) \
    && tar -C ${EXTENSION_DIR} -zxvf /tmp/AmazonElastiCacheClusterClient.tgz \
    && rm /tmp/AmazonElastiCacheClusterClient.tgz

# PHP settings
COPY docker/php $PHP_INI_DIR/conf.d/

# WP-cli
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
    && chmod +x wp-cli.phar \
    && mv wp-cli.phar /usr/local/bin/wp

# Wordpress artifacts
WORKDIR /usr/src/wordpress
COPY --chown=www-data . .
COPY --chown=www-data docker/htaccess .htaccess
RUN set -eux; \
    find /etc/apache2 -name '*.conf' -type f -exec sed -ri -e "s!/var/www/html!$PWD!g" -e "s!Directory /var/www/!Directory $PWD!g" '{}' +; \
    cp -s wp-config-docker.php wp-config.php; \
    rm -rf docker

# Enable W3TC features
RUN cd wp-content; \
    cp -s plugins/w3-total-cache/wp-content/advanced-cache.php advanced-cache.php; \
    cp -s plugins/w3-total-cache/wp-content/object-cache.php   object-cache.php; \
    cp -s plugins/w3-total-cache/wp-content/db.php            db.php;

# Composer install
RUN cd /usr/src/wordpress/wp-content/themes/elcano && composer install --no-dev

USER www-data

EXPOSE 8080

INPUT=replace-2.csv
OLDIFS=$IFS
IFS=';'
while read urlorigen
do
  echo "wp search-replace $urlorigen"
  wp --allow-root search-replace "$urlorigen" "" --all-tables
done < $INPUT
IFS=$OLDIFS

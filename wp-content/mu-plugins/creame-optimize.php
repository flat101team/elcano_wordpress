<?php
/*
Plugin Name:  Creame Optimize
Plugin URI:   https://crea.me/
Description:  Optimizaciones de Creame para mejorar tu <em>site</em>.
Version:      1.5.0
Author:       Creame
Author URI:   https://crea.me/
License:      MIT License
*/


/**
 * ============================================================================
 * WP Admin clean up
 * ============================================================================
 */

// Hide update WordPress message
function creame_hide_update_wordpress_notice() {
    remove_action('admin_notices', 'update_nag', 3);
}
add_action('admin_head', 'creame_hide_update_wordpress_notice', 1);

// Disable Heartbeat API
function creame_stop_heartbeat() {
    wp_deregister_script('heartbeat');
}
// add_action('init', 'creame_stop_heartbeat', 1);

// Or change Heartbeat interval setting
function creame_heartbeat_interval($settings) {
    $settings['interval'] = 60;
    return $settings;
}
add_filter('heartbeat_settings', 'creame_heartbeat_interval');

// Add wp-env-environment body class
function creame_body_env_class( $classes ) {
    $class = defined('WP_ENV') ? 'wp-env-' . WP_ENV : 'wp-env-none';
    return is_array($classes) ? array_merge($classes, [$class]) : "$classes $class";
}
add_filter('admin_body_class', 'creame_body_env_class' );
add_filter('login_body_class', 'creame_body_env_class' );
add_filter('body_class', 'creame_body_env_class' );

// Custom admin styles
function creame_custom_admin_styles() {
?>
<style>
  /* Hide */
  #wp-version-message,
  div[id^=gainwp-container-]>div:last-child, /* GAinWP */
  .publishpress-admin header img, /* Publishpress */
  .post-type-psppnotif_workflow .logo-header
  { display:none !important; }
  .meta-box-sortables select { box-sizing:border-box; }
  /* Bulk edit */
  #bulk-titles, ul.cat-checklist { height:14rem; }
  #wpbody-content .bulk-edit-row fieldset .inline-edit-group label { max-width:none; }
  .inline-edit-row fieldset .timestamp-wrap, .inline-edit-row fieldset label span.input-text-wrap { margin-left:10em; }
  .inline-edit-row fieldset label span.title, .inline-edit-row fieldset.inline-edit-date legend { min-width:9em; width:auto; margin-right:1em; }
  /* Status colors */
  tr.status-draft td, tr.status-draft th { background:rgba(243,238,195,0.5); }
  tr.status-trash td, tr.status-trash th { background:rgba(205,108,118,0.2); }
</style>
<?php
}
add_action('admin_head', 'creame_custom_admin_styles');

// Disable password change notification to admin
if (!function_exists('wp_password_change_notification')) {
    function wp_password_change_notification() {}
}

// Disable XML-RPC
add_filter('xmlrpc_enabled', '__return_false', PHP_INT_MAX);
add_filter('xmlrpc_methods', '__return_empty_array', PHP_INT_MAX);
add_filter('xmlrpc_element_limit', function (): int { return 1; }, PHP_INT_MAX);

// Disable post by email
add_filter('enable_post_by_email_configuration', '__return_false');

// Hide other themes on Admin > Appearance
function creame_hide_themes($wp_themes){
    return array_intersect_key($wp_themes, [WP_DEFAULT_THEME => 1]);
}
if (defined('WP_DEFAULT_THEME')) add_filter('wp_prepare_themes_for_js', 'creame_hide_themes');

// object-cache.php disable flush error
add_filter('pecl_memcached/warn_on_flush', '__return_false');


/**
 * ============================================================================
 * WP Media
 * ============================================================================
 */

// Sort media library by pdf file type
function creame_post_mime_types($post_mime_types) {
    $post_mime_types['application/pdf'] = [__('PDF'), __('Manage PDF'), _n_noop('PDF <span class="count">(%s)</span>', 'PDF <span class="count">(%s)</span>')];
    return $post_mime_types;
}
add_filter('post_mime_types', 'creame_post_mime_types');

// Attachment unique slugs (prevent post conflicts)
function creame_unique_attachment_slug($slug, $post_ID, $post_status, $post_type, $post_parent, $original_slug) {
    return 'attachment' == $post_type ? uniqid('media-') : $slug;
}
add_filter('wp_unique_post_slug', 'creame_unique_attachment_slug', 10, 6);

// Auto sanitize attachment title & alt text
function creame_attachment_title($post_ID) {
    $post  = get_post($post_ID);
    $title = preg_replace('%\s*[-_\s]+\s*%', ' ', $post->post_title);
    // $title = ucwords(strtolower($title)); // Title Case

    // Set image alt text
    if (wp_attachment_is_image($post_ID)) update_post_meta($post_ID, '_wp_attachment_image_alt', $title);

    wp_update_post([
        'ID'           => $post_ID,
        'post_title'   => $title, // Set attachment title
        // 'post_excerpt' => $title, // Set attachment caption (Excerpt)
        // 'post_content' => $title, // Set attachment description (Content)
    ]);
}
add_action('add_attachment', 'creame_attachment_title');

// Redirect attachment page to attachment file
function creame_attachment_redirect(){
    if (is_attachment()) wp_redirect(wp_get_attachment_url(), 301);
}
add_action('template_redirect', 'creame_attachment_redirect');


/**
 * ============================================================================
 * WP Head clean up
 * ============================================================================
 */

// Remove wordpress version
remove_action('wp_head', 'wp_generator');

// Remove wlwmanifest.xml (needed to support windows live writer)
remove_action('wp_head', 'wlwmanifest_link');

// Remove really simple discovery link
remove_action('wp_head', 'rsd_link');

// Remove all extra rss feed links
remove_action('wp_head', 'feed_links_extra', 3);

// Remove shortlink
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Remove oembed links
remove_action('wp_head', 'wp_oembed_add_discovery_links');
// remove_action('wp_head', 'wp_oembed_add_host_js'); // emded.js

// Remove rel links
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// Remove REST-API link
remove_action('wp_head', 'rest_output_link_wp_head');

// Disable REST-API
// add_filter('json_enabled', '__return_false');
// add_filter('json_jsonp_enabled', '__return_false');

// Remove commments cookies
remove_action('set_comment_cookies', 'wp_set_comment_cookies');

// Removes the generator name from the RSS feeds
add_filter('the_generator', '__return_false');

// Remove gallery inline styles
add_filter('use_default_gallery_style', '__return_false');

// Remove comments feed link
add_filter('feed_links_show_comments_feed', '__return_false');

// Remove recent comments widget styles
add_filter('show_recent_comments_widget_style', '__return_false');

// Remove plugin revslider generator meta
add_filter('revslider_meta_generator', '__return_false');

// Remove CSS and JS query strings versions
function creame_remove_cssjs_ver_filter($src){
    return strpos($src, 'ver=') ? remove_query_arg('ver', $src) : $src;
}
// add_filter('style_loader_src', 'creame_remove_cssjs_ver_filter', 10);
// add_filter('script_loader_src', 'creame_remove_cssjs_ver_filter', 10);

// Google Fonts add "font-display:swap"
function creame_google_fonts_swap($src){
    return strpos($src, 'fonts.googleapis.com') && !strpos($src, 'display=') ? add_query_arg('display', 'swap', $src) : $src;
}
add_filter('style_loader_src', 'creame_google_fonts_swap', 10);

// Clean enqueued style and script tags
function creame_clean_style_and_script_tags($tag) {
    $clean = [
        "/type=['\"]text\/(javascript|css)['\"]/" => "",
        "/media=['\"]all['\"]/"                   => "",
        "/ +/"                                    => " ",
        "/ \/?>/"                                 => ">",
    ];
    $tag = preg_replace(array_keys($clean), $clean, $tag);
    // only replace "'" on tag attributes (prevent errors on inline <script> content)
    return preg_replace_callback("/^<[^>]*>/", function($v) { return str_replace("'", '"', $v[0]); }, $tag);
}
add_filter('style_loader_tag', 'creame_clean_style_and_script_tags');
add_filter('script_loader_tag', 'creame_clean_style_and_script_tags');

// Remove hentry class on pages (fix error on google search console)
function creame_remove_hentry_class($classes) {
    return is_single() ? $classes : array_diff($classes, ['hentry']);
}
add_filter('post_class', 'creame_remove_hentry_class');

// Add custom metas
function creame_custom_metas() {
    // SEO no index search results
    if (is_search()) echo '<meta name="robots" content="noindex, follow">' . PHP_EOL;

    // Add other metas...
}
add_action('wp_head', 'creame_custom_metas');

// Remove filter capital P dangit
remove_filter('the_title', 'capital_P_dangit', 11);
remove_filter('the_content', 'capital_P_dangit', 11);
remove_filter('comment_text', 'capital_P_dangit', 31);

// Embed YouTube cookieless
function creame_embed_youtube_nocookie($output) {
    return str_replace('https://www.youtube.com/', 'https://www.youtube-nocookie.com/', $output);
}
add_filter('embed_oembed_html', 'creame_embed_youtube_nocookie', 10);


/**
 * ============================================================================
 * Custom Elcano scripts
 * ============================================================================
 */

// Fix premature load of ACFML (move from 'wpml_loaded' to 'acf/init')
add_action('wpml_loaded', function(){ remove_action('wpml_loaded', 'acfmlInit'); }, 1);
add_action('acf/init', 'acfmlInit');

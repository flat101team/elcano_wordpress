<?php
/*
Plugin Name: On The Fly Thumbnails
Plugin URI: http://wordpress.org/plugins/wp-sanitize-file-name-plus/
Description: Generate image thumbnails on the fly only when needed.
Author: Creame
Version: 1.0.0
Author URI: https://crea.me
*/

/**
 * Add custom image sizes
 *
 * Use this specially for image sizes without crop.
 * You can add named cropped images also but cropped images can be requested
 * with an array [width, height].
 */
function elcano_add_custom_image_sizes() {
	add_image_size( 'widget-logo', 400, 100, false );
}
// add_action( 'after_setup_theme', 'elcano_add_custom_image_sizes' );

class On_The_Fly_Thumbnails {

	/**
	 * The setings of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $settings    The current settings of this plugin.
	 */
	private $image_sizes;


	function __construct() {

		add_filter( 'intermediate_image_sizes_advanced', array( $this, 'only_thumbnail_size' ) );
		add_filter( 'image_downsize', array( $this, 'image_downsize' ), 10, 3 );

	}

	/**
	 * Only generate thumbnail on image upload
	 *
	 * Other sizes are generated on demand with elcano_otf_generate_thumbs()
	 *
	 * @param  array $sizes image sizes.
	 * @return array
	 */
	function only_thumbnail_size( $sizes ) {

		return array( 'thumbnail' => $sizes['thumbnail'] );

	}

	private function get_image_sizes() {

		global $_wp_additional_image_sizes;

		$this->image_sizes = array();
		$inter_image_sizes = get_intermediate_image_sizes();

		foreach ( $inter_image_sizes as $size_name ) {
			// WP image sizes get size from options.
			if ( in_array( $size_name, array( 'thumbnail', 'medium', 'large' ), true ) ) {

				$this->image_sizes[ $size_name ]['width']  = get_option( $size_name . '_size_w' );
				$this->image_sizes[ $size_name ]['height'] = get_option( $size_name . '_size_h' );
				$this->image_sizes[ $size_name ]['crop']   = (bool) get_option( $size_name . '_crop' );

			} elseif ( isset( $_wp_additional_image_sizes[ $size_name ] ) ) {

				$this->image_sizes[ $size_name ] = $_wp_additional_image_sizes[ $size_name ];

			}
		}

		return $this->image_sizes;

	}

	/**
	 * Automatically regenerates your thumbnails on the fly (OTF)
	 *
	 * Doesn't upsize, just downsizes like how WordPress likes it.
	 * If the image already exists, it's served. If not, the image is resized to the specified size, saved for
	 * future use, then served.
	 *
	 * view: https://github.com/gambitph/WP-OTF-Regenerate-Thumbnails
	 *
	 * @param   boolean $out false.
	 * @param   int     $id Attachment ID.
	 * @param   mixed   $size The size name, or an array containing the width & height.
	 * @return  mixed false if the custom downsize failed, or an array of the image if successful.
	 */
	function image_downsize( $out, $id, $size ) {

		if ( empty( $this->image_sizes ) ) {
			$this->get_image_sizes();
		}

		// All the data that we have for all the image sizes.
		$all_sizes = $this->image_sizes;

		// If image size exists let WP serve it like normally.
		$image_data = wp_get_attachment_metadata( $id );

		// Image attachment doesn't exist.
		if ( ! is_array( $image_data ) ) {
			return false;
		}

		$image_path    = get_attached_file( $id );
		$image_url_dir = dirname( wp_get_attachment_url( $id ) ) . '/';

		// Image file doesn't exist.
		if ( ! file_exists( $image_path ) ) {
			return false;
		}

		// If the size given is a string / a name of a size.
		if ( is_string( $size ) ) {

			// If WP doesn't know about the image size name.
			if ( empty( $all_sizes[ $size ] ) ) {
				return false;
			}

			// If the size has already been previously created, use it.
			if ( ! empty( $image_data['sizes'][ $size ] ) && ! empty( $all_sizes[ $size ] ) ) {

				// But only if the size remained the same.
				if ( $all_sizes[ $size ]['width'] === $image_data['sizes'][ $size ]['width']
					&& $all_sizes[ $size ]['height'] === $image_data['sizes'][ $size ]['height']
				) {
					return false;
				}

				// Or if the size is different and we found out before that the size really was different.
				// TODO: review this condition
				if ( ! empty( $image_data['sizes'][ $size ]['width_query'] )
					&& ! empty( $image_data['sizes'][ $size ]['height_query'] )
					&& $image_data['sizes'][ $size ]['width_query'] === $all_sizes[ $size ]['width']
					&& $image_data['sizes'][ $size ]['height_query'] === $all_sizes[ $size ]['height']
				) {
					return false;
				}
			}

			// Resize the image.
			$resized = image_make_intermediate_size(
				$image_path,
				$all_sizes[ $size ]['width'],
				$all_sizes[ $size ]['height'],
				$all_sizes[ $size ]['crop']
			);

			// Resize somehow failed.
			if ( ! $resized ) {
				return false;
			}

			// Save the new size in WP.
			$image_data['sizes'][ $size ] = $resized;

			// Save some additional info so that we'll know next time whether we've resized this before.
			$image_data['sizes'][ $size ]['width_query']  = $all_sizes[ $size ]['width'];
			$image_data['sizes'][ $size ]['height_query'] = $all_sizes[ $size ]['height'];

			wp_update_attachment_metadata( $id, $image_data );

			// Serve the resized image.
			return array(
				$image_url_dir . $resized['file'],
				$resized['width'],
				$resized['height'],
				true,
			);
		}

		// If the size given is a custom array size.
		elseif ( is_array( $size ) ) {

			$image_ext = pathinfo( $image_path, PATHINFO_EXTENSION );

			// This would be the path of our resized image if the dimensions existed.
			$original_size = getimagesize( $image_path );
			$resize_size   = image_resize_dimensions( $original_size[0], $original_size[1], $size[0], $size[1], true );

			if ( is_array( $resize_size ) ) {
				$resize_path = preg_replace( '/^(.*)\.' . $image_ext . '$/', sprintf( '$1-%sx%s.%s', $resize_size[4], $resize_size[5], $image_ext ), $image_path );
			} else {
				$resize_path = preg_replace( '/^(.*)\.' . $image_ext . '$/', sprintf( '$1-%sx%s.%s', $size[0], $size[1], $image_ext ), $image_path );
			}

			// If it already exists, serve it.
			if ( file_exists( $resize_path ) ) {
				return array(
					$image_url_dir . basename( $resize_path ),
					$size[0],
					$size[1],
					true,
				);
			}

			// If not, resize the image...
			$resized = image_make_intermediate_size( $image_path, $size[0], $size[1], true );

			// Resize somehow failed.
			if ( ! $resized ) {
				return false;
			}

			// Save the new size in WP so that it can also perform actions on it.
			$image_data['sizes'][ $size[0] . 'x' . $size[1] ] = $resized;
			wp_update_attachment_metadata( $id, $image_data );

			// Then serve it.
			return array(
				$image_url_dir . $resized['file'],
				$resized['width'],
				$resized['height'],
				true,
			);
		}

		return false;
	}

}

$on_the_fly_thumbnails = new On_The_Fly_Thumbnails();

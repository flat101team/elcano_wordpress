<?php
/**
 * Intervention settings
 *
 * @link https://github.com/soberwp/intervention
 *
 * @package Elcano
 */

return array(

	/**
	 * Site settings
	 *
	 * Set WordPress settings via code.
	 */

	'application'                    => array(
		// 'theme'      => 'elcano',
		'general'    => array(
			// 'site-title'   => 'Real Instituto Elcano',
			// 'tagline'      => 'El Real Instituto Elcano es el think-tank de estudios internacionales y estratégicos, realizados desde una perspectiva española, europea y global.',
			// 'wp-address' => 'http://elcano.test',
			// 'site-address' => 'http://elcano.test',
			// 'admin-email'  => 'admin@elcano.es',
			'membership'   => false,
			'default-role' => 'contributor',
			'language'     => false,
			'timezone'     => 'Europe/Madrid',
			'date-format'  => 'd M Y',
			'time-format'  => 'H:i',
			'week-starts'  => 'Monday',
		),
		'writing'    => array(
			'default-category'                => 1,
			'default-post-format'             => 'standard',
			'post-via-email.server'           => 'mail.example.com',
			'post-via-email.login'            => 'login@example.com',
			'post-via-email.pass'             => 'password',
			'post-via-email.port'             => 110,
			'post-via-email.default-category' => 1,
			'update-services'                 => 'http://rpc.pingomatic.com/',
			'emoji'                           => false,
		),
		'reading'    => array(
			// 'front-page'       => 'posts',
			// 'front-page.page'  => 0,
			// 'front-page.posts' => 0,
			'posts-per-page' => 12,
			'posts-per-rss'  => 15,
			'rss-excerpt'    => 'full',
			// 'discourage-search' => false,
		),
		'discussion' => array(
			'post.ping-flag'              => false,
			'post.ping-status'            => false,
			'post.comments'               => false,
			'comments.name-email'         => true,
			'comments.registration'       => false,
			'comments.close'              => false,
			'comments.close.days'         => 14,
			'comments.cookies'            => true,
			'comments.thread'             => true,
			'comments.thread.depth'       => 5,
			'comments.pages'              => false,
			'comments.pages.per-page'     => 50,
			'comments.pages.default'      => 'newest',
			'comments.order'              => 'asc',
			'emails.comment'              => true,
			'emails.moderation'           => true,
			'moderation.approve-manual'   => true,
			'moderation.approve-previous' => true,
			'moderation.queue-links'      => 2,
			'moderation.queue-keys'       => '',
			'moderation.disallowed-keys'  => '',
			'avatars'                     => false,
			'avatars.rating'              => 'G',
			'avatars.default'             => 'mystery',
		),
		'media'      => array(
			'sizes.thumbnail.width'  => 150,
			'sizes.thumbnail.height' => 150,
			'sizes.thumbnail.crop'   => true,
			'sizes.medium.width'     => 300,
			'sizes.medium.height'    => 300,
			'sizes.large.width'      => 1024,
			'sizes.large.height'     => 1024,
			'uploads.organize'       => true,
		),
		'permalinks' => array(
			// 'structure'     => '/%postname%/',
			'category-base' => 'tema',
			'tag-base'      => 'etiqueta',
		),
		'privacy'    => array(
			'policy-page' => 3,
		),
	),

	/**
	 * Wp Admin clean
	 *
	 * Remove unnecessary elements.
	 */

	'wp-admin.all'                   => array(
		'common.adminbar.wp',
		'common.adminbar.user' => array( 'howdy', 'avatar', 'edit' ),
		'common.footer.credit',
		'dashboard.home.tabs.help',
		'dashboard.home'       => array( 'welcome', 'news', 'quick-draft' ),
		'pages.item.author',
		'users.profile.role.subscriber',
	),
	'wp-admin.all-not-administrator' => array(
		'tools',
	),

);

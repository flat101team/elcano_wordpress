<?php
/**
 * Template Name: Contact
 *
 * @package Elcano
 */

// Ensure heading IDs for TOC anchors
add_filter( 'lwptoc_need_processing_headings', '__return_true' );

get_header();

elcano_breadcrumb();
?>

	<main id="primary" class="site-main">

		<?php the_post(); ?>

		<article id="contact-page" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title h1-alt baseline">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<div class="columns">

				<div class="entry-content maincol">
					<?php
					the_content();
					?>
				</div><!-- .entry-content -->

			</div>
		</article><!-- #post-<?php the_ID(); ?> -->

	</main><!-- #main -->

<?php
get_footer();

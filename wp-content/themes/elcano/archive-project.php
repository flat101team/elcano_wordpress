<?php
/**
 * The template for displaying projects archive
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$past = get_query_var( 'past' );

$the_page = get_field( 'page_for_project', 'option' );

get_header();
?>

	<main id="primary" class="site-main">

		<header class="page-header">
			<h1 class="page-title alt baseline"><?php echo get_the_title( $the_page ); ?></h1>
			<?php if ( get_the_content( null, false, $the_page ) ) : ?>
                <div class="columns">

                    <div class="maincol">
                        <div class="archive-description"><?php echo apply_filters( 'the_content', get_the_content( null, false, $the_page ) ); ?></div>
                    </div>
                    <aside>
						<?php
						// Contact section.
						get_template_part( 'template-parts/section/contact', '', array( 'post_id' => $the_page ) );
						?>
                    </aside>
                </div>
			<?php endif; ?>
		</header><!-- .page-header -->

		<div class="projects-select">
			<a class="next-projects <?php echo $past ? '' : 'current'; ?>" href="<?php echo get_post_type_archive_link( 'project' ); ?>"><?php _e( 'Ongoing Projects', 'elcano' ); ?></a>
			<span class="past-projects <?php echo $past ? 'current' : ''; ?>">
				<?php _e( 'Past Projects:', 'elcano' ); ?>
				<div class="years-select">
					<div class="current-year"><?php echo $past ?: __( 'Year', 'elcano' ); ?></div>
					<div class="wrapper">
						<ul>
							<?php
							$past  = get_post_type_archive_link( 'project' ) . 'past';
							$years = elcano_projects_get_years();

							foreach ( $years as $year ) {
								printf( '<li><a href="%s">%d</a></li>', esc_url( "$past/$year/" ), $year );
							}
							?>
						</ul>
					</div>
				</div>
			</span>
		</div>

		<?php if ( have_posts() ) : ?>
			<div class="the-archive the-archive--project">
				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/archive/project' );
				endwhile;
				?>
			</div>

			<?php elcano_posts_pagination(); ?>

		<?php else : ?>

			<div class="no-results">
				<p class="h2"><?php echo get_query_var( 'past' ) ? __( 'No projects', 'elcano' ) : __( 'No scheduled projects', 'elcano' ); ?></p>
			</div>

		<?php endif; ?>

	</main><!-- #main -->

<?php
get_footer();

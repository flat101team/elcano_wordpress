<?php
/**
 * The template for displaying Home Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

get_header();

the_post();

// Header posts
$headline     = get_field( 'home_headline' );
$featured     = get_field( 'home_featured' );
$header_posts = array_merge( array( $headline->ID ), wp_list_pluck( $featured, 'ID' ) );

// Web Content
$last_content = wp_get_recent_posts(
	array(
		'post_type'        => array(
			'post',
			'analysis',
			'work_document',
			'report',
			'poll',
			// 'special',
			'monograph',
			'policy_paper',
			'commentary',
			// 'podcast',
			// 'video',
			// 'archive',
			//'newsletter',
			//'news_on_net',
			//'ciber',
			//'magazine',
			// 'press_release',
			// 'activity',
			// 'job',
			// 'biography',
		),
		'post__not_in'     => $header_posts,
		'post_status'      => 'publish',
		'numberposts'      => 8,
		'suppress_filters' => false,
	),
	OBJECT
);

// Next Activities
$now             = new DateTime( 'now', wp_timezone() );
$next_activities = new WP_Query(
	array(
		'post_type'        => 'activity',
		'post_status'      => 'publish',
		'posts_per_page'   => 2,
		'meta_query'       => array(
			array(
				'key'     => 'date_end',
				'value'   => $now->format( 'Y-m-d H:i:s' ),
				'compare' => '>=',
				'type'    => 'DATETIME',
			),
		),
		'meta_key'         => 'date_start',
		'orderby'          => 'meta_value',
		'order'            => 'ASC',
		'suppress_filters' => false,
	),
	OBJECT
);

//Past activities
$past_activities = new WP_Query(
	array(
		'post_type'        => 'activity',
		'post_status'      => 'publish',
		'posts_per_page'   => 1,
		'meta_query'       => array(
			array(
				'key'     => 'date_end',
				'value'   => $now->format( 'Y-m-d H:i:s' ),
				'compare' => '<',
				'type'    => 'DATETIME',
			),
		),
		'meta_key'         => 'date_start',
		'orderby'          => 'meta_value',
		'order'            => 'DESC',
		'suppress_filters' => false,
	),
	OBJECT
);

// Last Podcast
$last_podcast = wp_get_recent_posts(
	array(
		'post_type'        => 'podcast',
		'post_status'      => 'publish',
		'numberposts'      => 3,
		'suppress_filters' => false,
	),
	OBJECT
);

// Last Videos
$last_videos = wp_get_recent_posts(
	array(
		'post_type'        => 'video',
		'post_status'      => 'publish',
		'numberposts'      => 4,
		'suppress_filters' => false,
	),
	OBJECT
);

// Last Specials
$last_specials = wp_get_recent_posts(
	array(
		'post_type'        => 'special',
		'post_status'      => 'publish',
		/*'post__not_in'     => $header_posts,*/
		'numberposts'      => 6,
		'suppress_filters' => false,
	),
	OBJECT
);

$the_specials_page = get_field( 'page_for_special', 'option' );
$specials_rows     = get_field( 'old-specials', $the_specials_page );

// Last Posts
$last_posts = wp_get_recent_posts(
	array(
		'post_type'        => 'commentary',
		'post_status'      => 'publish',
		'post__not_in'     => $header_posts,
		'numberposts'      => 4,
		'suppress_filters' => false,
	),
	OBJECT
);

if ( ! empty( $last_videos ) ) {
	elcano_append_dependency( 'elcano-theme', 'elcano-modal' );
}

?>

	<main id="primary" class="site-main">
		<header class="hero-header home-header">
			<div class="hero-thumbnail viewwith"><?php the_post_thumbnail( 'full', array( 'loading' => false ) ); ?></div>
			<div class="hero-content fullwidth">
				<div class="home-headline">
					<p class="post-category"><?php elcano_primary_category( $headline ); ?></p>
					<p class="post-title h1"><a href="<?php the_permalink( $headline ); ?>" rel="bookmark"><?php echo get_the_title( $headline ); ?></a></p>
					<p class="post-meta">
						<span class="post-authors"><?php elcano_authors( $headline ); ?></span>
						<?php elcano_posted_on( $headline, true, false ); ?>
					</p>
				</div>
			</div>
		</header>

		<div class="home-content">

			<div class="home-main-posts the-archive">
				<?php foreach ( $featured as $post ) : ?>
					<?php get_template_part( 'template-parts/archive/publications', null, array( 'show_post_type' => false ) ); ?>
				<?php endforeach; ?>
			</div>

			<?php if ( ! empty( $last_content ) ) : ?>
				<section class="home-latest">
					<div class="section-title">
						<h2 class="h2alt baseline"><?php _e( 'Publications', 'elcano' ); ?></h2>
					</div>
					<div class="latest-main-section the-archive">
						<div class="left-col">
							<?php foreach ( array_slice( $last_content, 0, 1 ) as $post ) : ?>
								<?php get_template_part( 'template-parts/archive/publications', null, array( 'show_post_type' => true, 'show_excerpt' => true ) ); ?>
							<?php endforeach; ?>
						</div>
						<div class="right-col the-archive">
							<?php foreach ( array_slice( $last_content, 1, 3 ) as $post ) : ?>
								<?php get_template_part( 'template-parts/archive/publications', null, array( 'mode' => 'horizontal', 'show_excerpt' => true ) ); ?>
							<?php endforeach; ?>
						</div>
					</div>

					<div class="secondary-publications the-archive">
						<?php foreach ( array_slice( $last_content, 4, 8 ) as $post ) : ?>
							<?php get_template_part( 'template-parts/archive/publications', null, array( 'no_thumbnail' => true, 'show_post_type' => true ) ); ?>
						<?php endforeach; ?>
					</div>

					<div class="all-button">
						<?php _e( '<a href="/en/publications" class="btn">See all publications</a>', 'elcano' ) ?>
					</div>
				</section>
			<?php endif; ?>

			<?php /*
			<section class="home-latest">
				<div class="section-title">
					<h2 class="h2alt baseline"><?php _e( 'Experts', 'elcano' ); ?></h2>
				</div>
				<div class="experts-publications the-archive">
					<?php foreach( $last_posts as $post ): ?>
						<?php get_template_part( 'template-parts/archive/publications' ); ?>
					<?php endforeach; ?>
				</div>
			</section>
			*/ ?>


				<section class="section-next-activities">
					<div class="section-title left-col">
						<h2 class="h2"><?php esc_html_e( 'Events', 'elcano' ); ?></h2>
						<?php # <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> ?>
						<a href="<?php esc_attr_e( get_post_type_archive_link( 'activity') ) ?>"><?php esc_html_e( 'See all events', 'elcano' ) ?></a>
					</div>
					<div class="right-col">
						<?php
						if( $next_activities->have_posts() ): ?>
							<p class="activities-secondary-title"><?php esc_html_e( 'Upcoming events', 'elcano' ) ?></p>
							<?php foreach ( $next_activities->posts as $post ) :
								$date_start = new DateTime( get_field( 'date_start', $post->ID, false ), wp_timezone() );
								$activity_type = get_field( 'activity_type', $post->ID, true );
								if ( 'presencial' == $activity_type ) {
									$location = get_field('location', $post->ID, true );
									$activity_info = $location->post_title;
								} elseif ( 'online' == $activity_type ) {
									$activity_info = 'Online';
								}
								?>
								<article id="post-<?php the_ID(); ?>" class="activity" data-date="<?php echo wp_date( 'j M', $date_start->getTimestamp() ); ?>">
									<div class="post-date">
										<span class="weekday"><?php echo wp_date( 'D', $date_start->getTimestamp() ); ?></span>
										<span class="date"><?php echo wp_date( 'j M', $date_start->getTimestamp() ); ?></span>
										<span class="time"><?php echo wp_date( 'H:i', $date_start->getTimestamp() ); ?> h</span>
									</div>
									<div class="post-info">
										<p class="post-category"><?php elcano_primary_category( $post ); ?></p>
										<h3 class="entry-title h2"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
										<div>
											<?php if ( $activity_type ): ?>
												<span class="activity-info <?php echo $activity_type == 'online' ? 'icon-link' : '' ?> <?php echo $activity_type ?>">
													<?php if ( 'presencial' == $activity_type ): ?>
														<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
															<g id="Communication / location on">
																<mask id="mask0_142_3202" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="5" y="2" width="14" height="20">
																	<g id="Icon Mask">
																		<path id="Round" fill-rule="evenodd" clip-rule="evenodd" d="M5 9.26501C5 5.39501 8.13 2.26501 12 2.26501C15.87 2.26501 19 5.39501 19 9.26501C19 13.435 14.58 19.185 12.77 21.375C12.37 21.855 11.64 21.855 11.24 21.375C9.42 19.185 5 13.435 5 9.26501ZM9.5 9.26501C9.5 10.645 10.62 11.765 12 11.765C13.38 11.765 14.5 10.645 14.5 9.26501C14.5 7.88501 13.38 6.76501 12 6.76501C10.62 6.76501 9.5 7.88501 9.5 9.26501Z" fill="black"/>
																	</g>
																</mask>
																<g mask="url(#mask0_142_3202)">
																	<rect id="Color Fill" width="24" height="24" fill="#2c3a4b"/>
																</g>
															</g>
														</svg>
													<?php endif; ?>

													<?php echo $activity_info ?>
												</span>
											<?php endif; ?>
										</div>
									</div>
								</article>
							<?php endforeach;
						endif; ?>

						<?php if( $past_activities->have_posts() ): ?>
							<p class="activities-secondary-title"><?php esc_html_e( 'Past events', 'elcano' ) ?></p>
							<?php foreach( $past_activities->posts as $post ):
								$date_start = new DateTime( get_field( 'date_start', $post->ID, false ), wp_timezone() );
								$activity_type = get_field( 'activity_type', $post->ID, true );
								if ( 'presencial' == $activity_type ) {
									$location = get_field('location', $post->ID, true );
									$activity_info = $location->post_title;
								} elseif ( 'online' == $activity_type ) {
									$activity_info = 'Online';
								}
								?>
								<article id="post-<?php the_ID(); ?>" class="activity" data-date="<?php echo wp_date( 'j M', $date_start->getTimestamp() ); ?>">
									<div class="post-date">
										<span class="weekday"><?php echo wp_date( 'D', $date_start->getTimestamp() ); ?></span>
										<span class="date"><?php echo wp_date( 'j M', $date_start->getTimestamp() ); ?></span>
										<span class="time"><?php echo wp_date( 'H:i', $date_start->getTimestamp() ); ?> h</span>
									</div>
									<div class="post-info">
										<p class="post-category"><?php elcano_primary_category( $post ); ?></p>
										<h3 class="entry-title h2"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
										<div>
											<?php if ( $activity_type ): ?>
												<span class="activity-info <?php echo $activity_type == 'online' ? 'icon-link' : '' ?> <?php echo $activity_type ?>">
													<?php if ( 'presencial' == $activity_type ): ?>
														<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
															<g id="Communication / location on">
																<mask id="mask0_142_3202" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="5" y="2" width="14" height="20">
																	<g id="Icon Mask">
																		<path id="Round" fill-rule="evenodd" clip-rule="evenodd" d="M5 9.26501C5 5.39501 8.13 2.26501 12 2.26501C15.87 2.26501 19 5.39501 19 9.26501C19 13.435 14.58 19.185 12.77 21.375C12.37 21.855 11.64 21.855 11.24 21.375C9.42 19.185 5 13.435 5 9.26501ZM9.5 9.26501C9.5 10.645 10.62 11.765 12 11.765C13.38 11.765 14.5 10.645 14.5 9.26501C14.5 7.88501 13.38 6.76501 12 6.76501C10.62 6.76501 9.5 7.88501 9.5 9.26501Z" fill="black"/>
																	</g>
																</mask>
																<g mask="url(#mask0_142_3202)">
																	<rect id="Color Fill" width="24" height="24" fill="#2c3a4b"/>
																</g>
															</g>
														</svg>
													<?php endif; ?>

													<?php echo $activity_info ?>
												</span>
											<?php endif; ?>
										</div>
									</div>
								</article>

							<?php endforeach;
						endif; ?>
					</div>
				</section>


			<?php if ( ! empty( $last_podcast ) ) : ?>
				<section class="section-recent-podcast">
					<div class="section-title">
						<h2 class="h2"><?php _e( 'Latest Podcasts', 'elcano' ); ?></h2>
						<a class="listen-all"
						   href="<?php esc_attr_e( get_post_type_archive_link( 'podcast') ) ?>"><?php _e( 'Listen to all podcasts', 'elcano' ); ?></a>
					</div>
					<div class="the-archive">
						<?php foreach ( $last_podcast as $post ) : ?>
							<article>
								<div class="post-category"><?php elcano_primary_category(); ?></div>
								<h3 class="entry-title h2"><a href="<?php the_permalink(); ?>"
															  rel="bookmark"><?php the_title(); ?></a></h3>
							</article>
						<?php endforeach; ?>
					</div>
				</section>
			<?php endif; ?>


			<?php if ( ! empty( $last_specials ) || have_rows( 'old-specials', $the_specials_page ) ) : ?>
				<section class="section-recent-specials">
					<div class="section-title">
						<h2 class="h2 baseline"><?php _e( 'Featured specials', 'elcano' ); ?></h2>
					</div>
					<div class="the-archive the-archive--main">
						<?php
                        $is_cpt = false;
						if ( $last_specials > 0 ) {
                            $count_specials = min( 2, count( $last_specials ) );
							$is_cpt = true;
							for ( $i = 0; $i < $count_specials; $i++ ) :
								$post = array_shift( $last_specials );
								?>
								<article>
									<a href="<?php the_permalink(); ?>" class="post-link" rel="bookmark">
										<div class="post-thumbnail"><?php the_post_thumbnail( array( 564, 240 ) ); ?></div>
										<div class="post-text">
											<h3 class="entry-title h2"><?php the_title(); ?></h3>
										</div>
									</a>
								</article>
								<?php
							endfor;
						} elseif ( $specials_rows > 0 ) {
							for ( $i = 0; $i < min( 2, count( $specials_rows ) ); $i++ ) :
								$row = $specials_rows[ $i ];
								printf(
									'<article class=""><a class="post-link" rel="bookmark" href="%s">%s %s</a></article>',
									esc_url( $row['link'] ),
									sprintf( '<div class="post-thumbnail">%s</div>', wp_get_attachment_image( $row['image'], array( 368, 240 ) ) ),
									sprintf( '<div class="post-text"><div class="post-meta"><span class="years">%s</span></div><h3 class="entry-title h2">%s</h3></div>', esc_html( $row['year'] ), esc_html( $row['title'] ) ),
								);
							endfor;
						}
						?>
					</div>
					<div class="the-archive the-archive--secondary">
						<?php
						if ( ! empty( $last_specials ) ) :
							foreach ( $last_specials as $post ) :
								?>
								<article>
									<a href="<?php the_permalink(); ?>" class="post-link" rel="bookmark">
										<div class="post-thumbnail"><?php the_post_thumbnail( array( 173, 123 ) ); ?></div>
										<div class="post-text">
											<h3 class="entry-title h2"><?php the_title(); ?></h3>
										</div>
									</a>
								</article>
							<?php endforeach; ?>
							<?php
						endif;
						?>
						<?php
						if ( !empty( $last_specials ) && count( $last_specials ) < 4 && $specials_rows > 2 && $is_cpt ) :
							for ( $i = 0; $i < ( 4 - count( $last_specials ) ); $i++ ) :
								$row = $specials_rows[ $i ];
								?>
								<article>
									<a href="<?php echo esc_url( $row['link'] ); ?>" class="post-link" rel="bookmark">
										<div class="post-thumbnail"><?php echo wp_get_attachment_image( $row['image'], array( 173, 123 ) ); ?></div>
										<div class="post-text">
											<h3 class="entry-title h2"><?php echo esc_html( $row['title'] ); ?></h3>
										</div>
									</a>
								</article>
							<?php endfor; ?>
							<?php
						endif;
						?>
					</div>
					<div class="all-button">
						<a href="<?php echo get_post_type_archive_link( 'special' ); ?>"
						   class="btn"><?php _e( 'See all specials', 'elcano' ); ?></a>
					</div>
				</section>
			<?php endif; ?>

			<section class="section-newsletter">
				<div class="envelope-icon">

				</div>
				<div class="left-col">
					<h2 class="h2"><?php esc_html_e( 'Subscribe to our newsletters and join our community', 'elcano' ) ?></h2>
				</div>
				<div class="right-col newsletter-form">
					<form action="#" method="POST">
						<div class="input-group email-input-group">
							<label for="newsletter-email">Email</label>
							<input id="newsletter-email" type="email" name="email" placeholder="<?php esc_attr_e( 'Enter your email', 'elcano' ) ?>">
						</div>
						<div class="input-group privacy-input-group">
							<label for="newsletter-privacy">
								<input id="newsletter-privacy" type="checkbox" name="privacy" value="1">
								<?php echo sprintf( __( 'I\'ve read and accepted the <a href="%s" target="_blank">privacy policy</a>', 'elcano' ), '#' ) ?>
							</label>
						</div>
						<button class="btn btn--fullwidth"><?php esc_html_e( 'Subscribe', 'elcano' ) ?></button>
						<p class="subscription-manage">
							<a href="#"><?php esc_html_e( 'Already subscribed? Manage your subscription', 'elcano' )?></a>
						</p>
					</form>
				</div>
			</section>

		</div><!-- .home-content -->
	</main><!-- #main -->

<?php
wp_reset_postdata();

get_footer();

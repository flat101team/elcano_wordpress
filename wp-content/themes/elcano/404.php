<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Elcano
 */

// Web Content
$last_content = wp_get_recent_posts(
	array(
		'post_type'        => array(
			// 'post',
			'analysis',
			'work_document',
			'report',
			'poll',
			// 'special',
			'monograph',
			'policy_paper',
			'commentary',
			// 'podcast',
			// 'video',
			// 'archive',
			'newsletter',
			'news_on_net',
			'ciber',
			'magazine',
			// 'press_release',
			// 'activity',
			// 'job',
			// 'biography',
		),
		'post_status'      => 'publish',
		'numberposts'      => 3,
		'suppress_filters' => false,
	),
	OBJECT
);

// Last Specials
$last_specials = wp_get_recent_posts(
	array(
		'post_type'        => 'special',
		'post_status'      => 'publish',
		'numberposts'      => 6,
		'suppress_filters' => false,
	),
	OBJECT
);


get_header();
?>

	<main id="primary" class="site-main">

		<header class="hero-header error404-header">
			<div class="hero-thumbnail viewwith"><img width="2880" height="1574" src="<?php bloginfo( 'template_directory' ); ?>/images/404-header.jpg" alt=""></div>
			<div class="hero-content fullwidth">

				<h1 class="page-title">
					<?php _ex( 'Error 404', 'Page title', 'elcano' ); ?><br>
					<?php _e( 'Sorry, something has gone wrong or the content you are looking for is no longer available', 'elcano' ); ?>
				</h1>

				<div class="search-404"><?php get_search_form(); ?></div>

				<div class="categories-title"><?php _e( 'All Themes', 'elcano' ); ?></div>
				<div class="categories">
					<div class="cat-wrap"><?php elcano_posts_categories( 'web' ); ?></div>
					<div class="cat-nav">
						<div class="cat-prev icon-left"></div>
						<div class="cat-next icon-right"></div>
					</div>
				</div>
			</div>
		</header>

		<div class="error404-content">

			<?php if ( ! empty( $last_content ) ) : ?>
				<section class="section-recent-web">
					<h2 class="h1-alt"><?php _e( 'Latest Publications', 'elcano' ); ?></h2>
					<p class="page-subtitle"><?php _e( 'The Most Recent', 'elcano' ); ?></p>
					<div class="the-archive">
						<?php foreach ( $last_content as $post ) : ?>
							<article>
								<div class="post-thumbnail"><?php the_post_thumbnail( array( 368, 240 ) ); ?></div>
								<div class="post-category"><?php elcano_primary_category(); ?></div>
								<h3 class="entry-title h2"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
								<div class="post-meta">
									<?php elcano_authors(); ?>
									<?php elcano_posted_on(); ?>
								</div>
							</article>
						<?php endforeach; ?>
					</div>
				</section>
			<?php endif; ?>

			<?php if ( ! empty( $last_specials ) ) : ?>
				<section class="section-recent-specials">
					<h2 class="h1-alt"><?php _e( 'Web specials', 'elcano' ); ?></h2>
					<p class="page-subtitle"><?php _e( 'Featured specials', 'elcano' ); ?></p>
					<div class="the-archive the-archive--main">
						<?php
						for ( $i = 0; $i < min( 2, count( $last_specials ) ); $i++ ) :
							$post = array_shift( $last_specials );
							?>
							<article>
								<a href="<?php the_permalink(); ?>" class="post-link" rel="bookmark">
									<div class="post-thumbnail"><?php the_post_thumbnail( array( 564, 240 ) ); ?></div>
									<div class="post-text">
										<div class="post-meta"><span class="years"><?php the_field( 'years' ); ?></span></div>
										<h3 class="entry-title h2"><?php the_title(); ?></h3>
									</div>
								</a>
							</article>
						<?php endfor; ?>
					</div>
					<div class="the-archive the-archive--secondary">
						<?php
						foreach ( $last_specials as $post ) :
							?>
							<article>
								<div class="post-thumbnail"><?php the_post_thumbnail( array( 173, 123 ) ); ?></div>
								<div class="post-text">
									<div class="post-meta"><span class="years"><?php the_field( 'years' ); ?></span></div>
									<h3 class="entry-title h2"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
								</div>
							</article>
						<?php endforeach; ?>
					</div>
					<div class="all-button">
						<a href="<?php echo get_post_type_archive_link( 'special' ); ?>" class="btn"><?php _e( 'View all specials', 'elcano' ); ?></a>
					</div>
				</section>
			<?php endif; ?>

		</div>

	</main><!-- #main -->

<?php
get_footer();

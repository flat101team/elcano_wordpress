<?php
/* Template Name: Nato public forum */
/**
 * The template for the landing of the nata public forum event
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>

	<?php wp_head(); ?>

	<!-- only for IE11 -->
	<link rel="none" id="ie-css" href="<?php echo get_stylesheet_directory_uri(); ?>/style-ie.css">
	<script>(function(H){window.msCrypto&&(H.getElementById('ie-css').rel='stylesheet')&&(H.getElementById('elcano-style-css').rel='none')})(document)</script>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open();
$blocks = parse_blocks(  get_the_content() );
$register_link = get_field('register_for_the_event');
?>
<div id="page" class="site">

<div class="site-content landing-nato">
<article class="page">
	<?php get_template_part( 'template-parts/landing-nato-public' ); ?>
    <div class="columns">
        <aside class="aside--left">
            <?php get_template_part( 'template-parts/section/share' ); ?>
            <section class="register">
                <div class="register__text">
	                <?php _e( 'Register at the event', 'elcano_nato' ); ?>
                </div>
                <div class="register__button">
                    <a href="<?php echo $register_link['url']?>"  target="<?php echo $register_link['target']?>">
	                    <?php _e( 'Register', 'elcano_nato' ); ?>
                    </a>
                </div>
            </section>
            <p></p>
            <section class="register">
                <div class="register__text">
			        <?php _e( 'Download Social Media Toolkit', 'elcano_nato' ); ?>
                </div>
                <div class="register__button">
                    <a href="https://media.realinstitutoelcano.org/wp-content/uploads/2022/06/nato-public-forum-social-media-toolkit-1.pdf"  target="_blank">
				        <?php _e( 'Social Media Toolkit', 'elcano_nato' ); ?>
                    </a>
                </div>
            </section>
        </aside>
        <div class="entry-content maincol maincol--small">
	        <?php echo apply_filters( 'the_content', get_the_content() );?>
        </div>
    </div>

</article>
<?php get_template_part( 'template-parts/section/share' ); ?>
<?php
get_footer();

<?php
/**
 * Template Name: Template Long Post
 * Template Post Type: commentary
 *
 * @package Elcano
 */

get_header();

elcano_breadcrumb();
?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content/content', 'long_post' );
		endwhile;
		?>

	</main><!-- #main -->

<?php
get_footer();

<?php
/**
 * Template Name: Template Filters
 *
 * @package Elcano
 */

global $wp_post_types, $wpdb;

$post_types = wp_filter_object_list( $wp_post_types, array( 'publicly_queryable' => true ), 'and', 'label' );
asort( $post_types, SORT_STRING | SORT_FLAG_CASE );

//Exclude post types from filters and queries
$exclude_cpt = array( 'activity', 'brussels_activity', 'archive', 'biography', 'ciber', 'attachment', 'inside_spain', 'press_release', 'job', 'post', 'project', 'podcast', 'magazine', 'news_on_net', 'scientific_council', 'video', 'location' );

foreach ( $exclude_cpt as $cpt ) {
	unset( $post_types[ $cpt ] );
}

//Get post type objects with custom order for submenu
$menu_cpts = array( 'commentary', 'analysis', 'policy_paper', 'monograph', 'report', 'poll', 'work_document', 'newsletter' );
$menu_cpts_obj = array_map( 'get_post_type_object', $menu_cpts );

get_header(); ?>

<div id="publications-menu">
	<nav class="nav-menu">
		<ul class="menu" id="publications-submenu">
			<?php foreach( $menu_cpts_obj as $post_type__obj ): ?>
				<li class="menu-item">
					<a href="<?php echo get_post_type_archive_link( $post_type__obj->name ); ?>" class="<?php echo $post_type__obj->name; ?>"><?php echo esc_html( $post_type__obj->label ) ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
	</nav>
	<div class="main-mobile">
		<div class="menu-toggle publications-menu" role="button" aria-controls="publications-menu" aria-expanded="false" aria-label="<?php esc_attr_e( 'Publication types', 'elcano' ) ?>"><span><?php esc_html_e( 'Publication types', 'elcano' )?></span></div>
	</div>
</div>

<?php
elcano_breadcrumb();



//Exclude terms
$exclude_tems_ids = array();
$exclude_terms = array( 'el-espectador-global', 'global-spectator', 'economia-internacional', 'european-union-europe', 'elcano-talks', 'elcano-talks-en', 'inside-spain-blog', 'inside-spain-blog-es', 'politica-global', 'seguridad-y-defensa', 'tecnologia' );
$terms_to_exclude = get_terms( array(
	'fields'     => 'ids',
	'hide_empty' => false,
	'taxonomy'   => 'category',
	'slug'       => $exclude_terms,
) );

if( !is_wp_error( $terms_to_exclude ) && count( $terms_to_exclude ) > 0){
	$exclude_tems_ids = $terms_to_exclude;
}

$terms = get_terms( array(
	'taxonomy'   => 'category',
	'hide_empty' => true,
	'exclude'    => $exclude_tems_ids,
) );

$post_types_list_in = "'" . implode( "', '", array_keys( $post_types ) ) . "'";

$years_result = array();
$years = $wpdb->get_results(
	"SELECT YEAR(post_date) FROM {$wpdb->posts} WHERE post_status = 'publish' AND post_type IN ($post_types_list_in) GROUP BY YEAR(post_date) ORDER BY YEAR(post_date) DESC",
	ARRAY_N
);

if ( is_array( $years ) && count( $years ) > 0 ) {
	foreach ( $years as $year ) {
		$years_result[] = $year[0];
	}
}

$query_args = array(
	'post_type'      => array_keys( $post_types ),
	'posts_per_page' => 9,
	'orderby' => 'date',
	'order' => 'DESC',
	'post_status' => 'publish',
);

$query = new WP_Query( $query_args );


$nonce = wp_create_nonce( 'form-filtros' );

ob_start();
?>

<main id="primary" class="site-main">

	<article id="publications">
		<header class="entry-header">
			<h1 class="entry-title h1-alt baseline"><?php esc_html_e( 'Publications', 'elcano' ); ?></h1>
		</header><!-- .entry-header -->

		<div class="entry-content">

			<!-- wp:group {"className":"entry-paragraph","layout":{"type":"constrained"}} -->
			<div class="wp-block-group entry-paragraph"><!-- wp:paragraph -->
				<p><?php esc_html_e( 'Written by the expert’s team and its network of contributors, the Elcano Royal Institute publishes a wide range of analyses, from quick responses to international current affairs to in-depth studies, highlighting our concise analyses with policy recommendations on key issue', 'elcano' ) ?>.</p>
				<!-- /wp:paragraph -->
				<!-- wp:paragraph -->
				<p><?php esc_html_e( 'Explore our publications by type, topic and year of publication', 'elcano' ) ?>.</p>
				<!-- /wp:paragraph -->
			</div>
			<!-- /wp:group -->
			<form method="POST" id="form-filtros">

				<!-- wp:columns {"className":"results-columns"} -->
				<div class="wp-block-columns results-columns"><!-- wp:column {"width":"25%"} -->
					<div class="wp-block-column" style="flex-basis:25%"></div><!-- /wp:column -->

					<!-- wp:column {"width":"75%"} -->
					<div class="wp-block-column" style="flex-basis:75%" id="results-holder">
						<p id="total-results"><span id="results-count"><?php echo esc_html( $query->found_posts ) ?></span> <?php  esc_html_e( 'results found', 'elcano' ) ?></p>

						<div id="applied-filters">
							<a href="#" id="remove-filters"><?php esc_html_e('Remove all filters', 'elcano' )?></a>
						</div>
					</div><!-- /wp:column -->
				</div><!-- /wp:columns -->
				<!-- wp:columns -->
				<div class="wp-block-columns"><!-- wp:column {"width":"25%"} -->
					<div class="wp-block-column" style="flex-basis:25%" id="filters-column"><!-- wp:greenshift-blocks/accordion {"id":"gsbp-c634b99"} -->
						<div class="wp-block-greenshift-blocks-accordion gs-accordion gspb_accordion-id-gsbp-c634b99" id="gspb_accordion-id-gsbp-c634b99" itemscope itemtype=""><!-- wp:greenshift-blocks/accordionitem {"id":"gsbp-8808a90","title":"Tipo de publicación","open":true} -->
							<div class="wp-block-greenshift-blocks-accordionitem gs-accordion-item gspb_accordionitem-gsbp-8808a90 gsopen" id="gspb_accordionitem-gsbp-8808a90" itemscope>
								<div class="gs-accordion-item__title" aria-expanded="true" role="button" tabindex="0" aria-controls="gspb-accordion-item-content-gsbp-8808a90">
									<div class="gs-accordion-item__heading"><?php esc_html_e( 'Type of publication', 'elcano' )?></div>
									<meta itemprop="name" content="<?php esc_attr_e( 'Type of publication', 'elcano' )?>"/>
									<span class="iconfortoggle">
										<span class="gs-iconbefore"></span>
										<span class="gs-iconafter"></span>
									</span>
								</div>
								<div class="gs-accordion-item__content" itemscope id="gspb-accordion-item-content-gsbp-8808a90" aria-hidden="false">
									<?php foreach( $post_types as $post_type__key => $post_type_label ): ?>
										<label for="post_type_<?php echo esc_attr( $post_type__key ) ?>">
											<input type="checkbox" name="post_type" value="<?php echo esc_attr( $post_type__key ) ?>" id="post_type_<?php echo esc_attr( $post_type__key ) ?>" data-label="<?php echo esc_attr( $post_type_label ) ?>">
											<?php echo $post_type_label ?>
										</label>
									<?php endforeach; ?>
								</div>
							</div>
							<!-- /wp:greenshift-blocks/accordionitem -->

							<!-- wp:greenshift-blocks/accordionitem {"id":"gsbp-82ac665","title":"Temática","open":false} -->
							<div class="wp-block-greenshift-blocks-accordionitem gs-accordion-item gspb_accordionitem-gsbp-82ac665 gsclose" id="gspb_accordionitem-gsbp-82ac665" itemscope>
								<div class="gs-accordion-item__title" aria-expanded="true" role="button" tabindex="0" aria-controls="gspb-accordion-item-content-gsbp-82ac665">
									<div class="gs-accordion-item__heading"><?php esc_html_e( 'Topics', 'elcano' )?></div>
									<meta itemprop="name" content="<?php esc_attr_e( 'Topics', 'elcano' )?>"/><span class="iconfortoggle">
										<span class="gs-iconbefore"></span>
										<span class="gs-iconafter"></span>
									</span>
								</div>
								<div class="gs-accordion-item__content" itemscope id="gspb-accordion-item-content-gsbp-82ac665" aria-hidden="false">
									<div class="gs-accordion-item__text" itemprop="text">
										<?php foreach( $terms as $term ): ?>
											<label for="category_<?php echo esc_attr( $term->term_taxonomy_id ) ?>">
												<input type="checkbox" name="category" value="<?php echo esc_attr( $term->term_taxonomy_id ) ?>" id="category_<?php echo esc_attr( $term->term_taxonomy_id ) ?>" data-label="<?php echo esc_attr( $term->name ) ?>">
												<?php echo $term->name ?>
											</label>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
							<!-- /wp:greenshift-blocks/accordionitem -->

							<!-- wp:greenshift-blocks/accordionitem {"id":"gsbp-40f130c","title":"Año de publicación","open":false} -->
							<div class="wp-block-greenshift-blocks-accordionitem gs-accordion-item gspb_accordionitem-gsbp-40f130c gsclose" id="gspb_accordionitem-gsbp-40f130c" itemscope>
								<div class="gs-accordion-item__title" aria-expanded="true" role="button" tabindex="0" aria-controls="gspb-accordion-item-content-gsbp-40f130c">
									<div class="gs-accordion-item__heading"><?php esc_html_e( 'Year of publication', 'elcano' )?></div>
									<meta itemprop="name" content="<?php esc_attr_e( 'Year of publication', 'elcano' )?>"/>
									<span class="iconfortoggle">
										<span class="gs-iconbefore"></span>
										<span class="gs-iconafter"></span>
									</span>
								</div>
								<div class="gs-accordion-item__content" itemscope id="gspb-accordion-item-content-gsbp-40f130c" aria-hidden="false">
									<div class="gs-accordion-item__text" itemprop="text">
										<?php foreach( $years_result as $year ): ?>
											<label for="year_<?php echo esc_attr( $year ) ?>">
												<input type="checkbox" name="year" value="<?php echo esc_attr( $year ) ?>" id="year_<?php echo esc_attr( $year ) ?>" data-label="<?php echo esc_attr( $year ) ?>">
												<?php echo $year ?>
											</label>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
							<!-- /wp:greenshift-blocks/accordionitem -->
						</div>
						<!-- /wp:greenshift-blocks/accordion -->
					</div>
					<!-- /wp:column -->

					<!-- wp:column {"width":"75%"} -->
					<div class="wp-block-column" style="flex-basis:75%" id="results-holder">

						<div id="filtros-resultados" class="the-archive the-archive--grid">
							<?php while( $query->have_posts() ): ?>
								<?php $query->the_post(); ?>
								<?php get_template_part( 'template-parts/archive/publications', get_post_type() ); ?>
							<?php endwhile; ?>
						</div>

						<button id="filters-load-more"><?php esc_html_e( 'Load more results', 'elcano' ) ?></button>

						<div id="results-overlay"></div>
					</div>
					<!-- /wp:column -->
				</div>
				<!-- /wp:columns -->
				<input type="hidden" value="1" name="page">
				<input type="hidden" value="<?php echo esc_attr( $nonce ) ?>" name="nonce">
			</form>
		</div>
	</article>
</main>

<?php

echo do_blocks( ob_get_clean() );

get_footer();

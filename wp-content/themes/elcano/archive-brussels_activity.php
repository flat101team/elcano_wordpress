<?php
/**
 * The template for displaying Brussels activities archive
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$past = get_query_var( 'past' );

$the_page = get_field( 'page_for_brussels_activity', 'option' );

get_header();
?>

	<main id="primary" class="site-main">

		<header class="page-header">
			<h1 class="page-title alt baseline"><?php echo get_the_title( $the_page ); ?></h1>
			<?php if ( get_the_content( null, false, $the_page ) ) : ?>
				<div class="archive-description"><?php echo apply_filters( 'the_content', get_the_content( null, false, $the_page ) ); ?></div>
			<?php endif; ?>
		</header><!-- .page-header -->

		<div class="activities-select">
			<a class="next-activities <?php echo $past ? '' : 'current'; ?>" href="<?php echo get_post_type_archive_link( 'brussels_activity' ); ?>"><?php _e( 'Upcoming events', 'elcano' ); ?></a>
			<span class="past-activities <?php echo $past ? 'current' : ''; ?>">
				<?php _e( 'Past events:', 'elcano' ); ?>
				<div class="years-select">
					<div class="current-year"><?php echo $past ?: __( 'Year', 'elcano' ); ?></div>
					<div class="wrapper">
						<ul>
							<?php
							$past  = get_post_type_archive_link( 'brussels_activity' ) . 'past';
							$years = elcano_brussels_activities_get_years();

							foreach ( $years as $year ) {
								printf( '<li><a href="%s">%d</a></li>', esc_url( "$past/$year/" ), $year );
							}
							?>
						</ul>
					</div>
				</div>
			</span>
		</div>

		<?php if ( have_posts() ) : ?>
			<div class="the-archive the-archive--activity">
				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/archive/brussels_activity' );
				endwhile;
				?>
			</div>

			<?php elcano_posts_pagination(); ?>

		<?php else : ?>

			<div class="no-results">
                <p class="h2">
					<?php if(get_query_var( 'past' )) {
						echo __( 'Sorry, there are no past events. Thank you for your interest!', 'elcano' );
					}else{
						echo __( 'Sorry, there are no upcoming events at this moment. ', 'elcano');
						?><a href="https://docs.google.com/forms/d/e/1FAIpQLSceIdHQl6Pyz6EyDunnKTBlKYlpbZsKkMnX78LSFpbOZDc2DQ/viewform?c=0&amp;amp;w=1" target="_blank"><?php echo __('You can subscribe', 'elcano') ?></a> <?php echo __(' to be informed about our activities and publications. Thank you for your interest!', 'elcano');
					} ?>
                </p>
			</div>

		<?php endif; ?>

	</main><!-- #main -->

<?php
get_footer();

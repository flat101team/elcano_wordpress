module.exports = (ctx) => ({
  map: ctx.options.map,
  plugins: ctx.env === 'ie' ?
    // IE styles
    [
      require('postcss-calc')(),
      require('postcss-inset')(),
      require('autoprefixer')({
        overrideBrowserslist: 'IE 11',
        grid: 'autoplace',
      }),
      require('stylelint')({
        fix: true,
        rules: {
          "color-function-notation": "legacy",
        }
      }),
      require('cssnano')({ preset: 'default' }),
    ] :
    // Non IE styles
    [
      require('postcss-calc')(),
      require('postcss-inset')(),
      require('postcss-sort-media-queries')(),
      require('autoprefixer')(),
      require('stylelint')({ fix: true }),
      require('cssnano')({ preset: 'default' }),
    ],
})

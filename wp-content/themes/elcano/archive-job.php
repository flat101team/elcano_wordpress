<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$the_page = get_field( 'page_for_job', 'option' );

get_header();

elcano_breadcrumb();
?>

	<main id="primary" class="site-main">

		<header class="page-header">
			<h1 class="page-title alt baseline"><?php echo get_the_title( $the_page ); ?></h1>
			<?php if ( get_the_content( null, false, $the_page ) ) : ?>
				<div class="archive-description"><?php echo apply_filters( 'the_content', get_the_content( null, false, $the_page ) ); ?></div>
			<?php endif; ?>
		</header>

		<?php if ( have_posts() ) : ?>
			<div class="the-archive the-archive--job">
				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/archive/job' );
				endwhile;
				?>
			</div>

			<?php elcano_posts_pagination(); ?>

		<?php else : ?>

			<div class="no-results">
				<p class="h2"><?php _e( 'No Job Offers', 'elcano' ); ?></p>
			</div>

		<?php endif; ?>

	</main

<?php
get_footer();

<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$the_page = get_field( 'page_for_special', 'option' );

get_header();
?>

	<main id="primary" class="site-main">

		<header class="page-header">
			<h1 class="page-title alt"><?php echo get_the_title( $the_page ); ?></h1>
			<p class="page-subtitle"><?php _e( 'Exclusive Contents', 'elcano' ); ?></p>
			<?php if ( get_the_content( null, false, $the_page ) ) : ?>
				<div class="archive-description"><?php echo apply_filters( 'the_content', get_the_content( null, false, $the_page ) ); ?></div>
			<?php endif; ?>
		</header><!-- .page-header -->

		<?php if ( have_posts() || have_rows( 'old-specials', $the_page ) ) : ?>
			<div class="the-archive the-archive--grid the-archive--special">
				<?php
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/archive/special' );
				endwhile;
				?>
                <?php
                $i = 0;

                while ( have_rows( 'old-specials', $the_page ) ) {
                    the_row();

                    printf(
                        '<article class="old-special special"><a href="%s">%s %s %s</a></article>',
                        esc_url( get_sub_field( 'link' ) ),
                        sprintf( '<div class="post-thumbnail">%s</div>', wp_get_attachment_image( get_sub_field( 'image' ), array( 368, 240 ) ) ),
                        sprintf( '<h2 class="entry-title">%s</h2>', esc_html( get_sub_field( 'title' ) ) ),
                        sprintf( '<div class="post-meta"><span class="years">%s</span></div>', esc_html( get_sub_field( 'year' ) ) )
                    );
                }
                ?>
			</div>

			<?php elcano_posts_pagination(); ?>

			<?php if ( ! is_paged() && have_rows( 'summaries', $the_page ) ) : ?>
				<?php elcano_append_dependency( 'elcano-theme', 'elcano-slider' ); ?>
				<div class="annual-summaries">
					<h2 class="section-title"><?php _e( 'Elcano Annual Summaries', 'elcano' ); ?></h2>
					<ul class="slider-controls">
						<li class="prev"><?php _e( 'Previous', 'elcano' ); ?></li>
						<li class="next"><?php _e( 'Next', 'elcano' ); ?></li>
						<li class="pages"><span class="current">1</span> <?php _e( 'of', 'elcano' ); ?> <span class="total">1</span></li>
					</ul>
					<div class="post-slides">
						<div class="post-slide">
							<?php
							$i = 0;

							while ( have_rows( 'summaries', $the_page ) ) {
								the_row();
								++$i;

								if ( $i > 1 && $i % 2 === 1 ) {
									echo '</div><div class="post-slide">';
								}

								printf(
									'<div class="post-related"><a href="%s">%s %s %s</a></div>',
									esc_url( get_sub_field( 'link' ) ),
									sprintf( '<div class="post-thumbnail">%s</div>', wp_get_attachment_image( get_sub_field( 'image' ), array( 100, 100 ) ) ),
									sprintf( '<h3 class="post-title">%s</h3>', esc_html( get_sub_field( 'title' ) ) ),
									sprintf( '<p class="post-intro">%s</p>', esc_html( get_sub_field( 'text' ) ) )
								);
							}
							?>
						</div>
					</div>
				</div>
			<?php endif; ?>

			<?php
		else :

			get_template_part( 'template-parts/content/content', 'none' );

		endif;
		?>

	</main><!-- #main -->

<?php
get_footer();

<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Elcano
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>

	<?php wp_head(); ?>

	<!-- only for IE11 -->
	<link rel="none" id="ie-css" href="<?php echo get_stylesheet_directory_uri(); ?>/style-ie.css">
	<script>(function(H){window.msCrypto&&(H.getElementById('ie-css').rel='stylesheet')&&(H.getElementById('elcano-style-css').rel='none')})(document)</script>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'elcano' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="top-nav">
			<div class="top-tabs">
				<span><?php _e( 'Elcano Royal Institute', 'elcano' ); ?></span>
				<a href="https://www.globalpresence.realinstitutoelcano.org/<?php echo defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE : 'es'; ?>/"><?php _e( 'Global Presence', 'elcano' ); ?></a>
			</div>

			<?php
			get_search_form();
			wp_nav_menu(
				array(
					'theme_location'  => 'top',
					'container_class' => 'nav-menu top-menu',
					'menu_id'         => 'top-menu',
					'menu_class'      => 'menu menu--top',
					'depth'           => 2,
					'walker'          => new Submenu_Wrap(),
				)
			);
			?>

			<div class="top-languages">
				<?php elcano_language_switcher(); ?>
			</div>
		</div><!-- .top-nav -->

		<div class="main-nav">
			<div class="site-branding">
				<?php if ( is_front_page() ) : ?>
					<h1 class="site-title"><svg class="site-logo" alt="<?php esc_attr_e( get_bloginfo('name') ) ?>"><use xlink:href="#svg__logo" /></svg></h1>
				<?php else : ?>
					<div class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><svg class="site-logo"><use xlink:href="#svg__logo" /></svg></a></div>
				<?php endif; ?>
			</div>

			<div class="main-mobile">
				<?php elcano_language_switcher(); ?>
				<div class="menu-search" role="button" aria-label="<?php esc_html_e( 'Search', 'elcano' ); ?>"></div>
				<div class="menu-toggle" role="button" aria-controls="mobile-menu" aria-expanded="false" aria-label="<?php esc_attr_e( 'Primary Menu', 'elcano' ); ?>"></div>
			</div>

			<?php
			wp_nav_menu(
				array(
					'theme_location'  => 'primary',
					'container'       => 'nav',
					'container_class' => 'nav-menu primary-menu',
					'menu_id'         => 'primary-menu',
					'menu_class'      => 'menu menu--primary',
					'depth'           => 3,
					'walker'          => new Submenu_Wrap(),
				)
			);
			?>
		</div><!-- .main-nav -->

		<!--<div class="second-nav">
			<?php
/*			wp_nav_menu(
				array(
					'theme_location'  => 'secondary',
					'container'       => 'nav',
					'container_class' => 'nav-menu secondary-menu',
					'menu_id'         => 'secondary-menu',
					'menu_class'      => 'menu menu--secondary',
					'depth'           => 2,
					'walker'          => new Submenu_Wrap(),
				)
			);

			get_search_form();
			*/?>
		</div>--><!-- .second-nav -->

		<?php if ( is_single() ) : ?>
			<div id="progress-header" class="progress-header">
				<svg aria-hidden="true"><use xlink:href="#svg__imagotipo" /></svg>
				<?php the_title( '<p>', '</p>' ); ?>
				<div class="progress-header-share">
					<div class="share-toggle" role="button"><span><?php printf( __( 'Share %s', 'elcano' ), esc_html( elcano_post_type_label( get_queried_object() ) ) ); ?></span></div>
					<div class="share-wrapper">
						<?php elcano_share( get_queried_object(), array( 'show_name' => true ) ); ?>
					</div>
				</div>
				<progress id="read-progress" value="0" max="100"></progress>
			</div>
		<?php endif; ?>

	</header><!-- #masthead -->

	<div class="site-content">

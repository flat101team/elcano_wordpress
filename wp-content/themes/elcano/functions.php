<?php
/**
 * Elcano functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Elcano
 */

// TODO: metas author from CPT
// TODO: image gallery slider
// TODO: review On The Fly Thumbs
// TODO: remove lazy load on header images

if ( ! defined( 'ELCANO_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( 'ELCANO_VERSION', '1.0.1' );
	define( 'ELCANO_PATH', get_template_directory() );
	define( 'ELCANO_URL', get_stylesheet_directory_uri() );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function elcano_setup() {

	// Load translations.
	load_theme_textdomain( 'elcano', ELCANO_PATH . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'top'       => esc_html__( 'Top', 'elcano' ),
			'primary'   => esc_html__( 'Primary', 'elcano' ),
			'secondary' => esc_html__( 'Secondary', 'elcano' ),
			'footer'    => esc_html__( 'Footer', 'elcano' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	// Allow extra sizes for content
	add_theme_support( 'align-wide' );

	add_theme_support( 'wp-block-styles' );

	add_theme_support( 'responsive-embeds' );

	// Color Palette
	add_theme_support(
		'editor-color-palette',
		array(
			array(
				'name'  => esc_attr__( 'red', 'elcano' ),
				'slug'  => 'red',
				'color' => '#da201c',
			),
			array(
				'name'  => esc_attr__( 'primary red', 'elcano' ),
				'slug'  => 'primary',
				'color' => '#bb2521',
			),
			array(
				'name'  => esc_attr__( 'primary red soft', 'elcano' ),
				'slug'  => 'primary-soft',
				'color' => '#f0a6a4',
			),
			array(
				'name'  => esc_attr__( 'primary red softer', 'elcano' ),
				'slug'  => 'primary-softer',
				'color' => '#f9eae9',
			),
			array(
				'name'  => esc_attr__( 'black', 'elcano' ),
				'slug'  => 'black',
				'color' => '#000',
			),
			array(
				'name'  => esc_attr__( 'gray', 'elcano' ),
				'slug'  => 'gray',
				'color' => '#545d69',
			),
			array(
				'name'  => esc_attr__( 'gray soft', 'elcano' ),
				'slug'  => 'gray-soft',
				'color' => '#989ea5',
			),
			array(
				'name'  => esc_attr__( 'gray softer', 'elcano' ),
				'slug'  => 'gray-softer',
				'color' => '#f0f0f0',
			),
			array(
				'name'  => esc_attr__( 'white', 'elcano' ),
				'slug'  => 'white',
				'color' => '#fff',
			),
			array(
				'name'  => esc_attr__( 'green', 'elcano' ),
				'slug'  => 'green',
				'color' => '#2a5757',
			),
			array(
				'name'  => esc_attr__( 'yellow', 'elcano' ),
				'slug'  => 'yellow',
				'color' => '#e9a350',
			),
		)
	);

	// Theme Gradients
	add_theme_support(
		'editor-gradient-presets',
		array(
			array(
				'name'     => esc_attr__( 'red gradient', 'elcano' ),
				'gradient' => 'linear-gradient(90deg, #9d3341 0%, #f04d64 100%)',
				'slug'     => 'red-gradient',
			),
		)
	);

	// Disable Custom Colors
	add_theme_support( 'disable-custom-colors' );

	// Disable font sizes
	add_theme_support( 'editor-font-sizes', array() );

	// Disable custom font sizes
	add_theme_support( 'disable-custom-font-sizes' );

	// Add support for Editor Styles
	add_theme_support( 'editor-styles' );

	// Enqueue Editor Styles
	add_editor_style();
}
add_action( 'after_setup_theme', 'elcano_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function elcano_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'elcano_content_width', 740 );
}
add_action( 'after_setup_theme', 'elcano_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function elcano_scripts() {
	wp_register_script( 'elcano-slider', get_template_directory_uri() . '/js/tiny-slider.js', array(), '2.9.3', true );
	wp_register_script( 'elcano-modal', get_template_directory_uri() . '/js/BigPicture.min.js', array(), '2.6.0', true );

	wp_enqueue_style( 'elcano-style', get_stylesheet_uri(), array(), ELCANO_VERSION );
	// IE 11 styles in (header.php)

	wp_enqueue_script( 'elcano-theme', get_template_directory_uri() . '/js/theme.js', array(), ELCANO_VERSION, true );
	wp_localize_script(
		'elcano-theme',
		'elcano_vars',
		array(
			'ajaxurl'     => admin_url( 'admin-ajax.php' ),
			'modal_error' => __( "Can't load media", 'elcano' ),
			'pdf_viewer'  => esc_url( plugins_url( 'pdf-viewer-block/inc/pdfjs/web/viewer.html' ) ),
		)
	);

	wp_enqueue_script( 'elcano-events', get_template_directory_uri() . '/js/events.js', array(), ELCANO_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_front_page() || is_home() || is_404() || get_post_type() === 'scientific_council') {
		elcano_append_dependency( 'elcano-theme', 'elcano-slider' );
	}

	if ( is_search() ) {
		wp_enqueue_script( 'elcano-zebra', ELCANO_URL . '/js/zebra_datepicker.min.js', array( 'jquery' ), '1.9.19', true );
		wp_enqueue_script( 'elcano-search', ELCANO_URL . '/js/search.js', array( 'elcano-zebra' ), ELCANO_VERSION, true );

		wp_localize_script(
			'elcano-search',
			'elcano_search',
			array(
				'months' => array(
					__( 'January' ),
					__( 'February' ),
					__( 'March' ),
					__( 'April' ),
					__( 'May' ),
					__( 'June' ),
					__( 'July' ),
					__( 'August' ),
					__( 'September' ),
					__( 'October' ),
					__( 'November' ),
					__( 'December' ),
				),
				'days'   => array(
					__( 'Sunday' ),
					__( 'Monday' ),
					__( 'Tuesday' ),
					__( 'Wednesday' ),
					__( 'Thursday' ),
					__( 'Friday' ),
					__( 'Saturday' ),
				),
				'clear'  => __( 'Clear', 'elcano' ),
				'today'  => __( 'Today', 'elcano' ),
				'from'   => _x( 'From', 'Filter date start', 'elcano' ),
				'to'     => _x( 'To', 'Filter date end', 'elcano' ),
			)
		);
	}

	if ( is_page_template( 'template-filtros.php' ) ) {
		wp_enqueue_script( 'gs-accordion' );
	}
}
add_action( 'wp_enqueue_scripts', 'elcano_scripts' );


/**
 * Composer dependencies.
 */
if ( file_exists( ELCANO_PATH . '/vendor/autoload.php' ) ) {
	require ELCANO_PATH . '/vendor/autoload.php';
} else {
	add_action(
		'admin_notices',
		function() {
			printf(
				'<div class="notice notice-warning is-dismissible"><p><strong>%s</strong> %s</p></div>',
				__( 'This theme require composer dependencies.', 'elcano' ),
				sprintf( __( 'Run %s in your theme path.', 'elcano' ), '<code>composer install --no-dev</code>' )
			);
		}
	);
}


/**
 * Include ACF Pro plugin
 */
//require ELCANO_PATH . '/lib/acf/acf.php';

// Customize the url setting to fix incorrect asset URLs.
function elcano_acf_settings_url( $url ) {
	return ELCANO_URL . '/lib/acf/';
}
//add_filter( 'acf/settings/url', 'elcano_acf_settings_url' );

// Hide the ACF admin menu item.
// add_filter('acf/settings/show_admin', '__return_false');


/**
 * Fix Category/Tags count for CPTs
 */
require ELCANO_PATH . '/lib/TaxonomyCountColumn.php';


/**
 * Required files
 */
$required_files = array(
	'utils.php',              // Functions utilities.
	'admin.php',              // wp-admin functions.
	'blocks.php',             // Custom blocks.
	'template-functions.php', // Functions which enhance the theme by hooking into WordPress.
	'template-tags.php',      // Custom template tags for this theme.
	'customizer.php',         // Customizer additions.
	'plugins.php',            // Declare required and recommended plugins for this theme.
	'publishpress.php',       // Customize PublishPress (posts statuses).
	'extras.php',             // Other WP & plugins minor tweaks.
	'love-me.php',            // Like post.
	'ajax.php',
	'blocks/class-elcano-register-custom-blocks.php',
);

// Require Extended CPTs package.
if ( function_exists( 'register_extended_post_type' ) ) {
	$required_files[] = 'cpts.php';               // Register Custom Post Types.
	$required_files[] = 'roles-capabilities.php'; // Roles and Capabilities (after cpts.php).
	$required_files[] = 'custom-fields.php';      // Custom Fields (ACF).
	$required_files[] = 'blog-terms.php';         // Blog terms archives separated from web content.
	$required_files[] = 'cpt/activity.php';       // CPT Activity functions.
	$required_files[] = 'cpt/project.php';       // CPT Project functions.
	$required_files[] = 'cpt/brussels_activity.php';       // CPT Activity functions.
	$required_files[] = 'cpt/biography.php';      // CPT Biography functions.
	$required_files[] = 'cpt/press_release.php';  // CPT Press Release functions.
	$required_files[] = 'cpt/archive.php';        // CPT Archive functions.
	$required_files[] = 'cpt/job.php';            // CPT Job functions.
    $required_files[] = 'cpt/member_council.php';        // CPT Scientific Council functions.
}

// Load Jetpack compatibility file.
if ( defined( 'JETPACK__VERSION' ) ) {
	$required_files[] = 'jetpack.php';
}

foreach ( $required_files as $required_file ) {
	require ELCANO_PATH . '/inc/' . $required_file;
}

// Show WordPress REST API Only to Registered Users
function elcano_rest_logged_in( $result ) {
    if ( empty( $result ) && ! is_user_logged_in() ) {
        return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
    }
    return $result;
}
add_filter( 'rest_authentication_errors', 'elcano_rest_logged_in' );


// Add copyright feed
function add_copyright_in_content_feeds() {
    echo '<copyright>Feeds Elcano Copyright (c), 2002-2022 Fundación Real Instituto Elcano</copyright>';
}
add_filter('rss2_head', 'add_copyright_in_content_feeds');

// Add authors custom field to content feed
function authors_custom_field_in_content_feed($content) {
    if(is_feed()) {
        $authors = get_field('authors');
        $output = '';
        $count = 1;

        if( !empty($authors) ):
            if(count($authors) > 1) {
                $output = 'Autores: ';
                foreach($authors as $author) {
                    if(count($authors) === $count) $output .= $author->post_title . '.';
                    else $output .= $author->post_title . ', ';
                    $count++;
                }
            } else {
                $output = 'Autor: ' . $authors[0]->post_title;
            }

        endif;

        $content = $content.$output;
    }
    return $content;
}
add_filter('the_content_feed','authors_custom_field_in_content_feed');

// Add authors custom field item to feed
function authors_custom_field_in_feed() {
    $authors = get_field('authors');
    $output = '';
    $count = 1;

    if( !empty($authors) ) {
        $output = '<author>';
        if(count($authors) > 1) {
            foreach($authors as $author) {
                if(count($authors) === $count) $output .= $author->post_title . '.';
                else $output .= $author->post_title . ', ';
                $count++;
            }
        } else {
            $output .= $authors[0]->post_title;
        }
        $output .= '</author>';
    }
    echo $output;
}

add_action('rss2_item', 'authors_custom_field_in_feed');

/* Add noindex only to paginated subpages of archives, search and 404 pages */
add_filter('wpseo_robots', 'custom_wpseo_robots');

function custom_wpseo_robots($robots) {
	if (is_paged()) {
		return 'noindex, follow';
	}

	return $robots;
}

function create_custom_feed() {
    load_template(get_template_directory().'/feed-rss2.php');
}

add_feed('rss2','create_custom_feed');

// Desactivar actualizaciones automáticas de traducciones
add_filter( 'auto_update_translation', '__return_false' );
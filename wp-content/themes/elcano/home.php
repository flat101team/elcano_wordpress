<?php
/**
 * The template for displaying blog archive page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title alt"><?php single_post_title(); ?></h1>
				<p class="page-subtitle"><?php _e( 'Latest News', 'elcano' ); ?></p>
			</header><!-- .page-header -->

			<div class="categories">
				<div class="cat-wrap"><?php elcano_posts_categories(); ?></div>
				<div class="cat-nav">
					<div class="cat-prev icon-left"></div>
					<div class="cat-next icon-right"></div>
				</div>
			</div>

			<div class="the-archive the-archive--grid the-archive--blog">
				<?php
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/archive/content' );
				endwhile;
				?>
			</div>
			<?php
			elcano_posts_pagination();

		else :

			get_template_part( 'template-parts/content/content', 'none' );

		endif;
		?>

	</main><!-- #main -->

<?php
get_footer();

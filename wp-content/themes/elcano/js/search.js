(function ($) {
  'use strict';

  var date = new Date();
  var today = ('00' + date.getDate()).slice(-2) + '-' + ('00' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear();

  function getUrlParam(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]')
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)')
    var results = regex.exec(location.search)
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '))
  }

  $('#date-from').Zebra_DatePicker({
    direction: false,
    pair: $('#date-to'),
    default_position: 'below',
    show_icon: false,
    offset: [-250, 40],
    format: 'd-m-Y',
    months: elcano_search.months,
    days: elcano_search.days,
    lang_clear_date: elcano_search.clear,
    show_select_today: false,
    onSelect: function (val) {
      $('#date-to').data('Zebra_DatePicker').update({ direction: [val, today] });

      if ($('.selected-filters .date-from').length) {
        $('.selected-filters .date-from').text(elcano_search.from + ': ' + val).data('filter', 'from__' + val);
      } else {
        $('.selected-filters').append('<li class="filter date-from" data-filter="from__' + val + '">' + elcano_search.from + ': ' + val + '</li>');
      }
      update_actions();
    },
    onClear: function () {
      $('.selected-filters .date-from').remove();
      update_actions();
    }
  });

  $('#date-to').Zebra_DatePicker({
    direction: [true, today],
    pair: $('#date-from'),
    default_position: 'below',
    show_icon: false,
    offset: [-250, 40],
    format: 'd-m-Y',
    months: elcano_search.months,
    days: elcano_search.days,
    lang_clear_date: elcano_search.clear,
    show_select_today: elcano_search.today,
    onSelect: function (val) {
      $('#date-from').data('Zebra_DatePicker').update({ direction: ['01-01-2000', val] });

      if ($('.selected-filters .date-to').length) {
        $('.selected-filters .date-to').text(elcano_search.to + ': ' + val).data('filter', 'to__' + val);
      } else {
        $('.selected-filters').append('<li class="filter date-to" data-filter="to__' + val + '">' + elcano_search.to + ': ' + val + '</li>');
      }
      update_actions();
    },
    onClear: function () {
      $('.selected-filters .date-to').remove();
      update_actions();
    }
  });

  // Toggle filters list (mobile only)
  $('.filters-toggle').on('click', function () {
    if ($('.search-form-wrap').is(':visible')) {
      $('.filters-list').toggleClass('filters-list--open');
      setTimeout(function () {
        $('.filter-name--open').removeClass('filter-name--open');
      }, 300);
    }
  });

  // Toggle filters list
  $('.filter-name>span').on('click', function () {
    var $filter = $(this).parent();
    $('.filter-name--open').not($filter).removeClass('filter-name--open');
    $filter.toggleClass('filter-name--open');
  });

  // Add selected filter
  $('.filters-list ul li:not(.filter-date)').on('click', function () {
    var $this = $(this);
    var key = $this.data('id');

    if (!$('.selected-filters').find('[data-filter="' + key + '"]').length) {
      $('.selected-filters').append('<li class="filter" data-filter="' + key + '">' + $this.text() + '</li>');
      update_actions();
    }
  });

  // Selected filters actions
  $('.selected-filters').on('click', 'li', function () {
    var $this = $(this);

    if ($this.hasClass('selected-filters__apply')) {
      window.location = window.location.origin + window.location.pathname + filters_url(); // apply filters
    } else if ($this.hasClass('selected-filters__clear')) {
      $('.selected-filters .filter').remove();  // clear filters
      update_actions();
    } else {
      if ($this.hasClass('date-from')) $('#date-from').val('');
      if ($this.hasClass('date-to')) $('#date-to').val('');
      $this.remove(); // delete one filter
      update_actions();
    }
  });

  // Create filters url query
  function filters_url() {
    var $filters = $('.selected-filters .filter');
    var filters = {};
    var search = '?s=' + getUrlParam('s');

    $('.selected-filters .filter').each(function () {
      var parts = $(this).data('filter').split('__');
      if (!!filters[parts[0]]) {
        filters[parts[0]].push(parts[1]);
      } else {
        filters[parts[0]] = [parts[1]];
      }
    });

    $.each(filters, function (key, value) {
      if (value.length) search += '&' + key + '=' + value.sort().join(',');
    });

    return search;
  }

  // Update clean/apply filters
  function update_actions() {
    $('.filter-name--open').removeClass('filter-name--open');
    $('.selected-filters__apply').toggle(window.location.search.replace('?s&', '?s=&') !== filters_url());
    $('.selected-filters__clear').toggle($('.selected-filters .filter').length > 0);
  }

}(jQuery));
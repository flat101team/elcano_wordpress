<?php
/**
 * Template Name: Content + Sidebar
 *
 * @package Elcano
 */

get_header();

elcano_breadcrumb();
?>

	<main id="primary" class="site-main">

		<?php the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title h1-alt baseline">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<div class="columns">

				<div class="entry-content maincol">
					<?php
					the_content();

					wp_link_pages(
						array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elcano' ),
							'after'  => '</div>',
						)
					);
					?>
				</div><!-- .entry-content -->

				<aside>
					<?php
					// More section.
					get_template_part( 'template-parts/section/more' );

					// Contact section.
					get_template_part( 'template-parts/section/contact' );
					?>
				</aside>

			</div>
		</article><!-- #post-<?php the_ID(); ?> -->

	</main><!-- #main -->

<?php
get_footer();

<?php
/**
 * The template for displaying podcasts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */



 $the_page = get_field( 'page_for_podcasts', 'option' );

 $posts_per_page = 8;
 $page           = get_query_var('paged');

 $podcasts_args  = array(
    'post_status'    => 'publish',
    'post_type'      => 'podcast',
    'posts_per_page' => 8,
    'orderby'        => 'date',
    'order'          => 'DESC',
    'paged'          => $page,
    //'offset'         => $posts_per_page * ( $page - 1 ) + 1,
 );

$latest_podcast_query = new WP_Query(
    array_merge(
        $podcasts_args,
        array(
            'posts_per_page' => 1,
            'paged'          => 1
        ),
    )
);

$latest_podcast = $latest_podcast_query->posts[0];
$meta_description = YoastSEO()->meta->for_current_page()->description;

$podcasts_args['post__not_in'] = array( $latest_podcast->ID );
$all_podcasts = new WP_Query(
    $podcasts_args
);

 get_header();

 elcano_breadcrumb(); ?>
    <main id="primary" class="site-main archive-podcast the-archive custom-archive-ajax">
        <header class="page-header">
            <h1 class="page-title alt baseline"><?php echo get_the_title( $the_page ); ?></h1>
            <?php if ( get_the_content( null, false, $the_page ) ) : ?>
                <div class="archive-description"><p><?php echo get_the_content( null, false, $the_page ) ?></p></div>
            <?php else: ?>
                <div class="archive-description"><p><?php echo $meta_description ?></p></div>
            <?php endif; ?>
        </header>

        <?php if( is_object( $latest_podcast ) && get_query_var('paged') <= 1 ): ?>
            <div class="latest-podcast">
                <article id="post-<?php the_ID(); ?>" class="<?php echo get_post_type(); ?> elcano-card">
                    <div class="post-thumbnail">
                        <a href="<?php the_permalink(); ?>">
                            <?php if ( has_post_thumbnail() ) {
                                the_post_thumbnail();
                            } else { ?>
                                <img src="<?php bloginfo('template_directory'); ?>/images/podcast.jpg" alt="<?php the_title(); ?>" />
                            <?php } ?>
                        </a>
                    </div>
                    <div class="post-info">
                        <?php if( elcano_primary_category( get_the_ID(), 'url', false ) !== false ): ?>
                            <div class="post-category">
                                <a href="<?php esc_attr( elcano_primary_category( get_the_ID(), 'url' ) ); ?>" class="category-link"><?php echo esc_html( elcano_primary_category( get_the_ID(), 'name' ) ) ?></a>
                            </div>
                        <?php endif; ?>

                        <h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
                        <div class="post-description"><?php the_excerpt() ?></div>
                        <div class="post-meta">
                            <?php elcano_authors(); ?>
                            <?php elcano_posted_on( get_the_ID(), true, false );?>
                        </div>
                        <?php if( get_field( "podcast_time_hours", get_the_ID() ) > 0 || get_field( "podcast_time_minutes", get_the_ID() ) > 0 ): ?>
                        <div class="podcast-duration">
                            <span class="icon-podcast">
                                <svg width="16" height="16" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g id="AV / timer">
                                        <mask id="mask0_142_2881" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="3" y="2" width="18" height="20">
                                            <g id="Icon Mask">
                                                <path id="Round" fill-rule="evenodd" clip-rule="evenodd" d="M11.002 3.99872C11.002 3.44872 11.452 2.99872 12.002 2.99872C17.072 2.99872 21.162 7.18872 20.992 12.2887C20.842 16.9887 16.912 20.8887 12.212 20.9987C7.14195 21.1187 3.00195 17.0387 3.00195 11.9987C3.00195 9.39872 4.10195 7.06872 5.86195 5.43872C6.26195 5.06872 6.88195 5.07872 7.26195 5.45872L12.702 10.8887C13.092 11.2787 13.092 11.9087 12.702 12.2987C12.312 12.6887 11.682 12.6887 11.292 12.2987L6.58195 7.57872C5.59195 8.77872 5.00195 10.3187 5.00195 11.9987C5.00195 15.9087 8.19195 19.0587 12.122 18.9987C15.972 18.9387 19.142 15.5887 19.002 11.7387C18.872 8.32872 16.312 5.55872 13.002 5.07872V5.99872C13.002 6.54872 12.552 6.99872 12.002 6.99872C11.452 6.99872 11.002 6.54872 11.002 5.99872V3.99872ZM6.00195 11.9987C6.00195 11.4464 6.44967 10.9987 7.00195 10.9987C7.55424 10.9987 8.00195 11.4464 8.00195 11.9987C8.00195 12.551 7.55424 12.9987 7.00195 12.9987C6.44967 12.9987 6.00195 12.551 6.00195 11.9987ZM11.002 16.9987C11.002 16.4464 11.4497 15.9987 12.002 15.9987C12.5542 15.9987 13.002 16.4464 13.002 16.9987C13.002 17.551 12.5542 17.9987 12.002 17.9987C11.4497 17.9987 11.002 17.551 11.002 16.9987ZM17.002 10.9987C16.4497 10.9987 16.002 11.4464 16.002 11.9987C16.002 12.551 16.4497 12.9987 17.002 12.9987C17.5542 12.9987 18.002 12.551 18.002 11.9987C18.002 11.4464 17.5542 10.9987 17.002 10.9987Z" fill="black"/>
                                            </g>
                                        </mask>
                                        <g mask="url(#mask0_142_2881)">
                                            <rect id="Color Fill" width="24" height="24" fill="#BB2521"/>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                            <?php
                                if (get_field( "podcast_time_hours", get_the_ID() ) > 0 ) {
                                    echo get_field( "podcast_time_hours", get_the_ID() ); echo " H ";
                                }
                                if (get_field( "podcast_time_minutes", get_the_ID() ) > 0 ) {
                                    echo get_field( "podcast_time_minutes", get_the_ID() ); echo " MIN";
                            } ?>
                        </div>
                        <?php endif; ?>
                        <div class="more-info">
                            <a href="<?php the_permalink(); ?>"><?php esc_html_e( 'See more information', 'elcano' )?></a>
                        </div>
                    </div>
                </article><!-- #post-<?php the_ID(); ?> -->
            </div>
        <?php endif; ?>
        <?php if (  $all_podcasts->have_posts() ) : ?>
			<div class="the-archive--grid the-archive--podcast">
				<?php
				/* Start the Loop */
				while (  $all_podcasts->have_posts() ) :
                    $all_podcasts->the_post();
					get_template_part( 'template-parts/archive/publications' );
				endwhile;
				?>
			</div>

            <?php elcano_posts_pagination( $all_podcasts ) ?>

		<?php else : ?>

			<div class="no-results">
				<p class="h2"><?php _e( 'No Podcasts found', 'elcano' ); ?></p>
			</div>

		<?php endif; ?>
    </main>
<?php
 get_footer();
<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$the_page = get_field( 'page_for_press_release', 'option' );

get_header();

elcano_breadcrumb();
?>

	<main id="primary" class="site-main">

		<header class="page-header">
			<h1 class="page-title alt baseline"><?php echo get_the_title( $the_page ); ?></h1>
		</header><!-- .page-header -->

		<div class="columns">

			<div class="maincol">
				<?php if ( get_the_content( null, false, $the_page ) ) : ?>
					<div class="page-content"><?php echo apply_filters( 'the_content', get_the_content( null, false, $the_page ) ); ?></div>
				<?php endif; ?>

				<div class="press_release-filter">
					<span><?php _e( 'Press releases by year:', 'elcano' ); ?></span>
					<div class="years-select">
						<div class="current-year"><?php echo get_query_var( 'year' ) ?: __( 'Year', 'elcano' ); ?></div>
						<div class="wrapper">
							<ul>
								<?php
								wp_get_archives(
									array(
										'type'      => 'yearly',
										'format'    => 'html',
										'post_type' => 'press_release',
									)
								);
								?>
							</ul>
						</div>
					</div>
				</div>

				<?php if ( have_posts() ) : ?>
					<div class="the-archive the-archive--press_release">
						<?php
						/* Start the Loop */
						while ( have_posts() ) :
							the_post();
							get_template_part( 'template-parts/archive/press_release' );
						endwhile;
						?>
					</div>

					<?php elcano_posts_pagination(); ?>

				<?php else : ?>

					<div class="no-results">
						<p class="h2"><?php _e( 'No Press Releases', 'elcano' ); ?></p>
					</div>

				<?php endif; ?>
			</div>

			<aside>
				<?php
				// More section.
				get_template_part( 'template-parts/section/more', '', array( 'post_id' => $the_page ) );

				// Contact section.
				get_template_part( 'template-parts/section/contact', '', array( 'post_id' => $the_page ) );
				?>
			</aside>

	</main><!-- #main -->

<?php
get_footer();

<?php
/**
 * The template for displaying tag archive page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

get_header();

elcano_breadcrumb();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php if ( 'post' === get_post_type() ) : ?>
					<h1 class="page-title alt"><?php echo get_the_title( get_option( 'page_for_posts' ) ); ?></h1>
					<p class="page-subtitle"><?php single_tag_title(); ?></p>
				<?php else : ?>
					<h1 class="page-title alt"><?php _e( 'Themes', 'elcano' ); ?></h1>
					<p class="page-subtitle"><?php single_tag_title(); ?></p>
				<?php endif; ?>
			</header><!-- .page-header -->

			<div class="the-archive the-archive--grid the-archive--tag">
				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/archive/content' );
				endwhile;
				?>
			</div>
			<?php
			elcano_posts_pagination();

		else :

			get_template_part( 'template-parts/content/content', 'none' );

		endif;
		?>

	</main><!-- #main -->

<?php
get_footer();

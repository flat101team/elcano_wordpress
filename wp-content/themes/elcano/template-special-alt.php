<?php
/**
 * Template Name: Alternate Header
 * Template Post Type: special
 *
 * @package Elcano
 */

get_header();

elcano_breadcrumb();
?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content/content', 'special-alt' );

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
get_footer();

<?php
/**
 * The template for displaying videos
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

 $the_page = get_field( 'page_for_videos', 'option' );

 var_dump( $the_page);
 $videos_args = array(
    'post_status'    => 'publish',
    'post_type'      => 'video',
    'posts_per_page' => 8,
    'orderby'        => 'date',
    'order'          => 'DESC'
 );

 $all_videos = new WP_Query(
    $videos_args
);
$meta_description = YoastSEO()->meta->for_current_page()->description;

 get_header();

 elcano_breadcrumb(); ?>
    <main id="primary" class="site-main archive-video the-archive custom-archive-ajax">
        <header class="page-header">
            <h1 class="page-title alt baseline"><?php echo get_the_title( $the_page ); ?></h1>
            <?php if ( get_the_content( null, false, $the_page ) ) : ?>
                <div class="archive-description"><p><?php echo get_the_content( null, false, $the_page ) ?></p></div>
            <?php else: ?>
                <div class="archive-description"><p><?php echo $meta_description ?></p></div>
            <?php endif; ?>
        </header>

        <?php if (  $all_videos->have_posts() ) : ?>
			<div class="the-archive--grid the-archive--video">
				<?php
				/* Start the Loop */
				while (  $all_videos->have_posts() ) :
                    $all_videos->the_post();
					get_template_part( 'template-parts/archive/publications' );
				endwhile;
				?>
			</div>

            <?php if( ( $all_videos->max_num_pages - 1 ) > 0 ): ?>
                <button id="load-more-button" class="btn" data-post-type="video" data-page="1"><?php esc_html_e( 'Load more results', 'elcano' ) ?></button>
                <?php wp_nonce_field( 'archive_load_more', 'archive_load_more_nonce' ) ?>
            <?php endif; ?>

		<?php else : ?>

			<div class="no-results">
				<p class="h2"><?php _e( 'No videos found', 'elcano' ); ?></p>
			</div>

		<?php endif; ?>
    </main>
<?php
 get_footer();
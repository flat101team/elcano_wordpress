<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Elcano
 */

global $wp_query, $wp_post_types;

$num_results = $wp_query->found_posts;

// Filters
$filter_types   = isset( $_GET['types'] ) ? explode( ',', $_GET['types'] ) : array();
$filter_cats    = isset( $_GET['cat'] ) ? explode( ',', $_GET['cat'] ) : array();
//$filter_tags    = isset( $_GET['tag'] ) ? explode( ',', $_GET['tag'] ) : array();
$filter_authors = isset( $_GET['authors'] ) ? explode( ',', $_GET['authors'] ) : array();
$filter_from    = isset( $_GET['from'] ) && preg_match( '/^\d{2}-\d{2}-\d{4}$/', $_GET['from'] ) ? $_GET['from'] : '';
$filter_to      = isset( $_GET['to'] ) && preg_match( '/^\d{2}-\d{2}-\d{4}$/', $_GET['to'] ) ? $_GET['to'] : '';
//$has_filters    = $filter_types || $filter_cats || $filter_tags || $filter_authors || $filter_from || $filter_to ? true : false;
$has_filters    = $filter_types || $filter_cats || $filter_authors || $filter_from || $filter_to ? true : false;

sort( $filter_types );
sort( $filter_cats, SORT_NUMERIC );
//sort( $filter_tags );
sort( $filter_authors, SORT_NUMERIC );

get_header();
?>
	<div class="search-form-wrap"><?php get_search_form( array( 'page' => 'search' ) ); ?></div>

	<div class="search-filters">
		<p class="filters-toggle"><?php _e( 'Search filtering by content type, topics…', 'elcano' ); ?></p>

		<ul class="filters-list">
			<?php
			// Post Types Select
			$post_types_list = array_merge( array( 'post' ), elcano_current_post_type( 'web' ) );
			$post_types      = wp_filter_object_list( $wp_post_types, array(), 'and', 'label' );
			$post_types      = array_intersect_key( $post_types, array_flip( $post_types_list ) );

			asort( $post_types, SORT_STRING | SORT_FLAG_CASE );

			echo '<li class="filter-name"><span>' . __( 'Type of content', 'elcano' ) . '</span><ul>';
            foreach ( $post_types as $post_type => $post_type_label ) {
                if ( $post_type === "inside_spain") {
                    printf( '<li data-id="types__%s">', $post_type, esc_html( $post_type_label ) );
                    _e('Inside Spain archive', 'elcano');
                    printf( '</li>', $post_type, esc_html( $post_type_label ) );
                } else printf( '<li data-id="types__%s">%s</li>', $post_type, esc_html( $post_type_label ) );
            }
			echo '</ul></li>';

			// Topics Select (categories & post_tags)
			$topics     = array();
			//$tags       = get_tags();
			$categories = get_categories();

			foreach ( $categories as $cat ) {
				$topics[ 'cat__' . $cat->term_id ] = $cat->name;
			}

/*			foreach ( $tags as $tag ) {
				$topics[ 'tag__' . $tag->slug ] = $tag->name;
			}*/

			// Remove duplicates (keep categories)
			$topics = array_unique( $topics );

			asort( $topics, SORT_STRING | SORT_FLAG_CASE );

			echo '<li class="filter-name"><span>' . __( 'Topics', 'elcano' ) . '</span><ul>';
			foreach ( $topics as $topic_id => $topic ) {
                if (!($topic === "Seguridad y defensa" || $topic === "Economía internacional")) {
                    printf( '<li data-id="%s">%s</li>', $topic_id, esc_html( $topic ) );
                }
			}
			echo '</ul></li>';

			// Authors Select
			$authors = elcano_get_authors();

			echo '<li class="filter-name"><span>' . __( 'Authors', 'elcano' ) . '</span><ul>';
			foreach ( $authors as $author ) {
				printf( '<li data-id="authors__%d">%s</li>', $author->ID, esc_html( $author->post_title ) );
			}
			echo '</ul></li>';

			// Dates Select
			echo '<li class="filter-name"><span>' . __( 'Time Period', 'elcano' ) . '</span><ul>';
			printf( '<li class="filter-date icon-cal"><input id="date-from" type="text" value="%s" placeholder="%s"></li>', $filter_from, __( 'Select start date', 'elcano' ) );
			printf( '<li class="filter-date icon-cal"><input id="date-to" type="text" value="%s" placeholder="%s"></li>', $filter_to, __( 'Select end date', 'elcano' ) );
			echo '</ul></li>';
			?>
		</ul>

		<ul class="selected-filters">
			<li class="selected-filters__clear" <?php echo $has_filters ? 'style="display:list-item;"' : ''; ?>><?php _e( 'Clean all filters', 'elcano' ); ?></li>
			<li class="selected-filters__apply"><?php _e( 'Apply filters', 'elcano' ); ?></li>
			<li class="selected-filters__sep"><!-- separator --></li>
			<?php
			foreach ( $filter_types as $type ) {
				printf( '<li class="filter" data-filter="types__%s">%s</li>', esc_attr( $type ), esc_html( $post_types[ $type ] ) );
			}

			foreach ( $filter_cats as $cat ) {
				printf( '<li class="filter" data-filter="cat__%d">%s</li>', (int) $cat, esc_html( get_cat_name( $cat ) ) );
			}

/*			foreach ( $filter_tags as $tag_slug ) {
				$tag = get_term_by( 'slug', $tag_slug, 'post_tag' );
				printf( '<li class="filter" data-filter="tag__%s">%s</li>', esc_attr( $tag_slug ), esc_html( $tag->name ) );
			}*/

			foreach ( $filter_authors as $author ) {
				printf( '<li class="filter" data-filter="authors__%d">%s</li>', (int) $author, esc_html( get_the_title( $author ) ) );
			}

			if ( $filter_from ) {
				printf( '<li class="filter date-from" data-filter="from__%1$s">%2$s: %1$s</li>', $filter_from, _x( 'From', 'Filter date start', 'elcano' ) );
			}

			if ( $filter_to ) {
				printf( '<li class="filter date-to" data-filter="to__%1$s">%2$s: %1$s</li>', $filter_to, _x( 'To', 'Filter date end', 'elcano' ) );
			}
			?>
		</ul>

	</div>

	<main id="primary" class="site-main">

		<header class="page-header">
			<h1 class="alt baseline"><?php echo get_search_query() ?: __( 'Search results', 'elcano' ); ?> (<?php echo $num_results; ?>)</h1>
		</header><!-- .page-header -->

		<?php if ( have_posts() ) : ?>
			<div class="the-archive the-archive--grid the-archive--search">
				<?php
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/archive/search', get_post_type() );
				endwhile;
				?>
			</div>

			<?php elcano_posts_pagination(); ?>

		<?php else : ?>

			<div class="no-results">
				<p class="h2"><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'elcano' ); ?></p>
			</div>

		<?php endif; ?>

	</main><!-- #main -->

<?php
get_footer();

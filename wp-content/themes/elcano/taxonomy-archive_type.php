<?php
/**
 * The template for displaying archive_type archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$the_term = get_queried_object();
$the_page = get_field( 'page_for_archive', 'option' );

get_header();

elcano_breadcrumb();
?>

	<main id="primary" class="site-main">

		<header class="page-header">
			<h1 class="entry-title alt baseline"><?php echo esc_html( $the_term->name ); ?></h1>
		</header><!-- .page-header -->

		<div class="columns">
			<aside class="aside--toc">
				<section class="widget terms-links">
					<p class="widget-title widget-title--alt"><?php _e( 'Archive Content Types', 'elcano' ); ?></p>
					<ul class="terms-list">
						<?php
						wp_list_categories(
							array(
								'taxonomy'           => 'archive_type',
								'title_li'           => '',
								'use_desc_for_title' => false,
							)
						);
						?>
					</ul>
				</section>
			</aside>

			<div class="entry-content maincol">
				<?php if ( $the_term->description ) : ?>
					<div class="entry-description"><?php echo apply_filters( 'the_content', $the_term->description ); ?></div>
				<?php endif; ?>

				<?php if ( have_posts() ) : ?>
					<div class="the-archive the-archive--archive">
						<?php
						/* Start the Loop */
						while ( have_posts() ) :
							the_post();
							get_template_part( 'template-parts/archive/archive' );
						endwhile;
						?>
					</div>

					<?php elcano_posts_pagination(); ?>

				<?php else : ?>

					<div class="no-results">
						<p class="h2"><?php _e( 'No Archive', 'elcano' ); ?></p>
					</div>

				<?php endif; ?>
			</div>

			<aside>
				<?php
				// More section.
				get_template_part( 'template-parts/section/more', '', array( 'post_id' => $the_page ) );

				// Contact section.
				get_template_part( 'template-parts/section/contact', '', array( 'post_id' => $the_page ) );
				?>
			</aside>
		</div>
	</main><!-- #main -->

<?php
get_footer();

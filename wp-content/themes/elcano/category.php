<?php
/**
 * The template for displaying category archive page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */


get_header();

elcano_breadcrumb();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php if ( 'post' === get_post_type() ) : ?>
					<?php if ( in_array( get_queried_object_id(), (array) get_field( 'featured_categories', 'option' ) ) ) : ?>
						<h1 class="page-title alt baseline"><?php single_cat_title(); ?></h1>
					<?php else : ?>
						<h1 class="page-title alt"><?php echo get_the_title( get_option( 'page_for_posts' ) ); ?></h1>
						<p class="page-subtitle"><?php single_cat_title(); ?></p>
					<?php endif; ?>
				<?php else : ?>
					<h1 class="page-title alt baseline"><?php single_cat_title(); ?></h1>
				<?php endif; ?>
			</header><!-- .page-header -->

			<div class="columns">

				<div class="the-archive the-archive--category maincol maincol--medium">
					<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/archive/category', get_post_type() );
					endwhile;
					?>
				</div>

				<aside class="aside--available">
					<section class="widget posts-tags">
						<p class="widget-title widget-title--big"><?php _e( 'Tags', 'elcano' ); ?></p>
						<?php elcano_posts_tags(); ?>
					</section>

					<section class="widget last-posts">
						<p class="widget-title widget-title--big"><?php echo 'post' === elcano_current_post_type() ? __( 'Latest Posts', 'elcano' ) : __( 'Latest Publications', 'elcano' ); ?></p>
						<?php
						$recent_posts = wp_get_recent_posts(
							array(
								'post_type'        => elcano_current_post_type(),
								'post_status'      => 'publish',
								'numberposts'      => 3,
								'suppress_filters' => false,
							),
							OBJECT
						);
						/*
						if ( is_array( $recent_posts ) ) {
							foreach ( $recent_posts as $recent_post ) {
								printf(
									'<article class="post-related"><a href="%s" rel="bookmark">%s %s %s</a></article>',
									esc_url( get_the_permalink( $recent_post ) ),
									sprintf( '<div class="post-thumbnail">%s</div>', get_the_post_thumbnail( $recent_post, array( 100, 100 ) ) ),
									sprintf( '<p class="post-title">%s</p>', get_the_title( $recent_post ) ),
									sprintf( '<p class="post-meta">%s</p>', esc_html( get_the_date( __( 'F j, Y' ), $recent_post ) ) )
								);
							}
						}*/
						?>
						<?php if ( is_array( $recent_posts ) ) : ?>
							<?php foreach ( $recent_posts as $recent_post ) : ?>
								<article class="post-related">
									<a href=<?php echo esc_url( get_the_permalink( $recent_post ) ); ?> >
										<div class="post-thumbnail">
											<?php
											if ( has_post_thumbnail( $recent_post ) ) {
												echo get_the_post_thumbnail( $recent_post, array( 100, 100 ) );
											} else {
												?>
												<?php if ( $recent_post->post_type === 'analysis' ) : ?>
													<img src="<?php bloginfo( 'template_directory' ); ?>/images/analisis.jpg" alt="<?php the_title(); ?>" />
												<?php elseif ( $recent_post->post_type === 'commentary' ) : ?>
													<img src="<?php bloginfo( 'template_directory' ); ?>/images/comentario.jpg" alt="<?php the_title(); ?>" />
												<?php elseif ( $recent_post->post_type === 'ciber' ) : ?>
													<img src="<?php bloginfo( 'template_directory' ); ?>/images/ciber.jpg" alt="<?php the_title(); ?>" />
												<?php elseif ( $recent_post->post_type === 'magazine' ) : ?>
													<img src="<?php bloginfo( 'template_directory' ); ?>/images/revista.jpg" alt="<?php the_title(); ?>" />
												<?php elseif ( $recent_post->post_type === 'poll' ) : ?>
													<img src="<?php bloginfo( 'template_directory' ); ?>/images/encuesta.jpg" alt="<?php the_title(); ?>" />
												<?php elseif ( $recent_post->post_type === 'report' ) : ?>
													<img src="<?php bloginfo( 'template_directory' ); ?>/images/informe.jpg" alt="<?php the_title(); ?>" />
												<?php elseif ( $recent_post->post_type === 'post' ) : ?>
													<img src="<?php bloginfo( 'template_directory' ); ?>/images/post.jpg" alt="<?php the_title(); ?>" />
												<?php elseif ( $recent_post->post_type === 'policy_paper' ) : ?>
													<img src="<?php bloginfo( 'template_directory' ); ?>/images/policy.jpg" alt="<?php the_title(); ?>" />
												<?php elseif ( $recent_post->post_type === 'work_document' ) : ?>
													<img src="<?php bloginfo( 'template_directory' ); ?>/images/documento.jpg" alt="<?php the_title(); ?>" />
												<?php elseif ( $recent_post->post_type === 'video' ) : ?>
													<img src="<?php bloginfo( 'template_directory' ); ?>/images/video.jpg" alt="<?php the_title(); ?>" />
												<?php elseif ( $recent_post->post_type === 'archive' ) : ?>
													<img src="<?php bloginfo( 'template_directory' ); ?>/images/archivo.jpg" alt="<?php the_title(); ?>" />
                                                <?php elseif ( $recent_post->post_type === 'activity' ) : ?>
                                                    <img src="<?php bloginfo( 'template_directory' ); ?>/images/actividad.jpg" alt="<?php the_title(); ?>" />
                                                <?php elseif ( $recent_post->post_type === 'single-report' ) : ?>
                                                    <img src="<?php bloginfo( 'template_directory' ); ?>/images/informe.jpg" alt="<?php the_title(); ?>" />
                                                <?php elseif ( $recent_post->post_type === 'newsletter' ) : ?>
                                                    <img src="<?php bloginfo( 'template_directory' ); ?>/images/newsletter.png" alt="<?php the_title(); ?>" />
                                                <?php elseif ( $recent_post->post_type === 'press_release' ) : ?>
                                                    <img src="<?php bloginfo( 'template_directory' ); ?>/images/nota-prensa.jpg" alt="<?php the_title(); ?>" />
                                                <?php elseif ( $recent_post->post_type === 'news_on_net' ) : ?>
                                                    <img src="<?php bloginfo( 'template_directory' ); ?>/images/novedades-red.jpg" alt="<?php the_title(); ?>" />
                                                <?php elseif ( $recent_post->post_type === 'podcast' ) : ?>
                                                    <img src="<?php bloginfo( 'template_directory' ); ?>/images/podcast.jpg" alt="<?php the_title(); ?>" />
                                                <?php elseif ( $recent_post->post_type === 'project' ) : ?>
                                                    <img src="<?php bloginfo( 'template_directory' ); ?>/images/proyecto.png" alt="<?php the_title(); ?>" />
                                                <?php elseif ( $recent_post->post_type === 'brussels_activity' ) : ?>
                                                    <img src="<?php bloginfo( 'template_directory' ); ?>/images/actividad.jpg" alt="<?php the_title(); ?>" />
												<?php endif; ?>
											<?php } ?>
										</div>
										<p class="post-title"><?php echo get_the_title( $recent_post ); ?></p>
										<p class="post-meta"><?php echo esc_html( get_the_date( __( 'F j, Y' ), $recent_post ) ); ?></p>
									</a>
								</article>
							<?php endforeach; ?>
						<?php endif; ?>

					</section>

<!--					--><?php
/*					// The Most Read.
					if ( 'post' !== get_post_type() ) {
						get_template_part( 'template-parts/section/most-read' );
					}
					*/?>
				</aside>

			</div>
			<?php
			elcano_posts_pagination();

		else :

			get_template_part( 'template-parts/content/content', 'none' );

		endif;
		?>

	</main><!-- #main -->

<?php
get_footer();

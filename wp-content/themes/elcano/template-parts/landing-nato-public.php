<?php
/**
 * Template part for displaying post share links
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */
global $post;
$other_language =  get_field('available_in_other_languaje');
$register_link = get_field('register_for_the_event');
$logo = get_field('logo');
$logos = get_field('bottom_logos');
$blocks = parse_blocks( $post->post_content );
$video = $timeline = $displayAgenda = $displayVideo = null;
foreach ( $blocks as $block ) {
	if ( 'core/embed' === $block['blockName'] ) {
		$video = true;
		if($block['attrs']['className'] !== 'oculto') {
			$displayVideo = true;
        }
	}
	if ( 'acf/elcano-timeline-hour' === $block['blockName'] ) {
		$agenda = true;
        if($block['attrs']['className'] !== 'oculto') {
            $displayAgenda = true;
        }
	}
}
?>
<header class="entry-header">
<div class="hero-nato-public">
    <div class="hero-nato-public__languages top-languages">
		<?php elcano_language_switcher(); ?>
    </div>
	<div class="hero-nato-public__container">
		<div class="hero-nato-public__top">
			<div  class="hero-nato-public__name">
                <div class="hero-nato-public__logo">
                    <div class="hero-nato-public__img">
                        <img src="<?php echo $logo['sizes']['large'] ?>" alt="<?php echo $logo['name']?>">
                    </div>
                    <div class="hero-nato-public__event">
                        <div class="hero-nato-public__event-top">
			                <?php the_field('name'); ?>
                        </div>
                        <div class="hero-nato-public__event-bottom">
			                <?php echo get_field('name_2'); ?>
                        </div>
                    </div>
                </div>
                <div class="hero-nato-public__date">
					<?php the_field('date_and_site'); ?>
                </div>
			</div>
            <div class="hero-nato-public__brands">
                <img src="<?php echo $logos['sizes']['large']?>" alt="<?php echo $logos['name']?>">
            </div>
		</div>
		<div class="hero-nato-public__bottom">
            <div class="hero-nato-public__description-wrapper">
                <div>
                    <p><?php the_field('description'); ?></p>
                    <div class="hero-nato-public__other-country">
                        <a href="<?php echo $other_language['url'] ?>" target="<?php echo $other_language['target'] ?>"><?php echo $other_language['title'] ?></a>
                    </div>
                </div>
            </div>
            <div class="hero-nato-public__register">
                <a href="<?php echo $register_link['url']?>" target="<?php echo $register_link['target']?>"><?php echo $register_link['title']?></a>
            </div>
		</div>

	</div>
    <div class="hero-nato-public__nav">
	    <?php if($video):?>
	        <?php if($displayAgenda):?>
                <div>
                    <a href="#live-broadcast" class="hero-nato-public__broadcast">
                        <?php _e( 'Live Broadcast', 'elcano_nato' ); ?>
                    </a>
                </div>
            <?php endif;?>
        <?php endif;?>
	    <?php if($agenda):?>
            <?php if($displayAgenda):?>
                <div>
                    <a class="hero-nato-public__event-agenda" href="#agenda">
                        <?php _e( 'See event agenda', 'elcano_nato' ); ?>
                    </a>
                </div>
            <?php endif;?>
        <?php endif;?>
        <div>
            <a class="hero-nato-public__calendar" target="_blank" href="https://calendar.google.com/calendar/render?action=TEMPLATE&dates=20220628T130000Z%2F20220629T170000Z&details=The%20NATO%20Public%20Forum%20forum%20will%20take%20place%20from%2028%20to%2029%20June%20during%20the%20NATO%20Summit%20in%20Madrid.%20It%20is%20co-organised%20by%20NATO%2C%20the%20Elcano%20Royal%20Institute%2C%20the%20German%20Marshall%20Fund%2C%20the%20Munich%20Security%20Conference%20and%20the%20Atlantic%20Council.&location=Madrid&text=NATO%20Public%20Forum">
	            <?php _e( 'Add event to calendar', 'elcano_nato' ); ?>
            </a>
        </div>
        <div>
            <a class="hero-nato-public__download" target="_blank" href="https://media.realinstitutoelcano.org/wp-content/uploads/2022/06/nato-public-forum-social-media-toolkit-1.pdf">
			    <?php _e( 'Download Social Media Toolkit', 'elcano_nato' ); ?>
            </a>
        </div>
    </div>
</div>
</header>

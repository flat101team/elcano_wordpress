<?php
/**
 * Template part for displaying single post content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */
$classes = get_body_class();
$captionText = get_the_post_thumbnail_caption();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header onecol onecol--medium">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        <!-- image fallback -->
		<div class="post-thumbnail fullwidth">
            <?php if ( has_post_thumbnail() ) {
                the_post_thumbnail();
            } else { ?>
                <?php if (in_array('analysis-template-default', $classes) || in_array('single-analysis', $classes)): ?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/analisis.jpg" alt="<?php the_title(); ?>" />
                <?php elseif (in_array('commentary-template-default', $classes) || in_array('single-commentary', $classes)): ?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/comentario.jpg" alt="<?php the_title(); ?>" />
                <?php elseif (in_array('ciber-template-default', $classes) || in_array('single-ciber', $classes)): ?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/ciber.jpg" alt="<?php the_title(); ?>" />
                <?php elseif (in_array('magazine-template-default', $classes) || in_array('single-magazine', $classes)): ?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/revista.jpg" alt="<?php the_title(); ?>" />
                <?php elseif (in_array('poll-template-default', $classes) || in_array('single-poll', $classes)): ?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/encuesta.jpg" alt="<?php the_title(); ?>" />
                <?php elseif (in_array('report-template-default', $classes) || in_array('single-report', $classes)): ?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/informe.jpg" alt="<?php the_title(); ?>" />
                <?php elseif (in_array('post-template-default', $classes) || in_array('single-post', $classes)): ?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/post.jpg" alt="<?php the_title(); ?>" />
                <?php endif; ?>
            <?php } ?>
        </div>
        <figcaption class="wp-caption-text thumbnail-caption"><?php echo wp_kses_post( wp_get_attachment_caption( get_post_thumbnail_id() ) ); ?></figcaption>

        <div class="entry-meta">
			<?php
			// The authors (from CPT biography).
			get_template_part( 'template-parts/section/authors' );

			// Published date.
			elcano_posted_on();
			?>
		</div>
		<div class="post-category"><?php elcano_primary_category(); ?></div>
	</header><!-- .entry-header -->

	<div class="columns">
		<div class="entry-content maincol maincol--medium">
			<?php
			the_content();

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elcano' ),
					'after'  => '</div>',
				)
			);
			?>
		</div><!-- .entry-content -->

		<aside class="aside--small aside--left">
			<?php
			// Share links.
			get_template_part( 'template-parts/section/share' );

			// Post downloads.
			get_template_part( 'template-parts/section/downloads' );

			// Newsletter link.
			get_template_part( 'template-parts/section/newsletter' );
			?>
		</aside>
	</div>

	<?php get_template_part( 'template-parts/entry-footer', '', array( 'classes' => 'onecol onecol--medium' ) ); ?>

</article><!-- #post-<?php the_ID(); ?> -->

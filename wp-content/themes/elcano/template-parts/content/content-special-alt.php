<?php
/**
 * Template part for displaying single special content (alt header)
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header fullwidth special-header-alt">
		<div class="post-info contentwidth">
			<div class="post-inner">
				<?php
				the_title( '<h1 class="entry-title">', '</h1>' );

				if ( apply_filters( 'wpml_element_has_translations', null, get_the_ID(), 'special' ) ) {

					$languages   = apply_filters( 'wpml_active_languages', null, 'skip_missing=1' );
					$translation = current( wp_list_filter( $languages, array( 'active' => 0 ) ) );

					printf(
						'<a class="btn" href="%s" rel="alternate" hreflang="%s" lang="%s">%s</a>',
						esc_url( $translation['url'] ),
						esc_attr( $translation['default_locale'] ),
						esc_attr( $translation['code'] ),
						_x( 'También disponible en Español', 'translation', 'elcano' )
					);

				}
				?>
			</div>
		</div>
		<div class="post-thumbnail"><?php the_post_thumbnail(); ?></div>
	</header><!-- .entry-header -->

	<div class="columns">
		<div class="entry-content maincol maincol--medium">
			<?php
			the_content();

			// Link to Full Document.
			$pdf_file = get_field( 'pdf_file' );
			if ( $pdf_file ) {
				printf(
					'<p><a href="%s" class="btn">%s</a></p>',
					esc_url( $pdf_file['url'] ),
					__( 'Read the Full Document', 'elcano' )
				);
			};

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elcano' ),
					'after'  => '</div>',
				)
			);
			?>
		</div><!-- .entry-content -->

		<aside class="aside--left">
			<?php
			// Share links.
			get_template_part( 'template-parts/section/share' );

			// Post downloads.
			get_template_part( 'template-parts/section/downloads' );

			// Newsletter link.
			get_template_part( 'template-parts/section/newsletter' );
			?>
		</aside>
	</div>

	<footer class="entry-footer">
	<?php
	// Post tags.
	elcano_post_tags();
	?>
</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->

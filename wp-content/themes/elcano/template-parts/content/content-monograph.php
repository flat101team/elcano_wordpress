<?php
/**
 * Template part for displaying single monograph content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */


$pdf_file = get_field( 'pdf_file' );
$classes = get_body_class();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'monograph' ); ?>>
	<header class="entry-header">
		<div class="header-overlay header-overlay--links">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<div class="post-thumbnail">
                <?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail();
                } else { ?>
                    <?php if (in_array('report-template-default', $classes) || in_array('single-report', $classes)): ?>
                        <img src="<?php bloginfo('template_directory'); ?>/images/informe.jpg" alt="<?php the_title(); ?>" />
                    <?php endif; ?>
                <?php } ?>
            </div>
			<div class="post-category"><?php elcano_primary_category(); ?></div>
			<div class="post-authors"><?php elcano_authors(); ?></div>
			<?php elcano_posted_on(); ?>
		</div>

		<div class="header-links">
			<a href="#<?php echo _x( 'see-study', 'Document PDF anchor', 'elcano' ); ?>" class="icon-eye"><?php _e( 'See Study', 'elcano' ); ?></a>
			<a href="<?php echo esc_url( $pdf_file['url'] ); ?>" download class="icon-download"><?php printf( '%s: %s %s', __( 'Download', 'elcano' ), $pdf_file['subtype'], size_format( $pdf_file['filesize'] ) ); ?></a>
		</div>
	</header><!-- .entry-header -->

	<div class="columns">
		<div class="entry-content maincol maincol--tiny">
			<?php
			the_content();

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elcano' ),
					'after'  => '</div>',
				)
			);
			?>
		</div><!-- .entry-content -->
		<aside class="aside--left">
			<?php
			// Share links.
			get_template_part( 'template-parts/section/share' );

			// Post downloads.
			get_template_part( 'template-parts/section/downloads' );

			// Newsletter link.
			get_template_part( 'template-parts/section/newsletter' );
			?>
		</aside>
	</div>

	<?php get_template_part( 'template-parts/entry-footer', '', array( 'classes' => 'onecol onecol--medium onecol--left' ) ); ?>

</article><!-- #post-<?php the_ID(); ?> -->

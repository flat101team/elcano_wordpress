<?php
/**
 * Template part for displaying single biography content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

// TODO: twitter compliance LOPD
// TODO: twitter smart load script

$group                = current( wp_get_post_terms( get_the_ID(), 'bio_group' ) );
$posts_by_author      = elcano_get_posts_by_author( get_the_ID(), true );
$activities_by_author = elcano_get_activities_by_author();

$author_links = array_filter(
	array(
		'facebook' => get_field( 'facebook' ),
		'twitter'  => get_field( 'twitter' ),
		'email'    => get_field( 'email' ),
		'web'      => get_field( 'web' ),
	)
);

// Twitter of Elcano
$wpseo_social = get_option( 'wpseo_social', array() );
$twitter_user = $wpseo_social['twitter_site'] ?? 'rielcano';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header vcard onecol onecol--to-left">
		<h1 class="fn n"><?php the_title(); ?></h1>
		<div class="author-photo">
			<?php
			if ( has_post_thumbnail() ) {
				the_post_thumbnail(
					get_the_ID(),
					array( 270, 270 ),
					array(
						'class'   => 'photo',
						'loading' => false,
					)
				);
			} else {
				?>
				<img src="<?php bloginfo( 'template_directory' ); ?>/images/bio.jpg" alt="<?php the_title(); ?>" />
			<?php } ?>
		</div>
		<div class="title"><?php echo get_field( 'position' ) ? get_field( 'position' ) : ( $group ? $group->name : '' ); ?></div>
		<div class="info">
			<?php if ( $posts_by_author ) : ?>
				<div class="articles"><?php printf( _n( '%d article published', '%d articles published', $posts_by_author, 'elcano' ), $posts_by_author ); ?></div>
			<?php endif; ?>
			<?php if ( count( $author_links ) ) : ?>
				<div class="links">
					<span><?php _e( 'Follow', 'elcano' ); ?>:</span>
					<ul class="social-links follow-links">
						<?php if ( isset( $author_links['facebook'] ) ) : ?>
							<li><a aria-label="<?php printf( __( 'Follow on %s', 'elcano' ), 'Facebook' ); ?>" href="<?php echo esc_url( $author_links['facebook'] ); ?>" rel="external nofollow noopener noreferrer"><span class="icon-facebook"></span></a></li>
						<?php endif; ?>
						<?php if ( isset( $author_links['twitter'] ) ) : ?>
							<li><a aria-label="<?php printf( __( 'Follow on %s', 'elcano' ), 'Twitter' ); ?>" href="https://twitter.com/<?php echo esc_attr( $author_links['twitter'] ); ?>" rel="external nofollow noopener noreferrer">
                                    <span>
                                        <svg width="14" height="14" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0_875_997)">
                                        <mask id="mask0_875_997" style="mask-type:luminance" maskUnits="userSpaceOnUse" x="0" y="0" width="28" height="28">
                                            <path d="M0 0H28V28H0V0Z" fill="white"/>
                                        </mask>
                                        <g mask="url(#mask0_875_997)">
                                            <path d="M22.05 1.31201H26.344L16.964 12.06L28 26.688H19.36L12.588 17.818L4.848 26.688H0.55L10.582 15.188L0 1.31401H8.86L14.972 9.42001L22.05 1.31201ZM20.54 24.112H22.92L7.56 3.75401H5.008L20.54 24.112Z" fill="#BB2521"/>
                                        </g>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_875_997">
                                            <rect width="28" height="28" fill="white"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                                    </span>
                                </a>
                            </li>
						<?php endif; ?>
						<?php if ( isset( $author_links['email'] ) ) : ?>
							<li><a class="email" aria-label="<?php _e( 'E-mail address', 'elcano' ); ?>" href="mailto:<?php echo esc_attr( $author_links['email'] ); ?>" rel="external nofollow noopener noreferrer"><span class="icon-email"></span></a></li>
						<?php endif; ?>
						<?php if ( isset( $author_links['web'] ) ) : ?>
							<li><a class="url" aria-label="<?php _e( 'Web page', 'elcano' ); ?>" href="<?php echo esc_url( $author_links['web'] ); ?>" rel="external nofollow noopener noreferrer"><span class="icon-link"></span></a></li>
						<?php endif; ?>
					</ul>
				</div>
			<?php endif; ?>
		</div>
	</header><!-- .entry-header -->

	<div class="columns">
		<div class="entry-content maincol">

			<?php the_content(); ?>

			<?php if ( count( $activities_by_author ) ) : ?>
				<h2 class="baseline"><?php _e( 'Events', 'elcano' ); ?></h2>
				<ul>
					<?php
					foreach ( $activities_by_author as $activity ) :
						$date_start = new DateTime( get_field( 'date_start', $activity->ID, false ) );
						printf( '<li>%s <span class="has-gray-soft-color">(%d)</span></li>', esc_html( $activity->post_title ), $date_start->format( 'Y' ) );
					endforeach;
					?>
				</ul>
				<p class="has-text-align-center">
					<a href="<?php echo esc_url( home_url( '/?s=&types=activity&authors=' . get_the_ID() ) ); ?>" class="btn"><?php _e( 'View all events', 'elcano' ); ?></a>
				</p>
			<?php endif; ?>
		</div><!-- .entry-content -->

		<aside>
			<section class="widget twitter-feed">
				<p class="widget-title widget-title--big"><?php _e( 'Elcano on Twitter', 'elcano' ); ?></p>
				<a class="twitter-timeline"
					data-width="100%"
					data-height="min(600px, 75vh)"
					data-dnt="true"
					data-chrome="noheader nofooter noborders"
					href="https://twitter.com/<?php echo esc_attr( $twitter_user ); ?>">Tweets de Real Instituto Elcano</a>
				<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
			</section>
		</aside>
	</div>

	<?php if ( $posts_by_author ) : ?>
		<section>
			<h2 class="baseline"><?php _e( 'Latest publications', 'elcano' ); ?></h2>
			<?php
			$recent_posts = wp_get_recent_posts(
				array(
					'post_type'        => array_merge( array( 'post' ), elcano_current_post_type( 'web' ) ),
					'post_status'      => 'publish',
					'numberposts'      => 9,
					'meta_query'       => array(
						array(
							'key'     => 'authors',                // Name of custom field
							'value'   => '"' . get_the_ID() . '"', // Matches exactly "ID" (with quotes)
							'compare' => 'LIKE',
						),
					),
					'suppress_filters' => false,
				),
				OBJECT
			);

			elcano_post_slider( $recent_posts );
			?>

			<?php if ( $posts_by_author > count($recent_posts) ) : ?>
				<p class="all-articles"><a href="<?php echo esc_url( home_url( '/?s&authors=' . get_the_ID() ) ); ?>" class="btn"><?php _e( 'View all publications', 'elcano' ); ?></a></p>
			<?php endif; ?>
		</section>
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for displaying single newsletter content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title h1-alt baseline">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php elcano_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		if ( has_block( 'core/code' ) || has_block( 'core/html' ) ) {

			$replaces = array(
				'<!-- wp:code -->'                  => '',
				'<!-- /wp:code -->'                 => '',
				'<!-- wp:html -->'                  => '',
				'<!-- /wp:html -->'                 => '',
				'<pre class="wp-block-code"><code>' => '',
				'</code></pre>'                     => '',
				'&lt;'                              => '<',
				'"'                                 => '&quot;',
			);

			$content = str_replace( array_keys( $replaces ), $replaces, $post->post_content );

			echo '<iframe class="code-iframe" srcdoc="' . $content . '"></iframe>';

		} else {

			the_content();

		}
		?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->

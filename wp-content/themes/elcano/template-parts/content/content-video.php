<?php
/**
 * Template part for displaying single video content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

// TODO: structured data video

// Enqueue modal script
elcano_append_dependency( 'elcano-theme', 'elcano-modal' );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="header-overlay" data-video="<?php echo esc_url( get_field( 'video' ) ); ?>">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<div class="post-thumbnail">
                <?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail();
                } else { ?>
                    <img src="<?php bloginfo('template_directory'); ?>/images/video.jpg" loading="lazy" alt="<?php the_title(); ?>" />
                <?php } ?>
            </div>
			<div class="post-category"><?php elcano_primary_category(); ?></div>
			<div class="entry-meta">
				<?php
				// The authors (from CPT biography).
				get_template_part( 'template-parts/section/authors' );

				// Published date.
				elcano_posted_on();
				?>
			</div>
		</div>

		<div class="entry-meta onecol onecol--small onecol--left-small">
			<?php
			// The authors (from CPT biography).
			get_template_part( 'template-parts/section/authors' );

			// Published date.
			elcano_posted_on();
			?>
		</div>
	</header><!-- .entry-header -->

	<div class="columns">
		<div class="entry-content maincol maincol--small">
			<?php
			the_content();

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elcano' ),
					'after'  => '</div>',
				)
			);
			?>
		</div><!-- .entry-content -->

		<aside class="aside--small aside--left">
			<?php
			// Share links.
			get_template_part( 'template-parts/section/share' );

			// Post downloads.
			get_template_part( 'template-parts/section/downloads' );

			// Newsletter link.
			get_template_part( 'template-parts/section/newsletter' );
			?>
		</aside>

		<aside>
			<section class="widget last-posts">
				<p class="widget-title widget-title--big"><?php _e( 'Latest Publications', 'elcano' ); ?></p>
				<?php
				$recent_posts = wp_get_recent_posts(
					array(
						'post__not_in'     => array( get_the_ID() ),
						'post_type'        => elcano_current_post_type( 'web' ), // TODO: only 'video' ???
						'post_status'      => 'publish',
						'numberposts'      => 3,
						'suppress_filters' => false,
					),
					OBJECT
				);

				if ( is_array( $recent_posts ) ) {
					foreach ( $recent_posts as $recent_post ) {
						printf(
							'<article class="post-related"><a href="%s" rel="bookmark">%s</a></article>',
							esc_url( get_the_permalink( $recent_post ) ),
							sprintf( '<p class="post-title">%s</p>', get_the_title( $recent_post ) ),
						);
					}
				}
				?>
			</section>
		</aside>

	</div><!-- .columns -->

	<?php get_template_part( 'template-parts/entry-footer', '', array( 'classes' => 'onecol onecol--small onecol--left-small' ) ); ?>

</article><!-- #post-<?php the_ID(); ?> -->

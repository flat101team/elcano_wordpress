<?php
/**
 * Template part for displaying single podcast content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

// TODO: podcast source
//die('as');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php the_title( '<h1 class="entry-title onecol onecol--to-left">', '</h1>' ); ?>

	<div class="columns">
		<div class="maincol">
			<header class="entry-header">
				<div class="post-thumbnail">
                    <?php if ( has_post_thumbnail() ) {
                        the_post_thumbnail();
                    } else { ?>
                        <?php if (get_post_type() === 'podcast'): ?>
                            <img src="<?php bloginfo('template_directory'); ?>/images/podcast.jpg" loading="lazy" alt="<?php the_title(); ?>" />
                        <?php endif; ?>
                    <?php } ?>
                </div>
				<div class="post-podcast">
				<?php
				echo wp_oembed_get(
					get_field( 'podcast' ),
					array(
						'width'  => 837,
						'height' => 100,
					)
				);
				?>
				</div>
				<div class="entry-meta">
					<div class="post-category"><?php elcano_primary_category(); ?></div>
					<?php
					// The authors (from CPT biography).
					get_template_part( 'template-parts/section/authors' );

					// Published date.
					elcano_posted_on();
					?>
				</div>
			</header><!-- .entry-header -->

			<div class="entry-content ">
				<?php
				the_content();

				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elcano' ),
						'after'  => '</div>',
					)
				);
				?>
			</div><!-- .entry-content -->

		</div><!-- .maincol -->

		<aside>
			<?php
			// Share links.
			get_template_part( 'template-parts/section/share' );

			// Post downloads.
			get_template_part( 'template-parts/section/downloads' );

			// Newsletter link.
			get_template_part( 'template-parts/section/newsletter' );
			?>
		</aside>
	</div>

	<?php get_template_part( 'template-parts/entry-footer', '', array( 'classes' => 'onecol onecol--to-left' ) ); ?>

</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for displaying single pdf template content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */


$pdf_file = get_field( 'pdf_file' );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'template-pdf' ); ?>>
	<header class="entry-header">
		<div class="header-overlay header-overlay--links">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<div class="post-thumbnail"><?php the_post_thumbnail(); ?></div>
			<div class="post-category"><?php elcano_primary_category(); ?></div>
			<div class="entry-meta">
				<?php
				// The authors (from CPT biography).
				get_template_part( 'template-parts/section/authors' );

				// Published date.
				elcano_posted_on();
				?>
			</div>
		</div>

		<div class="header-links">
			<a href="#<?php echo _x( 'see-study', 'Document PDF anchor', 'elcano' ); ?>" class="icon-eye"><?php _e( 'See Study', 'elcano' ); ?></a>
			<a href="<?php echo esc_url( $pdf_file['url'] ); ?>" download class="icon-download"><?php printf( '%s: %s %s', __( 'Download', 'elcano' ), $pdf_file['subtype'], size_format( $pdf_file['filesize'] ) ); ?></a>
		</div>

		<div class="entry-meta onecol onecol--left">
			<?php
			// The authors (from CPT biography).
			get_template_part( 'template-parts/section/authors' );

			// Published date.
			elcano_posted_on();
			?>
		</div>
	</header><!-- .entry-header -->


	<div class="columns">
		<div class="entry-content maincol maincol--tiny">
			<?php
			the_content();

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elcano' ),
					'after'  => '</div>',
				)
			);
			?>
		</div><!-- .entry-content -->
		<aside class="aside--left">
			<?php
			// Share links.
			get_template_part( 'template-parts/section/share' );

			// Post downloads.
			get_template_part( 'template-parts/section/downloads' );

			// Newsletter link.
			get_template_part( 'template-parts/section/newsletter' );
			?>
		</aside>
	</div>

	<?php get_template_part( 'template-parts/entry-footer', '', array( 'classes' => 'onecol onecol--medium' ) ); ?>

</article><!-- #post-<?php the_ID(); ?> -->

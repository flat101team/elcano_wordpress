<?php
/**
 * Template part for displaying single policy paper content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */


// Ensure heading IDs for TOC anchors
add_filter( 'lwptoc_need_processing_headings', '__return_true' );
$classes = get_body_class();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'long_post' ); ?>>
	<div class="columns">
		<header class="entry-header maincol">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<div class="post-thumbnail">
                <?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail();
                } else { ?>
                    <?php if (in_array('policy_paper-template-default', $classes) || in_array('single-policy_paper', $classes)): ?>
                        <img src="<?php bloginfo('template_directory'); ?>/images/policy.jpg" alt="<?php the_title(); ?>" />
                    <?php elseif (in_array('work_document-template-default', $classes) || in_array('single-work_document', $classes)): ?>
                        <img src="<?php bloginfo('template_directory'); ?>/images/documento.jpg" alt="<?php the_title(); ?>" />
                    <?php endif; ?>
                <?php } ?>
            </div>
			<div class="post-group">
				<div class="post-category"><?php elcano_primary_category(); ?></div>
				<div class="post-type"><?php echo elcano_post_type_label(); ?></div>
			</div>
			<div class="entry-meta">
				<?php
				// The authors (from CPT biography).
				get_template_part( 'template-parts/section/authors' );

				// Published date.
				elcano_posted_on();
				?>
			</div>
		</header><!-- .entry-header -->

		<div class="aside aside--header">
<!--			--><?php
/*			// The Most Read.
			get_template_part( 'template-parts/section/most-read' );
			*/?>
            <section class="widget last-posts">
                <p class="widget-title widget-title--big"><?php _e( 'Latest Publications', 'elcano' ); ?></p>
                <?php
                $recent_posts = wp_get_recent_posts(
                    array(
                        'post__not_in'     => array( get_the_ID() ),
                        'post_type'        => elcano_current_post_type(),
                        'post_status'      => 'publish',
                        'numberposts'      => 3,
                        'suppress_filters' => false,
                    ),
                    OBJECT
                );

                if ( is_array( $recent_posts ) ) {
                    foreach ( $recent_posts as $recent_post ) {
                        printf(
                            '<article class="post-related"><a href="%s" rel="bookmark">%s</a></article>',
                            esc_url( get_the_permalink( $recent_post ) ),
                            sprintf( '<p class="post-title">%s</p>', get_the_title( $recent_post ) ),
                        );
                    }
                }
                ?>
            </section>
		</div>
	</div>

	<div class="columns">

		<?php elcano_aside_toc(); ?>

		<div class="entry-content maincol maincol--tiny">
			<?php
			the_content();

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elcano' ),
					'after'  => '</div>',
				)
			);
			?>
		</div>

		<aside class="aside--content">
			<?php
			// Share links.
			get_template_part( 'template-parts/section/share' );

			// Post downloads.
			get_template_part( 'template-parts/section/downloads' );

			// Newsletter link.
			get_template_part( 'template-parts/section/newsletter' );
			?>
		</aside>
	</div>

	<footer class="entry-footer">
		<div class="onecol onecol--tiny onecol--left">
			<?php elcano_post_tags(); ?>
		</div>
		<div class="onecol">
			<?php
			// Related posts.
			elcano_related_posts();

			echo '<div class="post-share">';

			// Like button.
			elcano_love_me();

			// Share links.
			elcano_share();

			echo '</div>';

			// Post navigation.
			the_post_navigation(
				array(
					'prev_text'    => '<span class="nav-subtitle">' . esc_html__( 'Previous', 'elcano' ) . '</span> <span class="nav-title">%title</span>',
					'next_text'    => '<span class="nav-subtitle">' . esc_html__( 'Next', 'elcano' ) . '</span> <span class="nav-title">%title</span>',
					'in_same_term' => true,
				)
			);

			// Post authors (from CPT biography).
			get_template_part( 'template-parts/section/authors', 'bio' );
			?>
		</div>
	</footer><!-- .entry-footer -->

	<!-- </div> -->
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for displaying single special content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$yoast_meta_description = YoastSEO()->meta->for_current_page()->description;
$page_home_id   = apply_filters( 'wpml_object_id', get_the_ID(), 'page', false, 'en' );
$url = get_permalink($page_home_id);
$current_lang = apply_filters( 'wpml_current_language', null);
$languages = apply_filters( 'wpml_active_languages', null, 'skip_missing=0' );
$tagsOnPost = wp_get_post_tags( get_the_ID() );
$pdf_file = get_field( 'pdf_file' );
$special_category = get_field('special_category');
$authors = get_field( 'authors' );

if( !empty( $special_category ) ) {
	$query_args = array(
		'post_type' => 'any',
		'posts_per_page' => 4,
		'orderby' => 'date',
		'order' => 'DESC',
		'post_status' => 'publish',
		'tax_query' => array(
			array(
				'taxonomy' => 'specials_category',
				'field' => 'term_id',
				'terms' => $special_category->term_id
			)
		)
	);

	$query = new WP_Query( $query_args );

}

if ( $current_lang === 'es' ) {
	$lang = $languages['en'];
} else $lang = $languages['es'];

ob_start();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> class="single-special">
	<header class="entry-header onecol onecol--medium">
		<div class="post-thumbnail widthfull">
			<?php the_post_thumbnail(); ?>
		</div>

	</header><!-- .entry-header -->

	<!-- wp:columns {"style":{"spacing":{"blockGap":{"left":"32px"}}}} -->
	<div class="wp-block-columns"><!-- wp:column {"className": "special-title", "width":"75%"} -->
		<div class="wp-block-column special-title" style="flex-basis:75%">
			<?php if( !empty( $tagsOnPost ) ): ?>
				<!-- wp:group {"layout":{"type":"constrained"},"className":"special-tags"} -->
				<div class="wp-block-group special-tags">
					<?php foreach ( $tagsOnPost as $tag ): ?>
						<span class="tag-style alignright">
							<a class="special-tag" href="<?php echo esc_attr( get_term_link( $tag->term_id) ) ?>"><?php echo esc_html( $tag->name ) ?></a>
						</span>
					<?php endforeach; ?>
				</div>
				<!-- /wp:group -->
			<?php endif; ?>

		   <!-- <p>Puede consultar la versión en inglés en <a href="<?php /*echo elcano_fix_url( $lang['url'], $lang['code'] ) */?>"><?php /*echo get_the_title($page_home_id); */?></a>.</p>-->
			<!-- wp:heading {"className":"entry-title", "level":1} -->
				<h1 class="wp-block-heading entry-title"><?php echo esc_html( get_the_title() )?></h1>
			<!-- /wp:heading -->
		   <?php if( !empty( $yoast_meta_description ) ): ?>
				<!-- wp:paragraph {"className":"entry-description"} -->
					<p class="entry-description"><?php echo $yoast_meta_description ?></p>
				<!-- /wp:paragraph -->
			<?php endif; ?>
			<!-- wp:paragraph {"className":"special-author"} -->
				<p class="special-author"><b> <?php echo elcano_authors() ?> </b> // <?php echo get_the_date() ?> </p>
			<!-- /wp:paragraph -->
		</div><!-- /wp:column -->

		<!-- wp:column {"width":"25%"} -->
		<div class="wp-block-column" style="flex-basis:25%">
		<?php
			// Share links.
			get_template_part( 'template-parts/section/share' );

			// Post downloads.
			get_template_part( 'template-parts/section/downloads' );

			// Newsletter link.
			get_template_part( 'template-parts/section/newsletter' );
		?>
		</div><!-- /wp:column -->
	</div><!-- /wp:columns -->

	<!-- wp:columns -->
	<div class="wp-block-columns"><!-- wp:column {"width":"25%"} -->
		<div class="wp-block-column special-toc" style="flex-basis:25%">
			<?php  echo do_shortcode( '[lwptoc]' );?>
		</div><!-- /wp:column -->

		<!-- wp:column {"width":"75%", "className":"entry-content"} -->
		<div class="wp-block-column entry-content" style="flex-basis:75%">

			<!-- wp:group {"layout":{"type":"constrained"},"className":"content"} -->
			<div class="wp-block-group content">

				<?php the_content(); ?>
			</div>
			<!-- /wp:group -->

			<?php if( $pdf_file ): ?>
				<!-- wp:group {"layout":{"type":"constrained"},"className":"full-document"} -->
					<div class="wp-block-group full-document">
						<!-- wp:buttons -->
						<div class="wp-block-buttons">
							<!-- wp:button -->
							<div class="wp-block-button">
								<a class="wp-block-button__link wp-element-button" href="<?php echo esc_url( $pdf_file['url'] ) ?>"><?php esc_html_e( 'Read the Full Document', 'elcano' )?></a>
							</div>
							<!-- /wp:button -->
						</div>
					<!-- /wp:buttons -->
				</div>
				<!-- /wp:group -->
				<?php endif; ?>

			<?php if( !empty( $tagsOnPost ) ): ?>
				<!-- wp:group {"layout":{"type":"constrained"},"className":"special-tags"} -->
				<div class="wp-block-group special-tags">
					<?php foreach ( $tagsOnPost as $tag ): ?>
						<span class="tag-style alignright">
							<a class="special-tag" href="<?php echo esc_attr( get_term_link( $tag->term_id) ) ?>"><?php echo esc_html( $tag->name ) ?></a>
						</span>
					<?php endforeach; ?>
				</div>
				<!-- /wp:group -->
			<?php endif; ?>

			<!-- wp:columns {"className":"special-share"} -->
			<div class="wp-block-columns special-share">
				<!-- wp:column {"width":"50%"} -->
				<div class="wp-block-column" style="flex-basis:50%">
					<?php elcano_love_me() ?>
				</div>
				<!-- /wp:column -->
				<!-- wp:column {"width":"50%"} -->
				<div class="wp-block-column" style="flex-basis:50%">
					<?php get_template_part( 'template-parts/section/share' ); ?>
				</div><!-- /wp:column -->
			</div><!-- /wp:columns -->
		</div><!-- /wp:column -->
	</div><!-- /wp:columns -->

	<div class="columns">
		<div class="maincol">
			<?php

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elcano' ),
					'after'  => '</div>',
				)
			);
			?>
		</div><!-- .entry-content -->

	</div>

	<?php if( isset( $query ) && $query->have_posts() ): ?>
		<?php elcano_append_dependency( 'elcano-theme', 'elcano-slider' ); ?>
		<footer class="entry-footer">
			<!-- wp:heading {"level":3} -->
			<h3><?php esc_html_e( 'Other relevant articles', 'elcano' )?></h3>
			<!-- /wp:heading -->
			<!-- wp:group {"layout":{"type":"constrained"},"className":"the-archive the-archive--grid"} -->
			<div class="wp-block-group the-archive the-archive--grid">
				<?php while( $query->have_posts() ): ?>
					<?php $query->the_post(); ?>
					<?php get_template_part( 'template-parts/archive/publications', get_post_type() ); ?>
				<?php endwhile; ?>
			</div>
			<!-- /wp:group -->
		</footer><!-- .entry-footer -->
	<?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->

<?php echo do_blocks( ob_get_clean() ); ?>

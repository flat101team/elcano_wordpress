<?php
/**
 * Template part for displaying single job
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$the_page = get_field( 'page_for_job', 'option' );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="columns">
		<div class="maincol">
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title h1-alt">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php if ( get_field( 'job_summary' ) ) : ?>
					<p class="job-summary"><?php the_field( 'job_summary' ); ?></p>
				<?php endif; ?>
				<p class="job-reference"><?php _e( 'Reference', 'elcano' ); ?>: <?php echo get_field( 'job_reference' ) ?: '--'; ?></p>
				<?php if ( get_field( 'job_pdf' ) ) : ?>
					<p><a href="<?php the_field( 'job_pdf' ); ?>" class="job-pdf"><?php _e( 'Download PDF', 'elcano' ); ?></a></p>
				<?php endif; ?>

				<?php
				the_content();

				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elcano' ),
						'after'  => '</div>',
					)
				);
				?>
			</div><!-- .entry-content -->
		</div>

		<aside>
			<section class="widget last-posts">
				<p class="widget-title widget-title--big"><?php _e( 'Other Offers', 'elcano' ); ?></p>
				<?php
				$recent_posts = wp_get_recent_posts(
					array(
						'post__not_in'     => array( get_the_ID() ),
						'post_type'        => 'job',
						'post_status'      => 'publish',
						'numberposts'      => 3,
						'suppress_filters' => false,
					),
					OBJECT
				);

				if ( is_array( $recent_posts ) ) {
					foreach ( $recent_posts as $recent_post ) {
						printf(
							'<article class="post-related"><a href="%s" rel="bookmark">%s</a></article>',
							esc_url( get_the_permalink( $recent_post ) ),
							sprintf( '<p class="post-title">%s</p>', get_the_title( $recent_post ) ),
						);
					}
				}
				?>
			</section>

			<?php
			// Contact section.
			get_template_part( 'template-parts/section/contact', '', array( 'post_id' => $the_page ) );
			?>
		</aside>
	</div>

	<div class="more-jobs">
		<a href="<?php echo get_post_type_archive_link( 'job' ); ?>" class="btn"><?php _e( 'See other job offers', 'elcano' ); ?></a>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for displaying single project content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$date_start = new DateTime( get_field( 'date_start', false, false ), wp_timezone() );
$date_end   = new DateTime( get_field( 'date_end', false, false ), wp_timezone() );
$same_day   = $date_start->format( 'dmY' ) === $date_end->format( 'dmY' );
$same_month = $date_start->format( 'mY' ) === $date_end->format( 'mY' );

// Note: dates use timezone ES (Spain)
/*$add_to_cal = add_query_arg(
	array(
		'action'   => 'TEMPLATE',
		'text'     => urlencode( get_the_title() ),
		'details'  => '',
		'location' => '',
		'dates'    => urlencode( sprintf( '%s/%s', $date_start->format( 'Ymd\\THi00\\E\\S' ), $date_end->format( 'Ymd\\THi00\\E\\S' ) ) ),
	),
	'https://www.google.com/calendar/render'
);

$registration = get_field( 'registration' );*/

$classes = get_body_class();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="header-overlay header-overlay--links">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
<!--			<div class="project-date">
				<span class="day"><?php /*echo $same_day ? $date_start->format( 'j' ) : sprintf( '%s-%s', $date_start->format( 'j' ), $date_end->format( 'j' ) ); */?></span>
				<span class="month"><?php /*echo $same_month ? wp_date( 'M Y', $date_start->getTimestamp() ) : sprintf( '%s-%s', wp_date( 'M', $date_start->getTimestamp() ), wp_date( 'M Y', $date_end->getTimestamp() ) ); */?></span>
				<span class="time"><?php /*echo sprintf( '%s-%s', $date_start->format( 'G:i' ), $date_end->format( 'G:i' ) ); */?></span>
			</div>-->
			<div class="post-thumbnail">
                <?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail();
                } else { ?>
                    <?php if (in_array('project-template-default', $classes) || in_array('single-project', $classes)): ?>
                        <img src="<?php bloginfo('template_directory'); ?>/images/proyecto.png" alt="<?php the_title(); ?>" />
                    <?php endif; ?>
                <?php } ?>
            </div>
			<div class="post-category"><?php elcano_primary_category(); ?></div>
			<div class="post-authors"><?php printf( '<span class="byline"><span class="fn">%s</span></span>', get_field( 'organizer' ) ); ?></div>
			<?php /*elcano_posted_on(); */?>
		</div>

<!--		<div class="header-links">
			<?php /*if ( $registration ) : */?>
				<a href="<?php /*echo esc_url( $registration ); */?>" class="icon-email" rel="external nofollow noopener noreferrer" target="_blank"><?php /*_e( 'Subscribe to invitations', 'elcano' ); */?></a>
			<?php /*endif; */?>
			<?php /*if ( have_rows( 'sections' ) ) : */?>
				<a href="#<?php /*_ex( 'project-program', 'Project program anchor', 'elcano' ); */?>" class="icon-paste"><?php /*_e( 'See project program', 'elcano' ); */?></a>
			<?php /*elseif ( get_field( 'program_pdf' ) ) : */?>
				<a href="<?php /*echo esc_url( get_field( 'program_pdf' ) ); */?>" class="icon-download"><?php /*_e( 'See project program', 'elcano' ); */?></a>
			<?php /*endif; */?>
			<a href="<?php /*echo esc_url( $add_to_cal ); */?>" class="icon-cal-alt" rel="external nofollow noopener noreferrer" target="_blank"><?php /*_e( 'Add project to calendar', 'elcano' ); */?></a>
		</div>-->
	</header><!-- .entry-header  -->

	<div class="columns">
		<div class="maincol maincol--small">
			<div class="entry-content ">
				<?php
				the_content();

				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elcano' ),
						'after'  => '</div>',
					)
				);
				?>
			</div><!-- .entry-content -->
		</div>

		<aside class="aside--left">
			<?php
			// Share links.
			get_template_part( 'template-parts/section/share' );

			// Post downloads.
			get_template_part( 'template-parts/section/downloads' );

			// Newsletter link.
			get_template_part( 'template-parts/section/newsletter' );
			?>

			<section class="widget last-posts">
				<p class="widget-title widget-title--big"><?php _e( 'Latest related articles', 'elcano' ); ?></p>
				<?php
				// Web content with same categories.
				$recent_posts = wp_get_recent_posts(
					array(
						'post_type'        => 'project',
						'post__not_in'     => array( get_the_ID() ),
						'category__in'     => wp_list_pluck( get_the_category(), 'term_id' ),
						'post_status'      => 'publish',
						'numberposts'      => 3,
						'suppress_filters' => false,
					),
					OBJECT
				);

				if ( is_array( $recent_posts ) ) {
					foreach ( $recent_posts as $recent_post ) {
						printf(
							'<article class="post-related"><a href="%s" rel="bookmark">%s</a></article>',
							esc_url( get_the_permalink( $recent_post ) ),
							sprintf( '<p class="post-title">%s</p>', get_the_title( $recent_post ) ),
						);
					}
				}
				?>
			</section>
		</aside>
	</div>

	<?php if ( have_rows( 'sections' ) ) : ?>
	<div id="<?php _ex( 'project-program', 'Project program anchor', 'elcano' ); ?>">
		<h2 class="baseline"><?php _e( 'Project Program', 'elcano' ); ?></h2>
		<div class="project-program">
			<div class="program">
				<?php
				while ( have_rows( 'sections' ) ) {
					the_row();

					if ( get_row_layout() === 'header' ) {
						printf( '<h3>%s</h3>', get_sub_field( 'title' ) );
					} elseif ( get_row_layout() === 'text' ) {
						printf( '<div class="info">%s</div>', get_sub_field( 'text' ) );
					} elseif ( get_row_layout() === 'participants' ) {
						$participants = get_sub_field( 'participants' );

						echo '<div class="participants">';
						foreach ( $participants as $participant ) :
							printf(
								'<div class="participant">%s %s %s</div>',
								'<span class="name">' . $participant['name'] . '</span>',
								$participant['role'] ? '<span class="role">(' . $participant['role'] . ')</span>' : '',
								$participant['position'] ? '<span class="position">' . $participant['position'] . '</span>' : '',
							);
						endforeach;
						echo '</div>';
					}
				}
				?>
			</div>

			<?php if ( get_field( 'program_pdf' ) ) : ?>
				<div class="program-pdf">
					<p><?php _e( 'You can download the complete program of this project from the following link to obtain all the information about it:', 'elcano' ); ?></p>
					<p><a href="<?php echo esc_url( get_field( 'program_pdf' ) ); ?>" class="btn icon-download" download><?php _e( 'Download Program', 'elcano' ); ?></a></p>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>

	<div class="more-projects">
		<a href="<?php echo get_post_type_archive_link( 'project' ); ?>" class="btn"><?php _e( 'See other projects', 'elcano' ); ?></a>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->

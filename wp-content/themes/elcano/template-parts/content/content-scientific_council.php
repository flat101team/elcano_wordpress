<?php
/**
 * Template part for displaying single member_council content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

// TODO: twitter compliance LOPD
// TODO: twitter smart load script

$group                = current( wp_get_post_terms( get_the_ID(), 'sci_group' ) );
$posts_by_author      = elcano_get_posts_by_author( get_the_ID(), true );
$activities_by_author = elcano_get_activities_by_author();
$authors_ids = get_field('authors');
$author_id = $authors_ids[0] ?? new stdClass();

// Twitter of Elcano
$wpseo_social = get_option( 'wpseo_social', array() );
$twitter_user = $wpseo_social['twitter_site'] ?? 'rielcano';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header member-council">
        <div class="member-columns">
            <div class="sidebar">
                <div class="mobile-name">
                    <hr>
                    <h1 class="fn n"><?php echo get_field( 'name' )?>
                        <?php echo get_field( 'lastname' )?>
                    </h1>
                    <p class="subtitle"><?php echo get_field( 'entity' )?></p>
                    <!--<p class="subtitle"><?php /*echo get_field( 'job-position' )*/?></p>-->
                    <hr>
                </div>
                <div class="author-photo">
                    <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail(
                            get_the_ID(),
                            array( 270, 270 ),
                            array(
                                'class'   => 'photo',
                                'loading' => false,
                            )
                        );
                    } else {
                        ?>
                        <img src="<?php bloginfo( 'template_directory' ); ?>/images/bio.jpg" alt="<?php the_title(); ?>" />
                    <?php } ?>
                </div>
                <hr>
                <h3 class="contact"><?php _e('Contact', 'elcano'); ?></h3>
                <?php
                if(get_field( 'mail-contact' )){
                    ?>
                    <div class="mail-contact">
                        <img src="/wp-content/themes/elcano/images/icon_mail.png" alt="icon-mail"/>
                        <?php echo get_field( 'email' )?>
                    </div>
                <?php } ?>
                <!--<div><?php /*echo get_field( 'job-position' )*/?></div>-->
                <h3 class="title-sidebar"><?php _e('Area of Research', 'elcano'); ?></h3>
                <?php
                if(get_field( 'area_of_reseach' )){
                    ?>

                    <div class="eje-investigacion"><?php $field = get_field( 'area_of_reseach');
                        echo $field->name; ?>
                    </div>
                <?php } ?>
                <?php
                if( get_field( 'facebook' ) &&
                    get_field( 'twitter' ) &&
                    get_field( 'instagram' ) &&
                    get_field( 'linkedin' ) &&
                    get_field( 'youtube' )){
                    ?><h3 class="title-sidebar"><?php _e('Social Media', 'elcano'); ?></h3>
                <?php } ?>
                <div class="social-media">
                    <?php
                    if(get_field( 'facebook' )){
                        ?><a title=Facebook href=<?php echo get_field( 'facebook' )?>><span class="icon-facebook"></span></a>
                    <?php } ?>
                    <?php
                    if(get_field( 'twitter' )){
                        ?><a title=Twitter href=<?php echo get_field( 'twitter' )?>>
                            <span>
                                <svg width="14" height="14" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0_875_997)">
                                        <mask id="mask0_875_997" style="mask-type:luminance" maskUnits="userSpaceOnUse" x="0" y="0" width="28" height="28">
                                            <path d="M0 0H28V28H0V0Z" fill="white"/>
                                        </mask>
                                        <g mask="url(#mask0_875_997)">
                                            <path d="M22.05 1.31201H26.344L16.964 12.06L28 26.688H19.36L12.588 17.818L4.848 26.688H0.55L10.582 15.188L0 1.31401H8.86L14.972 9.42001L22.05 1.31201ZM20.54 24.112H22.92L7.56 3.75401H5.008L20.54 24.112Z" fill="#BB2521"/>
                                        </g>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_875_997">
                                            <rect width="28" height="28" fill="white"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                            </span>
                        </a>
                    <?php } ?>
                    <?php
                    if(get_field( 'instagram' )){
                        ?><a title=Instagram href=<?php echo get_field( 'instagram' )?>><span class="icon-instagram"></span></a>
                    <?php } ?>
                    <?php
                    if(get_field( 'linkedin' )){
                        ?><a title=Linkedin href=<?php echo get_field( 'linkedin' )?>><span class="icon-linkedin"></span></a>
                    <?php } ?>
                    <?php
                    if(get_field( 'youtube' )){
                        ?><a title=Youtube href=<?php echo get_field( 'youtube' )?>><span class="icon-youtube"></span></a>
                    <?php } ?>
                    <?php
                    if(get_field( 'curriculum' )){
                        ?>
                        <h3 class="title-sidebar"><?php _e('Curriculum Vitae', 'elcano'); ?></h3>
                        <div class="btn-cv">
                            <img src="/wp-content/themes/elcano/images/icon-download.svg" alt="icon-download"/>
                            <a href="<?php echo get_field( 'curriculum' )?>" class="btn">Descargar CV</a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="main">
                <div class="desktop-name">
                    <hr>
                    <h1 class="fn n"><?php echo get_field( 'name' )?>
                        <?php echo get_field( 'lastname' )?>
                    </h1>
                    <p class="subtitle"><?php echo get_field( 'entity' )?></p>
                    <!--<p class="subtitle"><?php /*echo get_field( 'job-position' )*/?></p>-->
                    <hr>
                </div>
                <div class="mobile-name about">
                    <div class="about-content">
                        <?php _e('ABOUT', 'elcano') ?>
                        <?php echo strtoupper( get_field( 'name' )); ?>
                    </div>
                     <hr>
                </div>
                <div class="text-scroll">
                    <div><?php echo get_field( 'biography' )?></div>
                </div>

            </div>
        </div>

        <?php if ( $authors_ids ) : ?>
            <section>
                <h2 class="baseline"><?php _e( 'Latest publications', 'elcano' ); ?></h2>
                <?php
                $recent_posts = wp_get_recent_posts(
                    array(
                        'post_type'        => array_merge( array( 'post' ), elcano_current_post_type( 'web' ) ),
                        'post_status'      => 'publish',
                        'numberposts'      => 9,
                        'meta_query'       => array(
                            array(
                                'key'     => 'authors',                // Name of custom field
                                'value'   => '"' . $author_id->ID ?? 0 . '"', // Matches exactly "ID" (with quotes)
                                'compare' => 'LIKE',
                            ),
                        ),
                        'suppress_filters' => false,
                    ),
                    OBJECT
                );

                elcano_post_slider( $recent_posts );
                ?>

                <?php if ( $posts_by_author > count($recent_posts) ) : ?>
                    <p class="all-articles"><a href="<?php echo esc_url( home_url( '/?s&authors=' . $author_id->ID ?? 0 ) ); ?>" class="btn"><?php _e( 'View all publications', 'elcano' ); ?></a></p>
                <?php endif; ?>
            </section>
        <?php endif; ?>
    </header><!-- .entry-header -->



</article><!-- #post-<?php the_ID(); ?> -->

<?php

get_footer();

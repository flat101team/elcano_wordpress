<?php

/**
 * Logo Grid Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$logo_base64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAABXCAIAAAAif/hJAAAesUlEQVR42u1dC3RTVbp2ro56h4cgL0Feymst71rjGhwvjuN41TtvxlHHUcfnODo6c0VEGARBAeUhD3lTebVQoUClIgXbAi0tbaFAaQttoZTSpjRt0+bRPJpXk6bJyf12drqze3JykhTqg8lee2Ul++xzTrL/7/z/9//73zs3eGMlVkKUG2JDECsxcMRKDByxEgNHrMTA8a0Wl9VTsdxz+hGhepvX0xEDx7Urbtf3fWgExU7PiRtoFZrzYuC4diNbtfl7DIvWWqH+a8+ZJxg4PGffElTZXocxBo5rMb6XNn7fEOHxtum9dq1wOT6ACVEtGC9oTnvtaq+zNQaOqxhqZaq33fz9GANPh1Cf5in8VUhMSFbokpbyGDi6BQ7dWcFU/T0YALfLUz5fLPjyuV6HweuyeYqeJYQDhNTdLphqPKceEPUUGg7FwBF9sakF1TH2aGKgvU6TTyGbvO2WaOkqUeY9BGLoDL8meBvazi9yQ4X/i5e8ST4qD/g713xOOhS/LNTthxdDO3stDTFwRFMcBojTU/IGaCmpip1C3T48ZOB05LVuv6BIEqq2kIo3mkIgiZj8UPIzXuoxZ0HwFE4mMq5Y5vW4vYLbUzCY1wddwCEIntIZ5GPlBt9vNHpO/4Z8rNkVA0f4gfZaG4S6r4SqOEGZDpuCNxFo9XbBfEVoyhEufSYo9gjGKjFKQAguriGS64nSYferCn1FJ4+OIy0nfwxmKgIHAOrvrDvr71ydSFrOL4yBQ0bATkFzEiIUmo55nQFnD0pY3nwIbrfH4ehobXUZjR1ms2AxQosIleuFxkyvy+rvo8qBse85Kuo50acLdWjTe05OJCIvnUGOMnDYNZ6T9/jpCIWvx+0p/9CnSNbGwCHN5gRVLlEVpstEc4hkD1tga+ryoFostvJy/Vf7NKtXNr76kvInE0S1/qH7m2ZMM25e2n7oXdfpTYJRKVxO6FlP5fwCnz7oIzQf7+Q3J/0aQnnQD44rKZ5z030aZSJxd6k+q1zr76bKjYEj6LnXX8AAyTzWQqtC0BXjDRRDa/ZRSD0YDTK1+fFRrowbbYkv284We5zOnhqANh1zYoXGLKG1Rmg86lcSUCoF432YuMdPWs9NBXS81kZwFH/LhUXXcWS9W+BotwiX48mjFppF+rpZOwo3qFeuiAoTrKqfG2Gaf7v2tWH2+F7Nz03S7/0CIOsZx6rJc+rn0QU5/B7vHK/bEXNlOX1grhUq1/HcQtpfUSia539o29hbed8E1aNjmv84Sv3sCPVfRqifHNU0eXT9pHFhwHHfeGCi/gHSreGhsdY1ffXvDFFOurclaSc4yrV3WkzVfiXB6skfw7mFYhAurvKc/9hz9i1xB5APt9N7XZfowAEaAX9E3ndwW62GXdtb/nmH5dPbXIdutMX1MS/qb3hvkP7dwfq3h+CN6YMB1tV9gRscMs4dqP7TSABIBA79tMG6N4cGWiZOMEwfbFl5W/394+t/+1jrsRyQ2WuMj5ZyYkfgfquyvTYV2HIwe4WOEVTHPGWzUL0dbd7rvUQODkFQphKLK0v+nWf22+Pvsay4TfvXOxt/MbZ1YX/Vr+6WUQ/QKMbZA+2be5kX92/63V20HarCvqk3ACE2NH8ZYfusN1UnTXNmufT6a80/WiIeDLf336DcECkyFHsE/XkZt8XTkO1M+5vu70PxcDNxQn9oXx0WCcMAMoAP6BLYHfPSfs2Pj5YmIk+OApIafuYzN09MtlVUeGPl2wUH0RmhJ5lwSKhYbfxsvpTHMRpGBBqi8eExoBrQE82TRzf+z5hQEGl8eKx1XZ/2/TfL9ME1oVf8EPzpf8HExKT47YGjwy60lEkfctmE6niPMle9cIFYhJNHG2YOghVwpd8EhgG/A2yDEg7zJ/3Qbt/Y27ykv+aVO+v/ezzPLeybe2v+PBLqQfd6SJUDbmtd25cxFWNGekyQ37EgmKWBxDmsOs3ypYEo1qRxIJ72Lb1aP7pd/eeReL6JwxJCxqr/vVs/dQhQAo5C7YjujaGgohQlxvcHAj3QOpLnwmChA/vYmn00JstvDxxt+q4RsAqhJklwOXVx6/2w+Ol4w78GARagojyXtK7qSymCXLxr8mh4Irb1fdrie/Fo0L5yJ1hIwwPSp4OagKIGblR0JibObwcchHZ0xolJroYylUQH0tMCPDG+l/Y1CUMAxECFRMJJQUiBLeOcgcBZADd/HEUYhlRcBGoJYAogb9K9jnplVD/e7XarOotarW5ra2Ptzc1qvjocgWBXS0uLyxWYM7LZbO3t7SHjPQ6n3W6n3czmLtlPuA57r9Vqq6qq8DUEQfCd5RB9AY1Gg3aDwYBXk8kkOoqWjo4OvHa9vh5X0+v1os4Wi3/eCr+3pkaB+4pO7I5ZEXTFZIYd9LNuH7m0ogZ8kMQk3hkCVtHwoPTzrX5+uN9SyFbVr+/CRYi2eHUYCAdPSNVPjSQe7P0S9kX99EgwmACffe0Vt90WRWjUZouPT6iouIh64UJFWlp6aWkpHf2dO5MuccVisfijvu3ta9euO3eulF3k9OnCxkZVqFsoFLVlZYTLnz9/YenS5U5uHgC3oG8OHTqM+zY3N+M7JCXtxi1wO3rfXbt2FxaewZuqqsvouXt3sg+4zfTomjXr6Jumpmaj0ZSVlc3fmnZWKuvRobKycu3a9bQzQIOjaNm37yscbWpqyvGVqw6CaQo9px/ztls8bW2NLzwL82Fe1g+6QUbqcEBAKcKCAzZF9Zg/IqL61d1QCbA17Kjm+eE8CETGpfmJUeyjbltCVOAAIPiWbdu2U3BkZWVJngJJ19fXJyXtoo94VODIzs5JTT0gAofVav366zTWCOFduHCBfcQpVFvw8mYfExN3sPehwNEZihAYFgldtFhTUvaxn4By6tTp2torVwEOt0u4uJqk4F5c07pnM5wFEuz6W/gwhj00J/XL/qXhsCZ8C/SQfWuvpt8H8GGcPVD3j6ES4PufMeAlfAtUWvfAgUcWAyoPjr17UzCmGEqYgGjBUV1djROhpXhwwBxAe+lDBPR6CBxFRcWAON8ZKg1wuQqzUrvXa20kF6qvs67vQ7zNN4ZGQiZgFILDnTx1AA66OLQsTrq1V+OjY5iXi+s0PiIR/zDNG6B5JsBMm6a/E2FwHeBYtWp1lq9At2P4qPXFK2wHWlj1eMgUI0BQUlLie/IsTAdEBQ4ICYaDyptJC1wkLy8/OTk5NfVgTU0N/0D3EDgyM7Nag6ao+A5RElJzHcuI0WxY1/LWHfbEH0EqwdMiwRVubdNv7wp11PDeoFBRVPi6gCBjG7gI5SXBMIJV4lusZ0ui1RwYR5j848dPyGgOGOn9+1MpmBYsWEiZZlTgoMBKTPwct6PCgLqC8mCqq6Dg5MmTp7oBDnznw4cz+VvD9lFMB4MD3xnERcTNk5P3dhccleu9bsLJ2zWapt+OJ8rgvgm6N4da1/WpfyCMpwoFEyqiBU1ALiVjcV4YDmzxSgIMN7gbDJzql4FJHNU7U7weT7RmBSMowznQ+eDBr9lHWGhQxW6AAwXsMj8/n0pLoVCUlgZijADKjh07uwEOnMiLn/0WSXCAkx4+fIT/nngweK4TTfjcUCGo/XDW7fgcHIKpd+LEbu3FC0YijPHkKDzxcHSBEggb+iAg1JW3sfm2UJX06SSnAKIkgwE70XR1mCOZdpEkpBh9gAMaQscVeKR4phnPoMOdkLAdrwAHBM96ivxVSXCgpKVlrFu3gWqL7dsT6+rq4FjiXMgMcuoGOFBOnCgoKTlrs9lBco8dy2XkJhgcKPg5eXl5YKa4L+4o4qfRgKNyA52H9Did2jd+AkPQ5el/dAzwoQkOZkycALoK1WJd09cwY7Du70NhPvRTh5iX9Ed/04cD8LF1Yf+wVolQTg4Qpvm3A5F+2D0xqmXKEMmztBvDpzfjaYMjx7c0NDS6XC6ws/NdCyz6lSt1otPhQKInAMH3rKvrEmuBvFlwgscN7gJ6wd7DsYS0oIo0Gi1/ulqt5r1fXJwX4ZUrYv8COgwXKS4uwbcSHQL+RC1arRbILigowHWCkREZOOCeKP2Pl7W4iJDHoJAUWoCAlil38NNjkCigECqvR/3USPirxEmZGJ61mD4YoH56JG+JjHMHEoTNGxBS8Uy61221xqKcPRshJcHQzti5cd08uJTSwpg4AZJuXURmQ2BBgAzwxLBSB7G1ru0b7KoEKw9ooMaHx+inD7Zv7t2e+kOYp7CospQUxwTcw+DoXH4C/9CyekzAt5RU5q8OgxdDol7cFAncDe0rd4JXoh1GQfNSF7nCHhEfJLTX0/CzcS3/vMOVdpMtro/mxeG4MiBFZnDCIu/z7TEB9yQ4nCZBedA/TaCsk/csiJvwy7uJ3eEmR9TPjmg/8ENRaq5j7y28cwuiKgqCMUiRhML1fcBX4O8weqF6dIx5ab/wZOWFZ71SpvRqCuhbUVExq//W4BBazgnGS37ukb3fOGtQmHhXXB/eGYGScOf+QDJ123XoJr4njItfJ903QfPMCPOyfnYfZWG2qeGBcSSHI+KoK60dIaaUul30ev2UKe+w+u8Njisp3g673/Hbu4CxwlA5WvwMCOTaceRGCgUoD5gGGAVoCPcxP1zAHviJN2gImB4yMTtrEI8bSUCQ+Mr948OCw6FQxMDRY+Co2hLwWnY+Lb+kAGy0y2zInIEUBB2ZN/KzrGAMTH/wvgbICvNRpQnm8tsafzGWRcNkoq6BU06dioHjGwFH4oNhJti29uKZpuOLWygCSEJXVzLhzvkPeoj3fQzTB/PJOxKB9pmDyDqGzkwwzcvhOek1zyCMgYMDx+WtAXDE/1h2GdIEnq4CAZ78TnqRdpNjz618ZZYF1oGPlOvflcv80L05lM3CwFuJZNrPsC8lBo6eAYfb6bmwhGyPYbiI2rZ7gPa1YagtU+6A48AqaCPJHJ47wLLyNi6NY0wkKwrt238UCIFPHk1m8mTA8fowlmwGtQHlERYc+p07Ip08EoTGRtXJk6cOHTqck3Ps3LlSljEVLTicTmdVVVVeXn5GxqGjR7PLyspbZVfpmc3mCxcuHDuWm5WVhVufPl2oUNTyyWZ8wJTmrdHEMPLE2u2lpWU5OO1Y7sWLlU7ZRcVGoxFfJi8vLzs7p7DwTFNTkyDrzcmCo8PuKZ8jaItJ9pfhoqMTHKiglmRtI1+fH84n9TQ8OI53XEE2JStZuMD47BOj+I+SQRT4tH5wvDgc3CV8qGN7RLk/SmX9ypWreamjTp36LqQrEpI8ODweD0Q7c+Ys0aXefntaSsqX1qCIbUdHR1paBo6K+qPOnj2Hn2Sh5ZNPltGjq1athlyLioqmT/8Xf9a8eQuCc3bIjJhOt3NnUvBdcB389mvBObbcIycJ36oC/mPHUT+xMC/uH4nbCXmTBbEykv6/O0gAjQHlzQjMyt7ksMjAkycpHlo3bdrC40MGHG63+4svUkJdB3XhwsWibM29e+X6o16+fDkUOKAqJE8BXPi5Ojr5J8KQCLg0OfLqwJEwKUyQw7dsOsBPN/Wm4HCm3CIKdYNaWpb3Q+WTdMgEyp/kXGW4P81/8HtDYCfqZ0eEJ6RfH5T/gfX19Twy5s//KCFh29Kly/nh49P4ZMCRm5sneojj4xOWLFnKN27cuInlWNTW1nZ9iNfu2rVnw4Y4vhFXkATHO+9Mp2/QApXw0UeL+LPS0zPYKQCKSJNNmzZD1IIREM0XdsNbmSzvyvLTY74I2J3MssBhoRMoIKrExT1OGt15P+DjGY5dt0LkMjMyMFvsaOtHt6t+Hd6VNcuuh4Ocli1bwcYIhINJDoPFRhBjxyY5Q4EDKgFmiLUfPnyEpfBUVlby+GNTqXv2JLPG4uJi3saxdoDAwyWmMHDQWlRUTEkD7pWYuIO1r1mzjp0CwLH2OXM+ABmiFwTh4C3pokVL3EHpc+HAUfcV2fzPVyw758q7mpA0z0l9hqYXw4c79wft+29mtoYgZlHA3EBnwPr44+Ub+sATCc4h6kYQzH5RLqujpkbBRicpabfoaEFBATt65EimPDh4JR8X95mI6B04cJAdzcz0pxFZLBaFQnHiRAGOivrz+obnmDw49u37SjRZzw4tWPAxuwXDJd6I8kbBuAEXdhb4bJTg0Few9dOWjBR5wkjjVHwueMPPxtkTfyTtp2wJ5P9RGLFAWcMD4wAOWxxJBCHrJX3d4P7g4oH+kYXP27VamV8HvslZ9+pgbs+OfvzxYnlwrFu3gTXyCxdoaW5uzuosZWVl8soMmgPshF2NrYoQgePSpUtdEivsdp5KU7QVF5ewRlFmEC3wXEKhLYKJN5eVRNDp7Ssru1BOyRnUh8bCO+2yhuU+ErBCIw18uTJuwnvihXLsxDh7oKTrgauBooK7ABam+bezwEZzOKfXH2t59OdCh9yeTLxEt2zZCg3cte7mzQFd8iQJDmh1vqfRGN2EDl1bBTcnKWkXnBQRYeRThHhwwK3tIiiXiz+LGjUe/bi+pJvGOsDCRj9lf8k/Ze+221uX9JcPWrdMucP2WW+yBkmSnUyUXvUaak1KwGA9drcj+RayBHdRf2gm/fTB6ufCs1H1yhXyP23BgoXyzgJf6VogSXBAP/ONzoh3MLPZbGAns2a9L3PfUJpDtJoBqiIYHLCVMlZD9HPee2929OBoOEQ2kfUV7bypPFEQCb51YX+a2QUCAfXQ9JswhBH2AgqA5AeFS9sBOCibafrdXaYPB4C4tC64XW5bGEpaw+3OAN8kcnBQTioJjtbWVr5RMn4VXOBHfPjhfP5E0M+EhO2nTp3maTIfHeHBEbyGMRgcKSlfyoMDiGcd3n9/btTg8DpbyYoVysmPHIZWCF75CG4BFsmvlQVFAGeESpCcX4Ve0f1jqHP/zdABES2jhcJ4fBRboAC3Rf3kKE24JbjOrtn3wWXVqgBdh09hkS2UzEuCw+Fw8o3B8VA81s7OwhbWbt2awE7Be4WilqmctWvXsUPQLpLgCL5LMDigluTNCpwy1mHFipXRg4PMsMTTzdFcej3ZeadrUIusLtnai+eh/CQ+6AKOmpf2A5k1vDcIegIwAo2ANSHT9F0Xq4VygsBMeYe2+Y+jwp/1r3fD/q79+1PZ0PDrA7oxtwJXUOYZheA5t2gXdX15titSNry9uxpwFBae4Qjp58E/h3ey4DR1CxytCrbEXv3xAuKSdAaj1E+P7LIuTXJLp9+PNi/Boz8a5ga2hndBQUXhj4SJrW3owywIPJqwpNivbPLDb5d+4UKFSGYinogHeuXK1aiMyYcCR1pahoxfkJp6gB2FhyxigvwCE5psxodMQhHSSMCh1Wr5SFeQK2uBKWEd6FrtqMHhn571BTysZaWND4+ljihMA57psPtG6l4fFmpRE91GQSZiAS/GwJkegEz9VPgNHep//YjbFj71HM/rBx/MlwyCwTPkY+HM/wwFDl4MqOANLHRRXl7OH6Kx7aamJtYyc+YsJmkgEkAM5jrdAwfKpk1beFYBBxi3oLOMn366ih1avvxTT9AysIjXyjqMgoKsxBI8HtXUtzQv3+nYcyvxJyNZWDBvAJ8EFLSK+s7gBNLA+ob1fdgtoKW6BNlkouZpX0f4s86fP8+PKbASH5+AygeYMXAseigTPueVB+rSpct37kwSReKZ6gYu+VvAjuTnH8/Ly+epKK38SqpugAMe77RpM0S0d8aMmaIpRtH6nSjBQdyWw4K5liiPokLr2r7gm/KbL/B2IdTuTQHD8djdkrnKjPwSHbPjP6G0IlIb0axYyczMknFSZs+eo+UiaTLggLxh12UutWFDHO/lFhSclOwGUfFclQ+pdQMcXt+2DiJ8iLAi6chEBw66Gzz5l52La/SLyUbm8Cfl03MCq+wj27mFb4G54dfUw2GJJPWL9Mw9Fm1WS3n5eUm3dvPmLTS8EeGUPRRMfn6+6LmkAsjIOCTaAAi6/ciRTFHPJUuWwoOAKWEtcG6vEhzU6kGNBU8+r18fJ6kzogeHb9Gs5+Q93jZ9u1qtnHQvUeDvDyQLnSfKrTqxruobielhcS3NC8MJyeWUBGhHJCHRqPZfCOYftbVXjh8/kZ6eAQ8QPF8UgmTdmrgieSkwSjyLcATS0tKzs3OAPD6QFez+FBUVZWVlwawoFAomVBVXgpN9UDqCgr/8KZJZPMATvgyMF77VmTNFuNpVJPuIkXGR7IdvqiZ/cyF4jBnp/tSKvw2DayoZz6BEwTAzvPUhK6R9Cy0BNYCJJ7kgs0SvREBulA9OdDZct3+q9Z3LIQ0gQ5UjKL+ma4QEU41QnSh0tKuXfcL4AWwHQBA8laqfOoQsXfxJRLzVue9mkV9D5mnXhaEsAYOSlxuT6DcLDlANRZKg7bIdimBRCpc+c+s1jX99kU/jA2c0fTCAz8kwf9JPcjsefmaObhJnmjtAtFWcftpg87J+ESJDt2XTNV/fFgOHvCm2CZUbAAUp09oiXFztulLe8MxTvKQ1Lw2H/4nHncRAfRpFmov8fCwwYVnezx7fCyCgdoRsSOqbl4cGgtdqmBFRcJ2cuHih4HLFxPkNgoPsDbda7j+63S6hdq+rOKHxD48G81DI3ry4v+sw+VcNVMuK2wAF+MBk2nZjb/IXKu8ODp7jxSmgn3BVZFIGwW/4tKPmubM9bW0xWX5z4BBMl8n/0UfwT0SCuc5d9JF+ziP8+mkmRbqiqf7+8VAVqPK7LdBdPdpTfygTdYUXQ7zczoC9ZsmibiMDbsXu3ck0B+fw4UxU5h/W1tbu358K/xMeR0FBAXxU+CmHDh3mfYSKios01dtgMMBRZBHG+vp6RYhlmCaTiW45h0vxad/wXelGPPCccUe4uAcOHKTbSpWWlsF7Qt2+PZG+qaqqwiGz2UI/pqYeSE5Opu9xHdGOovBK0EiPpqTs+/LLffS90WjEd6bvaW0OmqeUBofQfJwkCAqeiMdZ6Lic15Y0yTh7YOPDY/joZyQLCOCJaF4mO1lDbYBz4BR+H5guyPjFWJghlj2q25Ygn84TNp2C3/YJH+neSBAbYMGEfeXKFbqDlkajYSFO+IEs8Tgr62h+fr5CUUs/VldXB++vxRzXnBwShklM/BzVbvfDGl4xnTVNTNzBJuGKior5rZv4KZvMzCw2Zc/OpZ62aN83fuNAONgAVuePtfNbnEWmOQQ3LEX3/gHa0+40bltBdjHf2Jvsav3kKOOcgTLLX0FUta8Ng7mxb+4N2hHYqPq+CfYtvaBmxHro0THQGdQS1T90P0nXuDoGGrwnGBVAUtJu0TRpWloGHeJLly7hqcWJe/YkUy3S3t6Oi+A1JeXLaMCxA0oCp9BIAxUw3WcskCvhdPKxlm8fHEL1DqH1qlamW0uKG597UvPMCCDDlXaTfVNvSjjgtrQuuJ3QjtV9abaYeWk/3ZtDJX0Z9VMjRSts/TvT+aLszQvmtUtFqLoBDgxli6/QDbKgw6EwIPjg+Vu23Vt2dk5CwjY2k15aWkpnOwERKrAIwYHXkpKzNDrOBFxZWQloFhScxDVF6eDXHBybN28p40pwVE1Cc1z9oLvtdsMXycpJ9zJXpeHBcQABnBd4uWE3eQrkbXRuItgyZQgIbMMD4+AZWc8URrKNZITg2LRpc4mvQIfDTGCgAQ6YcFFPqHcGDt9GlEd5mZlh/327lefm5kUFDqiKL75IASfgBYxGKBXAdMeOncxUdRscoC+hwIGfqedKN3cT7F5x6bSO/a9D/3fvr0NJ6sam3mQrwfV94L80vvgs7Mi1/YNZkVmBGt+1a3cIs5LOhMGDo6GhEby1pLNs3ZqAxz1ycNDcAJiSujolBIwr89tR4lJQUVGBAwIWJfXs2/cV+zeIq+Yc17DAE2482oHHKvdYtP84TP/ABYqnbdethrWLbeVlPRHGCOYckC4lpGCg/AInPkuKBweGHqPMDuFxh/6IChy+69fFxX0GAUOKaGfqHSPHbx4aCTi8JLHoIFsOiR9I4f6dA4dv+XVgLrhdo7EUnNBujGv40+Nh/1TWMP8p58HnHQUpQtkyr7unolsQBjUErMDDpM8Z9DlGGRBJT884caKAN/+QGd2BGj3ZCiWme0BdgS0YC/g7tMKR4YFFdxIT5eTl5eXTfCKVSgXA4Syfj3qQTwPDldl7oIFN5oEtlZcH/pvR4XD4fPOjPv/8CJ+fDP+cLXukQGRfElWUJ9bD4KjPCBVAc9uszvp6W1kpCIQ5P781K9Ny4oSlpNh+rqjj/JeeipWC7ix1pPk912PlOzrx1h1wXN4aqavpMAjqU8LlTULNruBQPRwouShtrHwfweE5966gPChoTgumavJfHHa112kCEfFaVUJrjaApEpTpJAcAmKjPEMxXQsbc2s0k/T1Wrh9wuB2CIomsebEoBV2poC4QVLkwEEJTDgm/6s4RNAArkQVhyVmm6pi0rhNwEEzI/7d5dFqog/ynhzc2I399gAPWxFx3Ta1UR0xa1ws4lAfY7raxEgNHrMTAESsxcMRKrMTAESsxcMRK9OX/AQy1n049dYmaAAAAAElFTkSuQmCC';
?>
<div class="block-logo-grid">
	<?php if ( get_field( 'is_thumb' ) ) : ?>
		<div class="logo">
			<div class="logo-image"><img src="<?php echo $logo_base64; ?>" /></div>
			<div class="logo-name">Real Instituto Elcano</div>
		</div>
		<div class="logo">
			<div class="logo-image"><img src="<?php echo $logo_base64; ?>" /></div>
			<div class="logo-name">Real Instituto Elcano</div>
		</div>
		<div class="logo">
			<div class="logo-image"><img src="<?php echo $logo_base64; ?>" /></div>
			<div class="logo-name">Real Instituto Elcano</div>
		</div>
	<?php else : ?>
		<?php while ( have_rows( 'logos' ) ) : the_row(); ?>
			<div class="logo">
				<?php if ( get_sub_field( 'link' ) ) : ?>
					<a href="<?php the_sub_field( 'name' ); ?>" rel="external noopener noreferrer" target="_blank">
				<?php endif; ?>
				<div class="logo-image"><?php echo wp_get_attachment_image( get_sub_field( 'image' ), array( 150, 150 ), false, array( 'class' => 'logo' ) ); ?></div>
				<div class="logo-name"><?php the_sub_field( 'name' ); ?></div>
				<?php if ( get_sub_field( 'link' ) ) : ?>
					</a>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>
<?php if ( $is_preview ) : ?>
<style>
.block-logo-grid {
	display: grid;
	grid-template-columns: repeat(3, 1fr);
	align-items: start;
	justify-items: center;
	gap: 40px 20px;
	margin: 2em 0;
	font: normal normal 16px/24px "Roboto Condensed", sans-serif;
	letter-spacing: -0.02em;
	color: #09101d;
}

.block-logo-grid .logo-image {
	width: 100%;
	height: auto;
	aspect-ratio: 270/174;
	padding: 15px;
	margin-bottom: 10px;
	background-color: #f0f0f0;
	transition: background-color 0.2s ease-out;
}

.block-logo-grid .logo-image img {
	display: block;
	width: 100%;
	height: 100%;
	object-fit: scale-down;
}

.block-logo-grid a {
	display: block;
	text-decoration: none;
	color: inherit;
}

.block-logo-grid a:hover {
	text-decoration: underline;
}

.block-logo-grid a:hover .logo-image {
	background-color: #989ea5;
}
</style>
<?php endif; ?>

<?php

/**
 * Biographies Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

?>
<div class="block-biographies biographies-list">
	<?php if ( get_field( 'is_thumb' ) ) : ?>
		<div class="biography vcard">
			<div class="author-photo"><img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24'%3E%3Cpath d='M9 11.8A1.3 1.3 0 0 0 7.7 13 1.3 1.3 0 0 0 9 14.3a1.3 1.3 0 0 0 1.3-1.3A1.3 1.3 0 0 0 9 11.7m6 0a1.3 1.3 0 0 0-1.3 1.3 1.3 1.3 0 0 0 1.3 1.3 1.3 1.3 0 0 0 1.3-1.3 1.3 1.3 0 0 0-1.3-1.3M12 2A10 10 0 0 0 2 12a10 10 0 0 0 10 10 10 10 0 0 0 10-10A10 10 0 0 0 12 2m0 18a8 8 0 0 1-8-8v-.9c2.4-1 4.3-3 5.3-5.3a10 10 0 0 0 10.4 4A8 8 0 0 1 12 20Z'/%3E%3C/svg%3E" alt=""></div>
			<div class="fn n">John Smith</div>
			<div class="title">Director</div>
		</div>
		<div class="biography vcard">
			<div class="author-photo"><img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24'%3E%3Cpath d='M9 11.8A1.3 1.3 0 0 0 7.7 13 1.3 1.3 0 0 0 9 14.3a1.3 1.3 0 0 0 1.3-1.3A1.3 1.3 0 0 0 9 11.7m6 0a1.3 1.3 0 0 0-1.3 1.3 1.3 1.3 0 0 0 1.3 1.3 1.3 1.3 0 0 0 1.3-1.3 1.3 1.3 0 0 0-1.3-1.3M12 2A10 10 0 0 0 2 12a10 10 0 0 0 10 10 10 10 0 0 0 10-10A10 10 0 0 0 12 2m0 18a8 8 0 0 1-8-8v-.9c2.4-1 4.3-3 5.3-5.3a10 10 0 0 0 10.4 4A8 8 0 0 1 12 20Z'/%3E%3C/svg%3E" alt=""></div>
			<div class="fn n">John Smith</div>
			<div class="title">Director</div>
		</div>
		<div class="biography vcard">
			<div class="author-photo"><img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24'%3E%3Cpath d='M9 11.8A1.3 1.3 0 0 0 7.7 13 1.3 1.3 0 0 0 9 14.3a1.3 1.3 0 0 0 1.3-1.3A1.3 1.3 0 0 0 9 11.7m6 0a1.3 1.3 0 0 0-1.3 1.3 1.3 1.3 0 0 0 1.3 1.3 1.3 1.3 0 0 0 1.3-1.3 1.3 1.3 0 0 0-1.3-1.3M12 2A10 10 0 0 0 2 12a10 10 0 0 0 10 10 10 10 0 0 0 10-10A10 10 0 0 0 12 2m0 18a8 8 0 0 1-8-8v-.9c2.4-1 4.3-3 5.3-5.3a10 10 0 0 0 10.4 4A8 8 0 0 1 12 20Z'/%3E%3C/svg%3E" alt=""></div>
			<div class="fn n">John Smith</div>
			<div class="title">Director</div>
		</div>
	<?php else : ?>

		<?php
		$biographies = get_field( 'biographies' );

		foreach ( $biographies as $biography ) {

			if ( get_field( 'position' ) ) {
				$position = get_field( 'position' );
			} else {
				$group    = get_the_terms( $biography->ID, 'bio_group' );
				$position = $group ? $group[0]->name : '';
			}

			$photo = get_the_post_thumbnail( $biography, array( 270, 270 ), array( 'class' => 'photo' ) ) ?:
				'<img src="' . get_bloginfo( 'template_directory' ) . '/images/bio.jpg" alt="">';

			if ( 'publish' === get_post_status( $biography ) ) {
				printf(
					'<div class="biography vcard"><a href="%s" class="url" rel="bookmark">%s %s %s %s</a></div>',
					esc_url( get_the_permalink( $biography ) ),
					sprintf( '<div class="author-photo">%s</div>', $photo ),
					sprintf( '<h3 class="fn n">%s</h3>', get_the_title( $biography ) ),
					sprintf( '<p class="title">%s</p>', esc_html( $position ) ),
					sprintf( '<p class="note">%s</p>', esc_html( get_field( 'area', $biography ) ) )
				);
			} else {
				printf(
					'<div class="biography vcard">%s %s %s %s</div>',
					sprintf( '<div class="author-photo">%s</div>', $photo ),
					sprintf( '<h3 class="fn n">%s</h3>', get_the_title( $biography ) ),
					sprintf( '<p class="title">%s</p>', esc_html( $position ) ),
					sprintf( '<p class="note">%s</p>', esc_html( get_field( 'area', $biography ) ) )
				);
			}
		}
		?>

	<?php endif; ?>
</div>
<?php if ( $is_preview ) : ?>
<style>
.author-photo {
  width: 100%;
  max-width: 270px;
  margin-left: auto;
  margin-right: auto;
  border-radius: 50%;
  background-color: #f9eae9;
  overflow: hidden;
  position: relative;
}

.author-photo::before {
  content: "";
  float: left;
  padding-top: 100%;
}

.author-photo img {
  display: block;
  position: absolute;
  width: 100%;
  height: 100%;
  -o-object-fit: cover;
     object-fit: cover;
}

.biographies-list {
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	gap: 20px;
	margin: 60px 0;
	font-family: "Roboto Condensed", sans-serif;
	text-align: center;
}

.biographies-list .biography {
	flex: 0 0 calc(25% - 15px);
	min-width: 166px;
}

.biographies-list a {
	text-decoration: none;
	color: inherit;
}

.biographies-list a:hover .fn {
	text-decoration: underline;
}

.biographies-list a:hover .photo {
	filter: grayscale(100%);
}

.biographies-list .fn {
	font-size: 28px;
	line-height: 36px;
	color: #545d69;
}

.biographies-list .fn,
.biographies-list p {
	margin: 16px 0;
}

.biographies-list .title {
	line-height: 1.25;
	color: #989ea5;
}

.biographies-list .note {
	font-weight: 700;
	font-size: 14px;
	line-height: 18px;
}
</style>
<?php endif; ?>

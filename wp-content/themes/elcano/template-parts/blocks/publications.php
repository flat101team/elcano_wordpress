<?php

/**
 * Publications Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

?>
<div class="block-publications">
	<?php if ( get_field( 'is_thumb' ) ) : ?>
		<div class="post-slider">
			<div class="post-slides">
				<article class="post-slide">
					<div class="post-thumbnail"></div>
					<p class="post-category">Futuro de Europa</p>
					<h3 class="post-title">Real Instituto Elcano</h3>
				</article>
				<article class="post-slide">
					<div class="post-thumbnail"></div>
					<p class="post-category">Futuro de Europa</p>
					<h3 class="post-title">Real Instituto Elcano</h3>
				</article>
				<article class="post-slide">
					<div class="post-thumbnail"></div>
					<p class="post-category">Futuro de Europa</p>
					<h3 class="post-title">Real Instituto Elcano</h3>
				</article>
			</div>
		</div>
	<?php else : ?>

		<?php elcano_post_slider( get_field( 'publications' ) ); ?>

	<?php endif; ?>
</div>
<?php if ( $is_preview ) : ?>
<style>
.post-thumbnail {
  background-color: #f9eae9;
  overflow: hidden;
  position: relative;
}

.post-thumbnail::before {
  content: '';
  float: left;
  padding-top: 66%;
}

.post-thumbnail img {
  display: block;
  position: absolute;
  width: 100%;
  height: 100%;
  object-fit: cover;
}

.post-slider {
  display: block;
  margin-top: 20px;
  margin-bottom: 20px;
  overflow: hidden;
}

.post-slider .slider-controls {
  display: none;
}

.post-slider .post-slides {
  display: flex;
  flex-flow: row nowrap;
}

.post-slider .post-slide {
  flex: 0 0 calc(33.33% - 16px);
}

.post-slider .post-slide:not(:last-child) {
  margin-right: 24px;
}

.post-slider .post-slide .post-thumbnail {
  width: 100%;
  height: auto;
  margin-bottom: 16px;
}

.post-slider .post-slide .post-thumbnail::before {
  padding-top: 65.21739%;
}

.post-slider .post-slide .post-title {
  margin: 0;
  font: normal 400 24px/1.2 "Roboto Condensed", sans-serif;
  color: #000;
}

.post-slider .post-slide .post-title a {
  color: inherit;
  text-decoration: none;
}

.post-slider .post-slide .post-title a:hover {
  text-decoration: underline;
}

.post-slider .post-slide .post-category {
  margin: 16px 0 8px;
}

.post-slider .post-slide .post-category:empty {
  display: none;
}

.post-slider .post-slide .post-category a {
  padding: 0;
  background: transparent;
  color: #989ea5;
  font: normal 700 14px/1.2 "Roboto Condensed", sans-serif;
  white-space: normal;
}

.post-slider .post-slide .post-category a:hover {
  text-decoration: underline;
}

.post-slider .post-slides.tns-slider {
  display: block;
}

.post-slider .post-slides.tns-slider .post-slide {
  margin-right: 0 !important;
}
</style>
<?php endif; ?>

<?php

/**
 * Timeline Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
if ($block['className'] != 'oculto') :
?>
<div class="block-timeline" id="agenda">
	<?php if ( get_field( 'is_thumb' ) ) : ?>
		<div class="timeline__title"><?php _e( 'Timeline', 'elcano' ); ?></div>
		<dl class="timeline__events">
			<dt>
				<span class="day">5</span>
				<span class="month">Jun 2020</span>
			</dt>
			<dd>
				<p>Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
			</dd>
			<dt>
				<span class="day">23</span>
				<span class="month">Jun 2020</span>
			</dt>
			<dd>
				<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue.</p>
			</dd>
			<dt>
				<span class="day">10</span>
				<span class="month">Jul 2020</span>
			</dt>
			<dd>
				<p>Praesent vestibulum dapibus nibh. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Ut non enim eleifend felis pretium feugiat.</p>
			</dd>
		</dl>
	<?php else : ?>
		<div class="timeline__title"><?php the_field( 'title' ); ?></div>
		<dl class="timeline__events">
			<?php
			while ( have_rows( 'events' ) ) :
				the_row();
				$date = new DateTime( get_sub_field( 'date' ), wp_timezone() );
				?>
				<dt>
					<span class="day"><?php echo $date->format( 'j' ); ?></span>
					<span class="month"><?php echo wp_date( 'M Y', $date->getTimestamp() ); ?></span>
				</dt>
                <dd class="timeline__events-title">
                    <?php
                    the_sub_field( 'title' );
                    ?>
                </dd>
                <?php while(have_rows("contents")):
                    the_row();?>
                    <dt class="timeline__events-hour">
                        <span class="hour"><?php the_sub_field("hour");?></span>
                    </dt>
            <!--- webkit-mask-image: linear-gradient(90deg,#fff 11px,transparent 0,transparent 14px,#fff 0) -->
                    <dd class="timeline__events-detail"><?php the_sub_field( 'content' ); ?></dd>
			    <?php endwhile; ?>
			<?php endwhile; ?>
		</dl>
	<?php endif; ?>
</div>
<?php endif; ?>
<?php if ( $is_preview ) : ?>
<style>
.timeline__events-detail:before{
    -webkit-mask-image: linear-gradient(90deg,#fff 11px,transparent 0,transparent 14px,#fff 0);
}
.block-timeline {
	position: relative;
	padding-left: 100px;
}

.block-timeline::before {
	content: "";
	display: block;
	position: absolute;
	inset: 0 auto 0 70px;
	border-left: 3px solid #9d3341;
}

.block-timeline .timeline__title {
	font: normal 400 28px/1.286 "Roboto Condensed", sans-serif;
	color: #000;
}

.block-timeline dt {
	float: left;
	position: relative;
	width: 50px;
	margin: 0 0 0 -100px;
	padding: 0;
	color: #545d69;
	font: normal 700 12px/1.1 "Roboto Condensed", sans-serif;
	text-align: right;
	text-transform: uppercase;
}

.block-timeline dt .day {
	display: block;
	font-size: 33px;
}

.block-timeline dt::after {
	content: "";
	display: block;
	position: absolute;
	top: 10px;
	right: -32px;
	width: 22px;
	height: 22px;
	border-radius: 50%;
	background-color: #9d3341;
}

.block-timeline dd {
	margin: 0 0 25px;
	padding: 0;
}

.block-timeline dd > *:first-child {
	margin-top: 0;
}

.block-timeline dd > *:last-child {
	margin-bottom: 0;
}

@media (orientation: landscape) and (min-width: 1024px), (orientation: portrait) and (min-width: 768px) {
	.block-timeline {
		padding-left: 130px;
	}
	.block-timeline::before {
		left: 90px;
	}
	.block-timeline dt {
		margin-left: -120px;
	}
	.block-timeline dt::after {
		right: -42px;
	}
}
</style>
<?php endif; ?>

<?php

/**
 * Activities Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$activities = get_field( 'activities' );

?>
<div class="block-activities">
<div class="the-archive the-archive--activity">
	<?php if ( get_field( 'is_thumb' ) ) : ?>
		<article class="activity">
			<div class="post-inner">
				<div class="post-thumbnail" data-date="<?php echo wp_date( 'j M Y', time() ); ?>"></div>
				<div class="post-info">
				<p class="entry-title"><a href="#" rel="bookmark">Activity Title</a></p>
					<p><strong><?php _e( 'Organization', 'elcano' ); ?>:</strong> Real Instituto Elcano</p>
					<p><?php echo wp_date( __( 'j F, Y', 'elcano' ), time() ); ?></p>
				</div>
			</div>
		</article>
		<article class="activity">
			<div class="post-inner">
				<div class="post-thumbnail" data-date="<?php echo wp_date( 'j M Y', time() ); ?>"></div>
				<div class="post-info">
					<p class="entry-title"><a href="#" rel="bookmark">Activity Title</a></p>
					<p><strong><?php _e( 'Organization', 'elcano' ); ?>:</strong> Real Instituto Elcano</p>
					<p><?php echo wp_date( __( 'j F, Y', 'elcano' ), time() ); ?></p>
				</div>
			</div>
		</article>
		<?php
	else :
		foreach ( $activities as $activity ) :
			$date_start = new DateTime( get_field( 'date_start', $activity->ID, false ), wp_timezone() );
			?>
			<article class="activity">
				<div class="post-inner">
					<div class="post-thumbnail" data-date="<?php echo wp_date( 'j M Y', $date_start->getTimestamp() ); ?>"><?php echo get_the_post_thumbnail( $activity->ID, array( 270, 288 ) ); ?></div>
					<div class="post-info">
						<p class="entry-title"><a href="<?php the_permalink( $activity->ID ); ?>" rel="bookmark"><?php echo get_the_title( $activity->ID ); ?></a></p>
						<p><strong><?php _e( 'Organization', 'elcano' ); ?>:</strong> <?php the_field( 'organizer', $activity->ID ); ?></p>
						<p><?php echo wp_date( __( 'j F, Y', 'elcano' ), $date_start->getTimestamp() ); ?></p>
					</div>
				</div>
			</article>
		<?php endforeach; ?>
	<?php endif; ?>
</div>
</div>
<?php if ( $is_preview ) : ?>
<style>
.post-thumbnail {
  background-color: #f9eae9;
  overflow: hidden;
  position: relative;
}

.post-thumbnail::before {
  content: '';
  float: left;
}

.post-thumbnail img {
  display: block;
  position: absolute;
  width: 100%;
  height: 100%;
  object-fit: cover;
}

.the-archive--activity article {
  padding: 30px 0;
}

.the-archive--activity article:nth-child(n+2) {
  border-top: 1px solid #bb2521;
}

.the-archive--activity article .post-inner {
  display: flex;
  flex-direction: column;
}

.the-archive--activity article .post-thumbnail {
  align-self: center;
  position: relative;
  width: 224px;
  height: auto;
  margin: 0 0 24px;
  color: #fff;
  font: normal 700 40px/1.1 "Roboto Condensed", sans-serif;
  text-transform: uppercase;
}

.the-archive--activity article .post-thumbnail::before {
  position: relative;
  z-index: 2;
  width: 100%;
  padding-top: 100%;
  background: linear-gradient(0deg, #000, rgb(0 0 0 / 0%));
}

.the-archive--activity article .post-thumbnail::after {
  content: attr(data-date);
  display: block;
  position: absolute;
  z-index: 3;
  bottom: 0.5em;
  left: 0.55em;
  width: 2.5em;
}

.the-archive--activity article .post-thumbnail img {
  transition: filter 200ms ease-out;
}

.the-archive--activity article .entry-title {
  margin-top: 0;
}

.the-archive--activity article p {
  margin: 1em 0 0;
}

.block-activities .the-archive--activity article .post-thumbnail {
  width: 180px;
  font-size: 26.666px;
}

.block-activities .the-archive--activity article .entry-title {
  font: normal 400 18/1.333 "Roboto Condensed", sans-serif;
  color: #000;
}

@media (min-width: 769px){
  .the-archive--activity article {
      padding: 50px 0;
    }

  .the-archive--activity article .post-inner {
    flex-direction: row;
    max-width: 956px;
    margin: 0 auto;
  }

  .the-archive--activity article .post-thumbnail {
    flex-shrink: 0;
    align-self: start;
    width: 270px;
    margin: 0 24px 0 0;
  }

  .the-archive--activity article .post-thumbnail::before {
    padding-top: 106.66667%;
  }
  .block-activities .the-archive--activity article .post-thumbnail {
    margin-right: 74px;
  }

  .block-activities .the-archive--activity article .post-info {
    align-self: center;
  }
}
</style>
<?php endif; ?>

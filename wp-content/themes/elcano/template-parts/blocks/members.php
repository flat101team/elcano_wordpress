<?php

/**
 * Members Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

?>
<div class="block-members">
	<?php if ( get_field( 'is_thumb' ) ) : ?>
		<div class="member">
			<div class="member__name">John Smith</div>
			<div class="member__position">Director</div>
			<div class="member__institution">Real Instituto Elcano</div>
		</div>
		<div class="member">
			<div class="member__name">John Smith</div>
			<div class="member__position">Director</div>
			<div class="member__institution">Real Instituto Elcano</div>
		</div>
		<div class="member">
			<div class="member__name">John Smith</div>
			<div class="member__position">Director</div>
			<div class="member__institution">Real Instituto Elcano</div>
		</div>
		<div class="member">
			<div class="member__name">John Smith</div>
			<div class="member__position">Director</div>
			<div class="member__institution">Real Instituto Elcano</div>
		</div>
	<?php else : ?>
		<?php while ( have_rows( 'members' ) ) : the_row(); ?>
		<div class="member vcard">
			<div class="member__name fn n"><?php the_sub_field( 'name' ); ?></div>
			<div class="member__position title"><?php the_sub_field( 'position' ); ?></div>
			<div class="member__institution org"><?php the_sub_field( 'institution' ); ?></div>
		</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>
<?php if ( $is_preview ) : ?>
<style>
.block-members {
	display: grid;
	grid-template-columns: repeat(4, 1fr);
	gap: 15px;
	margin: 60px 0;
	font: normal normal 18px/1.3 "Roboto Condensed", sans-serif;
	letter-spacing: -0.02em;
	text-align: center;
	color: #545d69;
}

.block-members:empty {
	display: block;
	height: 80px;
	background-color: rgba(218, 32, 28, 0.1);
}

.block-members .member {
	margin-bottom: 40px;
}

.block-members .member::before {
	content: "";
	display: inline-block;
	position: static;
	width: 55px;
	height: 5px;
	border-radius: 3px;
	background-color: #da201c;
}

.block-members .member__name {
	margin-top: 16px;
	font-size: 28px;
}

.block-members .member__position {
	margin: 16px 0;
	color: #989ea5;
}

.block-members .member__institution {
	font-weight: bold;
	font-size: 14px;
}
</style>
<?php endif; ?>

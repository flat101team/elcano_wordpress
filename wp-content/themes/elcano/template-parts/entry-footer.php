<?php
/**
 * Template part for entry-footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$classes = isset( $args['classes'] ) ? $args['classes'] : '';
?>
<footer class="entry-footer <?php echo esc_attr( $classes ); ?>">
	<?php
	// Post tags.
	elcano_post_tags();

	// Related posts.
	elcano_related_posts();

	echo '<div class="post-share">';

	// Like button.
	elcano_love_me();

	// Share links.
	elcano_share();

	echo '</div>';

	// Post navigation.
	the_post_navigation(
		array(
			'prev_text'    => '<span class="nav-subtitle">' . esc_html__( 'Previous', 'elcano' ) . '</span> <span class="nav-title">%title</span>',
			'next_text'    => '<span class="nav-subtitle">' . esc_html__( 'Next', 'elcano' ) . '</span> <span class="nav-title">%title</span>',
			'in_same_term' => true,
		)
	);

	// Post authors (from CPT biography).
	get_template_part( 'template-parts/section/authors', 'bio' );
	?>
</footer><!-- .entry-footer -->

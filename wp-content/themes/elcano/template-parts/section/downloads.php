<?php
/**
 * Template part for displaying post downloads
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */


$files = get_field( 'files' );

if ( $files ) : ?>
<section class="widget downloads">
	<p class="widget-title"><?php _e( 'Downloads', 'elcano' ); ?></p>
	<p class="widget-intro"><?php _e( 'Files attached to this Elcano content', 'elcano' ); ?></p>
	<ul>
	<?php
	foreach ( $files as $file ) {
		printf(
			'<li><a href="%s">%s (.%s)</a></li>',
			esc_url( $file['file']['url'] ),
			esc_html( $file['name'] ?: $file['file']['title'] ),
			esc_html( $file['file']['subtype'] )
		);
	}
	?>
	</ul>
</section>
<?php endif; ?>

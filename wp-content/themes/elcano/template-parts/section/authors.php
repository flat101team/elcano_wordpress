<?php
/**
 * Template part for displaying post authors
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$authors = get_field( 'authors' );

if ( $authors ) : ?>
<div class="authors authors--small">
	<?php foreach ( $authors as $author ) : ?>
	<span class="authors__author vcard">
		<div class="author__photo">
            <?php if(has_post_thumbnail($author->ID)) : ?>
                <?php echo get_the_post_thumbnail( $author->ID, array( 74, 74 ), array( 'class' => 'photo' ) ); ?>
            <?php else : ?>
                <img src="<?php bloginfo( 'template_directory' ); ?>/images/bio.jpg" alt="<?php echo $author->post_title; ?>">
            <?php endif; ?>
        </div>
		<?php if ( 'publish' === $author->post_status ) : ?>
			<a class="url fn n" href="<?php the_permalink( $author ); ?>"><?php echo $author->post_title; ?></a>
		<?php else : ?>
			<span class="fn n"><?php echo $author->post_title; ?></span>
		<?php endif; ?>
	</span>
	<?php endforeach; ?>
</div>
<?php endif; ?>

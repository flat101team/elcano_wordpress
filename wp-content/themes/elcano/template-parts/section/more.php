<?php
/**
 * Template part for displaying more contect related
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

// Post ID (can be passed by args or current post ID)
$post_id = isset( $args['post_id'] ) ? $args['post_id'] : get_the_ID();

?>
<section class="widget more-about">
	<p class="widget-title widget-title--big"><?php _e( 'Related Content', 'elcano' ); ?></p>
	<?php
	$ancestors = get_post_ancestors( $post_id );
	$parent    = $ancestors ? $ancestors[0] : null;

	if ( $parent ) {
		$siblings = get_pages( 'parent=' . $parent . '&sort_column=menu_order&sort_order=asc' );
	}

	if ( is_array( $siblings ) ) {
		foreach ( $siblings as $sibling ) {
			if ( $sibling->ID !== $post_id ) {
				printf(
					'<article class="post-related"><a href="%s" rel="bookmark">%s</a></article>',
					esc_url( get_the_permalink( $sibling ) ),
					sprintf( '<p class="post-title">%s</p>', get_the_title( $sibling ) ),
				);
			}
		}
	}
	?>
</section>

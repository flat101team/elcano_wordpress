<?php
/**
 * Template part for displaying newsletter subscription link
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

?>
<section class="widget newsletter">
	<p class="widget-title"><?php _e( 'Newsletter', 'elcano' ); ?></p>
	<p class="widget-intro"><?php _e( 'Receive the latest news by email', 'elcano' ); ?></p>
	<a href="<?php echo esc_url( get_field( 'newsletter', 'option' ) ); ?>" class="btn icon-email" rel="external nofollow noopener noreferrer"><?php _e( 'Subscribe me', 'elcano' ); ?></a>
</section>

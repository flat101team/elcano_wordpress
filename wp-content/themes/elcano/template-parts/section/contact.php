<?php
/**
 * Template part for displaying contact widget
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

// Post ID (can be passed by args or current post ID)
$post_id = isset( $args['post_id'] ) ? $args['post_id'] : get_the_ID();

// Contact links
$contact_links = array_filter(
	array(
		'facebook' => get_field( 'contact_facebook', $post_id ),
		'twitter'  => get_field( 'contact_twitter', $post_id ),
		'email'    => get_field( 'contact_email', $post_id ),
	)
);

if ( get_field( 'contact_title', $post_id ) || get_field( 'contact_name', $post_id ) || get_field( 'contact_text', $post_id ) ) : ?>
<section class="widget contact">
	<p class="widget-title"><?php the_field( 'contact_title', $post_id ); ?></p>
	<p class="widget-name"><?php the_field( 'contact_name', $post_id ); ?></p>
	<p class="widget-text"><?php the_field( 'contact_text', $post_id ); ?></p>
	<?php if ( count( $contact_links ) ) : ?>
		<div class="links">
			<ul class="social-links follow-links">
				<?php if ( isset( $contact_links['facebook'] ) ) : ?>
					<li><a aria-label="<?php printf( __( 'Follow on %s', 'elcano' ), 'Facebook' ); ?>" href="<?php echo esc_url( $contact_links['facebook'] ); ?>" rel="external nofollow noopener noreferrer"><span class="icon-facebook"></span></a></li>
				<?php endif; ?>
				<?php if ( isset( $contact_links['twitter'] ) ) : ?>
					<li><a aria-label="<?php printf( __( 'Follow on %s', 'elcano' ), 'Twitter' ); ?>" href="https://twitter.com/<?php echo esc_attr( $contact_links['twitter'] ); ?>" rel="external nofollow noopener noreferrer"><span class="icon-twitter"></span></a></li>
				<?php endif; ?>
				<?php if ( isset( $contact_links['email'] ) ) : ?>
					<li><a class="email" aria-label="<?php _e( 'E-mail address', 'elcano' ); ?>" href="mailto:<?php echo esc_attr( $contact_links['email'] ); ?>" rel="external nofollow noopener noreferrer"><span class="icon-email"></span></a></li>
				<?php endif; ?>
			</ul>
		</div>
	<?php endif; ?>
</section>
<?php endif; ?>

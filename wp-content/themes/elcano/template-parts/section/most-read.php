<?php
/**
 * Template part for displaying the most read widget
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

// TODO: most read logic
// (view https://wordpress.org/plugins/toplytics/)
// (view https://wordpress.org/plugins/top-10/)
?>
<section class="widget most-read">
	<p class="widget-title widget-title--big"><?php _e( 'Latest Publications', 'elcano' ); ?></p>
	<?php
	$query_args = array(
		'post_type'        => elcano_current_post_type(),
		'post_status'      => 'publish',
		'numberposts'      => 3,
		'suppress_filters' => false,
	);

	if ( is_singular() ) {
		$query_args['post__not_in'] = array( get_the_ID() );
	}

	$recent_posts = wp_get_recent_posts( $query_args, OBJECT );

	if ( is_array( $recent_posts ) ) {
		foreach ( $recent_posts as $recent_post ) {
			printf(
				'<article class="post-related"><a href="%s" rel="bookmark">%s</a></article>',
				esc_url( get_the_permalink( $recent_post ) ),
				sprintf( '<p class="post-title">%s</p>', get_the_title( $recent_post ) ),
			);
		}
	}
	?>
</section>

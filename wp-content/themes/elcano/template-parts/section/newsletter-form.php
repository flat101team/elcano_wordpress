<?php
/**
 * Template part for newsletter form
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

?>

<section class="section-newsletter">
    <div class="envelope-icon">

    </div>
    <div class="left-col">
        <h2 class="h2"><?php esc_html_e( 'Subscribe to our newsletters and join our community', 'elcano' ) ?></h2>
    </div>
    <div class="right-col newsletter-form">
        <form action="#" method="POST">
            <div class="input-group email-input-group">
                <label for="newsletter-email">Email</label>
                <input id="newsletter-email" type="email" name="email" placeholder="<?php esc_attr_e( 'Enter your email', 'elcano' ) ?>">
            </div>
            <div class="input-group privacy-input-group">
                <label for="newsletter-privacy">
                    <input id="newsletter-privacy" type="checkbox" name="privacy" value="1">
                    <?php echo sprintf( __( 'I\'ve read and accepted the <a href="%s" target="_blank">privacy policy</a>', 'elcano' ), '#' ) ?>
                </label>
            </div>
            <button class="btn btn--fullwidth"><?php esc_html_e( 'Subscribe', 'elcano' ) ?></button>
            <p class="subscription-manage">
                <a href="#"><?php esc_html_e( 'Already subscribed? Manage your subscription', 'elcano' )?></a>
            </p>
        </form>
    </div>
</section>
<?php
/**
 * Template part for displaying post authors with bio
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$authors = get_field( 'authors' );

if ( $authors ) : ?>
<div class="authors authors--bio">
	<?php foreach ( $authors as $author ) : ?>
		<div class="authors__author vcard">
			<div class="author__photo">
                <?php if(has_post_thumbnail($author->ID)) : ?>
                    <?php echo get_the_post_thumbnail( $author->ID, array( 74, 74 ), array( 'class' => 'photo' ) ); ?>
                <?php else : ?>
                    <img src="<?php bloginfo( 'template_directory' ); ?>/images/bio.jpg" alt="<?php echo $author->post_title; ?>">
                <?php endif; ?>
            </div>
			<p class="author__by">
				<?php _e( 'Written by', 'elcano' ); ?>
				<?php if ( 'publish' === $author->post_status ) : ?>
					<a class="url fn n" href="<?php the_permalink( $author ); ?>"><?php echo $author->post_title; ?></a>
				<?php else : ?>
					<span class="fn n"><?php echo $author->post_title; ?></span>
				<?php endif; ?>
			</p>
			<?php
				$post_content = wp_strip_all_tags(strip_shortcodes($author->post_content));
				$post_content = preg_replace('/\[.*?\]/','', $post_content);

				$excerpt_length = apply_filters( 'excerpt_length', 55 );
				$excerpt_more = ' [...]';
				$excerpt      = wp_trim_words( $post_content, $excerpt_length, $excerpt_more );
			?>
			<p class="author__bio note"><?php echo $excerpt; ?></p>
		</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>

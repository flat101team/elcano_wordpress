<?php
/**
 * Template part for displaying post share links
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

?>
<section class="widget share">
	<p class="widget-title"><?php _e( 'Share this article', 'elcano' ); ?></p>
	<?php elcano_share(); ?>
</section>

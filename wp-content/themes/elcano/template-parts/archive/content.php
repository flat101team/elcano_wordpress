<?php
/**
 * Template part for archive items
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

?>

<article id="post-<?php the_ID(); ?>" class="<?php echo get_post_type(); ?>">
	<div class="post-thumbnail">
        <?php if ( has_post_thumbnail() ) {
            the_post_thumbnail( array( 368, 240 ) );
        } else { ?>
            <?php if (get_post_type() === 'post'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/post.jpg" alt="<?php the_title(); ?>" />
            <?php endif; ?>
        <?php } ?>
    </div>
	<div class="post-category"><?php elcano_primary_category(); ?></div>
	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<div class="post-meta">
		<?php elcano_authors(); ?>
		<?php elcano_posted_on(); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

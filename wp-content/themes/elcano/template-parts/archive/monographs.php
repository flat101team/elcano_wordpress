<?php
/**
 * Template part for archive special items
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

?>

<article id="post-<?php the_ID(); ?>" class="monograph">
	<div class="post-thumbnail"><?php the_post_thumbnail( array( 368, 240 ) ); ?></div>
	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<div class="post-meta">
		<span class="years"><?php the_field( 'years' ); ?></span>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for category archive video item
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

// Enqueue modal script
elcano_append_dependency( 'elcano-theme', 'elcano-modal' );

?>

<article id="post-<?php the_ID(); ?>" class="<?php echo get_post_type(); ?>">
	<div class="post-thumbnail" data-video="<?php echo esc_url( get_field( 'video' ) ); ?>"><?php the_post_thumbnail( array( 760, 406 ) ); ?></div>
	<?php if ( 'post' === get_post_type() ) : ?>
		<div class="post-category"><?php elcano_primary_category(); ?></div>
	<?php else : ?>
		<div class="post-group">
			<div class="post-category"><?php elcano_primary_category(); ?></div>
			<div class="post-type"><?php echo elcano_post_type_label(); ?></div>
		</div>
	<?php endif; ?>
	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<div class="post-meta">
		<?php elcano_authors(); ?>
		<?php elcano_posted_on(); ?>
	</div>
	<div class="read-more">
		<a href="<?php the_permalink(); ?>" rel="nofollow"><?php _e( 'Read more', 'elcano' ); ?></a>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

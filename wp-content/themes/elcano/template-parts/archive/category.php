<?php
/**
 * Template part for category archive items
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

?>

<article id="post-<?php the_ID(); ?>" class="<?php echo get_post_type(); ?>">
	<div class="post-thumbnail">
        <?php if ( has_post_thumbnail() ) {
            the_post_thumbnail( array( 760, 406 ) );
        } else { ?>
            <?php if (get_post_type() === 'analysis'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/analisis.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'commentary'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/comentario.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'ciber'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/ciber.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'magazine'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/revista.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'poll'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/encuesta.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'report'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/informe.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'post'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/post.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'policy_paper'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/policy.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'work_document'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/documento.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'video'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/video.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'archive'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/archivo.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'news_on_net'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/novedades-red.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'press_release'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/nota-prensa.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'inside_spain'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/inside-spain.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'newsletter'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/newsletter.png" alt="<?php the_title(); ?>" />
            <?php
            elseif (get_post_type() === 'podcast'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/podcast.jpg" alt="<?php the_title(); ?>" />
            <?php elseif (get_post_type() === 'activity'): ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/actividad.jpg" alt="<?php the_title(); ?>" />
            <?php endif; ?>
        <?php } ?>
    </div>
	<?php if ( 'post' === get_post_type() ) : ?>
		<div class="post-category"><?php elcano_primary_category(); ?></div>
	<?php else : ?>
		<div class="post-group">
			<div class="post-category"><?php elcano_primary_category(); ?></div>
			<div class="post-type"><?php echo elcano_post_type_label(); ?></div>
		</div>
	<?php endif; ?>
	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<div class="post-meta">
		<?php elcano_authors(); ?>
		<?php elcano_posted_on(); ?>
	</div>
	<div class="read-more">
		<a href="<?php the_permalink(); ?>" rel="nofollow"><?php _e( 'Read more', 'elcano' ); ?></a>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

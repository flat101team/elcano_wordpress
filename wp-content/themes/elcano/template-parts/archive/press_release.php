<?php
/**
 * Template part for archive press release item
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

?>

<article id="post-<?php the_ID(); ?>" class="press_release">
	<div class="thumb-date">
		<div class="post-thumbnail">
            <?php if(has_post_thumbnail()) :?>
                <?php the_post_thumbnail( array( 172, 152 ) ); ?>
            <?php else:?>
                <img src="<?php bloginfo('template_directory'); ?>/images/nota-prensa.jpg" alt="<?php the_title() ?>">
            <?php endif;?>
        </div>
		<div class="post-date">
			<span class="day"><?php echo get_the_date( 'j' ); ?></span>
			<span class="month"><?php echo get_the_date( 'M Y' ); ?></span>
		</div>
	</div>
	<div class="post-info">
		<div class="post-number"><?php printf( __( 'Press Release No. %d', 'elcano' ), get_field( 'number' ) ); ?></div>
		<h2 class="entry-title h1"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
		<div class="entry-summary"><?php the_excerpt(); ?></div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

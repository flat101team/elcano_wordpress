<?php
/**
 * Template part for archive items
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

?>
<article id="post-<?php the_ID(); ?>" class="archive">
	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<div class="entry-summary"><?php the_excerpt(); ?></div>
	<div class="post-meta">
		<?php elcano_authors(); ?>
		<?php elcano_posted_on(); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

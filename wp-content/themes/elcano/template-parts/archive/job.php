<?php
/**
 * Template part for job items
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

?>
<article id="post-<?php the_ID(); ?>" class="job">
	<div class="post-inner">
		<div class="job-offer"><?php _e( 'Job Offer', 'elcano' ); ?></div>
		<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
		<div class="entry-summary">
			<?php
			if ( get_field( 'job_summary' ) ) {
				the_field( 'job_summary' );
			} else {
				the_excerpt();
			}
			?>
		</div>
		<p class="job-reference"><?php _e( 'Reference', 'elcano' ); ?>: <?php echo get_field( 'job_reference' ) ?: '--'; ?></p>
		<a href="<?php the_permalink(); ?>" rel="bookmark"><?php _e( 'See offer details', 'elcano' ); ?></a>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

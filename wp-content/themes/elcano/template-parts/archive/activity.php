<?php
/**
 * Template part for archive activity item
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */


$thumb_size = array( 270, 288 );

$date_start = new DateTime( get_field( 'date_start', false, false ), wp_timezone() );
$date_end   = new DateTime( get_field( 'date_end', false, false ), wp_timezone() );

if ( $date_start->format( 'mY' ) !== $date_end->format( 'mY' ) ) {
	// Start/End in different months.
	$event_date = sprintf(
		_x( '%1$s %2$d to %3$s %4$d, %5$d', 'Activity dates in two months', 'elcano' ),
		wp_date( 'F', $date_start->getTimestamp() ),
		$date_start->format( 'j' ),
		wp_date( 'F', $date_end->getTimestamp() ),
		$date_end->format( 'j' ),
		$date_start->format( 'Y' )
	);
} elseif ( $date_start->format( 'dmY' ) !== $date_end->format( 'dmY' ) ) {
	// Start/End in different days (same month).
	$event_date = sprintf(
		_x( '%1$s %2$d to %3$d, %4$d', 'Activity dates in one month', 'elcano' ),
		wp_date( 'F', $date_start->getTimestamp() ),
		$date_start->format( 'j' ),
		$date_end->format( 'j' ),
		$date_start->format( 'Y' )
	);
} else {
	// Start/End in same day.
	$event_date = wp_date( _x( 'l, j F, Y', 'Activity in one day', 'elcano' ), $date_start->getTimestamp() ); // 'l, j \\d\\e F \\d\\e Y'
}

$event_time = sprintf(
	_x( 'from %1$s to %2$s', 'Activity hours', 'elcano' ),
	$date_start->format( _x( 'g:i a', 'Activity hour', 'elcano' ) ),
	$date_end->format( _x( 'g:i a', 'Activity hour', 'elcano' ) )
);

$event_tz = sprintf( '%s (UTC%+d)', $date_start->format( 'T' ), $date_start->format( 'P' ) );

?>

<article id="post-<?php the_ID(); ?>" class="activity">
	<div class="post-inner">
		<div class="post-thumbnail" data-date="<?php echo wp_date( 'j M Y', $date_start->getTimestamp() ); ?>">
            <?php if ( has_post_thumbnail() ) {
                the_post_thumbnail( $thumb_size );
            } else { ?>
                <img src="<?php bloginfo('template_directory'); ?>/images/actividad.jpg" loading="lazy" alt="<?php the_title(); ?>" />
            <?php } ?>
        </div>
		<div class="post-info">
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			<p><strong><?php _e( 'Organization', 'elcano' ); ?>:</strong> <?php the_field( 'organizer' ); ?></p>
			<?php if ( get_field( 'main_participants' ) ) : ?>
				<p class="activity-participants"><strong><?php _e( 'Participants', 'elcano' ); ?>:</strong> <?php the_field( 'main_participants' ); ?></p>
			<?php endif; ?>
			<p><strong><?php _e( 'Schedule', 'elcano' ); ?>:</strong> <?php printf( '%s, %s %s', $event_date, $event_time, $event_tz ); ?></p>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for category publications items
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$card_mode      = isset( $args['mode'] ) ? $args['mode'] : '';
$no_thumbnail   = isset( $args['no_thumbnail'] ) && $args['no_thumbnail'] === true;
$show_excerpt   = isset( $args['show_excerpt'] ) && $args['show_excerpt'] === true;
$show_post_type = isset( $args['show_post_type'] ) && $args['show_post_type'] === true;
$hide_category  = isset( $args['hide_category'] ) && $args['hide_category'] === true;
$heading        = isset( $args['heading'] ) ? tag_escape( $args['heading'] ) : 'h2';

$post_type      = get_post_type();

if( $show_excerpt ) {
	$meta_description = get_post_meta( get_the_ID(), '_yoast_wpseo_metadesc', true);
}
?>

<article id="post-<?php the_ID(); ?>" class="<?php echo $post_type; ?> elcano-card <?php echo $card_mode ?>">

	<?php if( !$no_thumbnail ): ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				} else { ?>
					<?php if ($post_type === 'analysis'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/analisis.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'commentary'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/comentario.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'ciber'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/ciber.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'magazine'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/revista.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'poll'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/encuesta.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'report'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/informe.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'post'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/post.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'policy_paper'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/policy.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'work_document'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/documento.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'video'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/video.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'archive'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/archivo.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'news_on_net'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/novedades-red.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'press_release'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/nota-prensa.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'inside_spain'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/inside-spain.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'newsletter'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/newsletter.png" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'podcast'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/podcast.jpg" alt="<?php the_title(); ?>" />
					<?php elseif ($post_type === 'activity'): ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/actividad.jpg" alt="<?php the_title(); ?>" />
					<?php endif; ?>
				<?php } ?>
			</a>
		</div>
	<?php endif; ?>
	<div class="post-info">
		<?php if(  elcano_primary_category( get_the_ID(), 'url', false ) !== false || $show_post_type ): ?>
			<div class="post-category">
				<?php if( ! $hide_category && elcano_primary_category( get_the_ID(), 'url', false ) !== false ):?>
					<a href="<?php esc_attr( elcano_primary_category( get_the_ID(), 'url' ) ); ?>" class="category-link"><?php echo esc_html( elcano_primary_category( get_the_ID(), 'name' ) ) ?></a>
				<?php endif; ?>
				<?php if( $show_post_type ):
					$post_type_obj = get_post_type_object( $post_type ) ?>
					<span class="icon-post-type">
						<svg width="11" height="14" viewBox="0 0 11 14" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M7.7 0H3.3C2.695 0 2.2 0.525 2.2 1.16667V10.5C2.2 11.1417 2.695 11.6667 3.3 11.6667H9.9C10.505 11.6667 11 11.1417 11 10.5V3.5L7.7 0ZM9.9 10.5H3.3V1.16667H7.15V4.08333H9.9V10.5ZM1.1 2.33333V12.8333H9.9V14H1.1C0.495 14 0 13.475 0 12.8333V2.33333H1.1ZM4.4 5.83333V7H8.8V5.83333H4.4ZM4.4 8.16667V9.33333H7.15V8.16667H4.4Z" fill="#2C3A4B"/>
						</svg>
					</span>
					<span class="post-post-type"><?php echo esc_html( $post_type_obj->label ) ?></span>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<<?php echo $heading ?> class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></<?php echo $heading ?>>
		<?php if( $show_excerpt && !empty( $meta_description ) ): ?>
			<p class="excerpt"><?php echo $meta_description; ?></p>
		<?php endif; ?>
		<div class="post-meta">
			<?php elcano_authors(); ?>
			<?php elcano_posted_on( get_the_ID(), true, false ); ?>
		</div>
		<?php if( !in_array( $post_type, array( 'podcast', 'video' ) ) || ( get_field( "podcast_time_hours", get_the_ID() ) > 0 || get_field( "podcast_time_minutes", get_the_ID() ) > 0 ) ): ?>
			<div class="podcast-duration">
				<span class="icon-podcast">
					<svg width="16" height="16" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<g id="AV / timer">
							<mask id="mask0_142_2881" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="3" y="2" width="18" height="20">
								<g id="Icon Mask">
									<path id="Round" fill-rule="evenodd" clip-rule="evenodd" d="M11.002 3.99872C11.002 3.44872 11.452 2.99872 12.002 2.99872C17.072 2.99872 21.162 7.18872 20.992 12.2887C20.842 16.9887 16.912 20.8887 12.212 20.9987C7.14195 21.1187 3.00195 17.0387 3.00195 11.9987C3.00195 9.39872 4.10195 7.06872 5.86195 5.43872C6.26195 5.06872 6.88195 5.07872 7.26195 5.45872L12.702 10.8887C13.092 11.2787 13.092 11.9087 12.702 12.2987C12.312 12.6887 11.682 12.6887 11.292 12.2987L6.58195 7.57872C5.59195 8.77872 5.00195 10.3187 5.00195 11.9987C5.00195 15.9087 8.19195 19.0587 12.122 18.9987C15.972 18.9387 19.142 15.5887 19.002 11.7387C18.872 8.32872 16.312 5.55872 13.002 5.07872V5.99872C13.002 6.54872 12.552 6.99872 12.002 6.99872C11.452 6.99872 11.002 6.54872 11.002 5.99872V3.99872ZM6.00195 11.9987C6.00195 11.4464 6.44967 10.9987 7.00195 10.9987C7.55424 10.9987 8.00195 11.4464 8.00195 11.9987C8.00195 12.551 7.55424 12.9987 7.00195 12.9987C6.44967 12.9987 6.00195 12.551 6.00195 11.9987ZM11.002 16.9987C11.002 16.4464 11.4497 15.9987 12.002 15.9987C12.5542 15.9987 13.002 16.4464 13.002 16.9987C13.002 17.551 12.5542 17.9987 12.002 17.9987C11.4497 17.9987 11.002 17.551 11.002 16.9987ZM17.002 10.9987C16.4497 10.9987 16.002 11.4464 16.002 11.9987C16.002 12.551 16.4497 12.9987 17.002 12.9987C17.5542 12.9987 18.002 12.551 18.002 11.9987C18.002 11.4464 17.5542 10.9987 17.002 10.9987Z" fill="black"/>
								</g>
							</mask>
							<g mask="url(#mask0_142_2881)">
								<rect id="Color Fill" width="24" height="24" fill="#BB2521"/>
							</g>
						</g>
					</svg>
				</span>
				<?php
				if( in_array( $post_type, array( 'podcast', 'video' ) ) ) {
					if (get_field( "podcast_time_hours", get_the_ID() ) > 0 ) {
						echo get_field( "podcast_time_hours", get_the_ID() ); echo " H ";
					}
					if (get_field( "podcast_time_minutes", get_the_ID() ) > 0 ) {
						echo get_field( "podcast_time_minutes", get_the_ID() ); echo " MIN";
					}
				} else {
					echo elcano_get_estimated_reading_time( get_the_ID() );
				}?>
			</div>
		<?php endif; ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for search archive video item
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

// Enqueue modal script
elcano_append_dependency( 'elcano-theme', 'elcano-modal' );

?>

<article id="post-<?php the_ID(); ?>" class="<?php echo get_post_type(); ?>">
	<div class="post-thumbnail" data-video="<?php echo esc_url( get_field( 'video' ) ); ?>"><?php the_post_thumbnail( array( 368, 240 ) ); ?></div>
	<div class="post-group">
		<div class="post-category"><?php elcano_primary_category(); ?></div>
		<div class="post-type"><?php echo elcano_post_type_label(); ?></div>
	</div>
	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<div class="post-meta">
		<?php elcano_authors(); ?>
		<?php elcano_posted_on(); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for search archive activity item
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

$date_start = new DateTime( get_field( 'date_start', false, false ), wp_timezone() );
?>

<article id="post-<?php the_ID(); ?>" class="activity">
	<div class="post-thumbnail" data-date="<?php echo wp_date( 'j M Y', $date_start->getTimestamp() ); ?>">
        <?php if(has_post_thumbnail()): ?>
            <?php the_post_thumbnail( array( 368, 240 ) ); ?>
        <?php else: ?>
            <img src="<?php bloginfo('template_directory'); ?>/images/actividad.jpg" alt="<?php the_title(); ?>" />
        <?php endif; ?>
    </div>
	<div class="post-group">
		<div class="post-category"><?php elcano_primary_category(); ?></div>
		<div class="post-type"><?php echo elcano_post_type_label(); ?></div>
	</div>
	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<div class="post-meta">
		<?php the_field( 'organizer' ); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

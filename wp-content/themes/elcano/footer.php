<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Elcano
 */

?>
	</div><!-- .site-content -->

	<footer id="footer" class="site-footer">
		<div class="site-info">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><svg class="site-logo"><use xlink:href="#svg__logo" /></svg></a>
		</div><!-- .site-info -->
		<?php
		wp_nav_menu(
			array(
				'theme_location'  => 'footer',
				'container'       => 'nav',
				'container_class' => 'footer-links',
				'menu_id'         => 'footer-links',
				'depth'           => 1,
			)
		);
		?>
		<div class="footer-social">
			<p><?php _e( 'Follow us on social media:', 'elcano' ); ?></p>
			<ul class="social-links follow-links">
				<?php $wpseo_social = get_option( 'wpseo_social', array() ); // From Yoast SEO. ?>
				<?php if ( isset( $wpseo_social['facebook_site'] ) ) : ?>
					<li><a aria-label="<?php printf( __( 'Follow on %s', 'elcano' ), 'Facebook' ); ?>" href="<?php echo esc_url( $wpseo_social['facebook_site'] ); ?>" rel="external nofollow noopener noreferrer"><span class="icon-facebook"></span></a></li>
				<?php endif; ?>
				<?php if ( isset( $wpseo_social['twitter_site'] ) ) : ?>
					<li><a aria-label="<?php printf( __( 'Follow on %s', 'elcano' ), 'Twitter' ); ?>" href="https://twitter.com/<?php echo esc_attr( $wpseo_social['twitter_site'] ); ?>" rel="external nofollow noopener noreferrer">
                            <span>
                                <svg width="14" height="14" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0_875_997)">
                                        <mask id="mask0_875_997" style="mask-type:luminance" maskUnits="userSpaceOnUse" x="0" y="0" width="28" height="28">
                                            <path d="M0 0H28V28H0V0Z" fill="white"/>
                                        </mask>
                                        <g mask="url(#mask0_875_997)">
                                            <path d="M22.05 1.31201H26.344L16.964 12.06L28 26.688H19.36L12.588 17.818L4.848 26.688H0.55L10.582 15.188L0 1.31401H8.86L14.972 9.42001L22.05 1.31201ZM20.54 24.112H22.92L7.56 3.75401H5.008L20.54 24.112Z" fill="#BB2521"/>
                                        </g>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_875_997">
                                            <rect width="28" height="28" fill="white"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                            </span>
                        </a></li>
				<?php endif; ?>
				<?php if ( isset( $wpseo_social['instagram_url'] ) ) : ?>
					<li><a aria-label="<?php printf( __( 'Follow on %s', 'elcano' ), 'Instagram' ); ?>" href="<?php echo esc_url( $wpseo_social['instagram_url'] ); ?>" rel="external nofollow noopener noreferrer"><span class="icon-instagram"></span></a></li>
				<?php endif; ?>
				<?php if ( isset( $wpseo_social['linkedin_url'] ) ) : ?>
					<li><a aria-label="<?php printf( __( 'Follow on %s', 'elcano' ), 'Linkedin' ); ?>" href="<?php echo esc_url( $wpseo_social['linkedin_url'] ); ?>" rel="external nofollow noopener noreferrer"><span class="icon-linkedin"></span></a></li>
				<?php endif; ?>
				<?php if ( isset( $wpseo_social['youtube_url'] ) ) : ?>
					<li><a aria-label="<?php printf( __( 'Follow on %s', 'elcano' ), 'Youtube' ); ?>" href="<?php echo esc_url( $wpseo_social['youtube_url'] ); ?>" rel="external nofollow noopener noreferrer"><span class="icon-youtube"></span></a></li>
				<?php endif; ?>
			</ul>
		</div>
	</footer><!-- #footer -->
</div><!-- #page -->

<div id="mobile-menu" class="mobile-menu">
	<div class="mm-prev" role="button"></div>
	<div class="mm-close" role="button"></div>
	<div class="mm-wrapper">
		<div class="mm-content"><!-- fill with javascript --></div>
	</div>
</div>

<?php
// Prev/Next post lateral navigation
elcano_lateral_nav();

wp_footer();
?>

</body>
</html>

<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Elcano
 */

get_header();

if ( !is_singular( array( 'special' ) ) ) {
	elcano_breadcrumb();
}

?>

	<main id="primary" class="site-main">

		<?php

        if ( is_singular( array( 'special' ) ) ) {
        elcano_breadcrumb();
        }

		while ( have_posts() ) :
			the_post();

			// Select content template for post type.
			if ( is_singular( array( 'analysis', 'commentary', 'magazine', 'poll' ) ) ) {
				$content_type = 'post';
			} elseif ( is_singular( 'report' ) ) {
				$content_type = get_field( 'pdf_file' ) ? 'monograph' : 'post';
			} elseif ( is_singular( array( 'policy_paper', 'work_document' ) ) ) {
				$content_type = 'long_post';
			} elseif ( is_singular( array( 'news_on_net', 'ciber' ) ) ) {
				$content_type = 'newsletter';
			} else {
				$content_type = get_post_type();
			}

			get_template_part( 'template-parts/content/content', $content_type );

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
get_footer();

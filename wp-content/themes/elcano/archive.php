<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elcano
 */

global $wp_query, $wpdb;

$post_type        = get_query_var( 'post_type' );
$post_type_obj    = get_post_type_object( $post_type );
$my_current_lang  = apply_filters( 'wpml_current_language', NULL );
$meta_description = YoastSEO()->meta->for_current_page()->description;

$exclude_terms = array( 'el-espectador-global', 'global-spectator', 'economia-internacional', 'european-union-europe', 'elcano-talks', 'elcano-talks-en', 'inside-spain-blog', 'inside-spain-blog-es', 'politica-global', 'seguridad-y-defensa', 'tecnologia' );
$exclude_terms_in = "'" . implode( "', '", $exclude_terms ) . "'";


$query = $wpdb->prepare(
	"SELECT t.*, COUNT(*) as count from $wpdb->terms AS t
	INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id
	INNER JOIN $wpdb->term_relationships AS r ON r.term_taxonomy_id = tt.term_taxonomy_id
	INNER JOIN $wpdb->posts AS p ON p.ID = r.object_id
	LEFT JOIN wp_icl_translations icl_t ON icl_t.element_id = tt.term_taxonomy_id AND icl_t.element_type IN ('tax_category')
	WHERE p.post_type = '%s'
	 	AND tt.taxonomy = '%s'
		AND t.slug NOT IN ($exclude_terms_in)
		AND p.post_status = 'publish'
		AND ( icl_t.element_type IN ('tax_category') AND ( icl_t.language_code = '%s' OR 0 ) )
	GROUP BY t.term_id
	ORDER BY t.name ASC",
	$post_type,
	'category',
	$my_current_lang
);

// LEFT JOIN wp_icl_translations icl_t ON icl_t.element_id = tt.term_taxonomy_id AND icl_t.element_type IN ('tax_category')
$terms = $wpdb->get_results( $query );

/*$terms = get_terms( array(
	'taxonomy'   => 'category',
	'hide_empty' => false,
) );*/

$years_result = array();
$years = $wpdb->get_results(
	"SELECT YEAR(post_date) FROM {$wpdb->posts} WHERE post_status = 'publish' AND post_type = '{$post_type}' GROUP BY YEAR(post_date) ORDER BY YEAR(post_date) DESC",
	ARRAY_N
);

if ( is_array( $years ) && count( $years ) > 0 ) {
	foreach ( $years as $year ) {
		$years_result[] = $year[0];
	}
}

$query_args = array(
	'post_type'      => $post_type,
	'posts_per_page' => 9,
	'orderby' => 'date',
	'order' => 'DESC',
	'post_status' => 'publish',
);

$query = new WP_Query( $query_args );


$nonce = wp_create_nonce( 'form-filtros' );


//Get post type objects with custom order for submenu
$menu_cpts = array( 'commentary', 'analysis', 'policy_paper', 'monograph', 'report', 'poll', 'work_document', 'newsletter' );
$menu_cpts_obj = array_map( 'get_post_type_object', $menu_cpts );

get_header(); ?>

<div id="publications-menu">
	<nav class="nav-menu">
		<ul class="menu" id="publications-submenu">
			<?php foreach( $menu_cpts_obj as $post_type__obj ): ?>
				<li class="menu-item">
					<a href="<?php echo get_post_type_archive_link( $post_type__obj->name ); ?>" class="<?php echo $post_type__obj->name; ?>"><?php echo esc_html( $post_type__obj->label ) ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
	</nav>
	<div class="main-mobile">
	<div class="menu-toggle publications-menu" role="button" aria-controls="publications-menu" aria-expanded="false" aria-label="<?php esc_attr_e( 'Publication types', 'elcano' ) ?>"><span><?php esc_html_e( 'Publication types', 'elcano' )?></span></div>
	</div>
</div>

<?php
elcano_breadcrumb();

ob_start();

?>

<main id="primary" class="site-main">

	<article id="publications">
		<header class="entry-header">
			<h1 class="entry-title h1-alt baseline"><?php echo esc_html( $post_type_obj->label ); ?></h1>
		</header><!-- .entry-header -->

		<div class="entry-content">

			<?php if( !empty( $meta_description ) ): ?>

				<!-- wp:group {"className":"entry-paragraph","layout":{"type":"constrained"}} -->
				<div class="wp-block-group entry-paragraph"><!-- wp:paragraph -->
					<p><?php echo esc_html( $meta_description ) ?></p>
					<!-- /wp:paragraph -->
				</div>
				<!-- /wp:group -->
			<?php endif; ?>

			<form method="POST" id="form-filtros">

				<!-- wp:columns {"className":"results-columns"} -->
				<div class="wp-block-columns results-columns"><!-- wp:column {"width":"25%"} -->
					<div class="wp-block-column" style="flex-basis:25%"></div><!-- /wp:column -->

					<!-- wp:column {"width":"75%"} -->
					<div class="wp-block-column" style="flex-basis:75%" id="results-holder">
						<p id="total-results"><span id="results-count"><?php echo esc_html( $query->found_posts ) ?></span> <?php  esc_html_e( 'results found', 'elcano' ) ?></p>

						<div id="applied-filters">
							<a href="#" id="remove-filters"><?php esc_html_e('Remove all filters', 'elcano' )?></a>
						</div>
					</div><!-- /wp:column -->
				</div><!-- /wp:columns -->
				<!-- wp:columns -->
				<div class="wp-block-columns"><!-- wp:column {"width":"25%"} -->
					<div class="wp-block-column" style="flex-basis:25%" id="filters-column"><!-- wp:greenshift-blocks/accordion {"id":"gsbp-c634b99"} -->
					<div class="wp-block-greenshift-blocks-accordion gs-accordion gspb_accordion-id-gsbp-c634b99" id="gspb_accordion-id-gsbp-c634b99" itemscope itemtype=""><!-- wp:greenshift-blocks/accordionitem {"id":"gsbp-8808a90","title":"Tipo de publicación","open":true} -->
							<!-- wp:greenshift-blocks/accordionitem {"id":"gsbp-82ac665","title":"Temática","open":true} -->
							<div class="wp-block-greenshift-blocks-accordionitem gs-accordion-item gspb_accordionitem-gsbp-82ac665 gsopen" id="gspb_accordionitem-gsbp-82ac665" itemscope>
								<div class="gs-accordion-item__title" aria-expanded="true" role="button" tabindex="0" aria-controls="gspb-accordion-item-content-gsbp-82ac665">
									<div class="gs-accordion-item__heading"><?php esc_html_e( 'Topics', 'elcano' )?></div>
									<meta itemprop="name" content="<?php esc_attr_e( 'Topics', 'elcano' )?>"/><span class="iconfortoggle">
										<span class="gs-iconbefore"></span>
										<span class="gs-iconafter"></span>
									</span>
								</div>
								<div class="gs-accordion-item__content" itemscope id="gspb-accordion-item-content-gsbp-82ac665" aria-hidden="false">
									<div class="gs-accordion-item__text" itemprop="text">
										<?php foreach( $terms as $term_obj ): $term = get_term( $term_obj->term_id ); ?>
											<label for="category_<?php echo esc_attr( $term->term_taxonomy_id ) ?>">
												<input type="checkbox" name="category" value="<?php echo esc_attr( $term->term_taxonomy_id ) ?>" id="category_<?php echo esc_attr( $term->term_taxonomy_id ) ?>" data-label="<?php echo esc_attr( $term->name ) ?>">
												<?php echo $term->name ?>
											</label>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
							<!-- /wp:greenshift-blocks/accordionitem -->

							<!-- wp:greenshift-blocks/accordionitem {"id":"gsbp-40f130c","title":"Año de publicación","open":false} -->
							<div class="wp-block-greenshift-blocks-accordionitem gs-accordion-item gspb_accordionitem-gsbp-40f130c gsclose" id="gspb_accordionitem-gsbp-40f130c" itemscope>
								<div class="gs-accordion-item__title" aria-expanded="true" role="button" tabindex="0" aria-controls="gspb-accordion-item-content-gsbp-40f130c">
									<div class="gs-accordion-item__heading"><?php esc_html_e( 'Year of publication', 'elcano' )?></div>
									<meta itemprop="name" content="<?php esc_attr_e( 'Year of publication', 'elcano' )?>"/>
									<span class="iconfortoggle">
										<span class="gs-iconbefore"></span>
										<span class="gs-iconafter"></span>
									</span>
								</div>
								<div class="gs-accordion-item__content" itemscope id="gspb-accordion-item-content-gsbp-40f130c" aria-hidden="false">
									<div class="gs-accordion-item__text" itemprop="text">
										<?php foreach( $years_result as $year ): ?>
											<label for="year_<?php echo esc_attr( $year ) ?>">
												<input type="checkbox" name="year" value="<?php echo esc_attr( $year ) ?>" id="year_<?php echo esc_attr( $year ) ?>" data-label="<?php echo esc_attr( $year ) ?>">
												<?php echo $year ?>
											</label>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
							<!-- /wp:greenshift-blocks/accordionitem -->
						</div>
						<!-- /wp:greenshift-blocks/accordion -->
					</div>
					<!-- /wp:column -->

					<!-- wp:column {"width":"75%"} -->
					<div class="wp-block-column" style="flex-basis:75%" id="results-holder">

						<div id="filtros-resultados" class="the-archive the-archive--grid">
							<?php while( $query->have_posts() ): ?>
								<?php $query->the_post(); ?>
								<?php get_template_part( 'template-parts/archive/publications', get_post_type() ); ?>
							<?php endwhile; ?>
						</div>

						<button id="filters-load-more"><?php esc_html_e( 'Load more results', 'elcano' ) ?></button>

						<div id="results-overlay"></div>
					</div>
					<!-- /wp:column -->
				</div>
				<!-- /wp:columns -->
				<input type="hidden" name="post_type" value="<?php echo esc_attr( $post_type ) ?>">
				<input type="hidden" value="1" name="page">
				<input type="hidden" value="<?php echo esc_attr( $nonce ) ?>" name="nonce">
			</form>
		</div>
	</article>
</main>

<?php

echo do_blocks( ob_get_clean() );

get_footer();

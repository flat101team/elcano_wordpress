<?php
/**
 * Template Name: Template PDF
 * Template Post Type: analysis, work_document, poll, commentary, ciber
 *
 * @package Elcano
 */

get_header();

elcano_breadcrumb();
?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content/content', 'pdf' );
		endwhile;
		?>

	</main><!-- #main -->

<?php
get_footer();

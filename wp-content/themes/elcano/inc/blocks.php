<?php
/**
 * Functions for Custom Blocks.
 *
 * @package Elcano
 */


/**
 * A "Elcano" blocks category
 *
 * @param  mixed $block_categories
 * @param  mixed $editor_context
 * @return void
 */
function elcano_add_block_category( $block_categories, $editor_context ) {
	if ( ! empty( $editor_context->post ) ) {
		array_push(
			$block_categories,
			array(
				'slug'  => 'elcano',
				'title' => __( 'Elcano', 'elcano' ),
				'icon'  => null,
			)
		);
	}
	return $block_categories;
}
add_filter( 'block_categories_all', 'elcano_add_block_category', 10, 2 );

/**
 * Register Elcano Custom Blocks
 *
 * @return void
 */
function elcano_register_blocks() {

	// Members Block.
	acf_register_block_type(
		array(
			'name'            => 'elcano-members',
			'title'           => __( 'Members', 'elcano' ),
			'description'     => __( 'Members list.', 'elcano' ),
			'category'        => 'elcano',
			'icon'            => 'businessperson',
			'mode'            => 'auto',
			'keywords'        => array( 'elcano', 'staff', 'directorio' ),
			'render_template' => ELCANO_PATH . '/template-parts/blocks/members.php',
			'supports'        => array(
				'align' => false,
			),
			'example'         => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						// don't work have_rows() with data array
						// 'members' => array(
						// array(
						// 'name'        => 'José Juan Ruiz',
						// 'position'    => 'Presidente',
						// 'institution' => 'Real Instituto Elcano',
						// ),
						// ),
						'is_thumb' => true,
					),
				),
			),
		)
	);

	// Members with Photo Block.
	acf_register_block_type(
		array(
			'name'            => 'elcano-members-photo',
			'title'           => __( 'Members with Photo', 'elcano' ),
			'description'     => __( 'Members list with photo.', 'elcano' ),
			'category'        => 'elcano',
			'icon'            => 'businessperson',
			'mode'            => 'auto',
			'keywords'        => array( 'elcano', 'staff', 'directorio' ),
			'render_template' => ELCANO_PATH . '/template-parts/blocks/members-photo.php',
			'supports'        => array(
				'align' => false,
			),
			'example'         => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'is_thumb' => true,
					),
				),
			),
		)
	);

	// Biographies Block.
	acf_register_block_type(
		array(
			'name'            => 'elcano-biographies-list',
			'title'           => __( 'Biographies', 'elcano' ),
			'description'     => __( 'Select biographies to show in list.', 'elcano' ),
			'category'        => 'elcano',
			'icon'            => 'businessperson',
			'mode'            => 'auto',
			'keywords'        => array( 'elcano', 'staff', 'directorio' ),
			'render_template' => ELCANO_PATH . '/template-parts/blocks/biographies.php',
			'supports'        => array(
				'align' => false,
			),
			'example'         => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'is_thumb' => true,
					),
				),
			),
		)
	);

	// Logo Grid Block.
	acf_register_block_type(
		array(
			'name'            => 'elcano-logo-grid',
			'title'           => __( 'Logo Grid', 'elcano' ),
			'description'     => __( 'Grid of logos with title.', 'elcano' ),
			'category'        => 'elcano',
			'icon'            => 'grid-view',
			'mode'            => 'auto',
			'keywords'        => array( 'elcano', 'logos', 'grid' ),
			'render_template' => ELCANO_PATH . '/template-parts/blocks/logo-grid.php',
			'supports'        => array(
				'align' => false,
			),
			'example'         => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'is_thumb' => true,
					),
				),
			),
		)
	);

	// Timeline Block.
	acf_register_block_type(
		array(
			'name'            => 'elcano-timeline',
			'title'           => __( 'Timeline', 'elcano' ),
			'description'     => __( 'List of events.', 'elcano' ),
			'category'        => 'elcano',
			'icon'            => 'calendar',
			'mode'            => 'auto',
			'keywords'        => array( 'elcano', 'date', 'timeline', 'event' ),
			'render_template' => ELCANO_PATH . '/template-parts/blocks/timeline.php',
			'supports'        => array(
				'align' => false,
			),
			'example'         => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'is_thumb' => true,
					),
				),
			),
		)
	);
	acf_register_block_type(
		array(
			'name'            => 'elcano-timeline-hour',
			'title'           => __( 'Timeline With Hours', 'elcano' ),
			'description'     => __( 'List of events.', 'elcano' ),
			'category'        => 'elcano',
			'icon'            => 'calendar',
			'mode'            => 'auto',
			'keywords'        => array( 'elcano', 'date', 'timeline', 'event' ),
			'render_template' => ELCANO_PATH . '/template-parts/blocks/timeline-hour.php',
			'supports'        => array(
				'align' => false,
				'customClassName' => true,
				'className' => true,
			),
			'example'         => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'is_thumb' => true,
					),
				),
			),
		)
	);

	// Activities Block.
	acf_register_block_type(
		array(
			'name'            => 'elcano-activities',
			'title'           => __( 'Activities', 'elcano' ),
			'description'     => __( 'List of activities.', 'elcano' ),
			'category'        => 'elcano',
			'icon'            => 'excerpt-view',
			'mode'            => 'auto',
			'keywords'        => array( 'elcano', 'date', 'activity', 'event', 'list' ),
			'render_template' => ELCANO_PATH . '/template-parts/blocks/activities.php',
			'supports'        => array(
				'align' => false,
			),
			'example'         => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'is_thumb' => true,
					),
				),
			),
		)
	);

	// Publications Block.
	acf_register_block_type(
		array(
			'name'            => 'elcano-publications',
			'title'           => __( 'Publications', 'elcano' ),
			'description'     => __( 'List of publications.', 'elcano' ),
			'category'        => 'elcano',
			'icon'            => 'grid-view',
			'mode'            => 'auto',
			'keywords'        => array( 'elcano', 'publication', 'list' ),
			'render_template' => ELCANO_PATH . '/template-parts/blocks/publications.php',
			'supports'        => array(
				'align' => false,
			),
			'example'         => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'is_thumb' => true,
					),
				),
			),
		)
	);

}
add_action( 'acf/init', 'elcano_register_blocks' );

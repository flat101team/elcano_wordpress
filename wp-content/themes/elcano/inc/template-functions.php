<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Elcano
 */


/**
 * Include GTM Tag.
 */
function elcano_gtm_tag() {
	if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) {
		gtm4wp_the_gtm_tag();
	}
}
add_action( 'wp_body_open', 'elcano_gtm_tag' );

/**
 * Add authors to dataLayer
 *
 * @param  array $dataLayer
 * @return array
 */
function elcano_add_authors_to_datalayer( array $dataLayer ) {
	if ( $dataLayer ) {
		$authors = get_field( 'authors' );

		if ( $authors ) {
			foreach ( $authors as $key => $author ) {
				$dataLayer['authors'][ ++$key ] = $author->post_title;
			}
		}
	}
	return $dataLayer;
}
add_filter( 'gtm4wp_compile_datalayer', 'elcano_add_authors_to_datalayer', 20 );

/**
 * Include inline SVGs.
 */
function elcano_inline_svg() {
	include ELCANO_PATH . '/template-parts/inline-svg.php';
}
add_action( 'wp_body_open', 'elcano_inline_svg' );

// Remove menu items id attribute.
add_filter( 'nav_menu_item_id', '__return_empty_string' );

/**
 * Don't prepend page title 'Private:'
 *
 * @param  string $prepend
 * @return string
 */
function elcano_dont_show_private_title( $prepend ) {
	return '%s';
}
add_filter( 'private_title_format', 'elcano_dont_show_private_title' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function elcano_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds class for header scroll control.
	if ( ! is_single() ) {
		$classes[] = 'no-single';
	}

	//Adds class for filters pages
	if( is_page_template( 'template-filtros.php' ) || is_post_type_archive() ) {
		$classes[] = 'filters-template';
	}

	return $classes;
}
add_filter( 'body_class', 'elcano_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function elcano_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'elcano_pingback_header' );

/**
 * Replace menu links # with tabindex.
 *
 * Links with tabindex can focus (for keyboard tab navigation) but don't show cursor pointer
 *
 * @param string $menu_item item HTML
 * @return string item HTML
 */
function elcano_menu_hash_to_tabindex( $menu_item ) {
	return str_replace(
		array( '<a>', 'href="#"' ),
		array( '<a href="#">', 'tabindex="0"' ),
		$menu_item
	);
}
add_filter( 'walker_nav_menu_start_el', 'elcano_menu_hash_to_tabindex', 999 );

/**
 * Customize search form
 *
 * @param  string $form
 * @return string
 */
function elcano_custom_search_form( $form, $args ) {

	// Change placeholder.
	$form = str_replace(
		'placeholder="' . esc_attr_x( 'Search &hellip;', 'placeholder' ) . '"',
		'placeholder="' . esc_attr_x( 'Find here what you are looking for &hellip;', 'placeholder', 'elcano' ) . '"',
		$form
	);
	// Disable autocomplete.
	$form = str_replace( 'class="search-field"', 'class="search-field" autocomplete="off"', $form );

	// Show label on search results page.
	if ( isset( $args['page'] ) && 'search' === $args['page'] ) {
		$form = str_replace( ' class="screen-reader-text"', '', $form );
	}

	return $form;

}
add_filter( 'get_search_form', 'elcano_custom_search_form', 10, 2 );

/**
 * Customize breadcrumb
 *
 * @param  array $links
 * @return array
 */
function elcano_custom_breadcrumb( $links ) {

	if ( is_category() || is_tag() ) {

		$breadcrumb = array();

		if ( elcano_is_blog() ) {

			// Add blog segment.
			$blog_page    = get_option( 'page_for_posts' );
			$breadcrumb[] = array(
				'url'  => get_permalink( $blog_page ),
				'text' => get_the_title( $blog_page ),
				'id'   => $blog_page,
			);

		} else {

			// Add themes segment.
			$breadcrumb[] = array(
				'url'  => '',
				'text' => __( 'Themes', 'elcano' ),
			);
		}

		array_splice( $links, 1, 0, $breadcrumb );
	}

	return $links;

}
add_filter( 'wpseo_breadcrumb_links', 'elcano_custom_breadcrumb' );

/**
 * Don't link private pages in breadcrumb
 *
 * @param  array $links
 * @return array
 */
function elcano_breadcrumb_dont_link_private( $links ) {

	foreach ( $links as $key => $link ) {
		if ( isset( $link['id'] ) && 'private' === get_post_status( $link['id'] ) ) {
			$links[ $key ]['url'] = '';
		}
	}

	return $links;

}
add_filter( 'wpseo_breadcrumb_links', 'elcano_breadcrumb_dont_link_private', 20 );

/**
 * Modify the posts navigation WHERE clause
 * to include our acceptable post types
 *
 * @param String $where - Generated WHERE SQL Clause
 * @return String
 */
function elcano_post_navigation_where( $where ) {

	global $wpdb, $post;

	// Return if no post.
	if ( empty( $post ) ) {
			return $where;
	}

	$search = sprintf( "p.post_type = '%s'", $post->post_type );

	// Return if $where already modified.
	if ( false === strpos( $where, $search ) ) {
		return $where;
	}

	// Blog or web content CPTs.
	$post_types = (array) elcano_current_post_type( $post );

	$placeholders = array_fill( 0, count( $post_types ), '%s' );
	$placeholders = implode( ',', $placeholders );

	$replace = $wpdb->prepare( "p.post_type IN ($placeholders)", $post_types );

	return str_replace( $search, $replace, $where );

}
add_filter( 'get_next_post_where', 'elcano_post_navigation_where' );
add_filter( 'get_previous_post_where', 'elcano_post_navigation_where' );

/**
 * Fix WPML prev/next post
 *
 * WPML add a JOIN rule to filter by the same post_type
 * But we need prev/next between all post_types
 *
 * @param  mixed $join
 * @param  mixed $in_same_term
 * @param  mixed $excluded_terms
 * @param  mixed $taxonomy
 * @param  mixed $post
 * @return void
 */
function elcano_wpml_fix_adjacent_join( $join, $in_same_term, $excluded_terms, $taxonomy, $post ) {

	return str_replace(
		" AND wpml_translations.element_type = 'post_{$post->post_type}'",
		" AND wpml_translations.element_type LIKE 'post_%'",
		$join
	);

}
add_filter( 'get_next_post_join', 'elcano_wpml_fix_adjacent_join', 10, 5 );
add_filter( 'get_previous_post_join', 'elcano_wpml_fix_adjacent_join', 10, 5 );

/**
 * Insert podcast related in podcast content
 *
 * Insert widget after first paragraph.
 *
 * @param  string $content
 * @return string
 */
function elcano_podcast_content_related( $content ) {

	if ( is_singular( 'podcast' ) ) {

		$recent_posts = wp_get_recent_posts(
			array(
				'post__not_in'     => array( get_the_ID() ),
				'post_type'        => 'podcast',
				'post_status'      => 'publish',
				'numberposts'      => 3,
				'suppress_filters' => false,
			),
			OBJECT
		);

		if ( is_array( $recent_posts ) ) {

			$insert = '<div class="widget last-posts alignleft"><p class="widget-title widget-title--big">' . __( 'Latest Podcasts', 'elcano' ) . '</p>';

			foreach ( $recent_posts as $recent_post ) {
				$insert .= sprintf(
					'<article class="post-related"><a href="%s" rel="bookmark">%s</a></article>',
					esc_url( get_the_permalink( $recent_post ) ),
					sprintf( '<p class="post-title">%s</p>', get_the_title( $recent_post ) ),
				);
			}

			$insert .= '</div>';

			$content = substr_replace( $content, $insert, strpos( $content, '</p>' ) + 4, 0 );
		}
	}

	return $content;

}
add_filter( 'the_content', 'elcano_podcast_content_related' );

/**
 * Filter Search Results
 *
 * @param  mixed $query
 * @return void
 */
function elcano_filter_search_results( $query ) {

	if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {

		// Categories filter (managed by WP).

		// Tags filter (managed by WP).

		// Filter post types.
		if ( ! empty( $_GET['types'] ) ) {
			$query->set( 'post_type', explode( ',', $_GET['types'] ) );
		} else {
			$query->set( 'post_type', array_merge( array( 'post' ), elcano_current_post_type( 'web' ) ) );
		}

		// Filter authors.
		if ( ! empty( $_GET['authors'] ) ) {
			// Secure only allow integers.
			$authors = array_map( 'intval', explode( ',', $_GET['authors'] ) );

			$meta_query = array(
				'relation' => 'OR',
			);

			foreach ( $authors as $author ) {
				$meta_query[] = array(
					'key'     => 'authors',           // Name of custom field
					'value'   => '"' . $author . '"', // Matches exactly "ID" (with quotes)
					'compare' => 'LIKE',
				);
				$meta_query[] = array(
					'key'     => 'participants',      // Name of custom field (in activities)
					'value'   => '"' . $author . '"', // Matches exactly "ID" (with quotes)
					'compare' => 'LIKE',
				);
			}

			$query->set( 'meta_query', $meta_query );
		}

		// Filter by dates.
		$regex_date = '/^\d{2}-\d{2}-\d{4}$/';

		if ( isset( $_GET['types'] ) && 'activity' === $_GET['types'] ) {

			$meta_query = array();

			if ( ! empty( $_GET['from'] ) && preg_match( $regex_date, $_GET['from'] ) ) {
				$date = new DateTime( $_GET['from'] );

				$meta_query[] = array(
					'key'     => 'date_start',
					'value'   => $date->format( 'Y-m-d' ),
					'compare' => '>=',
					'type'    => 'DATE',
				);
			}

			if ( ! empty( $_GET['to'] ) && preg_match( $regex_date, $_GET['to'] ) ) {
				$date = new DateTime( $_GET['to'] );

				$meta_query[] = array(
					'key'     => 'date_start',
					'value'   => $date->format( 'Y-m-d' ),
					'compare' => '<',
					'type'    => 'DATE',
				);
			}

			$query->set( 'meta_query', $meta_query );
			$query->set( 'meta_key', 'date_start' );
			$query->set( 'orderby', 'meta_value' );
			$query->set( 'order', 'DESC' );

		} else {

			$date_query = array();

			if ( ! empty( $_GET['from'] ) && preg_match( $regex_date, $_GET['from'] ) ) {
				$date_query['after'] = $_GET['from'];
			}

			if ( ! empty( $_GET['to'] ) && preg_match( $regex_date, $_GET['to'] ) ) {
				$date_query['before'] = $_GET['to'];
			}

			if ( ! empty( $date_query ) ) {
				$date_query['inclusive'] = true;
				$query->set( 'date_query', $date_query );
			}
		}
	}
}
add_action( 'pre_get_posts', 'elcano_filter_search_results' );

/**
 * List Categories Filter
 *
 * Filter wp_list_categories and change classes if taxonomy isn't category.
 *
 * 'cat-item'    => 'term-item'
 * 'current-cat' => 'current-term'
 *
 * @param  string $output
 * @param  array  $args
 * @return string
 */
function elcano_list_terms( $output, $args ) {

	if ( isset( $args['taxonomy'] ) && 'category' !== $args['taxonomy'] ) {
		$output = str_replace( 'cat-item', 'term-item', $output );
		$output = str_replace( 'current-cat', 'current-term', $output );
	}

	return $output;

}
add_filter( 'wp_list_categories', 'elcano_list_terms', 10, 2 );

/**
 * Enqueue modal script for gallery/image blocks
 *
 * @param  string $block_content
 * @param  array  $block
 * @return string
 */
function elcano_enqueue_modal( $block_content, $block ) {

	if ( 'core/gallery' === $block['blockName']
		&& isset( $block['attrs']['linkTo'] )
		&& in_array( $block['attrs']['linkTo'], array( 'post', 'file' ) )
	) {

		elcano_append_dependency( 'elcano-theme', 'elcano-modal' );

	} elseif ( 'core/image' === $block['blockName']
		&& isset( $block['attrs']['linkDestination'] )
		&& in_array( $block['attrs']['linkDestination'], array( 'media', 'attachment' ) )
	) {

		$block_content = str_replace( '<a ', '<a class="js-image-modal" ', $block_content );

		elcano_append_dependency( 'elcano-theme', 'elcano-modal' );
	}

	return $block_content;

}
add_filter( 'render_block', 'elcano_enqueue_modal', 10, 2 );

/**
 * Fix pagination first page link
 *
 * @param  string $link
 * @return string
 */
function elcano_fix_first_page_link( $link ) {

	return str_replace( '/page/1/', '/', $link );

}
add_filter( 'paginate_links', 'elcano_fix_first_page_link' );

/**
 * Fix SEO header links
 *
 * Fix secondary language missing pagination.
 * Common url fixes.
 *
 * @param  mixed $url
 * @return void
 */
function elcano_fix_wpml_seo_link( $url ) {

	if ( '' === $url ) {
		return $url;
	}

	$url = urldecode( $url );

	if ( is_archive() ) {
		$languages = (array) apply_filters( 'wpml_active_languages', null, 'skip_missing=1' );
		$current   = current( wp_list_filter( $languages, array( 'active' => 1 ) ) );

		if ( $current ) {
			preg_match( '~/page/\d+/$~', $url, $page );
			$page = count( $page ) ? current( $page ) : '/';

			$url = untrailingslashit( $current['url'] ) . $page;
		}
	}

	return elcano_fix_url( $url );

}
add_filter( 'wpseo_canonical', 'elcano_fix_wpml_seo_link' );
add_filter( 'wpseo_opengraph_url', 'elcano_fix_wpml_seo_link' );
add_filter( 'wpseo_adjacent_rel_url', 'elcano_fix_wpml_seo_link' );

/**
 * Fix alternate hreflang link metas
 *
 * Remove if only one language.
 * Common url fixes.
 *
 * @param  array $hreflang_items
 * @return array
 */
function elcano_fix_hreflangs( $hreflang_items ) {

	// If only one language remove alternate hreflang metas
	if ( 1 === count( $hreflang_items ) || ( 2 === count( $hreflang_items ) && isset( $hreflang_items['x-default'] ) ) ) {
		return array();
	}

	$default_lang = apply_filters( 'wpml_default_language', null );

	foreach ( $hreflang_items as $lang => $url ) {
		$hreflang_items[ $lang ] = elcano_fix_url( $url, $lang === 'x-default' ? $default_lang : $lang );
	}

	return $hreflang_items;

}
add_filter( 'wpml_hreflangs', 'elcano_fix_hreflangs' );

/**
 * Change page links with CPT archive link
 *
 * @param  mixed $permalink
 * @param  mixed $page_id
 * @return void
 */
function elcano_page_link_to_cpt_archive( $permalink, $page_id ) {

	$page_ids = array(
		'activity'          => get_field( 'page_for_activity', 'option' ),
		'archive'           => get_field( 'page_for_archive', 'option' ),
		'brussels_activity' => get_field( 'page_for_brussels_activity', 'option' ),
		'job'               => get_field( 'page_for_job', 'option' ),
		'press_release'     => get_field( 'page_for_press_release', 'option' ),
		'project'           => get_field( 'page_for_project', 'option' ),
		'special'           => get_field( 'page_for_special', 'option' ),
	);

	$cpt = array_search( $page_id, $page_ids, true );

	return $cpt ? get_post_type_archive_link( $cpt ) : $permalink;

}
add_filter( 'page_link', 'elcano_page_link_to_cpt_archive', 10, 2 );

/**
 * Redirect malformed language urls
 *
 * Redirect missing translations to original page.
 * Redirect bad language url e.g.:
 * - //elcano.com/en/biografias/nombre/ => //elcano.com/biografias/nombre/
 * - //elcano.com/en/activities/slug-en-espanol => //elcano.com/en/activities/english-slug
 *
 * @return void
 */
function elcano_redirect_bad_lang_urls() {

	global $wp;

	if ( is_404() ) {
		return;
	}

	$languages   = (array) apply_filters( 'wpml_active_languages', null, 'skip_missing=0' );
	$active_lang = current( wp_list_filter( $languages, array( 'active' => 1 ) ) );
	$current_url = trailingslashit( home_url( add_query_arg( array(), $wp->request ) ) );

	// Current lang is missing redirect to original
	if ( isset( $active_lang['missing'] ) && $active_lang['missing'] ) {

		$original = current( wp_list_filter( $languages, array( 'missing' => 0 ) ) );
		$redirect = elcano_fix_url( $original['url'], $original['code'] );

		// Current url don't match with current lang url
	} elseif (
		elcano_fix_url( $active_lang['url'] ) !== $current_url
		&& ! is_front_page()         // Exclude home
		&& ! is_feed()               // Exclude feed
		&& ! is_search()             // Exclude search
		&& ! is_paged()              // Exclude pagination
		&& ! get_query_var( 'year' ) // Exclude year filter (press_release)
	) {

		$redirect = elcano_fix_url( $active_lang['url'] );
	}
    if ( is_preview() ) {
        return;
    }
	if ( isset( $redirect ) ) {
		wp_redirect( $redirect );
		exit;
	}

}
add_action( 'template_redirect', 'elcano_redirect_bad_lang_urls' );

<?php
/**
 * Elcano Categories
 *
 * @package Elcano
 */


/**
 * Add rewrite rules for blog terms
 *
 * For blog category/tag ensure is set query post_type 'post'.
 *
 * @return void
 */
function elcano_blog_terms_rewrite() {

	$blog = get_post_field( 'post_name', get_option( 'page_for_posts' ) );
	$cat  = get_option( 'category_base' ) ?? 'category';
	$tag  = get_option( 'tag_base' ) ?? 'tag';

	if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

		$languages = apply_filters( 'wpml_active_languages', null, 'skip_missing=0' );

		foreach ( $languages as $lang ) {
			$cat = apply_filters( 'wpml_translate_single_string', $cat, 'WordPress', 'URL category tax slug', $lang['code'] );
			$tag = apply_filters( 'wpml_translate_single_string', $tag, 'WordPress', 'URL post_tag tax slug', $lang['code'] );

			add_rewrite_rule( "$blog/$cat/([^/]+)/?$", 'index.php?post_type=post&category_name=$matches[1]', 'top' );
			add_rewrite_rule( "$blog/$cat/([^/]+)/page/?([0-9]{1,})?/?$", 'index.php?post_type=post&category_name=$matches[1]&paged=$matches[2]', 'top' );
			add_rewrite_rule( "$blog/$tag/([^/]+)/?$", 'index.php?post_type=post&tag=$matches[1]', 'top' );
			add_rewrite_rule( "$blog/$tag/([^/]+)/page/?([0-9]{1,})?/?$", 'index.php?post_type=post&tag=$matches[1]&paged=$matches[2]', 'top' );
		}
	} else {

		add_rewrite_rule( "$blog/$cat/([^/]+)/?$", 'index.php?post_type=post&category_name=$matches[1]', 'top' );
		add_rewrite_rule( "$blog/$cat/([^/]+)/page/?([0-9]{1,})?/?$", 'index.php?post_type=post&category_name=$matches[1]&paged=$matches[2]', 'top' );
		add_rewrite_rule( "$blog/$tag/([^/]+)/?$", 'index.php?post_type=post&tag=$matches[1]', 'top' );
		add_rewrite_rule( "$blog/$tag/([^/]+)/page/?([0-9]{1,})?/?$", 'index.php?post_type=post&tag=$matches[1]&paged=$matches[2]', 'top' );
	}

}
add_action( 'init', 'elcano_blog_terms_rewrite' );

/**
 * Get post_types
 *
 * @param  mixed $post
 * @return string|array
 */
function elcano_current_post_type( $post = null ) {

	$current = is_string( $post ) ? $post : get_post_type( $post );

	if ( 'post' === $current ) {
		$post_types = 'post';
	} else {
		$post_types = array(
			'analysis',
			'work_document',
			'report',
			'poll',
			'special',
			'monograph',
			'policy_paper',
			'commentary',
			'podcast',
			'video',
			'archive',
			'newsletter',
			'news_on_net',
			'ciber',
			'magazine',
			'press_release',
			'activity',
			// 'job',
			// 'biography',
			'inside_spain',
			'project',
			'brussels_activity',
		);
	}

	return $post_types;

}

/**
 * Term Archive Post Types
 *
 * Category/Tag archive (if empty post_type) set query post types to all web content CPTs.
 *
 * @param  WP_Query $query
 * @return void
 */
function elcano_term_query_post_types( $query ) {

	if ( ! is_admin() && $query->is_main_query()
		&& ! $query->is_search()
		&& ! $query->get( 'post_type' )
		&& ( $query->is_category() || $query->is_tag() )
	) {
		$query->set( 'post_type', elcano_current_post_type( 'web' ) );
	}

}
add_action( 'pre_get_posts', 'elcano_term_query_post_types' );

/**
 * Blog term link
 *
 * @param  string      $link
 * @param  mixed       $taxonomy
 * @param  string|null $language_code
 * @return string
 */
function elcano_blog_term_link( $link, $taxonomy, $language_code = null ) {

	$blog_slug = get_post_field( 'post_name', get_option( 'page_for_posts' ) );

	if ( 'post_tag' === $taxonomy ) {
		$term_slug = get_option( 'tag_base' ) ?? 'tag';
		$term_slug = apply_filters( 'wpml_translate_single_string', $term_slug, 'WordPress', 'URL post_tag tax slug', $language_code );
	} else {
		$term_slug = get_option( 'category_base' ) ?? 'category';
		$term_slug = apply_filters( 'wpml_translate_single_string', $term_slug, 'WordPress', 'URL category tax slug', $language_code );
	}

	return str_replace( "/$term_slug/", "/$blog_slug/$term_slug/", $link );

}

/**
 * Filter blog term link
 *
 * @param  string $link
 * @param  mixed  $term
 * @param  mixed  $taxonomy
 * @return string
 */
function elcano_filter_blog_term_link( $link, $term, $taxonomy ) {

	return elcano_blog_term_link( $link, $taxonomy );

}

/**
 * Return if is blog
 *
 * For empty archives get_post_type() is false so also search for 'blog' in url
 *
 * @return bool
 */
function elcano_is_blog() {

	global $wp;

	if ( 'post' === get_post_type() ) {
		return true;
	}

	if ( ! get_post_type() ) {
		$blog = get_post_field( 'post_name', get_option( 'page_for_posts' ) );

		return 0 === strpos( $wp->request, "$blog/" );
	}

	return false;

}

/**
 * Returh if is a blog tag or category
 *
 * @return bool
 */
function elcano_is_blog_term() {

	return ( is_tag() || is_category() ) && elcano_is_blog();

}

/**
 * Get Taxonomy Terms for Post Types
 *
 * @param  string          $taxonomy
 * @param  string|string[] $post_types
 * @return array
 */
function elcano_get_terms_by_post_type( $taxonomy = 'post_tag', $post_types = 'post' ) {

	global $wpdb;

	$post_types = (array) $post_types;

	// Placeholders.
	$types_ph = implode( ', ', array_fill( 0, count( $post_types ), '%s' ) );

	if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

		// Filter by current language
		$query = $wpdb->prepare(
			"SELECT t.*, COUNT(*) AS `count`
			FROM $wpdb->terms AS t
				INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id
				INNER JOIN $wpdb->term_relationships AS r ON r.term_taxonomy_id = tt.term_taxonomy_id
				INNER JOIN $wpdb->posts AS p ON p.ID = r.object_id
				JOIN {$wpdb->prefix}icl_translations wpml_translations ON wpml_translations.element_id = t.term_id
			WHERE p.post_type IN($types_ph)
				AND tt.taxonomy = %s
				AND wpml_translations.element_type = %s
				AND (language_code = %s OR 0)
			GROUP BY t.term_id",
			array_merge( $post_types, array( $taxonomy, "tax_$taxonomy", ICL_LANGUAGE_CODE ) )
		);

	} else {

		$query = $wpdb->prepare(
			"SELECT t.*, COUNT(*) AS `count`
			FROM $wpdb->terms AS t
				INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id
				INNER JOIN $wpdb->term_relationships AS r ON r.term_taxonomy_id = tt.term_taxonomy_id
				INNER JOIN $wpdb->posts AS p ON p.ID = r.object_id
			WHERE p.post_type IN($types_ph)
				AND tt.taxonomy = %s
			GROUP BY t.term_id",
			array_merge( $post_types, array( $taxonomy ) )
		);
	}

	return $wpdb->get_results( $query );

}

/**
 * ACF Relationship Field filter taxonomies
 *
 * Only allow filter by tags & categories
 *
 * @return void
 */
function elcano_relation_taxonomies( $field ) {

	if ( 'field_61defa42ef057' === $field['key'] ) {

		// Biographies Block
		add_filter(
			'acf/get_taxonomies',
			function( $taxonomies ) {
				return array_intersect( $taxonomies, array( 'bio_group' ) );
			}
		);

	} elseif ( 'relationship' === $field['type'] ) {

		// General relationship
		add_filter(
			'acf/get_taxonomies',
			function( $taxonomies ) {
				return array_intersect( $taxonomies, array( 'category', 'post_tag' ) );
			}
		);
	}

	return $field;

}
add_filter( 'acf/prepare_field', 'elcano_relation_taxonomies' );

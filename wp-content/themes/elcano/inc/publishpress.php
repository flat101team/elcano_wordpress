<?php
/**
 * Elcano Customize PublishPress
 *
 * @package Elcano
 */

/**
 * Show only available posts statuses
 *
 * @param  array   $statuses
 * @param  WP_Post $post
 * @return array
 */
function elcano_custom_post_statuses( $statuses, $post ) {

	$current_user = wp_get_current_user();

	if ( $post ) {
		$post_type = get_post_type( $post );
	} else {
		$post_type = isset( $_GET['post_type'] ) ? $_GET['post_type'] : null;
	}

	// If not Biography don't have "Editor Review" status,
	if ( 'biography' !== $post_type ) {
		foreach ( $statuses as $key => $status ) {
			if ( 'manager_review' === $status->slug ) {
				unset( $statuses[ $key ] );
				break;
			}
		}
	}

	// Elcano "Autor" role.
	if ( ELCANO_ROLE_AUTHOR === $current_user->roles[0] ) {
		foreach ( $statuses as $key => $status ) {
			if ( 'pending' === $status->slug || ( 'editor_review' === $status->slug && 'biography' === $post_type ) ) {
				unset( $statuses[ $key ] );
			}
		}
	}

	return $statuses;

}
add_filter( 'pp_custom_status_list', 'elcano_custom_post_statuses', 10, 2 );


/**
 * Filter "pending" status
 *
 * Downgrade "Pending for Review" status for every role.
 *
 * @param array $data                An array of slashed, sanitized, and processed post data.
 * @return array
 */
function elcano_filter_pending_post_status( $data ) {

	if ( 'pending' === $data['post_status'] ) {
		$current_user = wp_get_current_user();

		if ( ELCANO_ROLE_AUTHOR === $current_user->roles[0] ) {
			$data['post_status'] = 'biography' === $data['post_type'] ? 'manager_review' : 'editor_review';
		}
	}

	return $data;

}
/*add_filter( 'wp_insert_post_data', 'elcano_filter_pending_post_status', 99 );*/


// Hide PublishPress wp-admin header
function elcano_publishpress_remove_header( $settings ) {
	$settings['publishpress']['screens'] = array();
	return $settings;
}
add_filter( 'pp_version_notice_top_notice_settings', 'elcano_publishpress_remove_header', 999 );

// Hide PublishPress wp-admin footer
add_filter( 'publishpress_show_footer', '__return_false' );

// Hide PublishPress wp-admin "go pro" submenu
function elcano_remove_publishpress_gopro() {
	remove_submenu_page( 'pp-calendar', 'pp-calendar-menu-upgrade-link' );
}
add_action( 'admin_menu', 'elcano_remove_publishpress_gopro', 999 );

// Disable PublishPress review admin notices
function elcano_remove_publishpress_review( $val, $object_id, $meta_key ) {
	return '_publishpress_wp_reviews_already_did' === $meta_key ? 1 : $val;
}
add_filter( 'get_user_metadata', 'elcano_remove_publishpress_review', 10, 3 );

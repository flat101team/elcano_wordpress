<?php
/**
 * Functions for wp-admin.
 *
 * @package Elcano
 */

// String for nonce & transient.
define( 'ELCANO_ACTIVATE', 'elcano_theme_activate' );

/**
 * Add theme options page
 */
function elcano_add_admin_options_page() {
	acf_add_options_page(
		array(
			'page_title'  => __( 'Elcano Settings', 'elcano' ),
			'menu_title'  => __( 'Elcano', 'elcano' ),
			'menu_slug'   => 'elcano',
			'parent_slug' => 'options-general.php',
			'position'    => '0',
			'capability'  => 'manage_options',
			'autoload'    => true,
		)
	);
}
add_action( 'acf/init', 'elcano_add_admin_options_page' );

/**
 * Output theme activation link
 *
 * @return void
 */
function elcano_theme_activation_link( $field ) {

	if ( 'message' !== $field['type'] ) {
		return;
	}

	if ( isset( $field['wrapper']['id'] ) && 'elcano_activation' === $field['wrapper']['id'] ) {

		printf(
			'<a href="%s" class="button">%s</a>',
			esc_url( wp_nonce_url( admin_url( 'options-general.php?elcano_activate=1' ), ELCANO_ACTIVATE ) ),
			__( 'Run Theme Activation', 'elcano' )
		);
	}
}
add_action( 'acf/render_field', 'elcano_theme_activation_link' );

/**
 * Run theme activation or show notification on admin init
 *
 * @return void
 */
function elcano_theme_activation() {

	if ( isset( $_GET['elcano_activate'], $_GET['_wpnonce'] )
		&& wp_verify_nonce( $_GET['_wpnonce'], ELCANO_ACTIVATE )
		&& current_user_can( 'manage_options' )
	) {

		do_action( 'elcano_theme_activation' );

		// Set a transient to show a success message and redirect.
		set_transient( ELCANO_ACTIVATE, 'yes', MINUTE_IN_SECONDS );
		wp_safe_redirect( admin_url( 'options-general.php?page=elcano' ) );
		die;

	} elseif ( get_transient( ELCANO_ACTIVATE ) ) {

		add_action(
			'admin_notices',
			function () {
				printf(
					'<div class="notice notice-success is-dismissible"><p><strong>%s</strong> %s</p></div>',
					__( 'Elcano Theme activation hooks executed.', 'elcano' ),
					__( "Roles and Capabilities have been reset to Elcano's defaults.", 'elcano' )
				);
			}
		);

		delete_transient( ELCANO_ACTIVATE );

	}

}
add_action( 'admin_init', 'elcano_theme_activation' );

/**
 * Show Elcano pages in pages list
 *
 * @param  array   $post_states
 * @param  WP_Post $post
 * @return array
 */
function elcano_pages_show_in_list( $post_states, $post ) {

	if ( 'page' === $post->post_type ) {

		switch ( $post->ID ) {
			case get_field( 'page_for_activity', 'option' ):
				$post_states['elcano_page'] = sprintf( __( 'Page of %s', 'elcano' ), strtolower( __( 'Activities', 'elcano' ) ) );
				break;

			case get_field( 'page_for_project', 'option' ):
				$post_states['elcano_page'] = sprintf( __( 'Page of %s', 'elcano' ), strtolower( __( 'Projects', 'elcano' ) ) );
				break;

			case get_field( 'page_for_archive', 'option' ):
				$post_states['elcano_page'] = sprintf( __( 'Page of %s', 'elcano' ), strtolower( __( 'Archives', 'elcano' ) ) );
				break;

			case get_field( 'page_for_job', 'option' ):
				$post_states['elcano_page'] = sprintf( __( 'Page of %s', 'elcano' ), strtolower( __( 'Job Offers', 'elcano' ) ) );
				break;

			case get_field( 'page_for_press_release', 'option' ):
				$post_states['elcano_page'] = sprintf( __( 'Page of %s', 'elcano' ), strtolower( __( 'Press Releases', 'elcano' ) ) );
				break;

			case get_field( 'page_for_special', 'option' ):
				$post_states['elcano_page'] = sprintf( __( 'Page of %s', 'elcano' ), strtolower( __( 'Web specials', 'elcano' ) ) );
				break;

			case get_field( 'page_for_team', 'option' ):
				$post_states['elcano_page'] = sprintf( __( 'Page of %s', 'elcano' ), strtolower( __( 'Team', 'elcano' ) ) );
				break;
		}
	}

	return $post_states;

}
add_filter( 'display_post_states', 'elcano_pages_show_in_list', 10, 2 );

/**
 * TinyMCE add "Styles" drop-down
 *
 * @param  array $buttons
 * @return array
 */
function elcano_mce_editor_buttons( $buttons ) {

	unset( $buttons[ array_search( 'fullscreen', $buttons ) ] );
	$buttons[] = 'styleselect';

	return $buttons;

}
add_filter( 'teeny_mce_buttons', 'elcano_mce_editor_buttons' );

/**
 * TinyMCE add styles/classes to the "Styles" drop-down
 *
 * @param  array $settings
 * @return array
 */
function elcano_mce_style_formats( $settings ) {

	$style_formats = array(
		array(
			'title'    => __( 'Button Link', 'elcano' ),
			'selector' => 'a',
			'classes'  => 'btn',
		),
		array(
			'title'    => __( 'Download Link', 'elcano' ),
			'selector' => 'a',
			'classes'  => 'icon-download',
		),
	);

	$settings['style_formats'] = json_encode( $style_formats );

	return $settings;

}
add_filter( 'tiny_mce_before_init', 'elcano_mce_style_formats' );

/**
 * WPML disable display as translated
 *
 * @param  array $args
 * @return array
 */
function elcano_wpml_disable_display_as_translated( $args ) {
	add_filter( 'wpml_should_use_display_as_translated_snippet', '__return_false', PHP_INT_MAX );
	return $args;
}
// Authors/Participants show biographies only in current language
add_filter( 'acf/fields/post_object/query/name=authors', 'elcano_wpml_disable_display_as_translated' );
add_filter( 'acf/fields/post_object/query/name=participants', 'elcano_wpml_disable_display_as_translated' );

/**
 * Return array of strings
 *
 * @param  mixed $value
 * @return mixed
 */
function elcano_array_of_strings( $value ) {

	if ( ! $value ) {
		return $value;
	}

	if ( is_serialized( $value ) ) {
		$value = unserialize( $value );
	}
	$value = array_map( 'strval', (array) $value );

	return $value;
}
// Authors/Participants ensure correct format
add_filter( 'sanitize_post_meta_authors', 'elcano_array_of_strings' );
add_filter( 'sanitize_post_meta_participants', 'elcano_array_of_strings' );

/**
 * Fix WPML duplicate ACF IDs as integer
 *
 * WPML_Post_Duplication->duplicate_custom_fields() uses $wpdb->query "INSERT INTO..."
 * don't captured by post_meta sanitize.
 *
 * Filter meta value translation.
 *
 * @param  mixed  $value
 * @param  string $lang
 * @param  array  $data
 * @return mixed
 */
function elcano_fix_wpml_duplicate_post_meta( $value, $lang, $data ) {

	if ( 'custom_field' === $data['context'] && in_array( $data['key'], array( 'authors', 'participants' ) ) ) {
		$value = elcano_array_of_strings( $value );
	}

	return $value;
}
add_filter( 'wpml_duplicate_generic_string', 'elcano_fix_wpml_duplicate_post_meta', 20, 3 );


/**
 * Disable non editable field
 *
 * Non translatable fields in a post tranlation don't apply changes.
 * For better UI disable this fields.
 *
 * @param  mixed $field
 * @return void
 */
function elcano_disable_acf_non_translatable_field( $field ) {

	if ( (int) $field['wpml_cf_preferences'] <= 1 ) {
		$field['disabled'] = 1;
	}

	return $field;
}

/**
 * If edit a post translation add fitler to disable non translatable fields
 *
 * @return void
 */
function elcano_disable_fields_on_translations() {

	$screen = get_current_screen();

	if ( $screen->is_block_editor && get_the_ID() && ! elcano_is_original() ) {
		//add_filter( 'acf/prepare_field', 'elcano_disable_acf_non_translatable_field' );
	}
}
add_action( 'admin_head', 'elcano_disable_fields_on_translations' );


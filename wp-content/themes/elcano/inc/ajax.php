<?php
/**
 * AJAX functions.
 *
 * @package Elcano
 */

/**
 * Filtered publications AJAX response
 */
function elcano_ajax_filtered_publications() {

	global $wp_post_types;

	$nonce = $_POST['nonce'] ?: '';

	if ( ! wp_verify_nonce( $nonce, 'form-filtros' ) ) {
		echo json_encode( array( 'error' => 1 ) );
		die();
	}

	$post_type      = $_REQUEST['post_type'] ?: array();
	$category       = $_REQUEST['category'] ?: array();
	$year           = $_REQUEST['year'] ?: array();
	$page           = (int)$_REQUEST['page'] ?: 1;
	$posts_per_page = $page == 1 ? 9 : 6;

	if( empty( $post_type[0] ) ) {
		$post_type = array_keys( wp_filter_object_list( $wp_post_types, array( 'publicly_queryable' => true ), 'and', 'label' ) );

		$exclude_cpt = array( 'activity', 'brussels_activity', 'archive', 'biography', 'ciber', 'attachment', 'inside_spain', 'press_release', 'job', 'post', 'project', 'podcast', 'magazine', 'news_on_net', 'scientific_council', 'video', 'location' );

		$post_type = array_diff( $post_type, $exclude_cpt );
	}

	$prev_args = array(
		'posts_per_page' => $posts_per_page,
		'post_status'    => 'publish',
		'post_type'      => $post_type,
		'paged'          => $page,
		'orderby'        => 'date',
		'order'          => 'DESC',
		'offset'         => $page <= 2 ? ( $page - 1 ) * 9 : ( $page - 1 ) * $posts_per_page + 3,
	);

	if( !empty( $category[0] ) ) {
		$prev_args['tax_query'] = array(
			array(
				'taxonomy' => 'category',
				'field'    => 'term_taxonomy_id',
				'terms'    => $category
			)
		);
	}

	if( !empty( $year[0] ) ) {

		$years_arr = array_map( function( $elem ) {
			return array( 'year' => $elem );
		}, $year);

		$prev_args['date_query'] = array(
			'relation' => 'OR',
		);

		$prev_args['date_query'] = $prev_args['date_query'] + $years_arr;
	}

	$query = new WP_Query( $prev_args );

	$results = '';
	if ( $query->have_posts() ) {
		ob_start();
		while ( $query->have_posts() ) {
			$query->the_post();
			get_template_part( 'template-parts/archive/publications', get_post_type() );
		}
		$results = ob_get_contents();
		ob_end_clean();
	} else {
		$results = '<p>' . __('There are no publications for the specified filters.') . '</p>';
	}

	echo json_encode(
		array(
			'args' => $prev_args,
			'results' => $results,
			'more_pages' => ( $query->max_num_pages - $page ) > 0,
			'total_pages' => $query->max_num_pages,
			'current_page' => $page,
			'total_elements' => $query->found_posts,
		)
	);
	die();

}
add_action( 'wp_ajax_filtered_publications', 'elcano_ajax_filtered_publications' );
add_action( 'wp_ajax_nopriv_filtered_publications', 'elcano_ajax_filtered_publications' );

function elcano_ajax_archive_load_more() {

	$nonce = $_POST['nonce'] ?: '';

	if ( ! wp_verify_nonce( $nonce, 'archive_load_more' ) ) {
		wp_send_json_error( 'Nonce error', 400 );
		die();
	}

	$post_type      = $_REQUEST['post_type'] ?: array();
	$page           = (int)$_REQUEST['page'] ?: 1;

	$posts_per_page = 8;

	$prev_args = array(
		'posts_per_page' => $posts_per_page,
		'post_status'    => 'publish',
		'post_type'      => $post_type,
		'orderby'        => 'date',
		'order'          => 'DESC',
		'offset'         => $posts_per_page * ( $page - 1 ),
	);

	if( $post_type == 'podcast' ) {
		$prev_args['offset'] = $posts_per_page * ( $page - 2 ) + 1;
	}

	$query = new WP_Query( $prev_args );

	$results = '';
	if ( $query->have_posts() ) {
		ob_start();
		while ( $query->have_posts() ) {
			$query->the_post();
			get_template_part( 'template-parts/archive/publications', get_post_type() );
		}
		$results = ob_get_contents();
		ob_end_clean();
	} else {
		$results = '<p>' . __('There are no publications for the specified filters.') . '</p>';
	}

	echo json_encode(
		array(
			'args' => $prev_args,
			'results' => $results,
			'more_pages' => ( $query->max_num_pages - $page ) > 0,
			'total_pages' => $query->max_num_pages,
			'current_page' => $page,
			'total_elements' => $query->found_posts,
		)
	);
	die();

}
add_action( 'wp_ajax_archive_load_more', 'elcano_ajax_archive_load_more' );
add_action( 'wp_ajax_nopriv_archive_load_more', 'elcano_ajax_archive_load_more' );

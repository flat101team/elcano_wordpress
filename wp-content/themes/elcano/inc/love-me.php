<?php
/**
 * Elcano Love (like button)
 *
 * Based in Love me plugin https://wordpress.org/plugins/love-me/
 *
 * @package Elcano
 */


/**
 * Show Love Me button
 *
 * @return void
 */
function elcano_love_me() {

	$post_id = get_the_ID();
	$hash_ip = elcano_love_me_hash_ip( $_SERVER['REMOTE_ADDR'] );
	$nonce   = wp_create_nonce( 'love-me' );

	$likes    = intval( get_post_meta( $post_id, 'love_me_likes', true ) );
	$has_like = elcano_love_me_has_like( $hash_ip, $post_id );

	?>
	<div class="love">
		<input id="love_check" data-id="<?php echo $post_id; ?>" data-nonce="<?php echo esc_attr( $nonce ); ?>" type="checkbox" class="love__check" <?php checked( true, $has_like ); ?> />
		<label for="love_check" class="love__label" aria-label="<?php _e( 'Like this', 'elcano' ); ?>"></label>
		<span class="love__count"><?php echo $likes ?: ''; ?></span>
	</div>
	<?php

}

/**
 * Love Me AJAX
 *
 * @return void
 */
function elcano_ajax_love_me() {

	$nonce = $_POST['nonce'] ?: '';

	if ( ! wp_verify_nonce( $nonce, 'love-me' ) ) {
		echo json_encode( array( 'error' => 1 ) );
		die();
	}

	$post_id = intval( $_POST['post_id'] ?: 0 );
	$hash_ip = elcano_love_me_hash_ip( $_SERVER['REMOTE_ADDR'] );

	if ( elcano_love_me_has_like( $hash_ip, $post_id ) ) {
		$likes = elcano_love_me_remove_like( $hash_ip, $post_id );
		$liked = false;
	} else {
		$likes = elcano_love_me_add_like( $hash_ip, $post_id );
		$liked = true;
	}

	if ( false !== $likes ) {
		echo json_encode(
			array(
				'likes' => $likes,
				'liked' => $liked,
			)
		);
	} else {
		echo json_encode( array( 'error' => 2 ) );
	}

	die();
}
add_action( 'wp_ajax_love_me', 'elcano_ajax_love_me' );
add_action( 'wp_ajax_nopriv_love_me', 'elcano_ajax_love_me' );

/**
 * Return hashes IP (for LOPD)
 *
 * @param String $ip IP.
 * @return String
 */
function elcano_love_me_hash_ip( $ip ) {
	return wp_hash( trim( $ip ), 'love-me' );
}

/**
 * Has post liked by IP
 *
 * @param  string $hash_ip
 * @param  int    $post_id
 * @return bool
 */
function elcano_love_me_has_like( $hash_ip, $post_id ) {

	global $wpdb;

	$select = $wpdb->prepare(
		"SELECT lm.* FROM {$wpdb->prefix}love_me AS lm WHERE lm.post_id=%d AND lm.client_ip=%s",
		$post_id,
		$hash_ip
	);

	return $wpdb->get_row( $select ) ? true : false;

}

/**
 * Add like
 *
 * @param  string $hash_ip
 * @param  int    $post_id
 * @return int|false
 */
function elcano_love_me_add_like( $hash_ip, $post_id ) {

	global $wpdb;

	$saved = $wpdb->insert(
		"{$wpdb->prefix}love_me",
		array(
			'post_id'   => $post_id,
			'client_ip' => $hash_ip,
		),
		array(
			'%d',
			'%s',
		)
	);

	if ( $saved ) {
		$likes = intval( get_post_meta( $post_id, 'love_me_likes', true ) );
		$likes = $likes + 1;
		update_post_meta( $post_id, 'love_me_likes', $likes );
	}

	return $saved ? $likes : false;

}

/**
 * Remove like
 *
 * @param  string $hash_ip
 * @param  int    $post_id
 * @return int|false
 */
function elcano_love_me_remove_like( $hash_ip, $post_id ) {

	global $wpdb;

	$saved = $wpdb->delete(
		"{$wpdb->prefix}love_me",
		array(
			'post_id'   => $post_id,
			'client_ip' => $hash_ip,
		),
		array(
			'%d',
			'%s',
		)
	);

	if ( $saved ) {
		$likes = intval( get_post_meta( $post_id, 'love_me_likes', true ) );
		$likes = max( 0, $likes - 1 );
		update_post_meta( $post_id, 'love_me_likes', $likes );
	}

	return $saved ? $likes : false;

}

/**
 * Create DB Table
 *
 * @return void
 */
function elcano_love_me_create_table() {

	global $wpdb;

	$sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}love_me` (
		`love_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		`post_id` bigint(20) unsigned NOT NULL DEFAULT 0,
		`client_ip` varchar(255) NOT NULL,
		PRIMARY KEY (`love_id`),
		UNIQUE KEY `like` (`post_id`, `client_ip`),
		KEY `post_id` (`post_id`),
		KEY `client_ip` (`client_ip`)
	)";

	if ( ! empty( $wpdb->charset ) ) {
		$sql .= " CHARACTER SET $wpdb->charset";
	}

	if ( ! empty( $wpdb->collate ) ) {
		$sql .= " COLLATE $wpdb->collate";
	}

	$sql .= ';';

	require_once ABSPATH . 'wp-admin/includes/upgrade.php';

	dbDelta( $sql );

}
add_action( 'elcano_theme_activation', 'elcano_love_me_create_table' );

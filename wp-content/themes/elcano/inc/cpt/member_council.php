<?php
/**
 * Functions for scientific_council post type.
 *
 * @package Elcano
 */

/**
 * Shortcode scientific_council
 *
 * Outpus scientific_council list for selected group.
 *
 * Usage:
 * [scientific_council]  (defaults to investors group)
 * [scientific_council group="destacado"]
 *
 * @param  array $atts
 * @return string
 */
function elcano_shortcode_scientific_council( $atts ) {

    $atts = shortcode_atts(
        array(
            'group' => 'destacado',
        ),
        $atts,
        'scientific_council'
    );

    $output = '';

    $args = array(
        'post_type'        => 'scientific_council',
        'numberposts'      => -1,
        'tax_query'        => array(
            array(
                'taxonomy' => 'sci_group',
                'field'    => 'slug',
                'terms'    => array_map( 'trim', explode( ',', $atts['group'] ) ),
            ),
        ),
        'meta_key'          => 'lastname',
        'orderby'           => 'meta_value',
        'order'             => 'ASC',
        'suppress_filters' => false,
    );

    $members = get_posts( $args );

    if ( count( $members ) ) {

        /*foreach ( $biographies as $biography ) {
            $names = explode( ' ', $biography->post_title );
            if ( $names[1] === 'Antonio' ) {
                $lastname = $names[2];
            } elseif ( $names[1] === 'M.' || $names[1] === 'Manuel' ) {
                $lastname = $names[3];
            } else {
                $lastname = $names[1];
            }
            $biography->lastname = $lastname;
        }*/

        // order by lastname
        /*usort(
            $biographies,
            function( $a, $b ) {
                return strcmp( $a->lastname, $b->lastname );
            }
        );*/

        $output = '<div class="member-council-list">';

        foreach ( $members as $member ) {

            if ( get_field( 'job-position' ) ) {
                $position = get_field( 'job-position' );
            } else {
                $group    = get_the_terms( $member->ID, 'sci_group' );
                $position = $group ? $group[0]->name : '';
            }

            $photo = get_the_post_thumbnail( $member, array( 162, 162 ), array( 'class' => 'photo' ) ) ?:
                '<img src="' . get_bloginfo( 'template_directory' ) . '/images/bio.jpg" alt="">';

            if ( 'publish' === get_post_status( $member ) ) {
                $output .= sprintf(
                    '<div class="scimember vcard"><a href="%s" class="url" rel="bookmark">%s %s %s %s %s</a></div>',
                    esc_url( get_the_permalink( $member ) ),
                    sprintf( '<div class="author-photo">%s</div><hr>', $photo ),
                    sprintf( '<h3 class="fn n">%s', esc_html( get_field( 'name', $member ) ) ),
                    sprintf( '%s</h3>', esc_html( get_field( 'lastname', $member ) ) ),
                    sprintf( '<p class="title">%s</p>', esc_html( $position ) ),
                    sprintf( '<p class="note">%s</p>', esc_html( get_field( 'entity', $member ) ) )
                );
            } else {
                $output .= sprintf(
                    '<div class="biography vcard">%s %s %s %s</div>',
                    sprintf( '<div class="author-photo">%s</div>', $photo ),
                    sprintf( '<h3 class="fn n">%s</h3>', get_the_title( $member ) ),
                    sprintf( '<p class="title">%s</p>', esc_html( $position ) ),
                    sprintf( '<p class="note">%s</p>', esc_html( get_field( 'area', $member ) ) )
                );
            }
        }

        $output .= '</div>';

    }

    return $output;

}
add_shortcode( 'scientific_council', 'elcano_shortcode_scientific_council' );

/**
 * Member_council add parent in breadcrumb
 *
 * @param  array $links
 * @return array
 */
function elcano_research_parent_breadcrumb_sci_council( $links ) {

    if ( is_singular( 'scientific_council' ) ) {

        // Check is group of Researchers
        $args = array(
            'meta_query' => array(
                array(
                    'key'     => 'is_researcher',
                    'compare' => '=',
                    'value'   => '1',
                ),
            ),
        );

        if ( count( wp_get_post_terms( get_the_ID(), 'sci_group', $args ) ) ) {
            $team_page = get_field( 'page_for_team', 'option' );
        } elseif ( get_field( 'team_elcano' ) ) {
            $team_page = get_field( 'page_for_team_elcano', 'option' );
        }

        if ( isset( $team_page ) ) {
            $ancestors = array_reverse( get_post_ancestors( $team_page ) );

            // Add parent pages (if exists).
            foreach ( $ancestors as $ancestor ) {
                $breadcrumb[] = array(
                    'url'  => get_permalink( $ancestor ),
                    'text' => get_the_title( $ancestor ),
                    'id'   => $ancestor,
                );
            }

            // Add archive page.
            $breadcrumb[] = array(
                'url'  => get_permalink( $team_page ),
                'text' => get_the_title( $team_page ),
            );

            array_splice( $links, 1, 0, $breadcrumb );

        } else {

            // Add archive page.
            if (str_contains(get_permalink(),'consejo-cientifico')) {
                $breadcrumb[] = array(
                    'url'  => '/sobre-elcano/gobierno-corporativo/consejo-cientifico/',
                    'text' => __( 'Scientific Council', 'elcano' ),
                );
            } elseif (str_contains(get_permalink(),'scientific-council')) {
                $breadcrumb[] = array(
                    'url'  => '/en/about-elcano/corporate-governance/scientific-council/',
                    'text' => __( 'Scientific Council', 'elcano' ),
                );
            };


            array_splice( $links, 1, 0, $breadcrumb );
        }
    }

    return $links;

}
add_filter( 'wpseo_breadcrumb_links', 'elcano_research_parent_breadcrumb_sci_council' );
<?php
/**
 * Functions for archive post type.
 *
 * @package Elcano
 */


/**
 * Customize CPT Archive breadcrumb
 *
 * @param  array $links
 * @return array
 */
function elcano_archive_breadcrumb( $links ) {

	if ( is_post_type_archive( 'archive' ) || is_singular( 'archive' ) || is_tax( 'archive_type' ) ) {

		$breadcrumb = array();

		$page_archive = get_field( 'page_for_archive', 'option' );
		$ancestors    = array_reverse( get_post_ancestors( $page_archive ) );

		// Add parent pages (if exists).
		foreach ( $ancestors as $ancestor ) {
			$breadcrumb[] = array(
				'url'  => get_permalink( $ancestor ),
				'text' => get_the_title( $ancestor ),
				'id'   => $ancestor,
			);
		}

		// Add archive page.
		$breadcrumb[] = array(
			'url'  => get_permalink( $page_archive ),
			'text' => get_the_title( $page_archive ),
		);

		// Add term page.
		if ( is_singular() ) {
			$terms = wp_get_post_terms( get_the_ID(), 'archive_type' );

			if ( count( $terms ) ) {
				$breadcrumb[] = array(
					'url'  => get_term_link( $terms[0] ),
					'text' => esc_html( $terms[0]->name ),
				);
			}
		}

		// Remove repeated Archive.
		if ( is_post_type_archive() ) {
			array_pop( $links );
		}

		if ( count( $breadcrumb ) ) {
			array_splice( $links, 1, -1, $breadcrumb );
		}
	}

	return $links;

}
add_filter( 'wpseo_breadcrumb_links', 'elcano_archive_breadcrumb' );


/**
 * Remove 'archive' class
 *
 * Prevent CSS conflict for CPT archive and archive pages
 *
 * @param  array $classes
 * @param  array $class
 * @param  int   $post_ID
 * @return array
 */
function elcano_archive_post_class( $classes, $class, $post_ID ) {

	if ( 'archive' === get_post_type( $post_ID ) ) {
		unset( $classes[ array_search( 'archive', $classes ) ] );
	}

	return $classes;

}
add_filter( 'post_class', 'elcano_archive_post_class', 10, 3 );

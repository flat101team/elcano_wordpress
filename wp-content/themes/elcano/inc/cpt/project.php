<?php
/**
 * Functions for project post type.
 *
 * @package Elcano
 */


/**
 * Register project past query var
 *
 * @param  mixed $vars
 * @return void
 */
function elcano_register_project_past_query_var( $vars ) {
	return array_merge( $vars, array( 'past' ) );
}
add_filter( 'query_vars', 'elcano_register_project_past_query_var' );

/**
 * Past Projects rewrite
 *
 * Add rewritre rule for past projects and their pagination.
 * Sets the query var 'past' to 1.
 *
 * @return void
 */
function elcano_project_past_rewrite() {

	if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

		$languages = apply_filters( 'wpml_active_languages', null, 'skip_missing=0' );

		foreach ( $languages as $lang ) {
			$slug = apply_filters( 'wpml_get_translated_slug', 'projects', 'project', $lang['code'] );

			add_rewrite_rule( "$slug/past/([0-9]{4})/?$", 'index.php?post_type=project&past=$matches[1]', 'top' );
			add_rewrite_rule( "$slug/past/([0-9]{4})/page/?([0-9]{1,})?/?$", 'index.php?post_type=project&past=$matches[1]&paged=$matches[2]', 'top' );
		}
	} else {

		$slug = trim( parse_url( get_post_type_archive_link( 'project' ), PHP_URL_PATH ), '/' );

		add_rewrite_rule( "$slug/past/([0-9]{4})/?$", 'index.php?post_type=project&past=$matches[1]', 'top' );
		add_rewrite_rule( "$slug/past/([0-9]{4})/page/?([0-9]{1,})?/?$", 'index.php?post_type=project&past=$matches[1]&paged=$matches[2]', 'top' );
	}

}
add_action( 'init', 'elcano_project_past_rewrite', 20 );

/**
 * Filter projects query
 *
 * Filter by meta field 'date_start'
 * Next projects 'date_start' >= now and order by date_start ASC
 * Past projects 'date_start' <  now and order by date_start DESC
 *
 * @param  mixed $query
 * @return void
 */
function elcano_query_projects( $query ) {

	if ( ! is_admin() && $query->is_main_query() && $query->is_post_type_archive( 'project' ) ) {

		$now  = new DateTime( 'now', wp_timezone() );
		$past = filter_var( get_query_var( 'past', false ), FILTER_VALIDATE_INT );

		if ( $past ) {

			// Filter date start on year.
			$query->set(
				'meta_query',
				array(
					'relation'     => 'AND',
					'year_clause'  => array(
						'key'     => 'date_end',
						'value'   => "{$past}-",
						'compare' => 'LIKE',
					),
					'end_clause'   => array(
						'key'     => 'date_end',
						'value'   => $now->format( 'Y-m-d H:i:s' ),
						'compare' => '<',
						'type'    => 'DATETIME',
					),
					'start_clause' => array(
						'key'     => 'date_start',
						'compare' => 'EXISTS',
						'type'    => 'DATETIME',
					),
				)
			);
		} else {

			// Filter future date start.
			$query->set(
				'meta_query',
				array(
					'relation'     => 'AND',
					'end_clause'   => array(
						'key'     => 'date_end',
						'value'   => $now->format( 'Y-m-d H:i:s' ),
						'compare' => '>=',
						'type'    => 'DATETIME',
					),
					'start_clause' => array(
						'key'     => 'date_start',
						'compare' => 'EXISTS',
						'type'    => 'DATETIME',
					),
				)
			);
		}

		// Order by.
		if ( $past ) {
			$query->set(
				'orderby',
				array(
					'end_clause'   => 'DESC',
					'start_clause' => 'DESC',
					'ID'           => 'DESC',
				)
			);
		} else {
			$query->set(
				'orderby',
				array(
					'end_clause'   => 'ASC',
					'start_clause' => 'ASC',
					'ID'           => 'ASC',
				)
			);
		}
	}
}
add_action( 'pre_get_posts', 'elcano_query_projects' );

/**
 * Return projects years
 *
 * @param  bool $past_years if true only return current and past years
 * @return array
 */
function elcano_projects_get_years( $past_years = true ) {

	global $wpdb;

	$query   = $wpdb->prepare(
		"SELECT DISTINCT SUBSTRING(pm.meta_value, 1, 4) AS year
		FROM $wpdb->postmeta AS pm LEFT JOIN $wpdb->posts p ON p.ID = pm.post_id
		WHERE pm.meta_key = %s AND p.post_status = %s AND p.post_type = 'project'
		ORDER BY year DESC",
		'date_end',
		'publish'
	);
	$results = $wpdb->get_results( $query );
	$results = wp_list_pluck( $results, 'year' );

	if ( $past_years ) {
		$results = array_filter(
			$results,
			function( $y ) {
				return $y < ( new DateTime() )->format( 'Y' );
			}
		);
	}

	return $results;
}

/**
 * Output Pren/Next lateral links for project
 *
 * Prev/Next based on postmeta "date_end"
 *
 * @return void
 */
function elcano_project_lateral_nav() {

	$date = get_field( 'date_end', false, false );

	// Need order by date_end, date_start, ID.
	// Include current in query and select next item in results.

	$prev_args = array(
		'posts_per_page' => 6,
		'post_type'      => 'project',
		// 'post__not_in'   => array( get_the_ID() ),
		'meta_query'     => array(
			'relation'     => 'AND',
			'end_clause'   => array(
				'key'     => 'date_end',
				'value'   => $date,
				'compare' => '<=',
				'type'    => 'DATETIME',
			),
			'start_clause' => array(
				'key'     => 'date_start',
				'compare' => 'EXISTS',
				'type'    => 'DATETIME',
			),
		),
		'orderby'        => array(
			'end_clause'   => 'DESC',
			'start_clause' => 'DESC',
			'ID'           => 'DESC',
		),
	);

	$query     = new WP_Query( $prev_args );
	$prev_post = $query->posts[ array_search( get_post(), $query->posts ) + 1 ];

	$next_args = array(
		'posts_per_page' => 6,
		'post_type'      => 'project',
		// 'post__not_in'   => array( get_the_ID() ),
		'meta_query'     => array(
			'relation'     => 'AND',
			'end_clause'   => array(
				'key'     => 'date_end',
				'value'   => $date,
				'compare' => '>=',
				'type'    => 'DATETIME',
			),
			'start_clause' => array(
				'key'     => 'date_start',
				'compare' => 'EXISTS',
				'type'    => 'DATETIME',
			),
		),
		'orderby'        => array(
			'end_clause'   => 'ASC',
			'start_clause' => 'ASC',
			'ID'           => 'ASC',
		),
	);

	$query     = new WP_Query( $next_args );
	$next_post = $query->posts[ array_search( get_post(), $query->posts ) + 1 ];

	if ( $prev_post || $next_post ) {

		echo '<div class="nav-lateral">';

		if ( $prev_post ) {
			$date = new DateTime( get_field( 'date_end', $prev_post->ID, false ), wp_timezone() );

			printf(
				'<div class="nav nav-prev"><a href="%s" id="clicAnterior" rel="prev"><div class="link-label">%s</div><div class="link-card">%s %s %s</div></a></div>',
				esc_url( get_the_permalink( $prev_post ) ),
				esc_html__( 'Previous', 'elcano' ),
				sprintf( '<div class="post-thumbnail">%s</div>', get_the_post_thumbnail( $prev_post, array( 100, 100 ) ) ),
				sprintf( '<div class="post-title">%s</div>', get_the_title( $prev_post ) ),
				sprintf( '<div class="post-date">// %s</div>', wp_date( get_option( 'date_format' ), $date->getTimestamp() ) )
			);
		}

		if ( $next_post ) {
			$date = new DateTime( get_field( 'date_end', $next_post->ID, false ), wp_timezone() );

			printf(
				'<div class="nav nav-next"><a href="%s" id="clicSiguiente" rel="next"><div class="link-label">%s</div><div class="link-card">%s %s %s</div></a></div>',
				esc_url( get_the_permalink( $next_post ) ),
				esc_html__( 'Next', 'elcano' ),
				sprintf( '<div class="post-thumbnail">%s</div>', get_the_post_thumbnail( $next_post, array( 100, 100 ) ) ),
				sprintf( '<div class="post-title">%s</div>', get_the_title( $next_post ) ),
				sprintf( '<div class="post-date">// %s</div>', wp_date( get_option( 'date_format' ), $date->getTimestamp() ) )
			);
		}

		echo '</div>';
	}

}

/**
 * Fix past projects URL
 *
 * @param string $url
 * @return string
 */
function elcano_fix_project_past_link( $url, $language_code ) {

	if ( '' === $url ) {
		return $url;
	}

	if ( is_post_type_archive( 'project' ) && get_query_var( 'past', false ) ) {

		$slug = apply_filters( 'wpml_get_translated_slug', 'projects', 'project', $language_code );
		$past = filter_var( get_query_var( 'past', false ), FILTER_VALIDATE_INT );

		$url = str_replace( "/$slug/", "/$slug/past/$past/", $url );
	}

	return $url;

}
add_filter( 'elcano_fix_url', 'elcano_fix_project_past_link', 10, 2 );

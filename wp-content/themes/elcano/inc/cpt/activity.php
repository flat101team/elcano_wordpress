<?php
/**
 * Functions for activity post type.
 *
 * @package Elcano
 */


/**
 * Register activity past query var
 *
 * @param  mixed $vars
 * @return void
 */
function elcano_register_activity_past_query_var( $vars ) {
	return array_merge( $vars, array( 'past' ) );
}
add_filter( 'query_vars', 'elcano_register_activity_past_query_var' );

/**
 * Past Activities rewrite
 *
 * Add rewritre rule for past activities and their pagination.
 * Sets the query var 'past' to 1.
 *
 * @return void
 */
function elcano_activity_past_rewrite() {

	if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

		$languages = apply_filters( 'wpml_active_languages', null, 'skip_missing=0' );

		foreach ( $languages as $lang ) {
			$slug = apply_filters( 'wpml_get_translated_slug', 'activities', 'activity', $lang['code'] );

			add_rewrite_rule( "$slug/past/([0-9]{4})/?$", 'index.php?post_type=activity&past=$matches[1]', 'top' );
			add_rewrite_rule( "$slug/past/([0-9]{4})/page/?([0-9]{1,})?/?$", 'index.php?post_type=activity&past=$matches[1]&paged=$matches[2]', 'top' );
		}
	} else {

		$slug = trim( parse_url( get_post_type_archive_link( 'activity' ), PHP_URL_PATH ), '/' );

		add_rewrite_rule( "$slug/past/([0-9]{4})/?$", 'index.php?post_type=activity&past=$matches[1]', 'top' );
		add_rewrite_rule( "$slug/past/([0-9]{4})/page/?([0-9]{1,})?/?$", 'index.php?post_type=activity&past=$matches[1]&paged=$matches[2]', 'top' );
	}

}
add_action( 'init', 'elcano_activity_past_rewrite', 20 );

/**
 * Filter activities query
 *
 * Filter by meta field 'date_start'
 * Next activities 'date_start' >= now and order by date_start ASC
 * Past activities 'date_start' <  now and order by date_start DESC
 *
 * @param  mixed $query
 * @return void
 */
function elcano_query_activities( $query ) {

	if ( ! is_admin() && $query->is_main_query() && $query->is_post_type_archive( 'activity' ) ) {

		$now  = new DateTime( 'now', wp_timezone() );
		$past = filter_var( get_query_var( 'past', false ), FILTER_VALIDATE_INT );

		if ( $past ) {

			// Filter date start on year.
			$query->set(
				'meta_query',
				array(
					array(
						'key'     => 'date_start',
						'value'   => "{$past}-",
						'compare' => 'LIKE',
					),
					array(
						'key'     => 'date_start',
						'value'   => $now->format( 'Y-m-d H:i:s' ),
						'compare' => '<',
						'type'    => 'DATETIME',
					),
				)
			);
		} else {

			// Filter future date start.
			$query->set(
				'meta_query',
				array(
					array(
						'key'     => 'date_end',
						'value'   => $now->format( 'Y-m-d H:i:s' ),
						'compare' => '>=',
						'type'    => 'DATETIME',
					),
				)
			);
		}

		// Order by date start.
		$query->set( 'meta_key', 'date_start' );
		$query->set( 'orderby', 'meta_value' );
		$query->set( 'order', $past ? 'DESC' : 'ASC' );

	}
}
add_action( 'pre_get_posts', 'elcano_query_activities' );

/**
 * Return activities years
 *
 * @param  bool $past_years if true only return current and past years
 * @return array
 */
function elcano_activities_get_years( $past_years = true ) {

	global $wpdb;

	$query   = $wpdb->prepare(
		"SELECT DISTINCT SUBSTRING(pm.meta_value, 1, 4) AS year
		FROM $wpdb->postmeta AS pm LEFT JOIN $wpdb->posts p ON p.ID = pm.post_id
		WHERE pm.meta_key = %s AND p.post_status = %s AND p.post_type = 'activity'
		ORDER BY year DESC",
		'date_start',
		'publish'
	);
	$results = $wpdb->get_results( $query );
	$results = wp_list_pluck( $results, 'year' );

	if ( $past_years ) {
		$results = array_filter(
			$results,
			function( $y ) {
				return $y <= date( 'Y' );
			}
		);
	}

	return $results;
}

/**
 * Output Pren/Next lateral links for activity
 *
 * Prev/Next based on postmeta "date_start"
 *
 * @return void
 */
function elcano_activity_lateral_nav() {

	$date = get_field( 'date_start', false, false );

	$prev_args = array(
		'posts_per_page' => 1,
		'post_type'      => 'activity',
		'post__not_in'   => array( get_the_ID() ),
		'meta_query'     => array(
			array(
				'key'     => 'date_start',
				'value'   => $date,
				'compare' => '<=',
				'type'    => 'DATETIME',
			),
		),
		'meta_key'       => 'date_start',
		'orderby'        => 'meta_value',
		'order'          => 'DESC',
	);

	$query     = new WP_Query( $prev_args );
	if (isset($query->posts[0])) {
		$prev_post = $query->posts[0];
	} else $prev_post = 0;

	$next_args = array(
		'posts_per_page' => 1,
		'post_type'      => 'activity',
		'post__not_in'   => array( get_the_ID() ),
		'meta_query'     => array(
			array(
				'key'     => 'date_start',
				'value'   => $date,
				'compare' => '>=',
				'type'    => 'DATETIME',
			),
		),
		'meta_key'       => 'date_start',
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
	);

	$query     = new WP_Query( $next_args );
	if (isset($query->posts[0])) {
		$next_post = $query->posts[0];
	} else $next_post = 0;

	if ( $prev_post || $next_post ) {

		echo '<div class="nav-lateral">';

		if ( $prev_post ) {
			$date = new DateTime( get_field( 'date_start', $prev_post->ID, false ), wp_timezone() );
            $post_thumbnail = elcano_get_activity_thumbnail( $prev_post, 100, 100 );

			printf(
				'<div class="nav nav-prev"><a href="%s" id="clicAnterior" rel="prev"><div class="link-label">%s</div><div class="link-card">%s %s %s</div></a></div>',
				esc_url( get_the_permalink( $prev_post ) ),
				esc_html__( 'Previous', 'elcano' ),
				sprintf( '<div class="post-thumbnail">%s</div>', $post_thumbnail ),
				sprintf( '<div class="post-title">%s</div>', get_the_title( $prev_post ) ),
				sprintf( '<div class="post-date">// %s</div>', wp_date( get_option( 'date_format' ), $date->getTimestamp() ) )
			);
		}

		if ( $next_post ) {
			$date = new DateTime( get_field( 'date_start', $next_post->ID, false ), wp_timezone() );
            $post_thumbnail = elcano_get_activity_thumbnail( $next_post, 100, 100 );

			printf(
				'<div class="nav nav-next"><a href="%s" id="clicSiguiente" rel="next"><div class="link-label">%s</div><div class="link-card">%s %s %s</div></a></div>',
				esc_url( get_the_permalink( $next_post ) ),
				esc_html__( 'Next', 'elcano' ),
				sprintf( '<div class="post-thumbnail">%s</div>', $post_thumbnail ),
				sprintf( '<div class="post-title">%s</div>', get_the_title( $next_post ) ),
				sprintf( '<div class="post-date">// %s</div>', wp_date( get_option( 'date_format' ), $date->getTimestamp() ) )
			);
		}

		echo '</div>';
	}

}

/**
 * Fix past activities URL
 *
 * @param string $url
 * @return string
 */
function elcano_fix_activity_past_link( $url, $language_code ) {

	if ( '' === $url ) {
		return $url;
	}

	if ( is_post_type_archive( 'activity' ) && get_query_var( 'past', false ) ) {

		$slug = apply_filters( 'wpml_get_translated_slug', 'activities', 'activity', $language_code );
		$past = filter_var( get_query_var( 'past', false ), FILTER_VALIDATE_INT );

		$url = str_replace( "/$slug/", "/$slug/past/$past/", $url );
	}

	return $url;

}
add_filter( 'elcano_fix_url', 'elcano_fix_activity_past_link', 10, 2 );

function elcano_get_activity_thumbnail($post, $width, $height): string {
    $directory = get_template_directory_uri();
    $post_thumbnail = get_the_post_thumbnail($post, array($width, $height));

    if( empty( $post_thumbnail ) ) {
        $post_thumbnail = '<img src="' . $directory . '/images/actividad.jpg" alt="'. $post->post_title . '" />';
    }

    return $post_thumbnail;
}

<?php
/**
 * Functions for archive post type.
 *
 * @package Elcano
 */


/**
 * Customize CPT Job breadcrumb
 *
 * @param  array $links
 * @return array
 */
function elcano_jobs_breadcrumb( $links ) {

	if ( is_post_type_archive( 'job' ) || is_singular( 'job' ) ) {

		$breadcrumb = array();

		$page_archive = get_field( 'page_for_job', 'option' );
		$ancestors    = array_reverse( get_post_ancestors( $page_archive ) );

		// Add parent pages (if exists).
		foreach ( $ancestors as $ancestor ) {
			$breadcrumb[] = array(
				'url'  => get_permalink( $ancestor ),
				'text' => get_the_title( $ancestor ),
				'id'   => $ancestor,
			);
		}

		// Add archive page.
		if ( is_singular() ) {
			$breadcrumb[] = array(
				'url'  => get_permalink( $page_archive ),
				'text' => get_the_title( $page_archive ),
			);
		} else {
			$breadcrumb[] = array(
				'url'  => '',
				'text' => get_the_title( $page_archive ),
			);
		}

		if ( count( $breadcrumb ) ) {
			array_splice( $links, 1, 1, $breadcrumb );
		}
	}

	return $links;

}
add_filter( 'wpseo_breadcrumb_links', 'elcano_jobs_breadcrumb' );

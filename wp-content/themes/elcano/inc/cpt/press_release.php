<?php
/**
 * Functions for press_release post type.
 *
 * @package Elcano
 */


/**
 * Press Releases year filter rewrite
 *
 * Add rewritre rule for press release year filter and their pagination.
 *
 * @return void
 */
function elcano_press_release_year_rewrite() {

	if ( defined( 'ICL_LANGUAGE_CODE' ) ) {

		$languages = apply_filters( 'wpml_active_languages', null, 'skip_missing=0' );

		foreach ( $languages as $lang ) {
			$slug = apply_filters( 'wpml_get_translated_slug', 'press-release', 'press_release', $lang['code'] );

			add_rewrite_rule( "$slug/([0-9]{4})/?$", 'index.php?post_type=press_release&year=$matches[1]', 'top' );
			add_rewrite_rule( "$slug/([0-9]{4})/page/?([0-9]{1,})?/?$", 'index.php?post_type=press_release&year=$matches[1]&paged=$matches[2]', 'top' );
		}
	} else {

		$slug = trim( parse_url( get_post_type_archive_link( 'press_release' ), PHP_URL_PATH ), '/' );

		add_rewrite_rule( "$slug/([0-9]{4})/?$", 'index.php?post_type=press_release&year=$matches[1]', 'top' );
		add_rewrite_rule( "$slug/([0-9]{4})/page/?([0-9]{1,})?/?$", 'index.php?post_type=press_release&year=$matches[1]&paged=$matches[2]', 'top' );
	}

}
add_action( 'init', 'elcano_press_release_year_rewrite', 20 );

/**
 * Add archive links filters for Press Releases
 *
 * (don't change $sql_where)
 *
 * @param  string $sql_where
 * @param  array  $parsed_args
 * @return string
 */
function elcano_press_release_archive_filters( $sql_where, $parsed_args ) {

	if ( 'press_release' === $parsed_args['post_type'] ) {
		add_filter( 'year_link', 'elcano_press_release_year_link', 10, 2 );
		add_filter( 'get_archives_link', 'elcano_press_release_archive_link', 10, 2 );
	}

	return $sql_where;
}
add_filter( 'getarchives_where', 'elcano_press_release_archive_filters', 10, 2 );

/**
 * Replace default year link for Press Releases
 *
 * Return press_release archive plus year.
 *
 * @param  string $yearlink
 * @param  string $year
 * @return string
 */
function elcano_press_release_year_link( $yearlink, $year ) {
	return trailingslashit( get_post_type_archive_link( 'press_release' ) . $year );
}

/**
 * Remove post_type arg for Press Release archive link
 *
 * @param  string $link_html
 * @param  string $url
 * @return string
 */
function elcano_press_release_archive_link( $link_html, $url ) {
	return str_replace( $url, add_query_arg( 'post_type', null, $url ), $link_html );
}

/**
 * Customize Press Release breadcrumb
 *
 * @param  array $links
 * @return array
 */
function elcano_press_release_breadcrumb( $links ) {

	if ( 'press_release' === get_post_type() ) {

		$breadcrumb = array();

		// Add parent pages (if exists).
		$page_for_pr = get_field( 'page_for_press_release', 'option' );
		$ancestors   = get_post_ancestors( $page_for_pr );

		foreach ( $ancestors as $ancestor ) {
			$breadcrumb[] = array(
				'url'  => get_permalink( $ancestor ),
				'text' => get_the_title( $ancestor ),
				'id'   => $ancestor,
			);
		}

		// Add archive page.
		if ( is_singular() || is_year() ) {
			$breadcrumb[] = array(
				'url'  => get_permalink( $page_for_pr ),
				'text' => get_the_title( $page_for_pr ),
			);
		}

		// Fix year link.
		if ( is_year() ) {
			$crumb     = count( $links ) - 2;
			$year_link = $links[ $crumb ]['url'];

			$links[ $crumb ]['url'] = elcano_press_release_year_link(
				$year_link,
				trim( parse_url( $year_link, PHP_URL_PATH ), '/' )
			);
		}

		if ( count( $breadcrumb ) ) {
            array_splice( $links, 1, -1, $breadcrumb );
		}
	}

	return $links;

}
add_filter( 'wpseo_breadcrumb_links', 'elcano_press_release_breadcrumb' );

/**
 * Fix press release year URL
 *
 * @param string $url
 * @return string
 */
function elcano_fix_press_release_year_link( $url, $language_code ) {

	if ( '' === $url ) {
		return $url;
	}

	if ( is_post_type_archive( 'press_release' ) && get_query_var( 'year', false ) ) {

		$slug = apply_filters( 'wpml_get_translated_slug', 'press-release', 'press_release', $language_code );
		$year = filter_var( get_query_var( 'year', false ), FILTER_VALIDATE_INT );

		$url = str_replace( "/$year/$slug/", "/$slug/$year/", $url );
	}

	return $url;

}
add_filter( 'elcano_fix_url', 'elcano_fix_press_release_year_link', 10, 2 );

<?php
/**
 * Functions for biography post type.
 *
 * @package Elcano
 */


/**
 * Shortcode Biographies
 *
 * Outpus biographies list for selected group.
 *
 * Usage:
 * [biographies]  (defaults to investors group)
 * [biographies group="ayudante"]
 *
 * @param  array $atts
 * @return string
 */
function elcano_shortcode_biographies( $atts ) {

	$atts = shortcode_atts(
		array(
			'group' => 'investigador',
		),
		$atts,
		'biographies'
	);

	$output = '';

	$args = array(
		'post_type'        => 'biography',
		'numberposts'      => -1,
		'tax_query'        => array(
			array(
				'taxonomy' => 'bio_group',
				'field'    => 'slug',
				'terms'    => array_map( 'trim', explode( ',', $atts['group'] ) ),
			),
		),
		'orderby'          => 'post_title',
		'order'            => 'ASC',
		'suppress_filters' => false,
	);

	$biographies = get_posts( $args );

	if ( count( $biographies ) ) {

		foreach ( $biographies as $biography ) {
			$names = explode( ' ', $biography->post_title );
			if ( $names[1] === 'Antonio' ) {
				$lastname = $names[2];
			} elseif ( $names[1] === 'M.' || $names[1] === 'Manuel' ) {
				$lastname = $names[3];
			} else {
				$lastname = $names[1];
			}
			$biography->lastname = $lastname;
		}

		// order by lastname
		usort(
			$biographies,
			function( $a, $b ) {
				return strcmp( $a->lastname, $b->lastname );
			}
		);

		$output = '<div class="biographies-list">';

		foreach ( $biographies as $biography ) {

			if ( get_field( 'position' ) ) {
				$position = get_field( 'position' );
			} else {
				$group    = get_the_terms( $biography->ID, 'bio_group' );
				$position = $group ? $group[0]->name : '';
			}

			$photo = get_the_post_thumbnail( $biography, array( 270, 270 ), array( 'class' => 'photo' ) ) ?:
				'<img src="' . get_bloginfo( 'template_directory' ) . '/images/bio.jpg" alt="">';

			if ( 'publish' === get_post_status( $biography ) ) {
				$output .= sprintf(
					'<div class="biography vcard"><a href="%s" class="url" rel="bookmark">%s %s %s %s</a></div>',
					esc_url( get_the_permalink( $biography ) ),
					sprintf( '<div class="author-photo">%s</div>', $photo ),
					sprintf( '<h3 class="fn n">%s</h3>', get_the_title( $biography ) ),
					sprintf( '<p class="title">%s</p>', esc_html( $position ) ),
					sprintf( '<p class="note">%s</p>', esc_html( get_field( 'area', $biography ) ) )
				);
			} else {
				$output .= sprintf(
					'<div class="biography vcard">%s %s %s %s</div>',
					sprintf( '<div class="author-photo">%s</div>', $photo ),
					sprintf( '<h3 class="fn n">%s</h3>', get_the_title( $biography ) ),
					sprintf( '<p class="title">%s</p>', esc_html( $position ) ),
					sprintf( '<p class="note">%s</p>', esc_html( get_field( 'area', $biography ) ) )
				);
			}
		}

		$output .= '</div>';

	}

	return $output;

}
add_shortcode( 'biographies', 'elcano_shortcode_biographies' );

/**
 * Biography add parent in breadcrumb
 *
 * @param  array $links
 * @return array
 */
function elcano_research_parent_breadcrumb( $links ) {

	if ( is_singular( 'biography' ) ) {

		// Check is group of Researchers
		$args = array(
			'meta_query' => array(
				array(
					'key'     => 'is_researcher',
					'compare' => '=',
					'value'   => '1',
				),
			),
		);

		if ( count( wp_get_post_terms( get_the_ID(), 'bio_group', $args ) ) ) {
			$team_page = get_field( 'page_for_team', 'option' );
		} elseif ( get_field( 'team_elcano' ) ) {
			$team_page = get_field( 'page_for_team_elcano', 'option' );
		}

		if ( isset( $team_page ) ) {
			$ancestors = array_reverse( get_post_ancestors( $team_page ) );

			// Add parent pages (if exists).
			foreach ( $ancestors as $ancestor ) {
				$breadcrumb[] = array(
					'url'  => get_permalink( $ancestor ),
					'text' => get_the_title( $ancestor ),
					'id'   => $ancestor,
				);
			}

			// Add archive page.
			$breadcrumb[] = array(
				'url'  => get_permalink( $team_page ),
				'text' => get_the_title( $team_page ),
			);

			array_splice( $links, 1, 0, $breadcrumb );

		} else {

			// Add archive page.
			$breadcrumb[] = array(
				'url'  => '',
				'text' => __( 'Biographies', 'elcano' ),
			);

			array_splice( $links, 1, 0, $breadcrumb );
		}
	}

	return $links;

}
add_filter( 'wpseo_breadcrumb_links', 'elcano_research_parent_breadcrumb' );

<?php
/**
 * Elcano Extras
 *
 * @package Elcano
 */

/**
 * LuckyWP Table of Contents
 *
 * Don't load plugin script (managed by theme.js)
 * Use id="toc--section-name" headings for javascrip filter
 */
add_filter( 'lwptoc_enqueue_script', '__return_false' );
add_filter( 'lwptoc_heading_id', 'elcano_toc_heading_id' );

function elcano_toc_heading_id( $label ) {
	return "toc--$label";
}


// Move SEO metabox after custom fields.
function elcano_return_low() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'elcano_return_low' );

/**
 * Custom SoundCloud iframe
 */
function elcano_oembed_soundcloud_custom( $html, $url ) {

	if ( false !== strpos( $url, 'soundcloud' ) ) {
		$html = str_replace( 'visual=true', 'visual=false', $html );
	}

	return $html;

}
add_filter( 'oembed_result', 'elcano_oembed_soundcloud_custom', 10, 2 );

/**
 * Add Gutenberg PDF Viewer Block assets
 *
 * In CPTs with field "pdf_file" add pdf reader assets and insert pdf block at the end.
 *
 * @return void
 */
function elcano_gpvb_enqueue_styles_public() {

	if ( is_singular() && 'special' !== get_post_type() && get_field( 'pdf_file' ) ) {

		// Add styles.
		wp_enqueue_style(
			'pdf-viewer-block-styles',
			plugins_url( 'pdf-viewer-block/public/css/pdf-viewer-block.css' ),
			array(),
			'',
			'all'
		);
		// Don't need script (managed by theme.js)

		// Insert pdf block programatically.
		add_filter( 'the_content', 'elcano_gpvb_pdf_block' );
	}
}
add_action( 'wp_enqueue_scripts', 'elcano_gpvb_enqueue_styles_public' );

/**
 * Insert pdf block at the content end
 *
 * @param  string $content
 * @return string
 */
function elcano_gpvb_pdf_block( $content ) {

	$pdf = get_field( 'pdf_file' );

	if ( false !== $pdf ) {
		$content .= sprintf(
			'<div id="%s" class="wp-block-pdf-viewer-block-standard"><div class="uploaded-pdf"><a href="%s" data-width="%s" data-height="%s"></a></div></div>',
			_x( 'see-study', 'Document PDF anchor', 'elcano' ),
			esc_url( $pdf['url'] ),
			'max(600px, 100%)',
			'min(760px, 75vh)'
		);
	}

	return $content;
}

/**
 * Output Language Switcher
 *
 * @return void
 */
function elcano_language_switcher() {

	$languages = apply_filters( 'wpml_active_languages', null, 'skip_missing=0' );

	if ( ! empty( $languages ) ) {
		echo '<ul class="languages-select">';
		foreach ( $languages as $lang ) {
			printf(
				'<li %s><a href="%s" rel="alternate" hreflang="%s">%s</a></li>',
				$lang['active'] ? 'class="current"' : '',
				esc_url( elcano_fix_url( $lang['url'], $lang['code'] ) ),
				esc_attr( $lang['default_locale'] ),
				esc_html( strtoupper( $lang['language_code'] ) )
			);
		}
		echo '</ul>';
	}
}

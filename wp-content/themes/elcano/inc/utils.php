<?php
/**
 * Functions utilities
 *
 * @package Elcano
 */


/**
 * Add $dep (script handle) to the array of dependencies for $handle
 *
 * @see http://wordpress.stackexchange.com/questions/100709/add-a-script-as-a-dependency-to-a-registered-script
 *
 * @param string $handle Script handle for which you want to add a dependency
 * @param string $dep Script handle - the dependency you wish to add
 */
function elcano_append_dependency( $handle, $dep ) {

	global $wp_scripts;

	$script = $wp_scripts->query( $handle, 'registered' );

	if ( ! $script ) {
		return false;
	}

	if ( ! in_array( $dep, $script->deps ) ) {
		$script->deps[] = $dep;
	}

	return true;

}

/**
 * List of authors with content
 *
 * @return void
 */
function elcano_get_authors() {

	global $wpdb;

	$query   = $wpdb->prepare(
		"SELECT DISTINCT(pm.meta_value)
		FROM $wpdb->postmeta AS pm LEFT JOIN $wpdb->posts p ON p.ID = pm.post_id
		WHERE pm.meta_key = %s AND p.post_status = %s",
		'authors',
		'publish'
	);
	$results = $wpdb->get_results( $query );

	$bio_ids = array();
	foreach ( $results as $result ) {
		$ids     = (array) maybe_unserialize( $result->meta_value );
		$bio_ids = array_merge( $bio_ids, $ids );
	}

	$bio_ids = array_unique( array_map( 'intval', $bio_ids ) );

	$args = array(
		'post_type'        => 'biography',
		'post__in'         => $bio_ids,
		'posts_per_page'   => -1,
		'orderby'          => 'title',
		'order'            => 'ASC',
		'suppress_filters' => false,
	);

	return get_posts( $args );

}

/**
 * Fix WPML CPTs archive urls
 *
 * In secondary languages some CPT archive links returns link to home
 *
 * @param  mixed $url
 * @param  mixed $language_code
 * @return void
 */
function elcano_fix_cpt_archive( $url, $language_code ) {

	$post_type        = get_query_var( 'post_type' ); // get_post_type(); don't work if empty
	$post_type_object = get_post_type_object( $post_type );

	if ( isset( $post_type_object->rewrite ) ) {
		$slug = untrailingslashit( $post_type_object->rewrite['slug'] );
	} else {
		$slug = $post_type_object->name;
	}

	$translated_slug = apply_filters( 'wpml_get_translated_slug', $slug, $post_type, $language_code );

	// TODO: add support for rewrite 'with_front'
	if ( false === strpos( $url, "/$translated_slug/" ) ) {
		$url = untrailingslashit( $url ) . "/$translated_slug/";
	} else {
		$url = $url;
	}

	return $url;

}

/**
 * Fix url
 *
 * Fix CPT archive.
 * Fix Blog term.
 * Other fixes via filter 'elcano_fix_url'.
 *
 * @param  string      $url
 * @param  string|null $language_code
 * @return string
 */
function elcano_fix_url( $url, $language_code = null ) {

	if ( '' === $url ) {
		return $url;
	}

	// Post types with archive
	$post_types = array_keys( get_post_types( array( 'has_archive' => true ), 'names' ) );

	if ( is_post_type_archive( $post_types ) ) {
		$url = elcano_fix_cpt_archive( $url, $language_code );
	} elseif ( function_exists ( 'elcano_is_blog_term' ) && elcano_is_blog_term() ) {
		$url = elcano_blog_term_link( $url, is_tag() ? 'post_tag' : 'category', $language_code );
	}

	return apply_filters( 'elcano_fix_url', $url, $language_code );

}

/**
 * Checks if a post is the original or not
 *
 * view (https://wpml.org/forums/topic/get-the-id-of-post-in-language-in-which-it-was-first-written/#post-130602)
 *
 * @param int    $post_id The post ID to check. Leave on 0 if using within a loop
 * @param string $type The element type to check: post_post|post_page|post_custom Defaults to post_post
 * @return array
 */
function elcano_translation_info( $post_id = null, $type = 'post_post' ) {
	global $sitepress;

	$post_id         = $post_id ?? get_the_ID();
	$el_trid         = $sitepress->get_element_trid( $post_id, $type );
	$el_translations = $sitepress->get_element_translations( $el_trid, $type );
	$output          = array();

	if ( ! empty( $el_translations ) ) {
		$is_original = false;
		foreach ( $el_translations as $lang => $details ) {
			if ( $details->original == 1 && $details->element_id == $post_id ) {
				$is_original = true;
			}
			if ( $details->original == 1 ) {
				$original_ID = $details->element_id;
			}
		}
		$output['is_original'] = $is_original;
		$output['original_ID'] = $original_ID;
	}

	return $output;
}

/**
 * Post is original
 *
 * @param  int|WP_Post|null $post
 * @return bool true if original, false if translation
 */
function elcano_is_original( $post = null ) {

	$post = get_post( $post );

	$info = elcano_translation_info( $post->ID, "post_{$post->post_type}" );

	return $info && $info['is_original'];

}

/**
 * Get estimated reading time
 *
 * @param  int|WP_Post|null $post
 * @return int The estimated reading time in minutes
 */
function elcano_get_estimated_reading_time( $post = null ) {

	if( !is_object( $post ) ) {
		$post = get_post( $post );
	}

	$speed = 200;
	$content = $post->post_content;
	$word_count = str_word_count( strip_tags ( $content ) );
	$reading_time = ceil( $word_count / $speed );

	return absint( $reading_time ) . ' ' . _n( 'min', 'mins', $reading_time, 'elcano' );
}

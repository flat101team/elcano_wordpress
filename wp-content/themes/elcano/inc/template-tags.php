<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Elcano
 */


/**
 * Menu Walker with submenu div wrapped.
 */
class Submenu_Wrap extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
			$indent  = str_repeat( "\t", $depth );
			$output .= "\n$indent<div class='sub-menu-wrap'><ul class='sub-menu'>\n";
	}
	function end_lvl( &$output, $depth = 0, $args = array() ) {
			$indent  = str_repeat( "\t", $depth );
			$output .= "$indent</ul></div>\n";
	}
}

/**
 * Prints HTML with meta information for the current post-date/time.
 *
 * @param  int|WP_Post|null $post
 * @param  bool             $output
 * @return string|void
 */
function elcano_posted_on( $post = null, $output = true, $posted_on_text = true ) {

	$post = get_post( $post );

	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U', $post ) !== get_the_modified_time( 'U', $post ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf(
		$time_string,
		esc_attr( get_the_date( DATE_W3C, $post ) ),
		esc_html( get_the_date( '', $post ) ),
		esc_attr( get_the_modified_date( DATE_W3C, $post ) ),
		esc_html( get_the_modified_date( '', $post ) )
	);

	$posted_on = esc_html( get_the_date( '', $post ) );

	if( $posted_on_text ) {
		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'elcano' ),
			$posted_on // '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);
	}


	$return = '<span class="posted-on" title="' . $posted_on . '">//  ' . $posted_on . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	if ( $output ) {
		echo $return;
	} else {
		return $return;
	}

}

/**
 * Prints HTML with meta information for the current author.
 */
function elcano_posted_by() {
	$byline = sprintf(
		/* translators: %s: post author. */
		esc_html_x( 'by %s', 'post author', 'elcano' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="byline"> ' . $byline . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

}

/**
 * Output post authors (from CPT biography)
 *
 * @param  int|WP_Post|null $post
 * @param  bool             $link
 * @param  bool             $output
 * @return string|void
 */
function elcano_authors( $post = null, $link = true, $output = true ) {

	$authors = get_field( 'authors', get_post( $post ) );
	$return  = '';

	if ( $authors ) {

		$names = array();

		foreach ( $authors as $author ) {
			if ( $link && 'publish' === $author->post_status ) {
				$names[] = '<a class="url fn n" href="' . get_permalink( $author ) . '">' . esc_html( $author->post_title ) . '</a>';
			} else {
				$names[] = '<span class="fn n">' . esc_html( $author->post_title ) . '</span>';
			}
		}

		$byline = sprintf(
			'<span class="by">' . esc_html_x( 'By', 'post author', 'elcano' ) . '</span> %s',
			join( ', ', $names )
		);

		$return = '<span class="byline"> ' . $byline . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	if ( $output ) {
		echo $return;
	} else {
		return $return;
	}

}

if ( ! function_exists( 'elcano_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function elcano_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$terms_list = get_the_category_list( esc_html__( ', ', 'elcano' ) );
			if ( $terms_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'elcano' ) . '</span>', $terms_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'elcano' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'elcano' ) . '</span>', $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'elcano' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'elcano' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'elcano_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function elcano_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

			<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
				<?php
					the_post_thumbnail(
						'post-thumbnail',
						array(
							'alt' => the_title_attribute(
								array(
									'echo' => false,
								)
							),
						)
					);
				?>
			</a>

			<?php
		endif; // End is_singular().
	}
endif;

/**
 * Get Post Terms with Primary Term
 *
 * Get all post terms for the taxonomy (default 'category').
 * If is set YOAST Primary Term return in $return['primary'].
 * If $return_all is true return all terms in $return['all'].
 *
 * @param  int|WP_Post|null $post
 * @param  string           $taxonomy
 * @param  bool             $return_all
 * @return array
 */
function elcano_get_terms( $post = null, $taxonomy = 'category', $return_all = false ) {

	$post   = get_post( $post );
	$return = array();

	if ( class_exists( 'WPSEO_Primary_Term' ) ) {
		// Show Primary term by Yoast if it is enabled & set
		$wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post->ID );
		$primary_term       = get_term( $wpseo_primary_term->get_primary_term() );

		if ( ! is_wp_error( $primary_term ) ) {
			$return['primary'] = $primary_term;
		}
	}

	if ( empty( $return['primary'] ) || $return_all ) {
		$terms_list = get_the_terms( $post->ID, $taxonomy );

		if ( is_array( $terms_list ) ) {
			if ( empty( $return['primary'] ) ) {
				$return['primary'] = $terms_list[0]; // get the first term
			}
			if ( $return_all ) {
				$return['all'] = $terms_list;
			}
		}
	}

	return $return;

}

/**
 * Primary Category
 *
 * @param  int|WP_Post|null $post
 * @param  string|null      $format ('name', 'url', 'link', null => WP_Term)
 * @param  bool             $output echo output
 * @return WP_Term|string|void
 */
function elcano_primary_category( $post = null, $format = 'link', $output = true ) {

	$return     = false;
	$post       = get_post( $post );
	$categories = elcano_get_terms( $post );

	if ( isset( $categories['primary'] ) ) {
		$category = $categories['primary'];

		if ( 'name' === $format ) {
			$return = $category->name;
		} elseif ( 'url' === $format || 'link' === $format ) {
			$url = get_term_link( $category );
			$url = 'post' === $post->post_type ? elcano_blog_term_link( $url, 'category' ) : $url;

			$return = 'url' === $format ? $url : sprintf( '<a href="%s" class="category" rel="category">%s</a>', esc_url( $url ), esc_html( $category->name ) );
		} else {
			$return = $category;
		}
	}

	if ( is_string( $return ) && $output ) {
		echo $return;
	} else {
		return $return;
	}

}

/**
 * Categories excluded current category
 *
 * @param  string $format
 * @param  bool   $output
 * @param  string $separator
 * @return mixed|void
 */
function elcano_other_categories( $format = null, $output = false, $separator = null ) {

	$return     = false;
	$categories = elcano_get_terms( null, 'category', true );

	if ( isset( $categories['all'] ) && count( $categories['all'] ) > 1 ) {
		unset( $categories['all'][ array_search( $categories['primary'], $categories['all'] ) ] );

		$return = array();

		foreach ( $categories['all'] as $category ) {
			if ( 'name' === $format ) {
				$return[] = $category->name;
			} elseif ( 'url' === $format ) {
				$return[] = get_term_link( $category );
			} elseif ( 'link' === $format ) {
				$return[] = '<a href="' . esc_url( get_term_link( $category ) ) . '" class="category" rel="category">' . $category->name . '</a>';
			} else {
				$return[] = $category;
			}
		}
	}

	if ( is_array( $return ) && is_string( $return[0] ) && is_string( $separator ) ) {
		$return = implode( $separator, $return );
	}

	if ( is_string( $return ) && $output ) {
		echo $return;
	} else {
		return $return;
	}

}

/**
 * Output Posts Pagination
 */
function elcano_posts_pagination( $query = null ) {

	global $wp_query;

	if ( null !== $query ) {
		$query_backup = $wp_query;
		$wp_query     = $query;
	}

	$links = paginate_links(
		array(
			'base'               => str_replace( PHP_INT_MAX, '%#%', esc_url( get_pagenum_link( PHP_INT_MAX ) ) ),
			'format'             => '?paged=%#%',
			'current'            => max( 1, get_query_var( 'paged' ) ),
			'total'              => $wp_query->max_num_pages,
			'show_all'           => false,
			'end_size'           => 1,
			'mid_size'           => 2,
			'prev_next'          => true,
			'prev_text'          => '<span class="screen-reader-text">' . __( 'Previous Page', 'elcano' ) . ' </span>',
			'next_text'          => '<span class="screen-reader-text">' . __( 'Next Page', 'elcano' ) . ' </span>',
			'before_page_number' => '<span class="screen-reader-text">' . __( 'Page', 'elcano' ) . ' </span>',
			'type'               => 'list',
		)
	);

	echo '<nav class="posts-pagination">' . $links . '</nav>';

	if ( null !== $query ) {
		$wp_query = $query_backup;
	}

}

/**
 * Output Breadcrumb
 *
 * Require plugin YoastSEO
 *
 * @uses yoast_breadcrumb()
 */
function elcano_breadcrumb() {

	if ( function_exists( 'yoast_breadcrumb' ) ) {
		yoast_breadcrumb( '<div id="breadcrumbs">', '</div>' );
	}

}

/**
 * Output Table of Contents
 *
 * Require plugin LuckyWP Table of Contents
 *
 * @param  bool $output
 * @return void
 */
function elcano_aside_toc( $class = '' ) {

	echo '<aside class="aside--toc ' . ( $class ?: 'aside--left' ) . '">' . do_shortcode( '[lwptoc]' ) . '</aside>';

}

/**
 * Output Current Post Tags
 */
function elcano_post_tags( $post = null ) {

	if ( 'post' === elcano_current_post_type( $post ) ) {
		add_filter( 'term_link', 'elcano_filter_blog_term_link', 10, 3 );
	}

	$tags_list = get_the_tag_list( '<li>', '</li><li>', '</li>' );

	if ( 'post' === elcano_current_post_type( $post ) ) {
		remove_filter( 'term_link', 'elcano_filter_blog_term_link' );
	}

	if ( $tags_list ) {
		printf( '<div class="post-tags" aria-label="%s"><ul class="tags-links">%s</ul></div>', esc_attr__( 'Tagged as', 'elcano' ), $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

}

/**
 * Output All Posts Tags
 *
 * Filter tags for blog or web content.
 *
 * @param  int|WP_Post|null $post
 * @return void
 */
function elcano_posts_tags( $post = null ) {

	$post_type = elcano_current_post_type( $post );
	$tags      = elcano_get_terms_by_post_type( 'post_tag', $post_type );

	// Sort by number of posts.
	$tags = wp_list_sort(
		$tags,
		array(
			'count' => 'DESC',
			'name'  => 'ASC',
		)
	);
	// Limit number of tags.
	$tags = array_slice( $tags, 0, 15 );

	if ( $tags ) {

		$tags_list = array();
		foreach ( $tags as $tag ) {
			$url = get_term_link( (int) $tag->term_id );
			$url = 'post' === $post_type ? elcano_blog_term_link( $url, 'post_tag' ) : $url;

			$tags_list[] = sprintf( '<li><a href="%s" rel="tag">%s</a></li>', esc_url( $url ), esc_html( $tag->name ) );
		}

		printf( '<ul class="tags-links">%s</ul>', implode( '', $tags_list ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

}

/**
 * Output All Posts Categories
 *
 * Filter categories for blog or web content.
 *
 * @param  int|WP_Post|null $post
 * @return void
 */
function elcano_posts_categories( $post = null ) {

	$post_type = elcano_current_post_type( $post );
	$cats      = elcano_get_terms_by_post_type( 'category', $post_type );

	if ( $cats ) {

		$cats_list = array();
		foreach ( $cats as $cat ) {
			$url = get_term_link( (int) $cat->term_id );
			$url = 'post' === $post_type ? elcano_blog_term_link( $url, 'category' ) : $url;

			$cats_list[] = sprintf( '<li><a href="%s" rel="category">%s</a></li>', esc_url( $url ), esc_html( $cat->name ) );
		}

		printf( '<ul class="categories-links">%s</ul>', implode( '', $cats_list ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

}


/**
 * Output related posts
 *
 * @param  int|WP_Post|null $post
 * @param  int              $number
 * @return void
 */
function elcano_related_posts( $post = null, $number = 2 ) {

	$post       = get_post( $post );
	$categories = get_the_category( $post->ID );

	if ( $categories ) {
		$args = array(
			'post_type'        => elcano_current_post_type(),
			'category__in'     => wp_list_pluck( $categories, 'term_id' ),
			'post__not_in'     => array( $post->ID ),
			'posts_per_page'   => $number,
			'suppress_filters' => false,
		);

		$related_posts = get_posts( $args );

		if ( count( $related_posts ) ) {

			echo '<div class="related-posts"><p class="widget-title widget-title--big">' . esc_html__( 'Related articles', 'elcano' ) . '</p><div>';

			foreach ( $related_posts as $related_post ) {
                $post_thumbnail = elcano_get_content_thumbnail($related_post, 100, 100);

				printf(
					'<div class="post-related"><a href="%s" rel="bookmark">%s %s %s</a></div>',
					esc_url( get_the_permalink( $related_post ) ),
					sprintf( '<div class="post-thumbnail">%s</div>', $post_thumbnail ),
					sprintf( '<p class="post-title">%s</p>', get_the_title( $related_post ) ),
					sprintf( '<p class="post-meta">%s %s</p>', elcano_authors( $related_post, false, false ), elcano_posted_on( $related_post, false ) )
				);
			}

			echo '</div></div>';

		}
	}
}

/**
 * Output share links
 *
 * @param  int|WP_Post|null $post
 * @param  array            $args
 * @return void
 */
function elcano_share( $post = null, $args = null ) {

	$url   = get_permalink( $post );
	$title = get_the_title( $post );

	// Twitter of Elcano
	$wpseo_social = get_option( 'wpseo_social', array() );
	$twitter_user = $wpseo_social['twitter_site'] ?? 'rielcano';

	$channels = array(
		'Facebook' => array(
			'link' => add_query_arg( 'u', urlencode( $url ), 'https://www.facebook.com/sharer/sharer.php' ),
			'aria' => sprintf( __( 'Share on %s', 'elcano' ), 'Facebook' ),
		),
		'Twitter'  => array(
			'link' => add_query_arg(
				array(
					'text' => urlencode( $title ),
					'url'  => urlencode( $url ),
					'via'  => urlencode( $twitter_user ),
				),
				'https://twitter.com/intent/tweet'
			),
			'aria' => sprintf( __( 'Share on %s', 'elcano' ), 'Twitter' ),
		),
		'LinkedIn' => array(
			'link' => add_query_arg( 'url', urlencode( $url ), 'https://www.linkedin.com/sharing/share-offsite/' ),
			'aria' => sprintf( __( 'Share on %s', 'elcano' ), 'LinkedIn' ),
		),
		'Email'    => array(
			'link' => add_query_arg(
				array(
					'body'    => urlencode( $title ) . ' ' . urlencode( $url ),
					'subject' => urlencode( get_bloginfo( 'name' ) ),
				),
				'mailto:'
			),
			'aria' => __( 'Share by e-mail', 'elcano' ),
			'name' => __( 'Email', 'elcano' ),
		),
		'Link'     => array(
			'link'  => $url,
			'aria'  => __( 'Permanent Link', 'elcano' ),
			'title' => __( 'Copy Link', 'elcano' ),
			'name'  => __( 'Link', 'elcano' ),
			'rel'   => 'bookmark',
		),
	);

	echo $args['before'] ?? '';
	echo '<ul class="social-links share-links">';

	foreach ( $channels as $channel => $settings ) {
		printf(
			'<li><a aria-label="%s" href="%s" rel="%s" %s><span class="icon-%s"></span> %s</a></li>',
			esc_attr( $settings['aria'] ),
			esc_url( $settings['link'] ),
			esc_attr( $settings['rel'] ?? 'external nofollow noopener noreferrer' ),
			isset( $settings['title'] ) ? sprintf( 'title="%s"', esc_attr( $settings['title'] ) ) : '',
			strtolower( $channel ),
			empty( $args['show_name'] ) ? '' : esc_html( $settings['name'] ?? $channel )
		);
	}

	echo '</ul>';
	echo $args['after'] ?? '';

}

/**
 * Get all articles by an author
 *
 * Return posts and web content (-activities)
 *
 * @param  int  $author ID
 * @param  bool $count true to return number of posts, false for list of posts
 * @return int|array
 */
function elcano_get_posts_by_author( $author_id = null, $count = false ) {

	// Biography post id.
	$author_id = $author_id ?? get_the_ID();

	$args = array(
		'posts_per_page' => -1,
		'post_type'      => array_merge( array( 'post' ), elcano_current_post_type( 'web' ) ),
		'meta_query'     => array(
			array(
				'key'     => 'authors',              // Name of custom field
				'value'   => '"' . $author_id . '"', // Matches exactly "ID" (with quotes)
				'compare' => 'LIKE',
			),
		),
	);

	$posts_by_author = new WP_Query( $args );

	return $count ? $posts_by_author->found_posts : $posts_by_author->posts;

}

/**
 * Get all the activities an author participates in
 *
 * Return posts and web content (-activities)
 *
 * @param  int  $author ID
 * @param  bool $count true to return number of posts, false for list of posts
 * @return int|array
 */
function elcano_get_activities_by_author( $author_id = null, $count = false ) {

	// Biography post id.
	$author_id = $author_id ?? get_the_ID();

	$args = array(
		'posts_per_page' => -1,
		'post_type'      => 'activity',
		'meta_query'     => array(
			array(
				'key'     => 'participants',         // Name of custom field
				'value'   => '"' . $author_id . '"', // Matches exactly "ID" (with quotes)
				'compare' => 'LIKE',
			),
		),
		'meta_key'       => 'date_start',
		'orderby'        => 'meta_value',
		'order'          => 'DESC',
	);

	$activities_by_author = new WP_Query( $args );

	return $count ? $activities_by_author->found_posts : $activities_by_author->posts;

}

/**
 * Get post's post_type label
 *
 * @param  int|WP_Post|null $post
 * @return string
 */
function elcano_post_type_label( $post = null ) {

	$post      = get_post( $post );
	$post_type = get_post_type_object( get_post_type( $post ) );

	return $post_type ? $post_type->labels->singular_name : '';

}

/**
 * Output Pren/Next lateral links
 *
 * @return void
 */
function elcano_lateral_nav() {

	if ( is_singular( array( 'activity', 'project', 'brussels_activity' ) ) ) {

		call_user_func( 'elcano_' . get_post_type() . '_lateral_nav' );

	} elseif ( is_single() ) {

		$prev_post = get_previous_post( true );
		$next_post = get_next_post( true );

		if ( $prev_post || $next_post ) {

			echo '<div class="nav-lateral">';

			if ( $prev_post ) {
                $post_thumbnail = elcano_get_content_thumbnail($prev_post, 100, 100);
				printf(
					'<div class="nav nav-prev"><a href="%s" id="clicAnterior" rel="prev"><div class="link-label">%s</div><div class="link-card">%s %s %s</div></a></div>',
					esc_url( get_the_permalink( $prev_post ) ),
					esc_html__( 'Previous', 'elcano' ),
					sprintf( '<div class="post-thumbnail">%s</div>', $post_thumbnail ),
					sprintf( '<div class="post-title">%s</div>', get_the_title( $prev_post ) ),
					sprintf( '<div class="post-date">%s</div>', elcano_posted_on( $prev_post, false ) )
				);
			}

			if ( $next_post ) {
                $post_thumbnail = elcano_get_content_thumbnail($next_post, 100, 100);
				printf(
					'<div class="nav nav-next"><a href="%s" id="clicSiguiente" rel="next"><div class="link-label">%s</div><div class="link-card">%s %s %s</div></a></div>',
					esc_url( get_the_permalink( $next_post ) ),
					esc_html__( 'Next', 'elcano' ),
					sprintf( '<div class="post-thumbnail">%s</div>', $post_thumbnail ),
					sprintf( '<div class="post-title">%s</div>', get_the_title( $next_post ) ),
					sprintf( '<div class="post-date">%s</div>', elcano_posted_on( $next_post, false ) )
				);
			}

			echo '</div>';
		}
	}

}

/**
 * Output Posts Slider Carousel
 *
 * @param  array WP_Posts $posts
 * @return void
 */
function elcano_post_slider( $posts ) {

	if ( ! $posts ) {
		return;
	}

	// Enqueue slider script.
	elcano_append_dependency( 'elcano-theme', 'elcano-slider' );

	?>
	<div class="post-slider">
		<ul class="slider-controls">
			<li class="prev"><?php _e( 'Previous', 'elcano' ); ?></li>
			<li class="next"><?php _e( 'Next', 'elcano' ); ?></li>
			<li class="pages"><span class="current">1</span> <?php _e( 'of', 'elcano' ); ?> <span class="total">1</span></li>
		</ul>
		<div class="post-slides">
			<?php
			foreach ( $posts as $post_slide ) {
                $post_thumbnail = elcano_get_content_thumbnail($post_slide, 368, 240);
				printf(
					'<article class="post-slide">%s %s %s</article>',
					sprintf( '<div class="post-thumbnail">%s</div>', $post_thumbnail ),
					sprintf( '<div class="post-category">%s</div>', elcano_primary_category( $post_slide, 'link', false ) ),
					sprintf( '<div class="post-title"><a href="%s" rel="bookmark">%s</a></div>', esc_url( get_the_permalink( $post_slide ) ), get_the_title( $post_slide ) ),
				);
			}
			?>
		</div>
	</div>
	<?php

}

function elcano_get_content_thumbnail($post, $width, $height): string {
    $directory = get_template_directory_uri();
    $post_thumbnail = get_the_post_thumbnail( $post, array( $width, $height ) );
    if( empty( $post_thumbnail ) ) {
        if($post->post_type === 'analysis') {
            $post_thumbnail = '<img src="' . $directory . '/images/analisis.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'poll') {
            $post_thumbnail = '<img src="' . $directory . '/images/encuesta.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'commentary') {
            $post_thumbnail = '<img src="' . $directory . '/images/comentario.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'ciber') {
            $post_thumbnail = '<img src="' . $directory . '/images/ciber.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'magazine') {
            $post_thumbnail = '<img src="' . $directory . '/images/revista.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'report') {
            $post_thumbnail = '<img src="' . $directory . '/images/informe.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'post') {
            $post_thumbnail = '<img src="' . $directory . '/images/post.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'policy_paper') {
            $post_thumbnail = '<img src="' . $directory . '/images/policy.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'work_document') {
            $post_thumbnail = '<img src="' . $directory . '/images/documento.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'video') {
            $post_thumbnail = '<img src="' . $directory . '/images/video.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'archive') {
            $post_thumbnail = '<img src="' . $directory . '/images/archivo.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'activity') {
            $post_thumbnail = '<img src="' . $directory . '/images/actividad.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'single-report') {
            $post_thumbnail = '<img src="' . $directory . '/images/informe.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'newsletter') {
            $post_thumbnail = '<img src="' . $directory . '/images/newsletter.png" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'press_release') {
            $post_thumbnail = '<img src="' . $directory . '/images/nota-prensa.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'news_on_net') {
            $post_thumbnail = '<img src="' . $directory . '/images/novedades-red.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'podcast') {
            $post_thumbnail = '<img src="' . $directory . '/images/podcast.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'project') {
            $post_thumbnail = '<img src="' . $directory . '/images/proyecto.png" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'brussels_activity') {
            $post_thumbnail = '<img src="' . $directory . '/images/actividad.jpg" alt="'. $post->post_title . '" />';
        } else if($post->post_type === 'inside_spain') {
            $post_thumbnail = '<img src="' . $directory . '/images/inside-spain.jpg" alt="'. $post->post_title . '" />';
        }
    }
    return $post_thumbnail;
}

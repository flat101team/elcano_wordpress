<?php
/**
 * Elcano Required & Recommended Plugins
 *
 * @package Elcano
 */

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once ELCANO_PATH . '/lib/tgmpa/class-tgm-plugin-activation.php';


/**
 * Register the required plugins for this theme.
 */
function elcano__register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		array(
			'name'     => 'PublishPress',
			'slug'     => 'publishpress',
			'required' => true,
		),

		array(
			'name'     => 'Disable Comments',
			'slug'     => 'disable-comments',
			'required' => true,
		),

		array(
			'name'     => 'Stream',
			'slug'     => 'stream',
			'required' => true,
		),

		array(
			'name'     => 'WordPress SEO by Yoast',
			'slug'     => 'wordpress-seo',
			'required' => false,
		),

		array(
			'name'     => 'Yoast Duplicate Post',
			'slug'     => 'duplicate-post',
			'required' => true,
		),

		array(
			'name'     => 'Gutenberg PDF Viewer Block',
			'slug'     => 'pdf-viewer-block',
			'required' => true,
		),

		array(
			'name'     => 'LuckyWP Table of Contents',
			'slug'     => 'luckywp-table-of-contents',
			'required' => true,
		),

		array(
			'name'         => 'Intervention',
			'slug'         => 'intervention',
			'source'       => 'https://github.com/soberwp/intervention/archive/master.zip',
			'external_url' => 'https://github.com/soberwp/intervention',
			'required'     => false,
		),

		array(
			'name'     => 'Google Tag Manager for WordPress',
			'slug'     => 'duracelltomi-google-tag-manager',
			'required' => true,
		),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'elcano',                // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'elcano__register_required_plugins' );

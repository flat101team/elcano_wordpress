<?php

/**
 * Elcano_Register_Custom_Blocks
 *
 * @package   Elcano
 * @author    Flat101
 * @copyright 2023 Flat101
 * @license   GPL 2.0+
 * @link      https://www.flat101.es
 */


class Elcano_Register_Custom_Blocks {

	/**
	 * @return Elcano_Register_Custom_Blocks
	 */
	public static function init(): Elcano_Register_Custom_Blocks {
		$self = new self();
		add_action( 'init', array( $self, 'load_blocks' ) );

		/* Add block categories */
		add_filter( 'block_categories_all', array( $self, 'load_categories' ), 10, 2 );

		add_action( 'acf/init', array( $self, 'load_custom_fields' ) );

		return $self;
	}

	/**
	 * @return void
	 */
	public function load_blocks(): void {
		register_block_type( ELCANO_PATH . '/inc/blocks/elcano-card/block.json' );
		register_block_type( ELCANO_PATH . '/inc/blocks/elcano-newsletter-form/block.json' );
		register_block_type( ELCANO_PATH . '/inc/blocks/elcano-timeline/build/block.json' );
		register_block_type( ELCANO_PATH . '/inc/blocks/elcano-timeline-element/build/block.json' );
	}

	public function load_custom_fields() {
		require_once ELCANO_PATH . '/inc/blocks/elcano-card/block-fields.php';
	}

	public function load_categories( $categories, $block_editor_context ) {
		return array_merge(
			array(
				array(
					'slug'  => 'elcano-blocks',
					'title' => __( 'Elcano Blocks', 'elcano' ),
				),
			),
			$categories
		);
	}
}

Elcano_Register_Custom_Blocks::init();

/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import {__} from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-block-editor/#useBlockProps
 */
import {
	useBlockProps,
	InnerBlocks,
} from '@wordpress/block-editor';

import {Button} from '@wordpress/components';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit({attributes, setAttributes}) {
	const blockProps = useBlockProps(
		{className: 'timeline-holder'},
	);
	const ALLOWED_BLOCKS = [
		'elcano/elcano-timeline-element'
	];

	/*const TEMPLATE = [
		[
			'core/cover',
			{
				url: '/wp-content/plugins/cambra-custom-blocks/images/bg-example-image.webp',
				backgroundType: 'image',
				id: 134,
				dimRatio: 70,
			},
			[
				[
					'cambra-custom-blocks/cambra-breadcrumbs',
					{
						textColor: 'cambra-bg',
					}],
				[
					'core/heading',
					{
						level: 1,
						content: 'Título de la sección',
						textColor: 'cambra-bg',
					}],
				[
					'core/heading',
					{
						level: 5,
						content: 'Subtítulo de la sección distribuidora',
						textColor: 'cambra-bg',
					}],
			],
		],
	];*/

	return (
		<>
			<div {...blockProps}>
				<InnerBlocks
					allowedBlocks={ALLOWED_BLOCKS}
					//template={TEMPLATE}
					//templateLock="all"
				/>
			</div>
		</>
	);
}

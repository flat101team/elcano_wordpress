/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import {__} from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-block-editor/#useBlockProps
 */
import {
	InnerBlocks,
	InspectorControls,
	useBlockProps,
} from '@wordpress/block-editor';

import {
	PanelBody,
	TextControl,
	RadioControl,
} from '@wordpress/components';

import {Button} from '@wordpress/components';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit({attributes, setAttributes}) {
	const ALLOWED_BLOCKS = [
		'core/heading',
		'core/paragraph',
		'core/cover',
		'core/separator',
		'cambra-custom-blocks/cambra-breadcrumbs'];

	const {
		title,
		position
	} = attributes;

	const blockProps = useBlockProps(
		{className: 'timeline-holder' + ' ' +  'title-' + position},
	);

	/*const TEMPLATE = [
		[
			'core/cover',
			{
				url: '/wp-content/plugins/cambra-custom-blocks/images/bg-example-image.webp',
				backgroundType: 'image',
				id: 134,
				dimRatio: 70,
			},
			[
				[
					'cambra-custom-blocks/cambra-breadcrumbs',
					{
						textColor: 'cambra-bg',
					}],
				[
					'core/heading',
					{
						level: 1,
						content: 'Título de la sección',
						textColor: 'cambra-bg',
					}],
				[
					'core/heading',
					{
						level: 5,
						content: 'Subtítulo de la sección distribuidora',
						textColor: 'cambra-bg',
					}],
			],
		],
	];*/

	console.log(blockProps);
	return (
		<>
			<InspectorControls>
				<PanelBody title={ __( 'Settings' ) }>
					<TextControl
						label={ __( 'Element title', 'elcano' ) } 
						help={ __( 'Title than will appear on the timeline element', 'elcano' ) } 
						value={ title }
						onChange={value => setAttributes({title: value})}
					/>
					<RadioControl
						label={ __( 'Title position', 'elcano' ) } 
						selected={ position }
						options={ [
							{ label: __( 'Top', 'elcano' ), value: 'top' },
							{ label: __( 'Bottom', 'elcano' ), value: 'bottom' },
						] }
						onChange={value => setAttributes({position: value})}
					/>
				</PanelBody>
			</InspectorControls>
			<div {...blockProps}>
				<p className="timeline-element--title">{ title }</p>
				<div className="timeline-element--content">
					<InnerBlocks
						//allowedBlocks={ALLOWED_BLOCKS}
						//template={TEMPLATE}
						//templateLock="all"
					/>
				</div>
				<span className="timeline-element--icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
						<path d="M24 12.5386C24 19.166 18.6274 24.5386 12 24.5386C5.37258 24.5386 0 19.166 0 12.5386C0 5.91116 5.37258 0.538574 12 0.538574C18.6274 0.538574 24 5.91116 24 12.5386ZM2.4 12.5386C2.4 17.8405 6.69807 22.1386 12 22.1386C17.3019 22.1386 21.6 17.8405 21.6 12.5386C21.6 7.23664 17.3019 2.93857 12 2.93857C6.69807 2.93857 2.4 7.23664 2.4 12.5386Z" fill="#BB2521"/>
						<circle cx="12" cy="12.5386" r="7" fill="#BB2521"/>
					</svg>
				</span>
			</div>
		</>
	);
}

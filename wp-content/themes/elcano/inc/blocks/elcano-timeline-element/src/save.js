/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import {__} from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-block-editor/#useBlockProps
 */
import {useBlockProps, InnerBlocks} from '@wordpress/block-editor';

/**
 * The save function defines the way in which the different attributes should
 * be combined into the final markup, which is then serialized by the block
 * editor into `post_content`.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#save
 *
 * @return {WPElement} Element to render.
 */
export default function save({attributes}) {

	const {
		title,
		position
	} = attributes;

	const blockProps = useBlockProps.save({
		className: 'timeline-element' + ' ' +  'title-' + position,
	});
	
	return (
			<div {...blockProps}>
				<p className="timeline-element--title">{ title }</p>
				<div className="timeline-element--content">
					<InnerBlocks.Content/>
				</div>
				<span className="timeline-element--icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
						<path d="M24 12.5386C24 19.166 18.6274 24.5386 12 24.5386C5.37258 24.5386 0 19.166 0 12.5386C0 5.91116 5.37258 0.538574 12 0.538574C18.6274 0.538574 24 5.91116 24 12.5386ZM2.4 12.5386C2.4 17.8405 6.69807 22.1386 12 22.1386C17.3019 22.1386 21.6 17.8405 21.6 12.5386C21.6 7.23664 17.3019 2.93857 12 2.93857C6.69807 2.93857 2.4 7.23664 2.4 12.5386Z" fill="#BB2521"/>
						<circle cx="12" cy="12.5386" r="7" fill="#BB2521"/>
					</svg>
				</span>
			</div>
	);
}

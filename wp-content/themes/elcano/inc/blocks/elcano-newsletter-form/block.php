<?php

// Dynamic block ID
if ( ! empty( $block ) ) {
	$block_id = 'elcano-card-' . $block['id'];
}

// Create class attribute allowing for custom "className" value.
$class_name = 'elcano-card';
if ( ! empty( $block['className'] ) ) {
	$class_name .= ' ' . $block['className'];
}


$data = array();

get_template_part( 'template-parts/section/newsletter-form', null, $data );

<?php

// Dynamic block ID
if ( ! empty( $block ) ) {
	$block_id = 'elcano-card-' . $block['id'];
}

// Create class attribute allowing for custom "className" value.
$class_name = 'elcano-card';
if ( ! empty( $block['className'] ) ) {
	$class_name .= ' ' . $block['className'];
}


$data = array(
	'mode'           => get_field('mode'),
	'no_thumbnail'   => get_field('hide_thumbnail'),
	'hide_category'  => get_field('hide_category'),
	'show_excerpt'   => get_field('show_excerpt'),
	'show_post_type' => get_field('show_post_type'),
	'heading'        => 'h4',
);

get_template_part( 'template-parts/archive/publications', null, $data );

<?php
/**
 * Elcano Roles & Capabilities
 *
 * @package Elcano
 */


// Define Elcano custom roles relation with WordPress roles.
define( 'ELCANO_ROLE_ADMIN', 'administrator' );
define( 'ELCANO_ROLE_EDITOR', 'editor' );
define( 'ELCANO_ROLE_AUTHOR', 'contributor' );

/**
 * Rename WordPress roles to Elcano roles
 *
 * @return void
 */
function elcano_rename_roles() {

	global $wp_roles;

	if ( ! isset( $wp_roles ) ) {
		$wp_roles = new WP_Roles();
	}

	$wp_roles->roles[ ELCANO_ROLE_ADMIN ]['name'] = __( 'Administrator', 'elcano' );
	$wp_roles->role_names[ ELCANO_ROLE_ADMIN ]    = __( 'Administrator', 'elcano' );

	$wp_roles->roles[ ELCANO_ROLE_EDITOR ]['name'] = __( 'Editor', 'elcano' );
	$wp_roles->role_names[ ELCANO_ROLE_EDITOR ]    = __( 'Editor', 'elcano' );

	$wp_roles->roles[ ELCANO_ROLE_AUTHOR ]['name'] = __( 'Author', 'elcano' );
	$wp_roles->role_names[ ELCANO_ROLE_AUTHOR ]    = __( 'Author', 'elcano' );

}
add_action( 'init', 'elcano_rename_roles' );


/**
 * Reset roles and capabilities to Elcano's defaults
 *
 * @param  bool $hard_reset if true also delete all other caps
 * @return void
 */
function elcano_reset_roles_capabilities( $hard_reset = false ) {

	global $wp_roles;
	global $wp_post_types;

	if ( ! function_exists( 'populate_roles' ) ) {
		require_once ABSPATH . 'wp-admin/includes/schema.php';
	}

	/**
	 * Remove all capabilities for WordPress roles and add defaults.
	 */
	if ( $hard_reset ) {
		if ( ! isset( $wp_roles ) ) {
			$wp_roles = new WP_Roles();
		}

		$roles = array( ELCANO_ROLE_ADMIN, ELCANO_ROLE_EDITOR, ELCANO_ROLE_AUTHOR );

		foreach ( $roles as $role_name ) {
			$role      = get_role( $role_name );
			$role_info = $wp_roles->roles[ $role_name ];

			foreach ( $role_info['capabilities'] as $capability => $_ ) :
				$role->remove_cap( $capability );
			endforeach;
		}
	}

	// Execute WordPress role creation.
	populate_roles();

	/**
	 * Customize WordPress roles to Elcano needs.
	 */

	// Elcano Custom Post Types (view /inc/cpts.php).
	$post_types = array(
		'analysis',
		'work_document',
		'report',
		'poll',
		'special',
		'monograph',
		'policy_paper',
		'commentary',
		'podcast',
		'video',
		'archive',
		'newsletter',
		'news_on_net',
		'ciber',
		'magazine',
		'press_release',
		'activity',
		'job',
		'biography',
		'inside_spain',
        'project',
        'brussels_activity',
        'scientific_council'
	);

	/**
	 * Elcano role "Administrador".
	 */
	$admin = get_role( ELCANO_ROLE_ADMIN );

	// Add all caps to admininistrators.
	foreach ( $post_types as $post_type ) {
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->edit_post );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->read_post );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->delete_post );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->edit_posts );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->edit_others_posts );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->delete_posts );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->publish_posts );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->read_private_posts );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->delete_private_posts );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->delete_published_posts );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->delete_others_posts );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->edit_private_posts );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->edit_published_posts );
		$admin->add_cap( $wp_post_types[ $post_type ]->cap->create_posts );
	}

	// Duplicate Posts.
	$admin->add_cap( 'copy_posts' );

	// Unset 'biography'.
	array_pop( $post_types );

	/**
	 * Elcano role "Editor".
	 */
	$editor = get_role( ELCANO_ROLE_EDITOR );

	// Can edit all CPTs but not can publish.
	foreach ( $post_types as $post_type ) {
		if ($post_type !== 'biography') {
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->edit_post );
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->read_post );
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->delete_post );
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->delete_posts );
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->read_private_posts );
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->delete_private_posts );
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->delete_published_posts );
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->edit_private_posts );
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->edit_published_posts );
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->create_posts );
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->edit_others_posts );
			$editor->add_cap( $wp_post_types[ $post_type ]->cap->edit_posts );
		}

		$editor->add_cap( 'upload_files' );

		// Can edit own biography.
		$editor->add_cap( $wp_post_types['biography']->cap->read_post );
		$editor->add_cap( $wp_post_types['biography']->cap->edit_post );
		$editor->add_cap( $wp_post_types['biography']->cap->edit_posts );
		$editor->remove_cap( $wp_post_types['biography']->cap->edit_others_posts );
		$editor->remove_cap( $wp_post_types['biography']->cap->delete_others_posts );
	}

	// Pages.
	$editor->remove_cap( $wp_post_types['page']->cap->delete_post );
	$editor->remove_cap( $wp_post_types['page']->cap->delete_posts );
	$editor->remove_cap( $wp_post_types['page']->cap->delete_private_posts );
	$editor->remove_cap( $wp_post_types['page']->cap->delete_published_posts );
	$editor->remove_cap( $wp_post_types['page']->cap->delete_others_posts );
	$editor->remove_cap( 'publish_pages' );
	$editor->add_cap( 'edit_published_pages' );

	// Can't manage categories or tags.
	$editor->remove_cap( 'manage_categories' );

	// Duplicate Posts.
	$editor->add_cap( 'copy_posts' );

	/**
	 * Elcano role "Autor".
	 */
	$author = get_role( ELCANO_ROLE_AUTHOR );

	// Can upload files.
	$author->remove_cap( 'upload_files' );

	// Can edit own cpt posts
	foreach ( $post_types as $post_type ) {
		$author->add_cap( $wp_post_types[ $post_type ]->cap->edit_post );
		$author->add_cap( $wp_post_types[ $post_type ]->cap->read_post );
		$author->remove_cap( $wp_post_types[ $post_type ]->cap->delete_post );
		$author->remove_cap( $wp_post_types[ $post_type ]->cap->delete_posts );
		$author->add_cap( $wp_post_types[ $post_type ]->cap->read_private_posts );
		$author->remove_cap( $wp_post_types[ $post_type ]->cap->delete_private_posts );
		$author->add_cap( $wp_post_types[ $post_type ]->cap->delete_published_posts );
		$author->add_cap( $wp_post_types[ $post_type ]->cap->edit_private_posts );
		$author->add_cap( $wp_post_types[ $post_type ]->cap->edit_published_posts );
		$author->add_cap( $wp_post_types[ $post_type ]->cap->create_posts );
		$author->remove_cap( $wp_post_types[ $post_type ]->cap->edit_others_posts );
		$author->add_cap( $wp_post_types[ $post_type ]->cap->edit_posts );
	}

	// Duplicate Posts.
	$author->add_cap( 'copy_posts' );

}
add_action( 'elcano_theme_activation', 'elcano_reset_roles_capabilities', 9 );

/**
 * Disallow add tags & categories for non admins
 *
 * @param  string|WP_Error $term
 * @param  string          $taxonomy
 * @return string|WP_Error
 */
function elcano_disallow_insert_terms( $term, $taxonomy ) {

	$user = wp_get_current_user();

	if ( in_array( $taxonomy, array( 'post_tag', 'category' ) ) && ! in_array( ELCANO_ROLE_ADMIN, $user->roles ) ) {
		return new WP_Error(
			'disallow_insert_term',
			__( 'Your role does not have permission to add tags or categories', 'elcano' )
		);
	}

	return $term;

}
add_filter( 'pre_insert_term', 'elcano_disallow_insert_terms', 10, 2 );


/**
 * Duplicate Post link only for own posts
 *
 * If user can't edit other posts don't show "republish link"
 *
 * @param  bool    $show
 * @param  WP_Post $post
 * @return bool
 */
function elcano_only_duplicate_own_posts( $show, $post ) {
	global $wp_post_types;

	$cap = $wp_post_types[ $post->post_type ]->cap->edit_others_posts;

	if ( ! current_user_can( $cap ) && intval( $post->post_author ) !== get_current_user_id() ) {
		$show = false;
	}

	return $show;
}
add_filter( 'duplicate_post_show_link', 'elcano_only_duplicate_own_posts', 10, 2 );


/**
 * Fix Biography submenu
 *
 * WordPress error when cap->edit_posts !== cap->create_posts and user can edit posts
 * but can't add posts don't allow one menu entry without submenu "Add post type".
 *
 * view: https://wordpress.stackexchange.com/a/178059
 * view: https://core.trac.wordpress.org/ticket/22895
 */
function elcano_fix_submenu_step_1() {
	add_submenu_page( 'edit.php?post_type=biography', 'Biography', 'Biography', 'edit_biographies', 'fix_biography_submenu' );
	add_filter( 'add_menu_classes', 'elcano_fix_submenu_step_2' );
}
add_action( 'admin_menu', 'elcano_fix_submenu_step_1' );

function elcano_fix_submenu_step_2( $menu ) {
	remove_submenu_page( 'edit.php?post_type=biography', 'fix_biography_submenu' );
	return $menu;
}

function wps_remove_role() {
	remove_role( 'author' );
}
add_action( 'init', 'wps_remove_role' );
<?php
/**
 * Elcano Custom Post Types
 *
 * @package Elcano
 */

/**
 * Generate translation ready post labels
 *
 * Change default behavior of Extended CPTs and move 'singular' and 'plural' values
 * from second to first parameter. Also added 'gender' value for female strings.
 *
 * @param  array  $args
 * @param  string $post_type
 * @return array
 */
function elcano_custom_post_type_labels( $args, $post_type ) {

	$singular     = isset( $args['singular'] ) ? $args['singular'] : ucwords( str_replace( array( '-', '_' ), ' ', $post_type ) );
	$plural       = isset( $args['plural'] ) ? $args['plural'] : $singular . 's';
	$singular_low = strtolower( $singular );
	$plural_low   = strtolower( $plural );
	$male         = isset( $args['gender'] ) ? strtolower( $args['gender'] ) !== 'female' : true;

	$labels = array(
		'name'                     => $plural,
		'singular_name'            => $singular,
		'menu_name'                => $plural,
		'name_admin_bar'           => $singular,
		'add_new'                  => $male ? _x( 'Add New', 'male', 'elcano' ) : _x( 'Add New', 'female', 'elcano' ),
		'add_new_item'             => sprintf( $male ? _x( 'Add New %s', 'male', 'elcano' ) : _x( 'Add New %s', 'female', 'elcano' ), $singular_low ),
		'edit_item'                => sprintf( __( 'Edit %s', 'elcano' ), $singular_low ),
		'new_item'                 => sprintf( $male ? _x( 'New %s', 'male', 'elcano' ) : _x( 'New %s', 'female', 'elcano' ), $singular_low ),
		'view_item'                => sprintf( __( 'View %s', 'elcano' ), $singular_low ),
		'view_items'               => sprintf( __( 'View %s', 'elcano' ), $plural_low ),
		'search_items'             => sprintf( __( 'Search %s', 'elcano' ), $plural_low ),
		'not_found'                => sprintf( __( 'No %s found.', 'elcano' ), $plural_low ),
		'not_found_in_trash'       => sprintf( __( 'No %s found in trash.', 'elcano' ), $plural_low ),
		'parent_item_colon'        => sprintf( __( 'Parent %s:', 'elcano' ), $singular ),
		'all_items'                => sprintf( $male ? _x( 'All %s', 'male', 'elcano' ) : _x( 'All %s', 'female', 'elcano' ), $plural_low ),
		'archives'                 => sprintf( __( '%s Archives', 'elcano' ), $singular ),
		'attributes'               => sprintf( __( '%s Attributes', 'elcano' ), $singular ),
		'insert_into_item'         => sprintf( __( 'Insert into %s', 'elcano' ), $singular_low ),
		'uploaded_to_this_item'    => sprintf( $male ? _x( 'Uploaded to this %s', 'male', 'elcano' ) : _x( 'Uploaded to this %s', 'female', 'elcano' ), $singular_low ),
		'filter_items_list'        => sprintf( __( 'Filter %s list', 'elcano' ), $plural_low ),
		'filter_by_date'           => __( 'Filter by date', 'elcano' ),
		'items_list_navigation'    => sprintf( __( '%s list navigation', 'elcano' ), $plural ),
		'items_list'               => sprintf( __( '%s list', 'elcano' ), $plural ),
		'item_published'           => sprintf( $male ? _x( '%s published.', 'male', 'elcano' ) : _x( '%s published.', 'female', 'elcano' ), $singular ),
		'item_published_privately' => sprintf( $male ? _x( '%s published privately.', 'male', 'elcano' ) : _x( '%s published privately.', 'female', 'elcano' ), $singular ),
		'item_reverted_to_draft'   => sprintf( $male ? _x( '%s reverted to draft.', 'male', 'elcano' ) : _x( '%s reverted to draft.', 'female', 'elcano' ), $singular ),
		'item_scheduled'           => sprintf( $male ? _x( '%s scheduled.', 'male', 'elcano' ) : _x( '%s scheduled.', 'female', 'elcano' ), $singular ),
		'item_updated'             => sprintf( $male ? _x( '%s updated.', 'male', 'elcano' ) : _x( '%s updated.', 'female', 'elcano' ), $singular ),
		'item_link'                => sprintf( __( '%s Link', 'elcano' ), $singular ),
		'item_link_description'    => sprintf( __( 'A link to a %s.', 'elcano' ), $singular_low ),
	);

	if ( isset( $args['labels'] ) && is_array( $args['labels'] ) ) {
		$args['labels'] = array_merge( $labels, $args['labels'] );
	} else {
		$args['labels'] = $labels;
	}

	unset( $args['singular'], $args['plural'], $args['gender'] );

	return $args;

}
add_filter( 'ext-cpts/args', 'elcano_custom_post_type_labels', 10, 2 );


/**
 * Register Custom Post Types
 *
 * @return void
 */
function elcano_register_custom_post_types() {

	/**
	 * Common CPT settings (can be overrriden)
	 */
	$default_config = array(

		// Supports.
		'supports'           => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			// 'excerpt',
			// 'trackbacks',
			// 'custom-fields',
			// 'comments',
			// 'revisions',
			// 'page-attributes',
			// 'post-formats',
		),

		// Use the block editor.
		'block_editor'       => true,
		'show_in_rest'       => true,

		// Archive.
		'has_archive'        => false,

		// Dashboard.
		'dashboard_activity' => true,
		'dashboard_glance'   => true,

		// Map capabilities.
		'map_meta_cap'       => true,

		// Site's main RSS feed
		'show_in_feed'       => false,

		// Common taxonomies
		'taxonomies'         => array(
			'category',
			'post_tag',
			'specials_category'
		),

		'menu_position'      => 30,

		// Add some custom columns to the admin screen.
		'admin_cols'         => array(
			'published' => array(
				'title'       => __( 'Published', 'elcano' ),
				'post_field'  => 'post_date',
				'date_format' => 'd/m/Y',
			),
			'category'          => array( 'taxonomy' => 'category' ),
			'tags'              => array( 'taxonomy' => 'post_tag' ),
			'specials_category' => array( 'taxonomy' => 'specials_category' ),
		),
	);

	/**
	 * CPT: Análisis (ARI)
	 */
	register_extended_post_type(
		'analysis',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Analysis', 'elcano' ),
				'plural'          => __( 'Analyses', 'elcano' ),
				'menu_icon'       => 'dashicons-chart-area',
				'capability_type' => array( 'analysis', 'analyses' ),
				'show_in_feed'    => true,
				'has_archive'     => true,
			)
		),
		array( 'slug' => 'analyses' )
	);

	/**
	 * CPT: Documento de trabajo (DT)
	 */
	register_extended_post_type(
		'work_document',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Working Paper', 'elcano' ),
				'plural'          => __( 'Working Papers', 'elcano' ),
				'menu_icon'       => 'dashicons-media-document',
				'capability_type' => 'work_document',
				'show_in_feed'    => true,
				'has_archive'     => true,
			)
		),
		array( 'slug' => 'work-document' )
	);

	/**
	 * CPT: Informe
	 */
	register_extended_post_type(
		'report',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Report', 'elcano' ),
				'plural'          => __( 'Reports', 'elcano' ),
				'menu_icon'       => 'dashicons-analytics',
				'capability_type' => 'report',
				'has_archive'     => true,
			)
		),
		array( 'slug' => 'report' )
	);

	/**
	 * CPT: Encuesta de opinión / Barómetro del Real Instituto Elcano (BRIE)
	 */
	register_extended_post_type(
		'poll',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Survey', 'elcano' ),
				'plural'          => __( 'Surveys', 'elcano' ),
				'gender'          => 'female',
				'menu_icon'       => 'dashicons-chart-pie',
				'capability_type' => 'poll',
				'has_archive'     => true,
			)
		),
		array( 'slug' => 'surveys' )
	);

	/**
	 * CPT: Especiales web
	 */
	register_extended_post_type(
		'special',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Web special', 'elcano' ),
				'plural'          => __( 'Web specials', 'elcano' ),
				'menu_icon'       => 'dashicons-star-filled',
				'capability_type' => 'special',
				'has_archive'     => true,
			)
		),
		array( 'slug' => 'specials' )
	);

	/**
	 * CPT: Monografía / Estudios Elcano
	 */
	register_extended_post_type(
		'monograph',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Monograph', 'elcano' ),
				'plural'          => __( 'Monographs', 'elcano' ),
				'gender'          => 'female',
				'menu_icon'       => 'dashicons-portfolio',
				'capability_type' => 'monograph',
				'has_archive'     => true,
			)
		),
		array( 'slug' => 'monographs' )
	);

	/**
	 * CPT: Elcano Policy Paper
	 */
	register_extended_post_type(
		'policy_paper',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Policy Paper', 'elcano' ),
				'plural'          => __( 'Policy Papers', 'elcano' ),
				'menu_icon'       => 'dashicons-privacy',
				'capability_type' => 'policy_paper',
				'has_archive'     => true,
			)
		),
		array( 'slug' => 'policy-paper' )
	);

	/**
	 * CPT: Comentario Elcano
	 */
	register_extended_post_type(
		'commentary',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Experts Comment', 'elcano' ),
				'plural'          => __( 'Experts Comments', 'elcano' ),
				'menu_icon'       => 'dashicons-format-quote',
				'capability_type' => 'commentary',
				'show_in_feed'    => true,
				'has_archive'     => true,
			)
		),
		array( 'slug' => 'commentaries' )
	);

	/**
	 * CPT: Podcast
	 */
	register_extended_post_type(
		'podcast',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Podcast', 'elcano' ),
				'plural'          => __( 'Podcasts', 'elcano' ),
				'menu_icon'       => 'dashicons-format-audio',
				'capability_type' => 'podcast',
				'has_archive'     => true,
			)
		),
		array( 'slug' => 'podcast' )
	);

	/**
	 * CPT: Video
	 */
	register_extended_post_type(
		'video',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Video', 'elcano' ),
				'plural'          => __( 'Videos', 'elcano' ),
				'menu_icon'       => 'dashicons-format-video',
				'capability_type' => 'video',
				'has_archive'     => true,
			)
			),
			array( 'slug' => 'videos' )
	);

	/**
	 * CPT: Location
	 */
	register_extended_post_type(
		'location',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Location', 'elcano' ),
				'plural'          => __( 'Locations', 'elcano' ),
				'menu_icon'       => 'dashicons-location',
				'capability_type' => 'video',
				'taxonomies'       => array(),
			)
			),
			array( 'slug' => 'locations' )
	);

	/**
	 * CPT: Archivo
	 */
	register_extended_post_type(
		'archive',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Archive', 'elcano' ),
				'plural'          => __( 'Archives', 'elcano' ),
				'menu_icon'       => 'dashicons-open-folder',
				'supports'        => array_merge( $default_config['supports'], array( 'excerpt' ) ),
				'capability_type' => 'archive',
				'has_archive'     => true,
				'rewrite'         => array( 'permastruct' => '/archive/%archive_type%/%archive%' ),
				'admin_cols'      => array_merge(
					array( 'archive_type' => array( 'taxonomy' => 'archive_type' ) ),
					$default_config['admin_cols']
				),
				'admin_filters'   => array(
					'archive_type' => array( 'taxonomy' => 'archive_type' ),
				),
			)
		)
	);

	/**
	 * Taxonomy: Type for Archivo
	 */
	register_extended_taxonomy(
		'archive_type',
		'archive',
		array(
			'meta_box'     => 'radio',
			'hierarchical' => false,
			'exclusive'    => true,
			'required'     => true,
		),
		array(
			'singular' => __( 'Archive Type', 'elcano' ),
			'plural'   => __( 'Archive Types', 'elcano' ),
			'slug'     => 'archive-type',
		)
	);

	/**
	 * Taxonomy: Specials categories for all post types
	 */
	register_extended_taxonomy(
		'specials_category',
		array( 'analysis','work_document','report','poll','special','monograph','policy_paper','commentary','podcast','video','archive','newsletter','news_on_net','ciber','magazine','press_release','activity','brussels_activity','project','job','biography','inside_spain','scientific_council', 'post' ),
		array(
			'meta_box'           => 'dropdown',
			'hierarchical'       => false,
			'exclusive'          => true,
			'required'           => false,
			'public'             => false,
			'show_in_quick_edit' => false,
		),
		array(
			'singular' => __( 'Specials category', 'elcano' ),
			'plural'   => __( 'Specials categories', 'elcano' ),
			'slug'     => 'specials-category',
		)
	);

	/**
	 * CPT: Newsletter / Boletín
	 */
	register_extended_post_type(
		'newsletter',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Newsletter', 'elcano' ),
				'plural'          => __( 'Newsletters', 'elcano' ),
				'gender'          => 'female',
				'menu_icon'       => 'dashicons-email-alt',
				'capability_type' => 'newsletter',
				'has_archive'     => true,
			)
		),
		array( 'slug' => 'newsletters' )
	);

	/**
	 * CPT: Novedades en la RED
	 */
	register_extended_post_type(
		'news_on_net',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Novedades en la RED', 'elcano' ),
				'plural'          => __( 'Novedades en la RED', 'elcano' ),
				'menu_icon'       => 'dashicons-megaphone',
				'capability_type' => 'news_on_net',
			)
		),
		array( 'slug' => 'news-on-net' )
	);

	/**
	 * CPT: CIBER Elcano
	 */
	register_extended_post_type(
		'ciber',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'CIBER elcano', 'elcano' ),
				'plural'          => __( 'CIBER elcano', 'elcano' ),
				'menu_icon'       => 'dashicons-admin-site-alt3',
				'capability_type' => 'ciber',
			)
		)
	);

	/**
	 * CPT: Revista Elcano
	 */
	register_extended_post_type(
		'magazine',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Revista Elcano', 'elcano' ),
				'plural'          => __( 'Revista Elcano', 'elcano' ),
				'gender'          => 'female',
				'menu_icon'       => 'dashicons-book',
				'capability_type' => 'magazine',
			)
		)
	);

	/**
	 * CPT: Nota de prensa
	 */
	register_extended_post_type(
		'press_release',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Press Release', 'elcano' ),
				'plural'          => __( 'Press Releases', 'elcano' ),
				'gender'          => 'female',
				'menu_icon'       => 'dashicons-welcome-widgets-menus',
				'supports'        => array_merge( $default_config['supports'], array( 'excerpt' ) ),
				'capability_type' => 'press_release',
				'has_archive'     => true,
				'admin_cols'      => array_merge(
					array(
						'number' => array(
							'title'    => __( 'Number', 'elcano' ),
							'meta_key' => 'number',
						),
					),
					$default_config['admin_cols']
				),
			)
		),
		array( 'slug' => 'press-release' )
	);

	/**
	 * CPT: Actividades
	 */
	register_extended_post_type(
		'activity',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Event', 'elcano' ),
				'plural'          => __( 'Events', 'elcano' ),
				'gender'          => 'female',
				'has_archive'     => true,
				'menu_icon'       => 'dashicons-calendar-alt',
				'capability_type' => array( 'activity', 'activities' ),
				'admin_cols'      => array(
					'date_start' => array(
						'title'       => __( 'Start', 'elcano' ),
						'meta_key'    => 'date_start',
						'date_format' => 'd/m/Y H:i',
					),
					'date_end'   => array(
						'title'       => __( 'End', 'elcano' ),
						'meta_key'    => 'date_end',
						'date_format' => 'd/m/Y H:i',
					),
					'published'  => array(
						'title'       => __( 'Published', 'elcano' ),
						'meta_key'    => 'published_date',
						'date_format' => 'd/m/Y',
					),
					'category'   => array( 'taxonomy' => 'category' ),
					'tags'       => array( 'taxonomy' => 'post_tag' ),
				),
			),
		),
		array( 'slug' => 'activities' )
	);

	/**
	 * CPT: Actividades en Bruselas
	 */
	register_extended_post_type(
		'brussels_activity',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Event in Brussels', 'elcano' ),
				'plural'          => __( 'Events in Brussels', 'elcano' ),
				'gender'          => 'female',
				'has_archive'     => true,
				'menu_icon'       => 'dashicons-admin-site-alt',
				'capability_type' => array( 'brussels_activity', 'brussels_activities' ),
				'admin_cols'      => array(
					'date_start' => array(
						'title'       => __( 'Start', 'elcano' ),
						'meta_key'    => 'date_start',
						'date_format' => 'd/m/Y H:i',
					),
					'date_end'   => array(
						'title'       => __( 'End', 'elcano' ),
						'meta_key'    => 'date_end',
						'date_format' => 'd/m/Y H:i',
					),
					'published'  => array(
						'title'       => __( 'Published', 'elcano' ),
						'meta_key'    => 'published_date',
						'date_format' => 'd/m/Y',
					),
					'category'   => array( 'taxonomy' => 'category' ),
					'tags'       => array( 'taxonomy' => 'post_tag' ),
				),
			),
		),
		array( 'slug' => 'brussels-office/events-in-brussels' )
	);

	/**
	 * CPT: Proyecto
	 */
	register_extended_post_type(
		'project',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Project', 'elcano' ),
				'plural'          => __( 'Projects', 'elcano' ),
				'gender'          => 'male',
				'has_archive'     => true,
				'menu_icon'       => 'dashicons-schedule',
				'capability_type' => array( 'project', 'projects' ),
				'admin_cols'      => array(
					'date_start' => array(
						'title'       => __( 'Start', 'elcano' ),
						'meta_key'    => 'date_start',
						'date_format' => 'd/m/Y H:i',
					),
					'date_end'   => array(
						'title'       => __( 'End', 'elcano' ),
						'meta_key'    => 'date_end',
						'date_format' => 'd/m/Y H:i',
					),
					'published'  => array(
						'title'       => __( 'Published', 'elcano' ),
						'meta_key'    => 'published_date',
						'date_format' => 'd/m/Y',
					),
					'category'   => array( 'taxonomy' => 'category' ),
					'tags'       => array( 'taxonomy' => 'post_tag' ),
				),
			),
		),
		array( 'slug' => 'projects' )
	);


	/**
	 * CPT: Empleo
	 */
	register_extended_post_type(
		'job',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Job Offer', 'elcano' ),
				'plural'          => __( 'Job Offers', 'elcano' ),
				'gender'          => 'female',
				'menu_icon'       => 'dashicons-businessman',
				'supports'        => array(
					'title',
					'editor',
					'author',
					'excerpt',
				),
				'has_archive'     => true,
				'capability_type' => 'job',
				'taxonomies'      => array(),
				'admin_cols'      => array(
					'reference' => array(
						'title'    => __( 'Reference', 'elcano' ),
						'meta_key' => 'job_reference',
					),
					'published' => array(
						'title'       => __( 'Published', 'elcano' ),
						'post_field'  => 'post_date',
						'date_format' => 'd/m/Y',
					),
				),
			)
		),
		array( 'slug' => 'job-offers' )
	);

	/**
	 * CPT: Biografía (Investigadores & Equipo Elcano)
	 */
	register_extended_post_type(
		'biography',
		array_merge(
			$default_config,
			array(
				'singular'         => __( 'Biography', 'elcano' ),
				'plural'           => __( 'Biographies', 'elcano' ),
				'gender'           => 'female',
				'menu_icon'        => 'dashicons-businessperson',
				'supports'         => array_merge( $default_config['supports'], array( 'excerpt' ) ),
				'enter_title_here' => __( 'Name', 'elcano' ),
				'featured_image'   => __( 'Photo', 'elcano' ),
				'capability_type'  => array( 'biography', 'biographies' ),
				'capabilities'     => array(
					'create_posts' => 'create_biographies',
				),
				'taxonomies'       => array(),
				'admin_cols'       => array(
					'featured_image' => array(
						'title'          => __( 'Photo', 'elcano' ),
						'featured_image' => 'thumbnail',
						'width'          => 50,
						'height'         => 50,
					),
					'bio_group'      => array( 'taxonomy' => 'bio_group' ),
					'published'      => array(
						'title'       => __( 'Published', 'elcano' ),
						'post_field'  => 'post_date',
						'date_format' => 'd/m/Y',
					),
				),
				'admin_filters'    => array(
					'bio_group' => array( 'taxonomy' => 'bio_group' ),
				),
			)
		),
		array( 'slug' => 'biographies' )
	);

	/**
	 * Taxonomy: Group for Briografía
	 */
	register_extended_taxonomy(
		'bio_group',
		'biography',
		array(
			'meta_box'           => 'dropdown',
			'hierarchical'       => false,
			'exclusive'          => true,
			'required'           => true,
			'public'             => false,
			'show_in_quick_edit' => false,
			'admin_cols'         => array(
				'is_researcher' => array(
					'title'    => __( 'Researcher', 'elcano' ),
					'function' => function( $term_id ) {
						echo get_term_meta( $term_id, 'is_researcher', true ) ? '<i class="dashicons-before dashicons-yes"></i>' : '—';
					},
				),
			),
		),
		array(
			'singular' => __( 'Group', 'elcano' ),
			'plural'   => __( 'Groups', 'elcano' ),
			'slug'     => 'bio-group',
		)
	);

	/**
	 * CPT: Inside Spain
	 */
	register_extended_post_type(
		'inside_spain',
		array_merge(
			$default_config,
			array(
				'singular'        => __( 'Inside Spain', 'elcano' ),
				'plural'          => __( 'Inside Spain', 'elcano' ),
				'menu_icon'       => 'dashicons-image-filter',
				'capability_type' => 'inside_spain',
			)
		),
		array( 'slug' => 'inside-spain' )
	);

	/**
	 * CPT: Scientific Council
	 */
	register_extended_post_type(
		'scientific_council',
		array_merge(
			$default_config,
			array(
				'singular'         => __( 'Scientific Council', 'elcano' ),
				'plural'           => __( 'Scientific Councils', 'elcano' ),
				'gender'           => 'male',
				'menu_icon'        => 'dashicons-businessperson',
				'supports'         => array_merge( $default_config['supports'], array( 'excerpt' ) ),
				'enter_title_here' => __( 'Name', 'elcano' ),
				'featured_image'   => __( 'Photo', 'elcano' ),
				'capability_type'  => 'post',
				'taxonomies'       => array(),
				'admin_cols'       => array(
					'featured_image' => array(
						'title'          => __( 'Photo', 'elcano' ),
						'featured_image' => 'thumbnail',
						'width'          => 50,
						'height'         => 50,
					),
					'sci_group'      => array( 'taxonomy' => 'sci_group' ),
					'published'      => array(
						'title'       => __( 'Published', 'elcano' ),
						'post_field'  => 'post_date',
						'date_format' => 'd/m/Y',
					),
				),
				'admin_filters'    => array(
					'sci_group' => array( 'taxonomy' => 'sci_group' ),
				),
			)
		),
		array( 'slug' => 'scientific-council' )
	);

	/**
	 * Taxonomy: Group for Scientific Council
	 */
	register_extended_taxonomy(
		'sci_group',
		'scientific_council',
		array(
			'meta_box'           => 'dropdown',
			'hierarchical'       => false,
			'exclusive'          => true,
			'required'           => true,
			'public'             => false,
			'show_in_quick_edit' => false,
			'admin_cols'         => array(
				'is_researcher' => array(
					'title'    => __( 'Researcher', 'elcano' ),
					'function' => function( $term_id ) {
						echo get_term_meta( $term_id, 'is_researcher', true ) ? '<i class="dashicons-before dashicons-yes"></i>' : '—';
					},
				),
			),
		),
		array(
			'singular' => __( 'Group', 'elcano' ),
			'plural'   => __( 'Groups', 'elcano' ),
			'slug'     => 'sci-group',
		)
	);

}
add_action( 'init', 'elcano_register_custom_post_types' );


/**
 * Rename Posts to Blog
 *
 * @param  array  $args post args.
 * @param  string $post_type post type name.
 * @return array
 */
function elcano_rename_posts_to_blog( $args, $post_type ) {

	if ( 'post' === $post_type ) {
		$args['labels'] = array_merge(
			$args['labels'],
			array(
				'name'          => _x('Posts', 'Post post type', 'elcano'),
				'singular_name' => _x('Post', 'Post post type', 'elcano'),
				'menu_name'     => __( 'Blog', 'elcano' ),
				'all_items'     => __( 'All Posts' ),
			)
		);
	}

	return $args;

}
add_filter( 'register_post_type_args', 'elcano_rename_posts_to_blog', 10, 2 );


/**
 * Create post Biography on add new user
 *
 * @param  int $user_id
 * @return void
 */
function elcano_create_biography( $user_id ) {

	$user = get_userdata( $user_id );

	$postdata = array(
		'post_title'  => sprintf( '%s %s', $user->first_name, $user->last_name ),
		'post_author' => $user_id,
		'post_type'   => 'biography',
		'post_status' => 'draft',
	);

	wp_insert_post( $postdata );

}
// add_action( 'user_register', 'elcano_create_biography' );


/**
 * Redirect CPT option page to CPT archive
 *
 * @return void
 */
function elcano_page_for_cpt_to_archive() {

	if ( ! is_page() ) {
		return;
	}

	$page_id = get_the_ID();
	$cpts    = array( 'archive', 'job', 'special', 'press_release', 'activity', 'project', 'brussels_activity', 'podcast' );

	foreach ( $cpts as $cpt ) {
		if ( $page_id === get_field( "page_for_{$cpt}", 'option' ) ) {
			wp_redirect( get_post_type_archive_link( $cpt ) );
			exit;
		}
	}

}
add_action( 'template_redirect', 'elcano_page_for_cpt_to_archive' );

/**
 * Override posts per page for podcast archive for correct pagination
 */
function elcano_override_podcasts_ppp( $query ) {
	if( $query->get('post_type') === 'podcast' && $query->is_post_type_archive && ! is_page() && ( $query->get('post_type') === 'podcast' && ! is_single() ) ) {
		$query->set('posts_per_page', 8);
		return $query;
	}
}
add_action('pre_get_posts', 'elcano_override_podcasts_ppp');
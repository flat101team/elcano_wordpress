<?php

defined( 'WP_CLI' ) or die();

global $wpdb;

// Unserialized meta "authors"
$metas = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE meta_key = 'authors' AND meta_value NOT LIKE 'a:%'" );

foreach ( $metas as $meta ) {
    if ( trim( $meta->meta_value ) ) {
        $out = update_post_meta( $meta->post_id, 'authors', array( strval( $meta->meta_value ) ), $meta->meta_value );
        printf( "UPDATE %s %s\n", $meta->meta_id, $out ? 'OK' : 'KO' );
    } else {
        $out = delete_post_meta( $meta->post_id, 'authors' );
        printf( "DELETE %s %s\n", $meta->meta_id, $out ? 'OK' : 'KO' );
    }
}

// Serialized meta "authors" with int
$metas = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE meta_key = 'authors' AND meta_value LIKE '%{i:0;i:%'" );

foreach ( $metas as $meta ) {
    $value = unserialize( $meta->meta_value );
    $value = array_map( 'strval', $value );
    $out   = update_post_meta( $meta->post_id, 'authors', $value );
    printf( "UPDATE %s %s\n", $meta->meta_id, $out ? 'OK' : 'KO' );
}
